// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc_mkpwm.h"
#include "VmkSoc__Syms.h"

//==========

VL_CTOR_IMP(VmkSoc_mkpwm) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VmkSoc_mkpwm::__Vconfigure(VmkSoc__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

VmkSoc_mkpwm::~VmkSoc_mkpwm() {
}

void VmkSoc_mkpwm::_initial__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__1(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_initial__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__1\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold = 3U;
    this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg = 0U;
    this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk = 0U;
    this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate = 1U;
    this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate = 1U;
    this->__PVT__pwm_control_reset__DOT__rst = 1U;
    this->__PVT__pwm_sync_pwm_output__DOT__sDataSyncIn = 0U;
    this->__PVT__pwm_sync_pwm_output_dD_OUT = 0U;
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg = 0x2aaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg = 0x2aaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg = 2U;
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg = 2U;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    this->__PVT__pwm_clock_divisor_sync__DOT__sDataSyncIn = 0xaaaaU;
    this->__PVT__pwm_clock_divisor_sync_dD_OUT = 0xaaaaU;
    this->__PVT__pwm_sync_period__DOT__sDataSyncIn = 0xaaaaU;
    this->__PVT__pwm_sync_period_dD_OUT = 0xaaaaU;
    this->__PVT__pwm_sync_duty_cycle__DOT__sDataSyncIn = 0xaaaaU;
    this->__PVT__pwm_sync_duty_cycle_dD_OUT = 0xaaaaU;
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg1 = 0U;
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg2 = 0U;
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dLastState = 0U;
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg = 0U;
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg1 = 1U;
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2 = 1U;
    this->__PVT__pwm_sync_pwm_start__DOT__sDataSyncIn = 0U;
    this->__PVT__pwm_sync_pwm_start_dD_OUT = 0U;
    this->__PVT__pwm_sync_continous_once__DOT__sDataSyncIn = 0U;
    this->__PVT__pwm_sync_continous_once_dD_OUT = 0U;
    this->__PVT__pwm_sync_pwm_enable__DOT__sDataSyncIn = 0U;
    this->__PVT__pwm_sync_pwm_enable_dD_OUT = 0U;
    this->__PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg1 = 0U;
    this->__PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg2 = 0U;
    this->__PVT__pwm_sync_period__DOT__sync__DOT__dLastState = 0U;
    this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg = 0U;
    this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg1 = 1U;
    this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2 = 1U;
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg1 = 0U;
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg2 = 0U;
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dLastState = 0U;
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg = 0U;
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg1 = 1U;
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2 = 1U;
    this->__PVT__pwm_clock_divider_clk = 0U;
    this->__PVT__pwm_clock_divider_rg_counter = 0xaaaaU;
    this->__PVT__pwm_clock_divider_rg_divisor = 0xaaaaU;
    this->__PVT__pwm_clock_divisor = 0xaaaaU;
    this->__PVT__pwm_clock_selector = 0U;
    this->__PVT__pwm_continous_once = 0U;
    this->__PVT__pwm_duty_cycle = 0xaaaaU;
    this->__PVT__pwm_interrupt = 0U;
    this->__PVT__pwm_period = 0xaaaaU;
    this->__PVT__pwm_pwm_enable = 0U;
    this->__PVT__pwm_pwm_output = 0U;
    this->__PVT__pwm_pwm_output_enable = 0U;
    this->__PVT__pwm_pwm_start = 0U;
    this->__PVT__pwm_reset_counter = 0U;
    this->__PVT__pwm_rg_counter = 0xaaaaU;
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg1 = 0U;
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg2 = 0U;
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dLastState = 0U;
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg = 0U;
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg1 = 1U;
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2 = 1U;
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg1 = 0U;
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg2 = 0U;
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dLastState = 0U;
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg = 0U;
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg1 = 1U;
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2 = 1U;
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg1 = 0U;
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg2 = 0U;
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dLastState = 0U;
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg = 0U;
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg1 = 1U;
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2 = 1U;
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg1 = 0U;
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg2 = 0U;
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dLastState = 0U;
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg = 0U;
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg1 = 1U;
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2 = 1U;
}

void VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__31(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__31\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset 
        = (1U | ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                 << 1U));
    this->__PVT__pwm_overall_reset_RST_OUT = (1U & 
                                              (~ ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (~ 
                                                     ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                                                      >> 1U)))));
    if ((1U & (~ (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate 
            = this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate;
    }
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__pwm_clock_divisor_sync_sRDY = ((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2) 
                                                == (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_output_sRDY = ((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2) 
                                             == (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_clock_divider_clk_D_IN = (1U & 
                                               (~ (IData)(this->__PVT__pwm_clock_divider_clk)));
    this->__PVT__pwm_clock_divider_rg_counter_D_IN 
        = (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
            & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
               >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))
            ? 0U : (0xffffU & ((IData)(1U) + (IData)(this->__PVT__pwm_clock_divider_rg_counter))));
    this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 
        = ((IData)(this->__PVT__pwm_rg_counter) < (0xffffU 
                                                   & ((0U 
                                                       == (IData)(this->__PVT__pwm_sync_period_dD_OUT))
                                                       ? (IData)(this->__PVT__pwm_sync_period_dD_OUT)
                                                       : 
                                                      ((IData)(this->__PVT__pwm_sync_period_dD_OUT) 
                                                       - (IData)(1U)))));
    this->__PVT__r1___05Fread___05Fh3256 = (((((IData)(this->__PVT__pwm_reset_counter) 
                                               << 6U) 
                                              | ((IData)(this->__PVT__pwm_interrupt) 
                                                 << 4U)) 
                                             | (((IData)(this->__PVT__pwm_pwm_output_enable) 
                                                 << 3U) 
                                                | ((IData)(this->__PVT__pwm_continous_once) 
                                                   << 2U))) 
                                            | (((IData)(this->__PVT__pwm_pwm_start) 
                                                << 1U) 
                                               | (IData)(this->__PVT__pwm_pwm_enable)));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((8U == 
                                               (0xfU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                             | ((((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  | (4U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (0xcU 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (1U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock 
        = ((((((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)) 
              & ((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2) 
                 == (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg))) 
             & ((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2) 
                == (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg))) 
            & ((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2) 
              == (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
    this->__PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = ((IData)(this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg)
            ? ((IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk) 
               & (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate))
            : (IData)(vlTOPp->CLK));
    this->__PVT__pwm_rg_counter_D_IN = (0xffffU & (
                                                   ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                                                    | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT))
                                                    ? 
                                                   ((IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__pwm_rg_counter))
                                                     : 0U)
                                                    : 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__pwm_rg_counter))));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                               | ((((0U 
                                                                     == 
                                                                     (0xfU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    | (4U 
                                                                       == 
                                                                       (0xfU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   | (0xcU 
                                                                      == 
                                                                      (0xfU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                 << 0x19U) 
                                                                | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                    << 0x18U) 
                                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                       << 0x11U) 
                                                                      | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                          << 0x10U) 
                                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                             << 9U) 
                                                                            | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                                << 8U) 
                                                                               | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__pwm_clock_selector))))))))
                                                                : 
                                                               (((0U 
                                                                  == 
                                                                  (0xfU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))) 
                                                                 & (1U 
                                                                    == 
                                                                    (3U 
                                                                     & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                 ? 
                                                                (((IData)(this->__PVT__pwm_period) 
                                                                  << 0x10U) 
                                                                 | (IData)(this->__PVT__pwm_period))
                                                                 : 
                                                                (((4U 
                                                                   == 
                                                                   (0xfU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__pwm_duty_cycle) 
                                                                   << 0x10U) 
                                                                  | (IData)(this->__PVT__pwm_duty_cycle))
                                                                  : 
                                                                 (((0xcU 
                                                                    == 
                                                                    (0xfU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__pwm_clock_divisor) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__pwm_clock_divisor))
                                                                   : 0U)))))));
}

void VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__32(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__32\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset 
        = (1U | ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                 << 1U));
    this->__PVT__pwm_overall_reset_RST_OUT = (1U & 
                                              (~ ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (~ 
                                                     ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                                                      >> 1U)))));
    if ((1U & (~ (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate 
            = this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate;
    }
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__pwm_clock_divisor_sync_sRDY = ((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2) 
                                                == (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_output_sRDY = ((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2) 
                                             == (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_clock_divider_clk_D_IN = (1U & 
                                               (~ (IData)(this->__PVT__pwm_clock_divider_clk)));
    this->__PVT__pwm_clock_divider_rg_counter_D_IN 
        = (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
            & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
               >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))
            ? 0U : (0xffffU & ((IData)(1U) + (IData)(this->__PVT__pwm_clock_divider_rg_counter))));
    this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 
        = ((IData)(this->__PVT__pwm_rg_counter) < (0xffffU 
                                                   & ((0U 
                                                       == (IData)(this->__PVT__pwm_sync_period_dD_OUT))
                                                       ? (IData)(this->__PVT__pwm_sync_period_dD_OUT)
                                                       : 
                                                      ((IData)(this->__PVT__pwm_sync_period_dD_OUT) 
                                                       - (IData)(1U)))));
    this->__PVT__r1___05Fread___05Fh3256 = (((((IData)(this->__PVT__pwm_reset_counter) 
                                               << 6U) 
                                              | ((IData)(this->__PVT__pwm_interrupt) 
                                                 << 4U)) 
                                             | (((IData)(this->__PVT__pwm_pwm_output_enable) 
                                                 << 3U) 
                                                | ((IData)(this->__PVT__pwm_continous_once) 
                                                   << 2U))) 
                                            | (((IData)(this->__PVT__pwm_pwm_start) 
                                                << 1U) 
                                               | (IData)(this->__PVT__pwm_pwm_enable)));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((8U == 
                                               (0xfU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                             | ((((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  | (4U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (0xcU 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (1U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock 
        = ((((((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)) 
              & ((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2) 
                 == (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg))) 
             & ((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2) 
                == (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg))) 
            & ((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2) 
              == (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
    this->__PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = ((IData)(this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg)
            ? ((IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk) 
               & (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate))
            : (IData)(vlTOPp->CLK));
    this->__PVT__pwm_rg_counter_D_IN = (0xffffU & (
                                                   ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                                                    | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT))
                                                    ? 
                                                   ((IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__pwm_rg_counter))
                                                     : 0U)
                                                    : 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__pwm_rg_counter))));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                               | ((((0U 
                                                                     == 
                                                                     (0xfU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    | (4U 
                                                                       == 
                                                                       (0xfU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   | (0xcU 
                                                                      == 
                                                                      (0xfU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                 << 0x19U) 
                                                                | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                    << 0x18U) 
                                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                       << 0x11U) 
                                                                      | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                          << 0x10U) 
                                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                             << 9U) 
                                                                            | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                                << 8U) 
                                                                               | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__pwm_clock_selector))))))))
                                                                : 
                                                               (((0U 
                                                                  == 
                                                                  (0xfU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))) 
                                                                 & (1U 
                                                                    == 
                                                                    (3U 
                                                                     & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                 ? 
                                                                (((IData)(this->__PVT__pwm_period) 
                                                                  << 0x10U) 
                                                                 | (IData)(this->__PVT__pwm_period))
                                                                 : 
                                                                (((4U 
                                                                   == 
                                                                   (0xfU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__pwm_duty_cycle) 
                                                                   << 0x10U) 
                                                                  | (IData)(this->__PVT__pwm_duty_cycle))
                                                                  : 
                                                                 (((0xcU 
                                                                    == 
                                                                    (0xfU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__pwm_clock_divisor) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__pwm_clock_divisor))
                                                                   : 0U)))))));
}

void VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__33(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__33\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset 
        = (1U | ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                 << 1U));
    this->__PVT__pwm_overall_reset_RST_OUT = (1U & 
                                              (~ ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (~ 
                                                     ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                                                      >> 1U)))));
    if ((1U & (~ (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate 
            = this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate;
    }
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__pwm_clock_divisor_sync_sRDY = ((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2) 
                                                == (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_output_sRDY = ((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2) 
                                             == (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_clock_divider_clk_D_IN = (1U & 
                                               (~ (IData)(this->__PVT__pwm_clock_divider_clk)));
    this->__PVT__pwm_clock_divider_rg_counter_D_IN 
        = (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
            & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
               >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))
            ? 0U : (0xffffU & ((IData)(1U) + (IData)(this->__PVT__pwm_clock_divider_rg_counter))));
    this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 
        = ((IData)(this->__PVT__pwm_rg_counter) < (0xffffU 
                                                   & ((0U 
                                                       == (IData)(this->__PVT__pwm_sync_period_dD_OUT))
                                                       ? (IData)(this->__PVT__pwm_sync_period_dD_OUT)
                                                       : 
                                                      ((IData)(this->__PVT__pwm_sync_period_dD_OUT) 
                                                       - (IData)(1U)))));
    this->__PVT__r1___05Fread___05Fh3256 = (((((IData)(this->__PVT__pwm_reset_counter) 
                                               << 6U) 
                                              | ((IData)(this->__PVT__pwm_interrupt) 
                                                 << 4U)) 
                                             | (((IData)(this->__PVT__pwm_pwm_output_enable) 
                                                 << 3U) 
                                                | ((IData)(this->__PVT__pwm_continous_once) 
                                                   << 2U))) 
                                            | (((IData)(this->__PVT__pwm_pwm_start) 
                                                << 1U) 
                                               | (IData)(this->__PVT__pwm_pwm_enable)));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((8U == 
                                               (0xfU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                             | ((((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  | (4U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (0xcU 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (1U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock 
        = ((((((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)) 
              & ((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2) 
                 == (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg))) 
             & ((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2) 
                == (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg))) 
            & ((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2) 
              == (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
    this->__PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = ((IData)(this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg)
            ? ((IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk) 
               & (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate))
            : (IData)(vlTOPp->CLK));
    this->__PVT__pwm_rg_counter_D_IN = (0xffffU & (
                                                   ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                                                    | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT))
                                                    ? 
                                                   ((IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__pwm_rg_counter))
                                                     : 0U)
                                                    : 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__pwm_rg_counter))));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                               | ((((0U 
                                                                     == 
                                                                     (0xfU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    | (4U 
                                                                       == 
                                                                       (0xfU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   | (0xcU 
                                                                      == 
                                                                      (0xfU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                 << 0x19U) 
                                                                | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                    << 0x18U) 
                                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                       << 0x11U) 
                                                                      | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                          << 0x10U) 
                                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                             << 9U) 
                                                                            | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                                << 8U) 
                                                                               | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__pwm_clock_selector))))))))
                                                                : 
                                                               (((0U 
                                                                  == 
                                                                  (0xfU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))) 
                                                                 & (1U 
                                                                    == 
                                                                    (3U 
                                                                     & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                 ? 
                                                                (((IData)(this->__PVT__pwm_period) 
                                                                  << 0x10U) 
                                                                 | (IData)(this->__PVT__pwm_period))
                                                                 : 
                                                                (((4U 
                                                                   == 
                                                                   (0xfU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__pwm_duty_cycle) 
                                                                   << 0x10U) 
                                                                  | (IData)(this->__PVT__pwm_duty_cycle))
                                                                  : 
                                                                 (((0xcU 
                                                                    == 
                                                                    (0xfU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__pwm_clock_divisor) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__pwm_clock_divisor))
                                                                   : 0U)))))));
}

void VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__34(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__34\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset 
        = (1U | ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                 << 1U));
    this->__PVT__pwm_overall_reset_RST_OUT = (1U & 
                                              (~ ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (~ 
                                                     ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                                                      >> 1U)))));
    if ((1U & (~ (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate 
            = this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate;
    }
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__pwm_clock_divisor_sync_sRDY = ((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2) 
                                                == (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_output_sRDY = ((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2) 
                                             == (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_clock_divider_clk_D_IN = (1U & 
                                               (~ (IData)(this->__PVT__pwm_clock_divider_clk)));
    this->__PVT__pwm_clock_divider_rg_counter_D_IN 
        = (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
            & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
               >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))
            ? 0U : (0xffffU & ((IData)(1U) + (IData)(this->__PVT__pwm_clock_divider_rg_counter))));
    this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 
        = ((IData)(this->__PVT__pwm_rg_counter) < (0xffffU 
                                                   & ((0U 
                                                       == (IData)(this->__PVT__pwm_sync_period_dD_OUT))
                                                       ? (IData)(this->__PVT__pwm_sync_period_dD_OUT)
                                                       : 
                                                      ((IData)(this->__PVT__pwm_sync_period_dD_OUT) 
                                                       - (IData)(1U)))));
    this->__PVT__r1___05Fread___05Fh3256 = (((((IData)(this->__PVT__pwm_reset_counter) 
                                               << 6U) 
                                              | ((IData)(this->__PVT__pwm_interrupt) 
                                                 << 4U)) 
                                             | (((IData)(this->__PVT__pwm_pwm_output_enable) 
                                                 << 3U) 
                                                | ((IData)(this->__PVT__pwm_continous_once) 
                                                   << 2U))) 
                                            | (((IData)(this->__PVT__pwm_pwm_start) 
                                                << 1U) 
                                               | (IData)(this->__PVT__pwm_pwm_enable)));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((8U == 
                                               (0xfU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                             | ((((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  | (4U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (0xcU 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (1U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock 
        = ((((((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)) 
              & ((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2) 
                 == (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg))) 
             & ((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2) 
                == (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg))) 
            & ((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2) 
              == (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
    this->__PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = ((IData)(this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg)
            ? ((IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk) 
               & (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate))
            : (IData)(vlTOPp->CLK));
    this->__PVT__pwm_rg_counter_D_IN = (0xffffU & (
                                                   ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                                                    | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT))
                                                    ? 
                                                   ((IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__pwm_rg_counter))
                                                     : 0U)
                                                    : 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__pwm_rg_counter))));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                               | ((((0U 
                                                                     == 
                                                                     (0xfU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    | (4U 
                                                                       == 
                                                                       (0xfU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   | (0xcU 
                                                                      == 
                                                                      (0xfU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                 << 0x19U) 
                                                                | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                    << 0x18U) 
                                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                       << 0x11U) 
                                                                      | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                          << 0x10U) 
                                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                             << 9U) 
                                                                            | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                                << 8U) 
                                                                               | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__pwm_clock_selector))))))))
                                                                : 
                                                               (((0U 
                                                                  == 
                                                                  (0xfU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))) 
                                                                 & (1U 
                                                                    == 
                                                                    (3U 
                                                                     & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                 ? 
                                                                (((IData)(this->__PVT__pwm_period) 
                                                                  << 0x10U) 
                                                                 | (IData)(this->__PVT__pwm_period))
                                                                 : 
                                                                (((4U 
                                                                   == 
                                                                   (0xfU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__pwm_duty_cycle) 
                                                                   << 0x10U) 
                                                                  | (IData)(this->__PVT__pwm_duty_cycle))
                                                                  : 
                                                                 (((0xcU 
                                                                    == 
                                                                    (0xfU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__pwm_clock_divisor) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__pwm_clock_divisor))
                                                                   : 0U)))))));
}

void VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__35(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__35\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset 
        = (1U | ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                 << 1U));
    this->__PVT__pwm_overall_reset_RST_OUT = (1U & 
                                              (~ ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (~ 
                                                     ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                                                      >> 1U)))));
    if ((1U & (~ (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate 
            = this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate;
    }
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__pwm_clock_divisor_sync_sRDY = ((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2) 
                                                == (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_output_sRDY = ((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2) 
                                             == (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_clock_divider_clk_D_IN = (1U & 
                                               (~ (IData)(this->__PVT__pwm_clock_divider_clk)));
    this->__PVT__pwm_clock_divider_rg_counter_D_IN 
        = (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
            & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
               >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))
            ? 0U : (0xffffU & ((IData)(1U) + (IData)(this->__PVT__pwm_clock_divider_rg_counter))));
    this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 
        = ((IData)(this->__PVT__pwm_rg_counter) < (0xffffU 
                                                   & ((0U 
                                                       == (IData)(this->__PVT__pwm_sync_period_dD_OUT))
                                                       ? (IData)(this->__PVT__pwm_sync_period_dD_OUT)
                                                       : 
                                                      ((IData)(this->__PVT__pwm_sync_period_dD_OUT) 
                                                       - (IData)(1U)))));
    this->__PVT__r1___05Fread___05Fh3256 = (((((IData)(this->__PVT__pwm_reset_counter) 
                                               << 6U) 
                                              | ((IData)(this->__PVT__pwm_interrupt) 
                                                 << 4U)) 
                                             | (((IData)(this->__PVT__pwm_pwm_output_enable) 
                                                 << 3U) 
                                                | ((IData)(this->__PVT__pwm_continous_once) 
                                                   << 2U))) 
                                            | (((IData)(this->__PVT__pwm_pwm_start) 
                                                << 1U) 
                                               | (IData)(this->__PVT__pwm_pwm_enable)));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((8U == 
                                               (0xfU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                             | ((((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  | (4U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (0xcU 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (1U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock 
        = ((((((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)) 
              & ((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2) 
                 == (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg))) 
             & ((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2) 
                == (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg))) 
            & ((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2) 
              == (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
    this->__PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = ((IData)(this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg)
            ? ((IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk) 
               & (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate))
            : (IData)(vlTOPp->CLK));
    this->__PVT__pwm_rg_counter_D_IN = (0xffffU & (
                                                   ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                                                    | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT))
                                                    ? 
                                                   ((IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__pwm_rg_counter))
                                                     : 0U)
                                                    : 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__pwm_rg_counter))));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                               | ((((0U 
                                                                     == 
                                                                     (0xfU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    | (4U 
                                                                       == 
                                                                       (0xfU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   | (0xcU 
                                                                      == 
                                                                      (0xfU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                 << 0x19U) 
                                                                | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                    << 0x18U) 
                                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                       << 0x11U) 
                                                                      | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                          << 0x10U) 
                                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                             << 9U) 
                                                                            | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                                << 8U) 
                                                                               | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__pwm_clock_selector))))))))
                                                                : 
                                                               (((0U 
                                                                  == 
                                                                  (0xfU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))) 
                                                                 & (1U 
                                                                    == 
                                                                    (3U 
                                                                     & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                 ? 
                                                                (((IData)(this->__PVT__pwm_period) 
                                                                  << 0x10U) 
                                                                 | (IData)(this->__PVT__pwm_period))
                                                                 : 
                                                                (((4U 
                                                                   == 
                                                                   (0xfU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__pwm_duty_cycle) 
                                                                   << 0x10U) 
                                                                  | (IData)(this->__PVT__pwm_duty_cycle))
                                                                  : 
                                                                 (((0xcU 
                                                                    == 
                                                                    (0xfU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__pwm_clock_divisor) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__pwm_clock_divisor))
                                                                   : 0U)))))));
}

void VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__36(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__36\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset 
        = (1U | ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                 << 1U));
    this->__PVT__pwm_overall_reset_RST_OUT = (1U & 
                                              (~ ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (~ 
                                                     ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                                                      >> 1U)))));
    if ((1U & (~ (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate 
            = this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate;
    }
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__pwm_clock_divisor_sync_sRDY = ((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2) 
                                                == (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_output_sRDY = ((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2) 
                                             == (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_clock_divider_clk_D_IN = (1U & 
                                               (~ (IData)(this->__PVT__pwm_clock_divider_clk)));
    this->__PVT__pwm_clock_divider_rg_counter_D_IN 
        = (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
            & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
               >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))
            ? 0U : (0xffffU & ((IData)(1U) + (IData)(this->__PVT__pwm_clock_divider_rg_counter))));
    this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 
        = ((IData)(this->__PVT__pwm_rg_counter) < (0xffffU 
                                                   & ((0U 
                                                       == (IData)(this->__PVT__pwm_sync_period_dD_OUT))
                                                       ? (IData)(this->__PVT__pwm_sync_period_dD_OUT)
                                                       : 
                                                      ((IData)(this->__PVT__pwm_sync_period_dD_OUT) 
                                                       - (IData)(1U)))));
    this->__PVT__r1___05Fread___05Fh3256 = (((((IData)(this->__PVT__pwm_reset_counter) 
                                               << 6U) 
                                              | ((IData)(this->__PVT__pwm_interrupt) 
                                                 << 4U)) 
                                             | (((IData)(this->__PVT__pwm_pwm_output_enable) 
                                                 << 3U) 
                                                | ((IData)(this->__PVT__pwm_continous_once) 
                                                   << 2U))) 
                                            | (((IData)(this->__PVT__pwm_pwm_start) 
                                                << 1U) 
                                               | (IData)(this->__PVT__pwm_pwm_enable)));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((8U == 
                                               (0xfU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                             | ((((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  | (4U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (0xcU 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (1U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock 
        = ((((((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)) 
              & ((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2) 
                 == (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg))) 
             & ((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2) 
                == (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg))) 
            & ((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2) 
              == (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
    this->__PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = ((IData)(this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg)
            ? ((IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk) 
               & (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate))
            : (IData)(vlTOPp->CLK));
    this->__PVT__pwm_rg_counter_D_IN = (0xffffU & (
                                                   ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                                                    | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT))
                                                    ? 
                                                   ((IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__pwm_rg_counter))
                                                     : 0U)
                                                    : 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__pwm_rg_counter))));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                               | ((((0U 
                                                                     == 
                                                                     (0xfU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    | (4U 
                                                                       == 
                                                                       (0xfU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   | (0xcU 
                                                                      == 
                                                                      (0xfU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                 << 0x19U) 
                                                                | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                    << 0x18U) 
                                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                       << 0x11U) 
                                                                      | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                          << 0x10U) 
                                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                             << 9U) 
                                                                            | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                                << 8U) 
                                                                               | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__pwm_clock_selector))))))))
                                                                : 
                                                               (((0U 
                                                                  == 
                                                                  (0xfU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))) 
                                                                 & (1U 
                                                                    == 
                                                                    (3U 
                                                                     & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                 ? 
                                                                (((IData)(this->__PVT__pwm_period) 
                                                                  << 0x10U) 
                                                                 | (IData)(this->__PVT__pwm_period))
                                                                 : 
                                                                (((4U 
                                                                   == 
                                                                   (0xfU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__pwm_duty_cycle) 
                                                                   << 0x10U) 
                                                                  | (IData)(this->__PVT__pwm_duty_cycle))
                                                                  : 
                                                                 (((0xcU 
                                                                    == 
                                                                    (0xfU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__pwm_clock_divisor) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__pwm_clock_divisor))
                                                                   : 0U)))))));
}

void VmkSoc_mkpwm::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_ctor_var_reset\n"); );
    // Body
    CLK = VL_RAND_RESET_I(1);
    RST_N = VL_RAND_RESET_I(1);
    slave_m_awvalid_awvalid = VL_RAND_RESET_I(1);
    slave_m_awvalid_awaddr = VL_RAND_RESET_I(32);
    slave_m_awvalid_awsize = VL_RAND_RESET_I(2);
    slave_m_awvalid_awprot = VL_RAND_RESET_I(3);
    slave_awready = VL_RAND_RESET_I(1);
    slave_m_wvalid_wvalid = VL_RAND_RESET_I(1);
    slave_m_wvalid_wdata = VL_RAND_RESET_I(32);
    slave_m_wvalid_wstrb = VL_RAND_RESET_I(4);
    slave_wready = VL_RAND_RESET_I(1);
    slave_bvalid = VL_RAND_RESET_I(1);
    slave_bresp = VL_RAND_RESET_I(2);
    slave_m_bready_bready = VL_RAND_RESET_I(1);
    slave_m_arvalid_arvalid = VL_RAND_RESET_I(1);
    slave_m_arvalid_araddr = VL_RAND_RESET_I(32);
    slave_m_arvalid_arsize = VL_RAND_RESET_I(2);
    slave_m_arvalid_arprot = VL_RAND_RESET_I(3);
    slave_arready = VL_RAND_RESET_I(1);
    slave_rvalid = VL_RAND_RESET_I(1);
    slave_rresp = VL_RAND_RESET_I(2);
    slave_rdata = VL_RAND_RESET_I(32);
    slave_m_rready_rready = VL_RAND_RESET_I(1);
    io_pwm_o = VL_RAND_RESET_I(1);
    sb_interrupt = VL_RAND_RESET_I(1);
    __PVT__RDY_sb_interrupt = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divider_clk = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divider_clk_D_IN = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divider_rg_counter = VL_RAND_RESET_I(16);
    __PVT__pwm_clock_divider_rg_counter_D_IN = VL_RAND_RESET_I(16);
    __PVT__pwm_clock_divider_rg_divisor = VL_RAND_RESET_I(16);
    __PVT__pwm_clock_divisor = VL_RAND_RESET_I(16);
    __PVT__pwm_clock_selector = VL_RAND_RESET_I(1);
    __PVT__pwm_continous_once = VL_RAND_RESET_I(1);
    __PVT__pwm_duty_cycle = VL_RAND_RESET_I(16);
    __PVT__pwm_interrupt = VL_RAND_RESET_I(1);
    __PVT__pwm_period = VL_RAND_RESET_I(16);
    __PVT__pwm_pwm_enable = VL_RAND_RESET_I(1);
    __PVT__pwm_pwm_output = VL_RAND_RESET_I(1);
    __PVT__pwm_pwm_output_enable = VL_RAND_RESET_I(1);
    __PVT__pwm_pwm_start = VL_RAND_RESET_I(1);
    __PVT__pwm_reset_counter = VL_RAND_RESET_I(1);
    __PVT__pwm_rg_counter = VL_RAND_RESET_I(16);
    __PVT__pwm_rg_counter_D_IN = VL_RAND_RESET_I(16);
    __PVT__pwm_clock_divider_clock_selector_CLK_OUT = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divisor_sync_dD_OUT = VL_RAND_RESET_I(16);
    __PVT__pwm_clock_divisor_sync_sRDY = VL_RAND_RESET_I(1);
    __PVT__pwm_overall_reset_RST_OUT = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_continous_once_dD_OUT = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_duty_cycle_dD_OUT = VL_RAND_RESET_I(16);
    __PVT__pwm_sync_period_dD_OUT = VL_RAND_RESET_I(16);
    __PVT__pwm_sync_pwm_enable_dD_OUT = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output_dD_OUT = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output_sRDY = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start_dD_OUT = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_data_D_IN = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_f_rd_data_DEQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_data_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_resp_D_IN = VL_RAND_RESET_I(2);
    __PVT__s_xactor_f_wr_resp_DEQ = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_read_request = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_write_request = VL_RAND_RESET_I(1);
    __PVT__r1___05Fread___05Fh3256 = VL_RAND_RESET_I(7);
    __PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divider_clock_selector__DOT__sel_reg = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divider_new_clock__DOT__current_clk = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divider_new_clock__DOT__current_gate = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divider_new_clock__DOT__new_gate = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divisor_sync__DOT__sDataSyncIn = VL_RAND_RESET_I(16);
    __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dLastState = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_control_reset__DOT__rst = VL_RAND_RESET_I(1);
    __PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold = VL_RAND_RESET_I(2);
    __PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset = VL_RAND_RESET_I(3);
    __PVT__pwm_sync_continous_once__DOT__sDataSyncIn = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_continous_once__DOT__sync__DOT__dLastState = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_duty_cycle__DOT__sDataSyncIn = VL_RAND_RESET_I(16);
    __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dLastState = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_period__DOT__sDataSyncIn = VL_RAND_RESET_I(16);
    __PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_period__DOT__sync__DOT__dLastState = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_enable__DOT__sDataSyncIn = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dLastState = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output__DOT__sDataSyncIn = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output__DOT__sync__DOT__dLastState = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start__DOT__sDataSyncIn = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start__DOT__sync__DOT__dLastState = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2 = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_rd_addr__DOT__data1_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_rd_data__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_data__DOT__data0_reg = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_f_rd_data__DOT__data1_reg = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_f_wr_addr__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_wr_addr__DOT__data1_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_wr_data__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_data__DOT__data0_reg = VL_RAND_RESET_Q(36);
    __PVT__s_xactor_f_wr_data__DOT__data1_reg = VL_RAND_RESET_Q(36);
    __PVT__s_xactor_f_wr_resp__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_resp__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_resp__DOT__data0_reg = VL_RAND_RESET_I(2);
    __PVT__s_xactor_f_wr_resp__DOT__data1_reg = VL_RAND_RESET_I(2);
    __Vdly__s_xactor_f_rd_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_rd_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_wr_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_wr_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __Vdly__s_xactor_f_wr_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_wr_data__DOT__data0_reg = VL_RAND_RESET_Q(36);
    __Vdly__s_xactor_f_wr_resp__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __Vdly__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __Vdly__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __Vdly__pwm_sync_period__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __Vdly__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __Vdly__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
    __Vdly__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg = VL_RAND_RESET_I(1);
}
