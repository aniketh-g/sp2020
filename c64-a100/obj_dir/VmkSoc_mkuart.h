// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VmkSoc.h for the primary calling header

#ifndef _VMKSOC_MKUART_H_
#define _VMKSOC_MKUART_H_  // guard

#include "verilated_heavy.h"

//==========

class VmkSoc__Syms;

//----------

VL_MODULE(VmkSoc_mkuart) {
  public:
    
    // PORTS
    VL_IN8(CLK,0,0);
    VL_IN8(RST_N,0,0);
    VL_IN8(m_awvalid_awvalid,0,0);
    VL_IN8(m_awvalid_awsize,1,0);
    VL_IN8(m_awvalid_awprot,2,0);
    VL_OUT8(awready,0,0);
    VL_IN8(m_wvalid_wvalid,0,0);
    VL_IN8(m_wvalid_wstrb,3,0);
    VL_OUT8(wready,0,0);
    VL_OUT8(bvalid,0,0);
    VL_OUT8(bresp,1,0);
    VL_IN8(m_bready_bready,0,0);
    VL_IN8(m_arvalid_arvalid,0,0);
    VL_IN8(m_arvalid_arsize,1,0);
    VL_IN8(m_arvalid_arprot,2,0);
    VL_OUT8(arready,0,0);
    VL_OUT8(rvalid,0,0);
    VL_OUT8(rresp,1,0);
    VL_IN8(m_rready_rready,0,0);
    VL_IN8(SIN,0,0);
    VL_OUT8(SOUT,0,0);
    VL_OUT8(SOUT_EN,0,0);
    VL_OUT8(__SYM__interrupt,0,0);
    VL_IN(m_awvalid_awaddr,31,0);
    VL_IN(m_wvalid_wdata,31,0);
    VL_IN(m_arvalid_araddr,31,0);
    VL_OUT(rdata,31,0);
    
    // LOCAL SIGNALS
    // Anonymous structures to workaround compiler member-count bugs
    struct {
        CData/*0:0*/ __PVT__user_ifc_uart_fifoRecv_r_deq_whas;
        CData/*0:0*/ __PVT__user_ifc_uart_fifoXmit_r_enq_whas;
        CData/*0:0*/ __PVT__user_ifc_uart_pwRecvResetBitCount_whas;
        CData/*0:0*/ __PVT__user_ifc_uart_pwXmitLoadBuffer_whas;
        CData/*0:0*/ __PVT__user_ifc_uart_pwXmitResetBitCount_whas;
        CData/*5:0*/ __PVT__user_ifc_rg_charsize;
        CData/*7:0*/ __PVT__user_ifc_rg_interrupt_en;
        CData/*1:0*/ __PVT__user_ifc_rg_parity;
        CData/*1:0*/ __PVT__user_ifc_rg_stopbits;
        CData/*3:0*/ __PVT__user_ifc_uart_error_status_register;
        CData/*3:0*/ __PVT__user_ifc_uart_error_status_register_D_IN;
        CData/*0:0*/ __PVT__user_ifc_uart_out_enable;
        CData/*5:0*/ __PVT__user_ifc_uart_rRecvBitCount;
        CData/*5:0*/ __PVT__user_ifc_uart_rRecvBitCount_D_IN;
        CData/*3:0*/ __PVT__user_ifc_uart_rRecvCellCount;
        CData/*3:0*/ __PVT__user_ifc_uart_rRecvCellCount_D_IN;
        CData/*0:0*/ __PVT__user_ifc_uart_rRecvData;
        CData/*0:0*/ __PVT__user_ifc_uart_rRecvParity;
        CData/*2:0*/ __PVT__user_ifc_uart_rRecvState;
        CData/*5:0*/ __PVT__user_ifc_uart_rXmitBitCount;
        CData/*5:0*/ __PVT__user_ifc_uart_rXmitBitCount_D_IN;
        CData/*3:0*/ __PVT__user_ifc_uart_rXmitCellCount;
        CData/*3:0*/ __PVT__user_ifc_uart_rXmitCellCount_D_IN;
        CData/*0:0*/ __PVT__user_ifc_uart_rXmitDataOut;
        CData/*0:0*/ __PVT__user_ifc_uart_rXmitParity;
        CData/*3:0*/ __PVT__user_ifc_uart_rXmitState;
        CData/*3:0*/ __PVT__user_ifc_uart_rXmitState_D_IN;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_0;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_1;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_10;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_11;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_12;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_13;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_14;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_15;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_16;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_17;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_18;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_19;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_2;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_20;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_21;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_22;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_23;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_24;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_25;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_26;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_27;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_28;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_29;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_3;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_30;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_31;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_4;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_5;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_6;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_7;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_8;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_9;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_0;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_1;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_10;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_11;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_12;
    };
    struct {
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_13;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_14;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_15;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_16;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_17;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_18;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_19;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_2;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_20;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_21;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_22;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_23;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_24;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_25;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_26;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_27;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_28;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_29;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_3;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_30;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_31;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_4;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_5;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_6;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_7;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_8;
        CData/*0:0*/ __PVT__user_ifc_uart_vrXmitBuffer_9;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data_DEQ;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data_ENQ;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp_D_IN;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp_DEQ;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_capture_read_request;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_capture_write_request;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit;
        CData/*0:0*/ __PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1;
        CData/*7:0*/ __PVT__status___05Fh37064;
        CData/*0:0*/ __PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30;
        CData/*0:0*/ __PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317;
        CData/*0:0*/ __PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297;
        CData/*0:0*/ __PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169;
        CData/*0:0*/ __PVT__z___05Fh27968;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data__DOT__empty_reg;
    };
    struct {
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp__DOT__empty_reg;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp__DOT__data0_reg;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp__DOT__data1_reg;
        CData/*0:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full;
        CData/*0:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__ring_empty;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__head;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__next_head;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__tail;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__next_tail;
        CData/*0:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__hasodata;
        CData/*0:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full;
        CData/*0:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__ring_empty;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__head;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__next_head;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__tail;
        CData/*3:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__next_tail;
        CData/*0:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__hasodata;
        SData/*15:0*/ __PVT__user_ifc_baud_value;
        SData/*15:0*/ __PVT__user_ifc_rg_delay_control;
        SData/*15:0*/ __PVT__user_ifc_uart_rg_delay_count;
        SData/*15:0*/ __PVT__user_ifc_uart_rg_delay_count_D_IN;
        SData/*15:0*/ __PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state;
        IData/*31:0*/ __PVT__user_ifc_uart_fifoRecv_D_OUT;
        IData/*31:0*/ __PVT__user_ifc_uart_fifoXmit_D_OUT;
        IData/*31:0*/ __PVT__bitdata___05Fh17072;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data_D_IN;
        QData/*36:0*/ __PVT__s_xactor_f_rd_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_f_rd_addr__DOT__data1_reg;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data__DOT__data0_reg;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data__DOT__data1_reg;
        QData/*36:0*/ __PVT__s_xactor_f_wr_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_f_wr_addr__DOT__data1_reg;
        QData/*35:0*/ __PVT__s_xactor_f_wr_data__DOT__data0_reg;
        QData/*35:0*/ __PVT__s_xactor_f_wr_data__DOT__data1_reg;
        IData/*31:0*/ __PVT__user_ifc_uart_fifoRecv__DOT__arr[15];
        IData/*31:0*/ __PVT__user_ifc_uart_fifoXmit__DOT__arr[15];
    };
    
    // LOCAL VARIABLES
    CData/*0:0*/ __Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    CData/*3:0*/ __Vdly__user_ifc_uart_fifoRecv__DOT__head;
    CData/*3:0*/ __Vdly__user_ifc_uart_fifoRecv__DOT__tail;
    CData/*0:0*/ __Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty;
    CData/*0:0*/ __Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full;
    CData/*0:0*/ __Vdly__user_ifc_uart_fifoRecv__DOT__hasodata;
    CData/*3:0*/ __Vdlyvdim0__user_ifc_uart_fifoRecv__DOT__arr__v0;
    CData/*0:0*/ __Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0;
    CData/*3:0*/ __Vdly__user_ifc_uart_fifoXmit__DOT__head;
    CData/*3:0*/ __Vdly__user_ifc_uart_fifoXmit__DOT__tail;
    CData/*0:0*/ __Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty;
    CData/*0:0*/ __Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full;
    CData/*0:0*/ __Vdly__user_ifc_uart_fifoXmit__DOT__hasodata;
    CData/*3:0*/ __Vdlyvdim0__user_ifc_uart_fifoXmit__DOT__arr__v0;
    CData/*0:0*/ __Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0;
    IData/*31:0*/ user_ifc_uart_fifoRecv__DOT____Vlvbound2;
    IData/*31:0*/ user_ifc_uart_fifoXmit__DOT____Vlvbound2;
    IData/*31:0*/ __Vdlyvval__user_ifc_uart_fifoRecv__DOT__arr__v0;
    IData/*31:0*/ __Vdlyvval__user_ifc_uart_fifoXmit__DOT__arr__v0;
    QData/*36:0*/ __Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    QData/*35:0*/ __Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    
    // INTERNAL VARIABLES
  private:
    VmkSoc__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(VmkSoc_mkuart);  ///< Copying not allowed
  public:
    VmkSoc_mkuart(const char* name = "TOP");
    ~VmkSoc_mkuart();
    
    // INTERNAL METHODS
    void __Vconfigure(VmkSoc__Syms* symsp, bool first);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
  public:
    void _initial__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__1(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__4(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__7(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart1__5(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart2__6(VmkSoc__Syms* __restrict vlSymsp);
    void _settle__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__10(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__uart_cluster__DOT__uart1__11(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__uart_cluster__DOT__uart2__12(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
