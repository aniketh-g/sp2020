// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc_mkspi.h"
#include "VmkSoc__Syms.h"

//==========

VL_INLINE_OPT void VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__3(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__3\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdlyvset__spi_tx_fifo__DOT__arr__v0 = 0U;
    this->__Vdly__s_xactor_spi_f_rd_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_spi_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_spi_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg;
    this->__Vdly__spi_tx_fifo__DOT__not_ring_full = this->__PVT__spi_tx_fifo__DOT__not_ring_full;
    this->__Vdly__spi_tx_fifo__DOT__ring_empty = this->__PVT__spi_tx_fifo__DOT__ring_empty;
    this->__Vdly__spi_tx_fifo__DOT__tail = this->__PVT__spi_tx_fifo__DOT__tail;
    this->__Vdly__spi_tx_fifo__DOT__head = this->__PVT__spi_tx_fifo__DOT__head;
    this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg;
    this->__Vdly__spi_tx_fifo__DOT__hasodata = this->__PVT__spi_tx_fifo__DOT__hasodata;
    this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg 
        = this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg;
    this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg;
    this->__Vdlyvset__spi_rx_fifo__DOT__arr__v0 = 0U;
    this->__Vdly__spi_rx_fifo__DOT__ring_empty = this->__PVT__spi_rx_fifo__DOT__ring_empty;
    this->__Vdly__spi_rx_fifo__DOT__tail = this->__PVT__spi_rx_fifo__DOT__tail;
    this->__Vdly__spi_rx_fifo__DOT__head = this->__PVT__spi_rx_fifo__DOT__head;
    this->__Vdly__spi_rx_fifo__DOT__not_ring_full = this->__PVT__spi_rx_fifo__DOT__not_ring_full;
    this->__Vdly__spi_rx_fifo__DOT__hasodata = this->__PVT__spi_rx_fifo__DOT__hasodata;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                         & (IData)(this->__PVT__spi_tx_fifo_DEQ)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi0.spi_tx_fifo.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__not_ring_full)) 
                          & (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)) 
                         & (~ (IData)(this->__PVT__spi_tx_fifo_DEQ))))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi0.spi_tx_fifo.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi0.s_xactor_spi_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi0.spi_rx_fifo.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY((((~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                          & (IData)(this->__PVT__spi_rx_fifo_ENQ)) 
                         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi0.spi_rx_fifo.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_from_controller) 
                         & ((IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle) 
                            != (IData)(this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi0.ff_sync_rd_resp.error_checks1 -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
                         & ((IData)(this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle) 
                            == (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi0.ff_sync_rd_resp.error_checks2 -- Dequeuing from an empty full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                         & ((IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle) 
                            != (IData)(this->__PVT__ff_rd_req__DOT__sDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi0.ff_rd_req.error_checks1 -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__ff_rd_req_dEMPTY_N) 
                         & ((IData)(this->__PVT__ff_rd_req__DOT__dEnqToggle) 
                            == (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi0.ff_rd_req.error_checks2 -- Dequeuing from an empty full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                         & ((IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle) 
                            != (IData)(this->__PVT__ff_wr_req__DOT__sDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi0.ff_wr_req.error_checks1 -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__ff_wr_req_dEMPTY_N) 
                         & ((IData)(this->__PVT__ff_wr_req__DOT__dEnqToggle) 
                            == (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi0.ff_wr_req.error_checks2 -- Dequeuing from an empty full fifo\n",
                      vlSymsp->name());
        }
    }
    if ((((~ (IData)(this->__PVT__spi_tx_fifo_CLR)) 
          & (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)) 
         & (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
             & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__spi_tx_fifo_DEQ)) 
                & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
               & (IData)(this->__PVT__spi_tx_fifo__DOT__not_ring_full))))) {
        this->spi_tx_fifo__DOT____Vlvbound2 = this->__PVT__v___05Fh2759;
        if ((0x12U >= (IData)(this->__PVT__spi_tx_fifo__DOT__tail))) {
            this->__Vdlyvval__spi_tx_fifo__DOT__arr__v0 
                = this->spi_tx_fifo__DOT____Vlvbound2;
            this->__Vdlyvset__spi_tx_fifo__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__spi_tx_fifo__DOT__arr__v0 
                = this->__PVT__spi_tx_fifo__DOT__tail;
        }
    }
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__data0_reg 
        = (((- (IData)(((IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ) 
                        & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg))))) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg)) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                   & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg))
            ? 0U : (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_spi_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)) 
                                          & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_spi_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                          & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_spi_f_wr_data__DOT__data1_reg);
    this->__Vdly__s_xactor_spi_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                          & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)))) {
            this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)))) {
                this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                 << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                            << 3U) 
                                           | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
            this->__Vdly__spi_tx_fifo__DOT__head = 0U;
            this->__Vdly__spi_tx_fifo__DOT__tail = 0U;
            this->__Vdly__spi_tx_fifo__DOT__ring_empty = 1U;
            this->__Vdly__spi_tx_fifo__DOT__not_ring_full = 1U;
            this->__Vdly__spi_tx_fifo__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                    << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                               << 3U) 
                                              | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                  << 2U) 
                                                 | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                     << 1U) 
                                                    | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                this->__Vdly__spi_tx_fifo__DOT__tail 
                    = this->__PVT__spi_tx_fifo__DOT__next_tail;
                this->__Vdly__spi_tx_fifo__DOT__head 
                    = this->__PVT__spi_tx_fifo__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                      << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                 << 3U) 
                                                | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                    << 2U) 
                                                   | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                       << 1U) 
                                                      | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                    this->__Vdly__spi_tx_fifo__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                          << 4U) | 
                                         (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                           << 3U) | 
                                          (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                        this->__Vdly__spi_tx_fifo__DOT__head 
                            = this->__PVT__spi_tx_fifo__DOT__next_head;
                        this->__Vdly__spi_tx_fifo__DOT__not_ring_full = 1U;
                        this->__Vdly__spi_tx_fifo__DOT__ring_empty 
                            = ((IData)(this->__PVT__spi_tx_fifo__DOT__next_head) 
                               == (IData)(this->__PVT__spi_tx_fifo__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                              << 4U) 
                                             | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                 << 3U) 
                                                | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                    << 2U) 
                                                   | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                       << 1U) 
                                                      | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                            this->__Vdly__spi_tx_fifo__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                                  << 4U) 
                                                 | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                     << 3U) 
                                                    | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                        << 2U) 
                                                       | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                           << 1U) 
                                                          | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                                if (this->__PVT__spi_tx_fifo__DOT__not_ring_full) {
                                    this->__Vdly__spi_tx_fifo__DOT__tail 
                                        = this->__PVT__spi_tx_fifo__DOT__next_tail;
                                    this->__Vdly__spi_tx_fifo__DOT__ring_empty = 0U;
                                    this->__Vdly__spi_tx_fifo__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__spi_tx_fifo__DOT__next_tail) 
                                           != (IData)(this->__PVT__spi_tx_fifo__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__spi_tx_fifo__DOT__head = 0U;
        this->__Vdly__spi_tx_fifo__DOT__tail = 0U;
        this->__Vdly__spi_tx_fifo__DOT__ring_empty = 1U;
        this->__Vdly__spi_tx_fifo__DOT__not_ring_full = 1U;
        this->__Vdly__spi_tx_fifo__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
                this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg = 0U;
    }
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                            << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                       << 2U) | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
        this->__PVT__spi_rx_fifo_D_OUT = ((0x12U >= (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                           ? this->__PVT__spi_rx_fifo__DOT__arr
                                          [this->__PVT__spi_rx_fifo__DOT__head]
                                           : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
            this->__PVT__spi_rx_fifo_D_OUT = this->__PVT__v___05Fh8399;
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                  << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                this->__PVT__spi_rx_fifo_D_OUT = ((0x12U 
                                                   >= (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                                   ? 
                                                  this->__PVT__spi_rx_fifo__DOT__arr
                                                  [this->__PVT__spi_rx_fifo__DOT__head]
                                                   : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                      << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                    this->__PVT__spi_rx_fifo_D_OUT 
                        = this->__PVT__v___05Fh8399;
                }
            }
        }
    }
    if (((IData)(this->__PVT__spi_rx_fifo_ENQ) & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                                   & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))) 
                                                  | (((~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)) 
                                                      & (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                                                     & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))))) {
        this->spi_rx_fifo__DOT____Vlvbound2 = this->__PVT__v___05Fh8399;
        if ((0x12U >= (IData)(this->__PVT__spi_rx_fifo__DOT__tail))) {
            this->__Vdlyvval__spi_rx_fifo__DOT__arr__v0 
                = this->spi_rx_fifo__DOT____Vlvbound2;
            this->__Vdlyvset__spi_rx_fifo__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__spi_rx_fifo__DOT__arr__v0 
                = this->__PVT__spi_rx_fifo__DOT__tail;
        }
    }
    this->__PVT__s_xactor_spi_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
                                     & (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)))))) 
             & (QData)((IData)(this->__PVT__ff_sync_rd_resp__DOT__syncFIFO1Data))) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
            & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg))
            ? (QData)((IData)(this->__PVT__ff_sync_rd_resp__DOT__syncFIFO1Data))
            : this->__PVT__s_xactor_spi_f_rd_data__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                 << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
            this->__Vdly__spi_rx_fifo__DOT__head = 0U;
            this->__Vdly__spi_rx_fifo__DOT__tail = 0U;
            this->__Vdly__spi_rx_fifo__DOT__ring_empty = 1U;
            this->__Vdly__spi_rx_fifo__DOT__not_ring_full = 1U;
            this->__Vdly__spi_rx_fifo__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                    << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                this->__Vdly__spi_rx_fifo__DOT__tail 
                    = this->__PVT__spi_rx_fifo__DOT__next_tail;
                this->__Vdly__spi_rx_fifo__DOT__head 
                    = this->__PVT__spi_rx_fifo__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                      << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                    this->__Vdly__spi_rx_fifo__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                        this->__Vdly__spi_rx_fifo__DOT__head 
                            = this->__PVT__spi_rx_fifo__DOT__next_head;
                        this->__Vdly__spi_rx_fifo__DOT__not_ring_full = 1U;
                        this->__Vdly__spi_rx_fifo__DOT__ring_empty 
                            = ((IData)(this->__PVT__spi_rx_fifo__DOT__next_head) 
                               == (IData)(this->__PVT__spi_rx_fifo__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                            this->__Vdly__spi_rx_fifo__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                                if (this->__PVT__spi_rx_fifo__DOT__not_ring_full) {
                                    this->__Vdly__spi_rx_fifo__DOT__tail 
                                        = this->__PVT__spi_rx_fifo__DOT__next_tail;
                                    this->__Vdly__spi_rx_fifo__DOT__ring_empty = 0U;
                                    this->__Vdly__spi_rx_fifo__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__spi_rx_fifo__DOT__next_tail) 
                                           != (IData)(this->__PVT__spi_rx_fifo__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__spi_rx_fifo__DOT__head = 0U;
        this->__Vdly__spi_rx_fifo__DOT__tail = 0U;
        this->__Vdly__spi_rx_fifo__DOT__ring_empty = 1U;
        this->__Vdly__spi_rx_fifo__DOT__not_ring_full = 1U;
        this->__Vdly__spi_rx_fifo__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
             & (0x28U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U)))))) {
            this->__PVT__spi_rg_spi_cfg_txcrcr = this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U];
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_txcrcr = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
             & (0x24U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U)))))) {
            this->__PVT__spi_rg_spi_cfg_rxcrcr = this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U];
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_rxcrcr = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
             & (0x20U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U)))))) {
            this->__PVT__spi_rg_spi_cfg_crcpr = this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U];
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_crcpr = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
               & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
              & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
             | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                 & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                & (7U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            this->__PVT__spi_rg_data_rx = this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1;
        }
    } else {
        this->__PVT__spi_rg_data_rx = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                     & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                    & ((7U > (IData)(this->__PVT__spi_rg_data_counter)) 
                       | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
                   | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                      & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315))) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                     & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                            << 0x1dU) 
                                           | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                              >> 3U)))))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                    & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                   & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                  & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle))) {
            this->__PVT__spi_rg_bit_count = this->__PVT__spi_rg_bit_count_D_IN;
        }
    } else {
        this->__PVT__spi_rg_bit_count = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr5 = this->__PVT__spi_rg_spi_cfg_dr5_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr5 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0xcU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                        << 0x1dU) | 
                                       (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                        >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr1 = this->__PVT__spi_rg_spi_cfg_dr1_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                 & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                  & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                 & (this->__PVT__spi_rg_spi_cfg_cr2 
                    >> 0x10U))) | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                   & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319)))) {
            this->__PVT__spi_rg_tx_rx_start = (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319))
                                                ? (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)
                                                : (
                                                   (~ (IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3)) 
                                                   & (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                                       & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                                                      & (this->__PVT__spi_rg_spi_cfg_cr2 
                                                         >> 0x10U))));
        }
    } else {
        this->__PVT__spi_rg_tx_rx_start = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                & (this->__PVT__spi_rg_spi_cfg_cr1 
                   >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                 & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214)))) {
            this->__PVT__spi_rg_data_tx = this->__PVT__spi_rg_data_tx_D_IN;
        }
    } else {
        this->__PVT__spi_rg_data_tx = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)) 
               | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                    & (this->__PVT__spi_rg_spi_cfg_cr1 
                       >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                  & (~ this->__PVT__spi_rg_spi_cfg_cr1))) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                 & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211)))) {
            this->__PVT__spi_wr_spi_out_io1 = (1U & 
                                               ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4)
                                                 ? (IData)(this->__PVT__v___05Fh5419)
                                                 : 
                                                (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                                    & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                       >> 6U)) 
                                                   & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                                  & (~ this->__PVT__spi_rg_spi_cfg_cr1))
                                                  ? 
                                                 ((0x80U 
                                                   & this->__PVT__spi_rg_spi_cfg_cr1)
                                                   ? (IData)(this->__PVT__spi_tx_fifo_D_OUT)
                                                   : 
                                                  ((IData)(this->__PVT__spi_tx_fifo_D_OUT) 
                                                   >> 7U))
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                   ? (IData)(this->__PVT__v___05Fh5419)
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7) 
                                                   & (IData)(this->__PVT__v___05Fh5419))))));
        }
    } else {
        this->__PVT__spi_wr_spi_out_io1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                 & (this->__PVT__spi_rg_spi_cfg_cr1 
                    >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                  & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                 & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            this->__PVT__spi_rg_transmit_state = (((IData)(this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6) 
                                                   | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4))
                                                   ? 0U
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                                    ? 1U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                     ? 2U
                                                     : 0U)));
        }
    } else {
        this->__PVT__spi_rg_transmit_state = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
               & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U)) 
              & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
             | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                  & ((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
                     | (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                 & (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209)) 
                & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))))) {
            this->__PVT__spi_rg_transfer_done = (1U 
                                                 & (~ (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)));
        }
    } else {
        this->__PVT__spi_rg_transfer_done = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                   & (this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                    & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233))) 
                | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                   & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315))) 
               | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                  & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter)))) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                 & (8U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                     << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                  >> 3U)))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish))) {
            this->__PVT__spi_rg_spi_cfg_sr = this->__PVT__spi_rg_spi_cfg_sr_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_sr = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x14U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr3 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (0x14U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                    ? 
                                                   ((0x80U 
                                                     & this->__PVT__spi_rg_spi_cfg_cr1)
                                                     ? this->__PVT__x___05Fh3146
                                                     : this->__PVT__x___05Fh2934)
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                     ? 
                                                    ((0x13U 
                                                      > (IData)(this->__PVT__spi_rg_data_counter))
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? 
                                                      ((this->__PVT___theResult___05F___05Fh10026[3U] 
                                                        << 0x18U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[2U] 
                                                          >> 8U))
                                                       : 
                                                      ((this->__PVT___theResult___05F___05Fh10026[2U] 
                                                        << 8U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[1U] 
                                                          >> 0x18U)))
                                                      : 
                                                     this->__PVT___theResult___05F___05Fh10026[2U])
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? this->__PVT__x___05Fh2934
                                                       : this->__PVT__x___05Fh3146)
                                                      : 0U))));
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr3 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x18U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr4 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (0x18U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                    ? 
                                                   ((0x80U 
                                                     & this->__PVT__spi_rg_spi_cfg_cr1)
                                                     ? this->__PVT__x___05Fh3171
                                                     : this->__PVT__x___05Fh2959)
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                     ? 
                                                    ((0x13U 
                                                      > (IData)(this->__PVT__spi_rg_data_counter))
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? 
                                                      ((this->__PVT___theResult___05F___05Fh10026[2U] 
                                                        << 0x18U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[1U] 
                                                          >> 8U))
                                                       : 
                                                      ((this->__PVT___theResult___05F___05Fh10026[1U] 
                                                        << 8U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                          >> 0x18U)))
                                                      : 
                                                     this->__PVT___theResult___05F___05Fh10026[1U])
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? this->__PVT__x___05Fh2959
                                                       : this->__PVT__x___05Fh3171)
                                                      : 0U))));
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr4 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x10U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr2 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (0x10U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                    ? 
                                                   ((0x80U 
                                                     & this->__PVT__spi_rg_spi_cfg_cr1)
                                                     ? this->__PVT__x___05Fh3121
                                                     : this->__PVT__x___05Fh2909)
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                     ? 
                                                    ((0x13U 
                                                      > (IData)(this->__PVT__spi_rg_data_counter))
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? 
                                                      ((this->__PVT___theResult___05F___05Fh10026[4U] 
                                                        << 0x18U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[3U] 
                                                          >> 8U))
                                                       : 
                                                      ((this->__PVT___theResult___05F___05Fh10026[3U] 
                                                        << 8U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[2U] 
                                                          >> 0x18U)))
                                                      : 
                                                     this->__PVT___theResult___05F___05Fh10026[3U])
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? this->__PVT__x___05Fh2909
                                                       : this->__PVT__x___05Fh3121)
                                                      : 0U))));
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr2 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__spi_rg_nss) | (IData)(this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130))) {
            this->__PVT__spi_rg_clk = this->__PVT__spi_rg_clk_D_IN;
        }
    } else {
        this->__PVT__spi_rg_clk = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__spi_rg_nss)))) {
            this->__PVT__spi_rg_clk_counter = this->__PVT__spi_rg_clk_counter_D_IN;
        }
    } else {
        this->__PVT__spi_rg_clk_counter = 0U;
    }
    this->__PVT__spi_tx_fifo__DOT__not_ring_full = this->__Vdly__spi_tx_fifo__DOT__not_ring_full;
    this->__PVT__spi_tx_fifo__DOT__tail = this->__Vdly__spi_tx_fifo__DOT__tail;
    if (this->__Vdlyvset__spi_rx_fifo__DOT__arr__v0) {
        this->__PVT__spi_rx_fifo__DOT__arr[this->__Vdlyvdim0__spi_rx_fifo__DOT__arr__v0] 
            = this->__Vdlyvval__spi_rx_fifo__DOT__arr__v0;
    }
    this->__PVT__spi_rx_fifo__DOT__ring_empty = this->__Vdly__spi_rx_fifo__DOT__ring_empty;
    this->__PVT__spi_rx_fifo__DOT__tail = this->__Vdly__spi_rx_fifo__DOT__tail;
    this->__PVT__spi_rx_fifo__DOT__head = this->__Vdly__spi_rx_fifo__DOT__head;
    this->__PVT__spi_rx_fifo__DOT__hasodata = this->__Vdly__spi_rx_fifo__DOT__hasodata;
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)))) {
            this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg = 1U;
    }
    this->__PVT__spi_tx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__tail))));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
                this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)))) {
                this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg = 1U;
    }
    this->__PVT__spi_rx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__tail))));
    this->__PVT__spi_rx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__head))));
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                            << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                       << 3U) | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                  << 2U) 
                                                 | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                     << 1U) 
                                                    | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
        this->__PVT__spi_tx_fifo_D_OUT = ((0x12U >= (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                           ? this->__PVT__spi_tx_fifo__DOT__arr
                                          [this->__PVT__spi_tx_fifo__DOT__head]
                                           : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                           << 3U) | 
                                          (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
            this->__PVT__spi_tx_fifo_D_OUT = this->__PVT__v___05Fh2759;
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                  << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                             << 3U) 
                                            | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                << 2U) 
                                               | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                   << 1U) 
                                                  | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                this->__PVT__spi_tx_fifo_D_OUT = ((0x12U 
                                                   >= (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                                   ? 
                                                  this->__PVT__spi_tx_fifo__DOT__arr
                                                  [this->__PVT__spi_tx_fifo__DOT__head]
                                                   : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                      << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                 << 3U) 
                                                | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                    << 2U) 
                                                   | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                       << 1U) 
                                                      | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                    this->__PVT__spi_tx_fifo_D_OUT 
                        = this->__PVT__v___05Fh2759;
                }
            }
        }
    }
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4 
        = (0xffffff7fU & this->__PVT__spi_rg_spi_cfg_sr);
    this->__PVT__x___05Fh2959 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 >> 8U)));
    this->__PVT__x___05Fh3146 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                             >> 0x18U)));
    this->__PVT__x___05Fh3171 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2984 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                 >> 8U)));
    this->__PVT__x___05Fh2909 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 >> 8U)));
    this->__PVT__x___05Fh2934 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 >> 8U)));
    this->__PVT__x___05Fh3121 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                             >> 0x18U)));
    this->__PVT__x___05Fh3086 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                             >> 0x18U)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__spi_tx_data_en_EN) {
            this->__PVT__spi_tx_data_en = (1U & (~ (IData)(this->__PVT__MUX_spi_tx_data_en_write_1___05FSEL_1)));
        }
    } else {
        this->__PVT__spi_tx_data_en = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__spi_rg_data_counter_EN) {
            this->__PVT__spi_rg_data_counter = this->__PVT__spi_rg_data_counter_D_IN;
        }
    } else {
        this->__PVT__spi_rg_data_counter = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                    & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                   & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                  & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                 | (IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1)) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                    & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                   & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                         >> 0x10U)))) | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control)) 
              | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop))) {
            this->__PVT__spi_rg_nss = (1U & ((IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1)
                                              ? ((~ 
                                                  (this->__PVT__spi_rg_spi_cfg_cr1 
                                                   >> 6U)) 
                                                 | (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)))
                                              : ((~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                                 & ((IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3) 
                                                    | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control)
                                                        ? 
                                                       (this->__PVT__spi_rg_spi_cfg_cr1 
                                                        >> 8U)
                                                        : 
                                                       ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                                          & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                                                         & (~ 
                                                            (this->__PVT__spi_rg_spi_cfg_cr2 
                                                             >> 0x10U))) 
                                                        | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop)))))));
        }
    } else {
        this->__PVT__spi_rg_nss = 1U;
    }
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg 
        = this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg;
    this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg;
    this->__PVT__spi_tx_fifo__DOT__ring_empty = this->__Vdly__spi_tx_fifo__DOT__ring_empty;
    if (this->__Vdlyvset__spi_tx_fifo__DOT__arr__v0) {
        this->__PVT__spi_tx_fifo__DOT__arr[this->__Vdlyvdim0__spi_tx_fifo__DOT__arr__v0] 
            = this->__Vdlyvval__spi_tx_fifo__DOT__arr__v0;
    }
    this->__PVT__spi_tx_fifo__DOT__head = this->__Vdly__spi_tx_fifo__DOT__head;
    this->__PVT__s_xactor_spi_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_resp__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_spi_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_data__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg));
    this->__PVT__spi_tx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__head))));
    this->__PVT__MUX_spi_tx_data_en_write_1___05FSEL_1 
        = (((IData)(this->__PVT__spi_tx_data_en) & 
            (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6 
        = ((0x13U > (IData)(this->__PVT__spi_rg_data_counter))
            ? (0xffU & ((IData)(1U) + (IData)(this->__PVT__spi_rg_data_counter)))
            : 0U);
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11 
        = ((IData)(this->__PVT__spi_tx_data_en) & (0x14U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter)));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
              & (0U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                  << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U))))) 
             | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                  & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                 & (IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156)) 
                & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                      >> 0x10U))))) {
            this->__PVT__spi_rg_spi_cfg_cr1 = this->__PVT__spi_rg_spi_cfg_cr1_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_cr1 = 0U;
    }
    this->__PVT__spi_tx_fifo__DOT__hasodata = this->__Vdly__spi_tx_fifo__DOT__hasodata;
    this->__PVT__v___05Fh5419 = (1U & ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)
                                        ? (IData)(this->__PVT__spi_rg_data_tx)
                                        : ((IData)(this->__PVT__spi_rg_data_tx) 
                                           >> 7U)));
    this->__PVT__spi_rg_clk_D_IN = (1U & ((IData)(this->__PVT__spi_rg_nss)
                                           ? (this->__PVT__spi_rg_spi_cfg_cr1 
                                              >> 1U)
                                           : (~ (IData)(this->__PVT__spi_rg_clk))));
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT__v___05Fh2759 = (0xffU & this->__PVT__spi_rg_spi_cfg_dr5);
        this->__PVT___theResult___05F___05Fh10026[0U] 
            = this->__PVT__spi_rg_spi_cfg_dr5;
    } else {
        this->__PVT__v___05Fh2759 = (0xffU & (this->__PVT__spi_rg_spi_cfg_dr1 
                                              >> 0x18U));
        this->__PVT___theResult___05F___05Fh10026[0U] 
            = ((0xffffff00U & this->__PVT__spi_rg_spi_cfg_dr5) 
               | (IData)(this->__PVT__spi_rx_fifo_D_OUT));
    }
    this->__PVT___theResult___05F___05Fh10026[1U] = this->__PVT__spi_rg_spi_cfg_dr4;
    this->__PVT___theResult___05F___05Fh10026[2U] = this->__PVT__spi_rg_spi_cfg_dr3;
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                          << 0x18U) 
                                         | (0xffffffU 
                                            & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                           << 0x18U) 
                                          | (0xffffffU 
                                             & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    } else {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    }
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x10U) - (IData)(1U))));
    this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130 
        = ((7U & (this->__PVT__spi_rg_spi_cfg_cr1 >> 3U)) 
           == (IData)(this->__PVT__spi_rg_clk_counter));
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                 & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                   & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
                  & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                 & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                & (4U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U))))))) {
            this->__PVT__spi_rg_spi_cfg_cr2 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (4U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : 0U);
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_cr2 = 0U;
    }
    this->__PVT__spi_rg_clk_counter_D_IN = ((IData)(this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130)
                                             ? 0U : 
                                            (7U & ((IData)(1U) 
                                                   + (IData)(this->__PVT__spi_rg_clk_counter))));
    this->__PVT__spi_rx_fifo__DOT__not_ring_full = this->__Vdly__spi_rx_fifo__DOT__not_ring_full;
    this->__PVT__spi_wr_clk_en_wget = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                   >> 1U)) 
                                               & (~ (IData)(this->__PVT__spi_rg_clk))) 
                                              | ((this->__PVT__spi_rg_spi_cfg_cr1 
                                                  & (~ 
                                                     (this->__PVT__spi_rg_spi_cfg_cr1 
                                                      >> 1U))) 
                                                 & (IData)(this->__PVT__spi_rg_clk))) 
                                             | ((~ this->__PVT__spi_rg_spi_cfg_cr1) 
                                                & (((this->__PVT__spi_rg_spi_cfg_cr1 
                                                     >> 1U) 
                                                    & (IData)(this->__PVT__spi_rg_clk)) 
                                                   | ((~ 
                                                       (this->__PVT__spi_rg_spi_cfg_cr1 
                                                        >> 1U)) 
                                                      & (~ (IData)(this->__PVT__spi_rg_clk)))))));
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x18U) - (IData)(1U))));
    this->__PVT__spi_wr_transfer_en_whas = ((~ (IData)(this->__PVT__spi_rg_nss)) 
                                            & (IData)(this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315 
        = ((((~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU)) 
             & (0U != (IData)(this->__PVT__spi_rg_transmit_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
           & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)));
    this->__PVT_____05Fduses58 = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                    & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                          >> 0x10U))) 
                                   & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                  & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223 
        = ((((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
             & (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156 
        = (((~ (IData)(this->__PVT__spi_wr_clk_en_wget)) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             | (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233 
        = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (IData)(this->__PVT__spi_rg_transfer_done));
    this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182 
        = ((IData)(this->__PVT__spi_wr_clk_en_wget) 
           | (this->__PVT__spi_rg_spi_cfg_cr1 & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273 
        = ((((8U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209 
        = ((((7U == (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (~ (IData)(this->__PVT__spi_rg_transfer_done)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__spi_rg_receive_state_EN) {
            this->__PVT__spi_rg_receive_state = (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5) 
                                                  | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_done))
                                                  ? 0U
                                                  : 
                                                 ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)
                                                   ? 1U
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                    ? 2U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3)
                                                     ? 3U
                                                     : 0U))));
        }
    } else {
        this->__PVT__spi_rg_receive_state = 0U;
    }
    this->__PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget 
        = ((IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233) 
           & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280 
        = (((IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273) 
            & ((7U > (IData)(this->__PVT__spi_rg_data_counter)) 
               | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
           | (((~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
               & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
              & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211 
        = (((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
              & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             | ((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | ((((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | ((IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209) 
                 & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop 
        = (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
              & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
             & ((~ (this->__PVT__spi_rg_spi_cfg_cr2 
                    >> 0xfU)) | (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                    >> 0x10U)))) & 
            (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319 
        = (((IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315) 
            & (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)) 
           | (((0U != (IData)(this->__PVT__spi_rg_receive_state)) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (IData)(this->__PVT__spi_wr_clk_en_wget)));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg 
        = ((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
           & (0U == (IData)(this->__PVT__spi_rg_receive_state)));
    this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx = ((IData)(this->__PVT__spi_wr_transfer_en_whas) 
                                                   & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                         >> 6U) 
                                                        & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
                                                       & (0U 
                                                          != (IData)(this->__PVT__spi_rg_transmit_state))) 
                                                      | ((((this->__PVT__spi_rg_spi_cfg_cr2 
                                                            >> 0xfU) 
                                                           | (this->__PVT__spi_rg_spi_cfg_cr2 
                                                              >> 0x10U)) 
                                                          & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                         & (0U 
                                                            != (IData)(this->__PVT__spi_rg_receive_state)))));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_done = 
        ((3U == (IData)(this->__PVT__spi_rg_receive_state)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
             & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4 
        = ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
           & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle = 
        (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & ((this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU) 
               | ((this->__PVT__spi_rg_spi_cfg_cr2 
                   >> 0x10U) & (IData)(this->__PVT__spi_rg_tx_rx_start)))) 
           & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_receive = 
        (((2U == (IData)(this->__PVT__spi_rg_receive_state)) 
          & (~ (IData)(this->__PVT__spi_rg_nss))) & 
         (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive 
        = (((1U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle 
        = ((((0U == (IData)(this->__PVT__spi_rg_transmit_state)) 
             & (~ (IData)(this->__PVT__spi_tx_data_en))) 
            & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start 
        = (((1U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit 
        = (((2U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
             & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rx_fifo_ENQ = (((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                       & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
                                      & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                     & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                    | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                                        & (7U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                                       & (7U == (IData)(this->__PVT__spi_rg_data_counter))));
    this->__PVT__spi_rg_receive_state_EN = ((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                  & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                                                 & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                                & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                                               | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                    & (0U 
                                                       != (IData)(this->__PVT__spi_rg_receive_state))) 
                                                   & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                                 & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_done)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control 
        = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                    >> 9U) & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive))) 
                  & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle))) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle))));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
           & (((this->__PVT__spi_rg_spi_cfg_cr1 >> 6U) 
               & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
              | ((IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156) 
                 & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                       >> 0x10U)))));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
            & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U)) 
           & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182));
    this->__PVT__spi_tx_fifo_DEQ = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                    & (IData)(this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223));
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214));
    this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish 
        = (((((((~ (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                & (0U == (IData)(this->__PVT__spi_rg_receive_state))) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start))) 
            & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx))) 
           & (~ (IData)(this->__PVT__spi_tx_data_en)));
    this->__PVT__spi_rg_data_tx_D_IN = ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                         ? (IData)(this->__PVT__spi_tx_fifo_D_OUT)
                                         : ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                             ? ((0x80U 
                                                 & this->__PVT__spi_rg_spi_cfg_cr1)
                                                 ? 
                                                (0x7fU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    >> 1U))
                                                 : 
                                                (0xfeU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    << 1U)))
                                             : ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9)
                                                 ? 
                                                ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217)
                                                  ? 
                                                 ((0x80U 
                                                   & this->__PVT__spi_rg_spi_cfg_cr1)
                                                   ? 
                                                  (0x7fU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      >> 1U))
                                                   : 
                                                  (0xfeU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      << 1U)))
                                                  : (IData)(this->__PVT__spi_tx_fifo_D_OUT))
                                                 : 0U)));
    this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
            & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0U != (IData)(this->__PVT__spi_rg_data_counter)));
}

VL_INLINE_OPT void VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__5(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__5\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__ff_sync_rd_resp__DOT__dDeqToggle 
        = this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle;
    this->__Vdly__ff_sync_rd_resp__DOT__sEnqToggle 
        = this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle;
    this->__Vdly__ff_rd_req__DOT__sEnqToggle = this->__PVT__ff_rd_req__DOT__sEnqToggle;
    this->__Vdly__ff_rd_req__DOT__dDeqToggle = this->__PVT__ff_rd_req__DOT__dDeqToggle;
    this->__Vdly__ff_wr_req__DOT__dDeqToggle = this->__PVT__ff_wr_req__DOT__dDeqToggle;
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
             & ((IData)(this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle) 
                != (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle)))) {
            this->__Vdly__ff_sync_rd_resp__DOT__dDeqToggle 
                = (1U & (~ (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle)));
        }
    } else {
        this->__Vdly__ff_sync_rd_resp__DOT__dDeqToggle = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_from_controller) 
             & ((IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle) 
                == (IData)(this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle)))) {
            this->__Vdly__ff_sync_rd_resp__DOT__sEnqToggle 
                = (1U & (~ (IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle)));
            this->__PVT__ff_sync_rd_resp__DOT__syncFIFO1Data 
                = this->__PVT__spi_wr_rd_data_wget;
        }
    } else {
        this->__Vdly__ff_sync_rd_resp__DOT__sEnqToggle = 0U;
        this->__PVT__ff_sync_rd_resp__DOT__syncFIFO1Data = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
             & ((IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle) 
                == (IData)(this->__PVT__ff_rd_req__DOT__sDeqToggle)))) {
            this->__Vdly__ff_rd_req__DOT__sEnqToggle 
                = (1U & (~ (IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle)));
            this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                = (((QData)((IData)((this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg 
                                     >> 5U))) << 3U) 
                   | (QData)((IData)((3U & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg)))));
        }
    } else {
        this->__Vdly__ff_rd_req__DOT__sEnqToggle = 0U;
        this->__PVT__ff_rd_req__DOT__syncFIFO1Data = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__ff_rd_req_dEMPTY_N) 
             & ((IData)(this->__PVT__ff_rd_req__DOT__dEnqToggle) 
                != (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle)))) {
            this->__Vdly__ff_rd_req__DOT__dDeqToggle 
                = (1U & (~ (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle)));
        }
    } else {
        this->__Vdly__ff_rd_req__DOT__dDeqToggle = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__ff_wr_req_dEMPTY_N) 
             & ((IData)(this->__PVT__ff_wr_req__DOT__dEnqToggle) 
                != (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle)))) {
            this->__Vdly__ff_wr_req__DOT__dDeqToggle 
                = (1U & (~ (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle)));
        }
    } else {
        this->__Vdly__ff_wr_req__DOT__dDeqToggle = 0U;
    }
    this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle = 
        ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__ff_sync_rd_resp__DOT__dSyncReg1));
    this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle = 
        (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__ff_sync_rd_resp__DOT__sSyncReg1)));
    this->__PVT__spi_wr_rd_data_wget = ((1U & (IData)(
                                                      (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                       >> 0xaU)))
                                         ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                         : ((1U & (IData)(
                                                          (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                           >> 9U)))
                                             ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                             : ((1U 
                                                 & (IData)(
                                                           (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                            >> 8U)))
                                                 ? 
                                                ((1U 
                                                  & (IData)(
                                                            (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                             >> 7U)))
                                                  ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_rxcrcr))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_crcpr)))))
                                                 : 
                                                ((1U 
                                                  & (IData)(
                                                            (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                             >> 7U)))
                                                  ? 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr5))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr4)))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr3))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr2))))
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr1))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_sr)))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_cr2))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_cr1))))))));
    this->__PVT__ff_rd_req__DOT__sDeqToggle = (1U & 
                                               ((~ (IData)(vlTOPp->RST_N)) 
                                                | (IData)(this->__PVT__ff_rd_req__DOT__sSyncReg1)));
    this->__PVT__ff_rd_req__DOT__dEnqToggle = ((IData)(vlTOPp->RST_N) 
                                               & (IData)(this->__PVT__ff_rd_req__DOT__dSyncReg1));
    this->__PVT__ff_wr_req__DOT__dEnqToggle = ((IData)(vlTOPp->RST_N) 
                                               & (IData)(this->__PVT__ff_wr_req__DOT__dSyncReg1));
    this->__PVT__ff_sync_rd_resp__DOT__dSyncReg1 = 
        ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle));
    this->__PVT__ff_sync_rd_resp__DOT__sSyncReg1 = 
        ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle));
    this->__PVT__ff_rd_req__DOT__sSyncReg1 = ((IData)(vlTOPp->RST_N) 
                                              & (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle));
    this->__PVT__ff_rd_req__DOT__dSyncReg1 = ((IData)(vlTOPp->RST_N) 
                                              & (IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle));
    this->__PVT__ff_wr_req__DOT__dSyncReg1 = ((IData)(vlTOPp->RST_N) 
                                              & (IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle));
    this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle = this->__Vdly__ff_sync_rd_resp__DOT__sEnqToggle;
    this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle = this->__Vdly__ff_sync_rd_resp__DOT__dDeqToggle;
    this->__PVT__ff_rd_req__DOT__dDeqToggle = this->__Vdly__ff_rd_req__DOT__dDeqToggle;
    this->__PVT__ff_rd_req__DOT__sEnqToggle = this->__Vdly__ff_rd_req__DOT__sEnqToggle;
    this->__PVT__CAN_FIRE_RL_rl_read_response_to_core 
        = (((IData)(this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle) 
            != (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle)) 
           & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg));
    this->__PVT__ff_rd_req_dEMPTY_N = ((IData)(this->__PVT__ff_rd_req__DOT__dEnqToggle) 
                                       != (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle));
    this->__PVT__CAN_FIRE_RL_rl_read_request_from_core 
        = (((IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle) 
            == (IData)(this->__PVT__ff_rd_req__DOT__sDeqToggle)) 
           & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_rl_read_response_from_controller 
        = (((IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle) 
            == (IData)(this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle)) 
           & (IData)(this->__PVT__ff_rd_req_dEMPTY_N));
}

VL_INLINE_OPT void VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__7(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__7\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) {
            this->__PVT__v___05Fh11853 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) {
            VL_WRITEF("%10# SPI : Controller Write Channel\n",
                      32,this->__PVT__v___05Fh11853);
        }
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__spi_tx_data_en) & 
             (0x14U > (IData)(this->__PVT__spi_rg_data_counter)))) {
            this->__PVT__v___05Fh3428 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__spi_tx_data_en) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("%10# SPI : DR to  tx_fifo data %x rg_data_counter %3#\n",
                      32,this->__PVT__v___05Fh3428,
                      8,(IData)(this->__PVT__v___05Fh2759),
                      8,this->__PVT__spi_rg_data_counter);
        }
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
             & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) {
            this->__PVT__v___05Fh5769 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                         & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            VL_WRITEF("%10# SPI : START_TRANSMIT case2 counter %x data %x rg_bit_count %x\n",
                      32,this->__PVT__v___05Fh5769,
                      8,(IData)(this->__PVT__spi_rg_data_counter),
                      1,this->__PVT__v___05Fh5419,8,
                      (IData)(this->__PVT__spi_rg_bit_count));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                           & (IData)(this->__PVT__spi_tx_data_en)) 
                          & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 414, column 6: (R0002)\n  Conflict-free rules RL_spi_rl_transmit_start and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
               & (6U > (IData)(this->__PVT__spi_rg_data_counter))) 
              & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             & (IData)(this->__PVT__spi_wr_transfer_en_whas))) {
            this->__PVT__v___05Fh6289 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                           & (6U > (IData)(this->__PVT__spi_rg_data_counter))) 
                          & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                         & (IData)(this->__PVT__spi_wr_transfer_en_whas)))) {
            VL_WRITEF("%10# SPI : DATA_TRANSMIT case 1 data %x rg_data_counter %x rg_bit_count %3# \n",
                      32,this->__PVT__v___05Fh6289,
                      1,(IData)(this->__PVT__v___05Fh5419),
                      8,this->__PVT__spi_rg_data_counter,
                      8,(IData)(this->__PVT__spi_rg_bit_count));
        }
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
             & (IData)(this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223))) {
            this->__PVT__v___05Fh6816 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                         & (IData)(this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223)))) {
            VL_WRITEF("%10# SPI : DATA_TRANSMIT case 2 data %x rg_data_counter %x rg_bit_count %3# \n",
                      32,this->__PVT__v___05Fh6816,
                      1,(IData)(this->__PVT__v___05Fh5419),
                      8,this->__PVT__spi_rg_data_counter,
                      8,(IData)(this->__PVT__spi_rg_bit_count));
        }
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
               & ((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
                  | (IData)(this->__PVT__spi_wr_clk_en_wget))) 
              & (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209)) 
             & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) {
            this->__PVT__v___05Fh7130 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                           & ((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
                              | (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                          & (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209)) 
                         & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)))) {
            VL_WRITEF("%10# SPI : tx_fifo data %x\n",
                      32,this->__PVT__v___05Fh7130,
                      8,(IData)(this->__PVT__spi_tx_fifo_D_OUT));
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
              & ((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
                 | (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             & (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209))) {
            this->__PVT__v___05Fh7329 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                          & ((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
                             | (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                         & (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209)))) {
            VL_WRITEF("%10# SPI : DATA_TRANSMIT case 3 data %x rg_data_counter %x rg_bit_count %3#\n",
                      32,this->__PVT__v___05Fh7329,
                      1,(IData)(this->__PVT__v___05Fh5419),
                      8,this->__PVT__spi_rg_data_counter,
                      8,(IData)(this->__PVT__spi_rg_bit_count));
        }
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
             & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233))) {
            this->__PVT__v___05Fh7628 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                         & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)))) {
            VL_WRITEF("%10# SPI : Transmit state going to idle\n",
                      32,this->__PVT__v___05Fh7628);
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                           & (IData)(this->__PVT__spi_tx_data_en)) 
                          & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 443, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_data_transmit and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
              & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
             & (IData)(this->__PVT__spi_wr_transfer_en_whas))) {
            this->__PVT__v___05Fh8248 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                          & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                         & (IData)(this->__PVT__spi_wr_transfer_en_whas)))) {
            VL_WRITEF("%10# SPI : START_RECEIVE case2 counter %x\n",
                      32,this->__PVT__v___05Fh8248,
                      8,(IData)(this->__PVT__spi_rg_data_counter));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                            & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 545, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_start_receive and\n  RL_spi_rl_data_transmit called conflicting methods read and write of module\n  instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                            & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 545, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_start_receive and\n  RL_spi_rl_data_transmit called conflicting methods read and write of module\n  instance spi_rg_bit_count.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                           & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit)) 
                          & (1U == (IData)(this->__PVT__spi_rg_receive_state))) 
                         & (IData)(this->__PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 545, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_start_receive and\n  RL_spi_rl_data_transmit called conflicting methods read and write of module\n  instance spi_rg_nss.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                            & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 545, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_start_receive and\n  RL_spi_rl_transmit_start called conflicting methods read and write of module\n  instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                            & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 545, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_start_receive and\n  RL_spi_rl_transmit_start called conflicting methods read and write of module\n  instance spi_rg_bit_count.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                            & (IData)(this->__PVT__spi_tx_data_en)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 545, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_start_receive and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
              & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
             & (7U > (IData)(this->__PVT__spi_rg_data_counter)))) {
            this->__PVT__v___05Fh8744 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                          & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                         & (7U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("%10# SPI : DATA_RECEIVE case1 counter %x data_rx %x rg_bit_count %3#\n",
                      32,this->__PVT__v___05Fh8744,
                      8,(IData)(this->__PVT__spi_rg_data_counter),
                      8,this->__PVT__v___05Fh8399,8,
                      (IData)(this->__PVT__spi_rg_bit_count));
        }
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
               & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
              & (7U <= (IData)(this->__PVT__spi_rg_data_counter))) 
             & (7U == (IData)(this->__PVT__spi_rg_data_counter)))) {
            this->__PVT__v___05Fh8863 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                          & (7U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                         & (7U == (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("%10# SPI : DATA_RECEIVE case2 counter %x data_rx %x rg_bit_count %3#\n",
                      32,this->__PVT__v___05Fh8863,
                      8,(IData)(this->__PVT__spi_rg_data_counter),
                      8,this->__PVT__v___05Fh8399,8,
                      (IData)(this->__PVT__spi_rg_bit_count));
        }
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
               & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
              & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             & (IData)(this->__PVT__spi_wr_transfer_en_whas))) {
            this->__PVT__v___05Fh9087 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                           & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                          & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                         & (IData)(this->__PVT__spi_wr_transfer_en_whas)))) {
            VL_WRITEF("%10# SPI : DATA_RECEIVE going to receive done\n",
                      32,this->__PVT__v___05Fh9087);
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                             & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit)) 
                            & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 567, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_data_receive and RL_spi_rl_data_transmit\n  called conflicting methods read and write of module instance\n  spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                               & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit)) 
                              & (((7U == (IData)(this->__PVT__spi_rg_data_counter)) 
                                  | (7U > (IData)(this->__PVT__spi_rg_data_counter))) 
                                 | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
                             & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                            & (8U > (IData)(this->__PVT__spi_rg_data_counter))) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 567, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_data_receive and RL_spi_rl_data_transmit\n  called conflicting methods read and write of module instance\n  spi_rg_bit_count.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                           & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit)) 
                          & (2U == (IData)(this->__PVT__spi_rg_receive_state))) 
                         & (IData)(this->__PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 567, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_data_receive and RL_spi_rl_data_transmit\n  called conflicting methods read and write of module instance spi_rg_nss.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                             & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start)) 
                            & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 567, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_data_receive and RL_spi_rl_transmit_start\n  called conflicting methods read and write of module instance\n  spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                               & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start)) 
                              & (((7U == (IData)(this->__PVT__spi_rg_data_counter)) 
                                  | (7U > (IData)(this->__PVT__spi_rg_data_counter))) 
                                 | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
                             & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                            & (8U > (IData)(this->__PVT__spi_rg_data_counter))) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 567, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_data_receive and RL_spi_rl_transmit_start\n  called conflicting methods read and write of module instance\n  spi_rg_bit_count.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                             & (IData)(this->__PVT__spi_tx_data_en)) 
                            & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                           & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 567, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_data_receive and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__WILL_FIRE_RL_spi_rl_receive_done) {
            this->__PVT__v___05Fh9152 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(this->__PVT__WILL_FIRE_RL_spi_rl_receive_done)) {
            VL_WRITEF("%10# SPI : RECEIVE_DONE going to idle\n",
                      32,this->__PVT__v___05Fh9152);
        }
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) {
            this->__PVT__v___05Fh10919 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)) {
            VL_WRITEF("%10# SPI : rx_fifo to dr reg %x\n",
                      32,this->__PVT__v___05Fh10919,
                      160,this->__PVT___theResult___05F___05Fh10026);
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit)) 
                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_data_transmit called conflicting methods read and write of module\n  instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start)) 
                         & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_transmit_start called conflicting methods read and write of module\n  instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__spi_tx_data_en)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_spi_cfg_dr1.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__spi_tx_data_en)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_spi_cfg_dr2.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__spi_tx_data_en)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_spi_cfg_dr3.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__spi_tx_data_en)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_spi_cfg_dr4.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__spi_tx_data_en)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_spi_cfg_dr5.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                          & (IData)(this->__PVT__spi_tx_data_en)) 
                         & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 663, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_receive_fifo_to_read_datareg and\n  RL_spi_rl_transmit_data_to_fifo called conflicting methods read and write of\n  module instance spi_rg_data_counter.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
             & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315))) {
            this->__PVT__v___05Fh9738 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                         & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)))) {
            VL_WRITEF("%10# SPI: ABORT tx %x\n",32,
                      this->__PVT__v___05Fh9738,1,(IData)(this->__PVT__v___05Fh5419));
        }
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
               & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
              & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
             & (IData)(this->__PVT__spi_wr_clk_en_wget))) {
            this->__PVT__v___05Fh9992 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                           & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
                          & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                         & (IData)(this->__PVT__spi_wr_clk_en_wget)))) {
            VL_WRITEF("%10# SPI: ABORT rg_data_rx %x\n",
                      32,this->__PVT__v___05Fh9992,
                      8,(IData)(this->__PVT__v___05Fh8399));
        }
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle) {
            this->__PVT__v___05Fh7823 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) {
            VL_WRITEF("%10# SPI : Receive has started\n",
                      32,this->__PVT__v___05Fh7823);
        }
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) {
            this->__PVT__v___05Fh4119 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg)) {
            VL_WRITEF("%10# SPI : Write request wr_addr %x wr_data %x \n",
                      32,this->__PVT__v___05Fh4119,
                      32,((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                           << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                        >> 3U)),32,
                      this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]);
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
              & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U)) 
             & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) {
            this->__PVT__v___05Fh5228 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                          & (this->__PVT__spi_rg_spi_cfg_cr1 
                             >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)))) {
            VL_WRITEF("%10# SPI : Transmit state has started rg_data_tx %x\n",
                      32,this->__PVT__v___05Fh5228,
                      8,(IData)(this->__PVT__spi_tx_fifo_D_OUT));
        }
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
               & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
              & (IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156)) 
             & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                   >> 0x10U)))) {
            this->__PVT__v___05Fh5330 = (IData)(VL_TIME_UNITED_Q(1));
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                           & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                          & (IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156)) 
                         & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                               >> 0x10U))))) {
            VL_WRITEF("%10# SPI : Transmit state is in idle\n",
                      32,this->__PVT__v___05Fh5330);
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                           & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg)) 
                          & ((IData)(this->__PVT_____05Fduses58) 
                             | (((IData)(this->__PVT_____05Fduses58) 
                                 | (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                | ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                      >> 6U) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                    | ((IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156) 
                                       & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                             >> 0x10U)))) 
                                   & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))))) 
                         & (0U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                             << 0x1dU) 
                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U))))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 385, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_transmit_idle and RL_spi_rl_write_to_cfg\n  called conflicting methods read and write of module instance\n  spi_rg_spi_cfg_cr1.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                           & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg)) 
                          & (((((~ (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                               & (this->__PVT__spi_rg_spi_cfg_cr1 
                                  >> 6U)) & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                             | ((0U == (IData)(this->__PVT__spi_rg_transmit_state)) 
                                & (~ (IData)(this->__PVT__spi_tx_data_en))))) 
                         & (4U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                             << 0x1dU) 
                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U))))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 385, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_transmit_idle and RL_spi_rl_write_to_cfg\n  called conflicting methods read and write of module instance\n  spi_rg_spi_cfg_cr2.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                            & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg)) 
                           & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
                          & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                >> 0xfU))) & (0x1cU 
                                              == (0xffU 
                                                  & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                      << 0x1dU) 
                                                     | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                        >> 3U))))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 385, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_transmit_idle and RL_spi_rl_write_to_cfg\n  called conflicting methods read and write of module instance spi_tx_data_en.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                            & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg)) 
                           & (this->__PVT__spi_rg_spi_cfg_cr1 
                              >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                         & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                << 0x1dU) 
                                               | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                  >> 3U))))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 385, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_transmit_idle and RL_spi_rl_write_to_cfg\n  called conflicting methods first and clear of module instance spi_tx_fifo.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                           & (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive)) 
                          & (((((~ (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                               & (this->__PVT__spi_rg_spi_cfg_cr1 
                                  >> 6U)) & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                             | ((0U == (IData)(this->__PVT__spi_rg_transmit_state)) 
                                & (~ (IData)(this->__PVT__spi_tx_data_en))))) 
                         & (((~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                            & (IData)(this->__PVT__spi_wr_transfer_en_whas))))) {
            VL_WRITEF("Error: \"devices/spi//spi.bsv\", line 385, column 8: (R0002)\n  Conflict-free rules RL_spi_rl_transmit_idle and RL_spi_rl_data_receive\n  called conflicting methods read and write of module instance\n  spi_rg_spi_cfg_cr2.\n\n");
        }
    }
}

VL_INLINE_OPT void VmkSoc_mkspi::_combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__11(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__11\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = (((IData)(vlTOPp->spi0_io_miso_dat) << 6U) 
               | (0x3fU & ((IData)(this->__PVT__spi_rg_data_rx) 
                           >> 1U)));
        this->__PVT__v___05Fh8399 = (((IData)(vlTOPp->spi0_io_miso_dat) 
                                      << 7U) | (0x7fU 
                                                & (IData)(this->__PVT__spi_rg_data_rx)));
    } else {
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = ((0xfcU & ((IData)(this->__PVT__spi_rg_data_rx) 
                         << 1U)) | ((IData)(vlTOPp->spi0_io_miso_dat) 
                                    << 1U));
        this->__PVT__v___05Fh8399 = ((0xfeU & (IData)(this->__PVT__spi_rg_data_rx)) 
                                     | (IData)(vlTOPp->spi0_io_miso_dat));
    }
}

VL_INLINE_OPT void VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__13(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__13\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_spi_f_rd_addr__DOT__data0_reg;
}

VL_INLINE_OPT void VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__15(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__15\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
             & ((IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle) 
                == (IData)(this->__PVT__ff_wr_req__DOT__sDeqToggle)))) {
            this->__PVT__ff_wr_req__DOT__sEnqToggle 
                = (1U & (~ (IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle)));
            this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U] 
                = (IData)((((QData)((IData)((3U & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg)))) 
                            << 0x20U) | (QData)((IData)(
                                                        (this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg 
                                                         >> 4U)))));
            this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                = ((0xfffffff8U & ((IData)((this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg 
                                            >> 5U)) 
                                   << 3U)) | (IData)(
                                                     ((((QData)((IData)(
                                                                        (3U 
                                                                         & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg)))) 
                                                        << 0x20U) 
                                                       | (QData)((IData)(
                                                                         (this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg 
                                                                          >> 4U)))) 
                                                      >> 0x20U)));
            this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                = (7U & ((IData)((this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg 
                                  >> 5U)) >> 0x1dU));
        }
    } else {
        this->__PVT__ff_wr_req__DOT__sEnqToggle = 0U;
        this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U] = 0U;
        this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] = 0U;
        this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] = 0U;
    }
    this->__PVT__spi_rg_spi_cfg_cr1_D_IN = (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                               & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                                              & (IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156)) 
                                             & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                                   >> 0x10U)))
                                             ? (0xffffffbfU 
                                                & this->__PVT__spi_rg_spi_cfg_cr1)
                                             : this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]);
    this->__PVT__ff_wr_req__DOT__sDeqToggle = (1U & 
                                               ((~ (IData)(vlTOPp->RST_N)) 
                                                | (IData)(this->__PVT__ff_wr_req__DOT__sSyncReg1)));
    this->__PVT__CAN_FIRE_RL_rl_write_request_from_core 
        = (((((IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle) 
              == (IData)(this->__PVT__ff_wr_req__DOT__sDeqToggle)) 
             & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg)) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg)) 
           & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg));
    this->__PVT__ff_wr_req__DOT__sSyncReg1 = ((IData)(vlTOPp->RST_N) 
                                              & (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle));
    this->__PVT__ff_wr_req__DOT__dDeqToggle = this->__Vdly__ff_wr_req__DOT__dDeqToggle;
    this->__PVT__ff_wr_req_dEMPTY_N = ((IData)(this->__PVT__ff_wr_req__DOT__dEnqToggle) 
                                       != (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle));
    this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg = 
        (((IData)(this->__PVT__ff_wr_req_dEMPTY_N) 
          & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) 
         & (~ (IData)(this->__PVT__spi_tx_data_en)));
    this->__PVT__spi_tx_data_en_EN = ((((IData)(this->__PVT__spi_tx_data_en) 
                                        & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
                                       & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                                      | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                         & (0x1cU == 
                                            (0xffU 
                                             & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                 << 0x1dU) 
                                                | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                   >> 3U))))));
    this->__PVT__spi_tx_fifo_CLR = (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                     & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)) 
                                    | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                       & (0x1cU == 
                                          (0xffU & 
                                           ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                             << 0x1dU) 
                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U))))));
    this->__PVT__spi_rg_spi_cfg_dr1_D_IN = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                             & (0xcU 
                                                == 
                                                (0xffU 
                                                 & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                     << 0x1dU) 
                                                    | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                       >> 3U)))))
                                             ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                             : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                 ? 
                                                ((0x80U 
                                                  & this->__PVT__spi_rg_spi_cfg_cr1)
                                                  ? this->__PVT__x___05Fh3086
                                                  : 
                                                 (0xffffffU 
                                                  & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                     >> 8U)))
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? 
                                                 ((0x13U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter))
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   (0xffffffU 
                                                    & (this->__PVT___theResult___05F___05Fh10026[4U] 
                                                       >> 8U))
                                                    : 
                                                   ((this->__PVT___theResult___05F___05Fh10026[4U] 
                                                     << 8U) 
                                                    | (this->__PVT___theResult___05F___05Fh10026[3U] 
                                                       >> 0x18U)))
                                                   : 
                                                  this->__PVT___theResult___05F___05Fh10026[4U])
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   (0xffffffU 
                                                    & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                       >> 8U))
                                                    : this->__PVT__x___05Fh3086)
                                                   : 0U))));
    this->__PVT__spi_rg_spi_cfg_sr_D_IN = ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                            ? (0x80U 
                                               | (0xffffff7fU 
                                                  & this->__PVT__spi_rg_spi_cfg_sr))
                                            : (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (8U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)
                                                    ? 
                                                   (1U 
                                                    | (0xfffffffeU 
                                                       & this->__PVT__spi_rg_spi_cfg_sr))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4)
                                                     ? this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4
                                                     : 
                                                    (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                                      & (0x13U 
                                                         <= (IData)(this->__PVT__spi_rg_data_counter)))
                                                      ? 
                                                     (1U 
                                                      | (0xfffffffeU 
                                                         & this->__PVT__spi_rg_spi_cfg_sr))
                                                      : 
                                                     ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6)
                                                       ? this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4
                                                       : 0U))))));
    this->__PVT__spi_rg_data_counter_EN = (((((((((
                                                   (((IData)(this->__PVT__spi_tx_data_en) 
                                                     & (0x14U 
                                                        > (IData)(this->__PVT__spi_rg_data_counter))) 
                                                    | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                                        & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                           >> 6U)) 
                                                       & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                                                   | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                        & (0U 
                                                           != (IData)(this->__PVT__spi_rg_receive_state))) 
                                                       & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                      & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                                  | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                     & (0x1cU 
                                                        == 
                                                        (0xffU 
                                                         & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                             << 0x1dU) 
                                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                               >> 3U)))))) 
                                                 | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                                                    & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
                                                | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214))) 
                                               | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                                                   & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                                  & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                 & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280))) 
                                             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                           | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
           & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                  << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U)))));
    this->__PVT__spi_rg_spi_cfg_dr5_D_IN = ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1)
                                             ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                             : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                 ? 
                                                ((0x80U 
                                                  & this->__PVT__spi_rg_spi_cfg_cr1)
                                                  ? 
                                                 (0xffffff00U 
                                                  & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                     << 8U))
                                                  : this->__PVT__x___05Fh2984)
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? 
                                                 ((0x13U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter))
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   ((this->__PVT___theResult___05F___05Fh10026[1U] 
                                                     << 0x18U) 
                                                    | (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                       >> 8U))
                                                    : 
                                                   (0xffffff00U 
                                                    & (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                       << 8U)))
                                                   : 
                                                  this->__PVT___theResult___05F___05Fh10026[0U])
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? this->__PVT__x___05Fh2984
                                                    : 
                                                   (0xffffff00U 
                                                    & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                       << 8U)))
                                                   : 0U))));
    this->__PVT__spi_rg_bit_count_D_IN = (((((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                           | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4))
                                           ? 0U : (0xffU 
                                                   & ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                        & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                                                       & ((7U 
                                                           > (IData)(this->__PVT__spi_rg_data_counter)) 
                                                          | (7U 
                                                             == (IData)(this->__PVT__spi_rg_data_counter))))
                                                       ? 
                                                      ((IData)(1U) 
                                                       + (IData)(this->__PVT__spi_rg_bit_count))
                                                       : 
                                                      ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                        ? 
                                                       ((IData)(1U) 
                                                        + (IData)(this->__PVT__spi_rg_bit_count))
                                                        : 
                                                       ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7)
                                                         ? 
                                                        ((IData)(1U) 
                                                         + (IData)(this->__PVT__spi_rg_bit_count))
                                                         : 
                                                        ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                          ? 
                                                         ((IData)(1U) 
                                                          + (IData)(this->__PVT__spi_rg_bit_count))
                                                          : 0U))))));
    this->__PVT__spi_rg_data_counter_D_IN = ((((((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1) 
                                                 | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1)) 
                                                | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                               | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                              | (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5))
                                              ? 0U : 
                                             (0xffU 
                                              & ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6)
                                                  : 
                                                 (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280))
                                                   ? 
                                                  ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)
                                                    ? 
                                                   ((7U 
                                                     > (IData)(this->__PVT__spi_rg_data_counter))
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__spi_rg_data_counter))
                                                     : 0U)
                                                    : 0U)
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                    ? 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rg_data_counter))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9)
                                                     ? 
                                                    ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217)
                                                      ? 
                                                     ((IData)(1U) 
                                                      + (IData)(this->__PVT__spi_rg_data_counter))
                                                      : 0U)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                      ? 
                                                     ((IData)(1U) 
                                                      + (IData)(this->__PVT__spi_rg_data_counter))
                                                      : 
                                                     ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                       ? (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6)
                                                       : 0U))))))));
}

VL_INLINE_OPT void VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__17(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__17\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_spi_f_wr_addr__DOT__data0_reg;
    this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg 
        = this->__Vdly__s_xactor_spi_f_wr_data__DOT__data0_reg;
}

VL_INLINE_OPT void VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__4(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__4\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdlyvset__spi_tx_fifo__DOT__arr__v0 = 0U;
    this->__Vdly__s_xactor_spi_f_rd_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_spi_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_spi_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg;
    this->__Vdly__spi_tx_fifo__DOT__not_ring_full = this->__PVT__spi_tx_fifo__DOT__not_ring_full;
    this->__Vdly__spi_tx_fifo__DOT__ring_empty = this->__PVT__spi_tx_fifo__DOT__ring_empty;
    this->__Vdly__spi_tx_fifo__DOT__tail = this->__PVT__spi_tx_fifo__DOT__tail;
    this->__Vdly__spi_tx_fifo__DOT__head = this->__PVT__spi_tx_fifo__DOT__head;
    this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg;
    this->__Vdly__spi_tx_fifo__DOT__hasodata = this->__PVT__spi_tx_fifo__DOT__hasodata;
    this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg 
        = this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg;
    this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg;
    this->__Vdlyvset__spi_rx_fifo__DOT__arr__v0 = 0U;
    this->__Vdly__spi_rx_fifo__DOT__ring_empty = this->__PVT__spi_rx_fifo__DOT__ring_empty;
    this->__Vdly__spi_rx_fifo__DOT__tail = this->__PVT__spi_rx_fifo__DOT__tail;
    this->__Vdly__spi_rx_fifo__DOT__head = this->__PVT__spi_rx_fifo__DOT__head;
    this->__Vdly__spi_rx_fifo__DOT__not_ring_full = this->__PVT__spi_rx_fifo__DOT__not_ring_full;
    this->__Vdly__spi_rx_fifo__DOT__hasodata = this->__PVT__spi_rx_fifo__DOT__hasodata;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                         & (IData)(this->__PVT__spi_tx_fifo_DEQ)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi1.spi_tx_fifo.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__not_ring_full)) 
                          & (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)) 
                         & (~ (IData)(this->__PVT__spi_tx_fifo_DEQ))))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi1.spi_tx_fifo.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.spi_cluster.spi1.s_xactor_spi_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi1.spi_rx_fifo.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY((((~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
                          & (IData)(this->__PVT__spi_rx_fifo_ENQ)) 
                         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.spi_cluster.spi1.spi_rx_fifo.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_from_controller) 
                         & ((IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle) 
                            != (IData)(this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi1.ff_sync_rd_resp.error_checks1 -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
                         & ((IData)(this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle) 
                            == (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi1.ff_sync_rd_resp.error_checks2 -- Dequeuing from an empty full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                         & ((IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle) 
                            != (IData)(this->__PVT__ff_rd_req__DOT__sDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi1.ff_rd_req.error_checks1 -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__ff_rd_req_dEMPTY_N) 
                         & ((IData)(this->__PVT__ff_rd_req__DOT__dEnqToggle) 
                            == (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi1.ff_rd_req.error_checks2 -- Dequeuing from an empty full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                         & ((IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle) 
                            != (IData)(this->__PVT__ff_wr_req__DOT__sDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi1.ff_wr_req.error_checks1 -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__ff_wr_req_dEMPTY_N) 
                         & ((IData)(this->__PVT__ff_wr_req__DOT__dEnqToggle) 
                            == (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle))))) {
            VL_WRITEF("Warning: SyncFIFO1: %NmkSoc.spi_cluster.spi1.ff_wr_req.error_checks2 -- Dequeuing from an empty full fifo\n",
                      vlSymsp->name());
        }
    }
    if ((((~ (IData)(this->__PVT__spi_tx_fifo_CLR)) 
          & (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)) 
         & (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
             & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__spi_tx_fifo_DEQ)) 
                & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
               & (IData)(this->__PVT__spi_tx_fifo__DOT__not_ring_full))))) {
        this->spi_tx_fifo__DOT____Vlvbound2 = this->__PVT__v___05Fh2759;
        if ((0x12U >= (IData)(this->__PVT__spi_tx_fifo__DOT__tail))) {
            this->__Vdlyvval__spi_tx_fifo__DOT__arr__v0 
                = this->spi_tx_fifo__DOT____Vlvbound2;
            this->__Vdlyvset__spi_tx_fifo__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__spi_tx_fifo__DOT__arr__v0 
                = this->__PVT__spi_tx_fifo__DOT__tail;
        }
    }
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__data0_reg 
        = (((- (IData)(((IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ) 
                        & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg))))) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg)) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                   & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg))
            ? 0U : (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_spi_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)) 
                                          & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_spi_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                          & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_spi_f_wr_data__DOT__data1_reg);
    this->__Vdly__s_xactor_spi_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                          & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)))) {
            this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)))) {
                this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                 << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                            << 3U) 
                                           | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
            this->__Vdly__spi_tx_fifo__DOT__head = 0U;
            this->__Vdly__spi_tx_fifo__DOT__tail = 0U;
            this->__Vdly__spi_tx_fifo__DOT__ring_empty = 1U;
            this->__Vdly__spi_tx_fifo__DOT__not_ring_full = 1U;
            this->__Vdly__spi_tx_fifo__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                    << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                               << 3U) 
                                              | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                  << 2U) 
                                                 | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                     << 1U) 
                                                    | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                this->__Vdly__spi_tx_fifo__DOT__tail 
                    = this->__PVT__spi_tx_fifo__DOT__next_tail;
                this->__Vdly__spi_tx_fifo__DOT__head 
                    = this->__PVT__spi_tx_fifo__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                      << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                 << 3U) 
                                                | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                    << 2U) 
                                                   | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                       << 1U) 
                                                      | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                    this->__Vdly__spi_tx_fifo__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                          << 4U) | 
                                         (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                           << 3U) | 
                                          (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                        this->__Vdly__spi_tx_fifo__DOT__head 
                            = this->__PVT__spi_tx_fifo__DOT__next_head;
                        this->__Vdly__spi_tx_fifo__DOT__not_ring_full = 1U;
                        this->__Vdly__spi_tx_fifo__DOT__ring_empty 
                            = ((IData)(this->__PVT__spi_tx_fifo__DOT__next_head) 
                               == (IData)(this->__PVT__spi_tx_fifo__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                              << 4U) 
                                             | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                 << 3U) 
                                                | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                    << 2U) 
                                                   | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                       << 1U) 
                                                      | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                            this->__Vdly__spi_tx_fifo__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                                  << 4U) 
                                                 | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                     << 3U) 
                                                    | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                        << 2U) 
                                                       | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                           << 1U) 
                                                          | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                                if (this->__PVT__spi_tx_fifo__DOT__not_ring_full) {
                                    this->__Vdly__spi_tx_fifo__DOT__tail 
                                        = this->__PVT__spi_tx_fifo__DOT__next_tail;
                                    this->__Vdly__spi_tx_fifo__DOT__ring_empty = 0U;
                                    this->__Vdly__spi_tx_fifo__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__spi_tx_fifo__DOT__next_tail) 
                                           != (IData)(this->__PVT__spi_tx_fifo__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__spi_tx_fifo__DOT__head = 0U;
        this->__Vdly__spi_tx_fifo__DOT__tail = 0U;
        this->__Vdly__spi_tx_fifo__DOT__ring_empty = 1U;
        this->__Vdly__spi_tx_fifo__DOT__not_ring_full = 1U;
        this->__Vdly__spi_tx_fifo__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
                this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg = 0U;
    }
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                            << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                       << 2U) | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
        this->__PVT__spi_rx_fifo_D_OUT = ((0x12U >= (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                           ? this->__PVT__spi_rx_fifo__DOT__arr
                                          [this->__PVT__spi_rx_fifo__DOT__head]
                                           : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
            this->__PVT__spi_rx_fifo_D_OUT = this->__PVT__v___05Fh8399;
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                  << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                this->__PVT__spi_rx_fifo_D_OUT = ((0x12U 
                                                   >= (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                                   ? 
                                                  this->__PVT__spi_rx_fifo__DOT__arr
                                                  [this->__PVT__spi_rx_fifo__DOT__head]
                                                   : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                      << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                    this->__PVT__spi_rx_fifo_D_OUT 
                        = this->__PVT__v___05Fh8399;
                }
            }
        }
    }
    if (((IData)(this->__PVT__spi_rx_fifo_ENQ) & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                                   & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))) 
                                                  | (((~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)) 
                                                      & (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                                                     & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))))) {
        this->spi_rx_fifo__DOT____Vlvbound2 = this->__PVT__v___05Fh8399;
        if ((0x12U >= (IData)(this->__PVT__spi_rx_fifo__DOT__tail))) {
            this->__Vdlyvval__spi_rx_fifo__DOT__arr__v0 
                = this->spi_rx_fifo__DOT____Vlvbound2;
            this->__Vdlyvset__spi_rx_fifo__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__spi_rx_fifo__DOT__arr__v0 
                = this->__PVT__spi_rx_fifo__DOT__tail;
        }
    }
    this->__PVT__s_xactor_spi_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
                                  & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
                                     & (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)))))) 
             & (QData)((IData)(this->__PVT__ff_sync_rd_resp__DOT__syncFIFO1Data))) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_spi_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)) 
                                           & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_spi_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_spi_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
            & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg))
            ? (QData)((IData)(this->__PVT__ff_sync_rd_resp__DOT__syncFIFO1Data))
            : this->__PVT__s_xactor_spi_f_rd_data__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                 << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
            this->__Vdly__spi_rx_fifo__DOT__head = 0U;
            this->__Vdly__spi_rx_fifo__DOT__tail = 0U;
            this->__Vdly__spi_rx_fifo__DOT__ring_empty = 1U;
            this->__Vdly__spi_rx_fifo__DOT__not_ring_full = 1U;
            this->__Vdly__spi_rx_fifo__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                    << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                this->__Vdly__spi_rx_fifo__DOT__tail 
                    = this->__PVT__spi_rx_fifo__DOT__next_tail;
                this->__Vdly__spi_rx_fifo__DOT__head 
                    = this->__PVT__spi_rx_fifo__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                      << 3U) | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                    this->__Vdly__spi_rx_fifo__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                        this->__Vdly__spi_rx_fifo__DOT__head 
                            = this->__PVT__spi_rx_fifo__DOT__next_head;
                        this->__Vdly__spi_rx_fifo__DOT__not_ring_full = 1U;
                        this->__Vdly__spi_rx_fifo__DOT__ring_empty 
                            = ((IData)(this->__PVT__spi_rx_fifo__DOT__next_head) 
                               == (IData)(this->__PVT__spi_rx_fifo__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                            this->__Vdly__spi_rx_fifo__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__spi_rx_fifo_ENQ) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__spi_rx_fifo__DOT__ring_empty))))))) {
                                if (this->__PVT__spi_rx_fifo__DOT__not_ring_full) {
                                    this->__Vdly__spi_rx_fifo__DOT__tail 
                                        = this->__PVT__spi_rx_fifo__DOT__next_tail;
                                    this->__Vdly__spi_rx_fifo__DOT__ring_empty = 0U;
                                    this->__Vdly__spi_rx_fifo__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__spi_rx_fifo__DOT__next_tail) 
                                           != (IData)(this->__PVT__spi_rx_fifo__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__spi_rx_fifo__DOT__head = 0U;
        this->__Vdly__spi_rx_fifo__DOT__tail = 0U;
        this->__Vdly__spi_rx_fifo__DOT__ring_empty = 1U;
        this->__Vdly__spi_rx_fifo__DOT__not_ring_full = 1U;
        this->__Vdly__spi_rx_fifo__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
             & (0x28U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U)))))) {
            this->__PVT__spi_rg_spi_cfg_txcrcr = this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U];
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_txcrcr = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
             & (0x24U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U)))))) {
            this->__PVT__spi_rg_spi_cfg_rxcrcr = this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U];
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_rxcrcr = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
             & (0x20U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U)))))) {
            this->__PVT__spi_rg_spi_cfg_crcpr = this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U];
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_crcpr = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
               & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
              & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
             | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                 & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                & (7U > (IData)(this->__PVT__spi_rg_data_counter))))) {
            this->__PVT__spi_rg_data_rx = this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1;
        }
    } else {
        this->__PVT__spi_rg_data_rx = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                     & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                    & ((7U > (IData)(this->__PVT__spi_rg_data_counter)) 
                       | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
                   | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                      & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315))) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                     & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                            << 0x1dU) 
                                           | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                              >> 3U)))))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                    & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                   & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                  & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle))) {
            this->__PVT__spi_rg_bit_count = this->__PVT__spi_rg_bit_count_D_IN;
        }
    } else {
        this->__PVT__spi_rg_bit_count = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr5 = this->__PVT__spi_rg_spi_cfg_dr5_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr5 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0xcU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                        << 0x1dU) | 
                                       (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                        >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr1 = this->__PVT__spi_rg_spi_cfg_dr1_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                 & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                  & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                 & (this->__PVT__spi_rg_spi_cfg_cr2 
                    >> 0x10U))) | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                   & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319)))) {
            this->__PVT__spi_rg_tx_rx_start = (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319))
                                                ? (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)
                                                : (
                                                   (~ (IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3)) 
                                                   & (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                                       & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                                                      & (this->__PVT__spi_rg_spi_cfg_cr2 
                                                         >> 0x10U))));
        }
    } else {
        this->__PVT__spi_rg_tx_rx_start = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                & (this->__PVT__spi_rg_spi_cfg_cr1 
                   >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                 & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214)))) {
            this->__PVT__spi_rg_data_tx = this->__PVT__spi_rg_data_tx_D_IN;
        }
    } else {
        this->__PVT__spi_rg_data_tx = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)) 
               | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                    & (this->__PVT__spi_rg_spi_cfg_cr1 
                       >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                  & (~ this->__PVT__spi_rg_spi_cfg_cr1))) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                 & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211)))) {
            this->__PVT__spi_wr_spi_out_io1 = (1U & 
                                               ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4)
                                                 ? (IData)(this->__PVT__v___05Fh5419)
                                                 : 
                                                (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                                    & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                       >> 6U)) 
                                                   & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                                  & (~ this->__PVT__spi_rg_spi_cfg_cr1))
                                                  ? 
                                                 ((0x80U 
                                                   & this->__PVT__spi_rg_spi_cfg_cr1)
                                                   ? (IData)(this->__PVT__spi_tx_fifo_D_OUT)
                                                   : 
                                                  ((IData)(this->__PVT__spi_tx_fifo_D_OUT) 
                                                   >> 7U))
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                   ? (IData)(this->__PVT__v___05Fh5419)
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7) 
                                                   & (IData)(this->__PVT__v___05Fh5419))))));
        }
    } else {
        this->__PVT__spi_wr_spi_out_io1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                 & (this->__PVT__spi_rg_spi_cfg_cr1 
                    >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                  & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                 & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182)))) {
            this->__PVT__spi_rg_transmit_state = (((IData)(this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6) 
                                                   | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4))
                                                   ? 0U
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                                    ? 1U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                     ? 2U
                                                     : 0U)));
        }
    } else {
        this->__PVT__spi_rg_transmit_state = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
               & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U)) 
              & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
             | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                  & ((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
                     | (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                 & (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209)) 
                & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))))) {
            this->__PVT__spi_rg_transfer_done = (1U 
                                                 & (~ (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)));
        }
    } else {
        this->__PVT__spi_rg_transfer_done = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                   & (this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 6U)) & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                    & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233))) 
                | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                   & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315))) 
               | ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                  & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter)))) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                 & (8U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                     << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                  >> 3U)))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish))) {
            this->__PVT__spi_rg_spi_cfg_sr = this->__PVT__spi_rg_spi_cfg_sr_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_sr = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x14U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr3 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (0x14U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                    ? 
                                                   ((0x80U 
                                                     & this->__PVT__spi_rg_spi_cfg_cr1)
                                                     ? this->__PVT__x___05Fh3146
                                                     : this->__PVT__x___05Fh2934)
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                     ? 
                                                    ((0x13U 
                                                      > (IData)(this->__PVT__spi_rg_data_counter))
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? 
                                                      ((this->__PVT___theResult___05F___05Fh10026[3U] 
                                                        << 0x18U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[2U] 
                                                          >> 8U))
                                                       : 
                                                      ((this->__PVT___theResult___05F___05Fh10026[2U] 
                                                        << 8U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[1U] 
                                                          >> 0x18U)))
                                                      : 
                                                     this->__PVT___theResult___05F___05Fh10026[2U])
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? this->__PVT__x___05Fh2934
                                                       : this->__PVT__x___05Fh3146)
                                                      : 0U))));
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr3 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x18U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr4 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (0x18U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                    ? 
                                                   ((0x80U 
                                                     & this->__PVT__spi_rg_spi_cfg_cr1)
                                                     ? this->__PVT__x___05Fh3171
                                                     : this->__PVT__x___05Fh2959)
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                     ? 
                                                    ((0x13U 
                                                      > (IData)(this->__PVT__spi_rg_data_counter))
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? 
                                                      ((this->__PVT___theResult___05F___05Fh10026[2U] 
                                                        << 0x18U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[1U] 
                                                          >> 8U))
                                                       : 
                                                      ((this->__PVT___theResult___05F___05Fh10026[1U] 
                                                        << 8U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                          >> 0x18U)))
                                                      : 
                                                     this->__PVT___theResult___05F___05Fh10026[1U])
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? this->__PVT__x___05Fh2959
                                                       : this->__PVT__x___05Fh3171)
                                                      : 0U))));
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr4 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__spi_tx_data_en) 
                & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                  & (0x10U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                         << 0x1dU) 
                                        | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                           >> 3U)))))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
                  & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
                 & (0U != (IData)(this->__PVT__spi_rg_data_counter)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) {
            this->__PVT__spi_rg_spi_cfg_dr2 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (0x10U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                    ? 
                                                   ((0x80U 
                                                     & this->__PVT__spi_rg_spi_cfg_cr1)
                                                     ? this->__PVT__x___05Fh3121
                                                     : this->__PVT__x___05Fh2909)
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                     ? 
                                                    ((0x13U 
                                                      > (IData)(this->__PVT__spi_rg_data_counter))
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? 
                                                      ((this->__PVT___theResult___05F___05Fh10026[4U] 
                                                        << 0x18U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[3U] 
                                                          >> 8U))
                                                       : 
                                                      ((this->__PVT___theResult___05F___05Fh10026[3U] 
                                                        << 8U) 
                                                       | (this->__PVT___theResult___05F___05Fh10026[2U] 
                                                          >> 0x18U)))
                                                      : 
                                                     this->__PVT___theResult___05F___05Fh10026[3U])
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                      ? 
                                                     ((0x80U 
                                                       & this->__PVT__spi_rg_spi_cfg_cr1)
                                                       ? this->__PVT__x___05Fh2909
                                                       : this->__PVT__x___05Fh3121)
                                                      : 0U))));
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_dr2 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__spi_rg_nss) | (IData)(this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130))) {
            this->__PVT__spi_rg_clk = this->__PVT__spi_rg_clk_D_IN;
        }
    } else {
        this->__PVT__spi_rg_clk = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__spi_rg_nss)))) {
            this->__PVT__spi_rg_clk_counter = this->__PVT__spi_rg_clk_counter_D_IN;
        }
    } else {
        this->__PVT__spi_rg_clk_counter = 0U;
    }
    this->__PVT__spi_tx_fifo__DOT__not_ring_full = this->__Vdly__spi_tx_fifo__DOT__not_ring_full;
    this->__PVT__spi_tx_fifo__DOT__tail = this->__Vdly__spi_tx_fifo__DOT__tail;
    if (this->__Vdlyvset__spi_rx_fifo__DOT__arr__v0) {
        this->__PVT__spi_rx_fifo__DOT__arr[this->__Vdlyvdim0__spi_rx_fifo__DOT__arr__v0] 
            = this->__Vdlyvval__spi_rx_fifo__DOT__arr__v0;
    }
    this->__PVT__spi_rx_fifo__DOT__ring_empty = this->__Vdly__spi_rx_fifo__DOT__ring_empty;
    this->__PVT__spi_rx_fifo__DOT__tail = this->__Vdly__spi_rx_fifo__DOT__tail;
    this->__PVT__spi_rx_fifo__DOT__head = this->__Vdly__spi_rx_fifo__DOT__head;
    this->__PVT__spi_rx_fifo__DOT__hasodata = this->__Vdly__spi_rx_fifo__DOT__hasodata;
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core)))) {
            this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg = 1U;
    }
    this->__PVT__spi_tx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__tail))));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
                this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core)))) {
            this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_rl_write_request_from_core) 
                 & (~ (IData)(this->__PVT__s_xactor_spi_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core) 
             & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_spi_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_rl_read_response_to_core)))) {
                this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg = 1U;
    }
    this->__PVT__spi_rx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__tail))));
    this->__PVT__spi_rx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__head))));
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                            << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                       << 3U) | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                  << 2U) 
                                                 | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                     << 1U) 
                                                    | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
        this->__PVT__spi_tx_fifo_D_OUT = ((0x12U >= (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                           ? this->__PVT__spi_tx_fifo__DOT__arr
                                          [this->__PVT__spi_tx_fifo__DOT__head]
                                           : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                           << 3U) | 
                                          (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
            this->__PVT__spi_tx_fifo_D_OUT = this->__PVT__v___05Fh2759;
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                  << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                             << 3U) 
                                            | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                << 2U) 
                                               | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                   << 1U) 
                                                  | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                this->__PVT__spi_tx_fifo_D_OUT = ((0x12U 
                                                   >= (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                                   ? 
                                                  this->__PVT__spi_tx_fifo__DOT__arr
                                                  [this->__PVT__spi_tx_fifo__DOT__head]
                                                   : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__spi_tx_fifo_CLR) 
                                      << 4U) | (((IData)(this->__PVT__spi_tx_fifo_DEQ) 
                                                 << 3U) 
                                                | (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11) 
                                                    << 2U) 
                                                   | (((IData)(this->__PVT__spi_tx_fifo__DOT__hasodata) 
                                                       << 1U) 
                                                      | (IData)(this->__PVT__spi_tx_fifo__DOT__ring_empty)))))))) {
                    this->__PVT__spi_tx_fifo_D_OUT 
                        = this->__PVT__v___05Fh2759;
                }
            }
        }
    }
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4 
        = (0xffffff7fU & this->__PVT__spi_rg_spi_cfg_sr);
    this->__PVT__x___05Fh2959 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 >> 8U)));
    this->__PVT__x___05Fh3146 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                             >> 0x18U)));
    this->__PVT__x___05Fh3171 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2984 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                 >> 8U)));
    this->__PVT__x___05Fh2909 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 >> 8U)));
    this->__PVT__x___05Fh2934 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 >> 8U)));
    this->__PVT__x___05Fh3121 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                             >> 0x18U)));
    this->__PVT__x___05Fh3086 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                             >> 0x18U)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__spi_tx_data_en_EN) {
            this->__PVT__spi_tx_data_en = (1U & (~ (IData)(this->__PVT__MUX_spi_tx_data_en_write_1___05FSEL_1)));
        }
    } else {
        this->__PVT__spi_tx_data_en = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__spi_rg_data_counter_EN) {
            this->__PVT__spi_rg_data_counter = this->__PVT__spi_rg_data_counter_D_IN;
        }
    } else {
        this->__PVT__spi_rg_data_counter = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                    & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                   & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                  & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                 | (IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1)) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                    & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                   & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                         >> 0x10U)))) | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control)) 
              | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop))) {
            this->__PVT__spi_rg_nss = (1U & ((IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1)
                                              ? ((~ 
                                                  (this->__PVT__spi_rg_spi_cfg_cr1 
                                                   >> 6U)) 
                                                 | (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)))
                                              : ((~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                                 & ((IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3) 
                                                    | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control)
                                                        ? 
                                                       (this->__PVT__spi_rg_spi_cfg_cr1 
                                                        >> 8U)
                                                        : 
                                                       ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                                          & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233)) 
                                                         & (~ 
                                                            (this->__PVT__spi_rg_spi_cfg_cr2 
                                                             >> 0x10U))) 
                                                        | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop)))))));
        }
    } else {
        this->__PVT__spi_rg_nss = 1U;
    }
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg 
        = this->__Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg;
    this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg;
    this->__PVT__spi_tx_fifo__DOT__ring_empty = this->__Vdly__spi_tx_fifo__DOT__ring_empty;
    if (this->__Vdlyvset__spi_tx_fifo__DOT__arr__v0) {
        this->__PVT__spi_tx_fifo__DOT__arr[this->__Vdlyvdim0__spi_tx_fifo__DOT__arr__v0] 
            = this->__Vdlyvval__spi_tx_fifo__DOT__arr__v0;
    }
    this->__PVT__spi_tx_fifo__DOT__head = this->__Vdly__spi_tx_fifo__DOT__head;
    this->__PVT__s_xactor_spi_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_resp__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_spi_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_data__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg));
    this->__PVT__spi_tx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__head))));
    this->__PVT__MUX_spi_tx_data_en_write_1___05FSEL_1 
        = (((IData)(this->__PVT__spi_tx_data_en) & 
            (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6 
        = ((0x13U > (IData)(this->__PVT__spi_rg_data_counter))
            ? (0xffU & ((IData)(1U) + (IData)(this->__PVT__spi_rg_data_counter)))
            : 0U);
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11 
        = ((IData)(this->__PVT__spi_tx_data_en) & (0x14U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter)));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
              & (0U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                  << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U))))) 
             | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                  & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                 & (IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156)) 
                & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                      >> 0x10U))))) {
            this->__PVT__spi_rg_spi_cfg_cr1 = this->__PVT__spi_rg_spi_cfg_cr1_D_IN;
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_cr1 = 0U;
    }
    this->__PVT__spi_tx_fifo__DOT__hasodata = this->__Vdly__spi_tx_fifo__DOT__hasodata;
    this->__PVT__v___05Fh5419 = (1U & ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)
                                        ? (IData)(this->__PVT__spi_rg_data_tx)
                                        : ((IData)(this->__PVT__spi_rg_data_tx) 
                                           >> 7U)));
    this->__PVT__spi_rg_clk_D_IN = (1U & ((IData)(this->__PVT__spi_rg_nss)
                                           ? (this->__PVT__spi_rg_spi_cfg_cr1 
                                              >> 1U)
                                           : (~ (IData)(this->__PVT__spi_rg_clk))));
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT__v___05Fh2759 = (0xffU & this->__PVT__spi_rg_spi_cfg_dr5);
        this->__PVT___theResult___05F___05Fh10026[0U] 
            = this->__PVT__spi_rg_spi_cfg_dr5;
    } else {
        this->__PVT__v___05Fh2759 = (0xffU & (this->__PVT__spi_rg_spi_cfg_dr1 
                                              >> 0x18U));
        this->__PVT___theResult___05F___05Fh10026[0U] 
            = ((0xffffff00U & this->__PVT__spi_rg_spi_cfg_dr5) 
               | (IData)(this->__PVT__spi_rx_fifo_D_OUT));
    }
    this->__PVT___theResult___05F___05Fh10026[1U] = this->__PVT__spi_rg_spi_cfg_dr4;
    this->__PVT___theResult___05F___05Fh10026[2U] = this->__PVT__spi_rg_spi_cfg_dr3;
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                          << 0x18U) 
                                         | (0xffffffU 
                                            & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                           << 0x18U) 
                                          | (0xffffffU 
                                             & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    } else {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    }
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x10U) - (IData)(1U))));
    this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130 
        = ((7U & (this->__PVT__spi_rg_spi_cfg_cr1 >> 3U)) 
           == (IData)(this->__PVT__spi_rg_clk_counter));
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                 & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                   & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
                  & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                 & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                & (4U == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                    << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                 >> 3U))))))) {
            this->__PVT__spi_rg_spi_cfg_cr2 = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (4U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : 0U);
        }
    } else {
        this->__PVT__spi_rg_spi_cfg_cr2 = 0U;
    }
    this->__PVT__spi_rg_clk_counter_D_IN = ((IData)(this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130)
                                             ? 0U : 
                                            (7U & ((IData)(1U) 
                                                   + (IData)(this->__PVT__spi_rg_clk_counter))));
    this->__PVT__spi_rx_fifo__DOT__not_ring_full = this->__Vdly__spi_rx_fifo__DOT__not_ring_full;
    this->__PVT__spi_wr_clk_en_wget = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                   >> 1U)) 
                                               & (~ (IData)(this->__PVT__spi_rg_clk))) 
                                              | ((this->__PVT__spi_rg_spi_cfg_cr1 
                                                  & (~ 
                                                     (this->__PVT__spi_rg_spi_cfg_cr1 
                                                      >> 1U))) 
                                                 & (IData)(this->__PVT__spi_rg_clk))) 
                                             | ((~ this->__PVT__spi_rg_spi_cfg_cr1) 
                                                & (((this->__PVT__spi_rg_spi_cfg_cr1 
                                                     >> 1U) 
                                                    & (IData)(this->__PVT__spi_rg_clk)) 
                                                   | ((~ 
                                                       (this->__PVT__spi_rg_spi_cfg_cr1 
                                                        >> 1U)) 
                                                      & (~ (IData)(this->__PVT__spi_rg_clk)))))));
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x18U) - (IData)(1U))));
    this->__PVT__spi_wr_transfer_en_whas = ((~ (IData)(this->__PVT__spi_rg_nss)) 
                                            & (IData)(this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315 
        = ((((~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU)) 
             & (0U != (IData)(this->__PVT__spi_rg_transmit_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
           & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)));
    this->__PVT_____05Fduses58 = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                    & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                          >> 0x10U))) 
                                   & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                  & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223 
        = ((((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
             & (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156 
        = (((~ (IData)(this->__PVT__spi_wr_clk_en_wget)) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             | (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233 
        = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (IData)(this->__PVT__spi_rg_transfer_done));
    this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182 
        = ((IData)(this->__PVT__spi_wr_clk_en_wget) 
           | (this->__PVT__spi_rg_spi_cfg_cr1 & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273 
        = ((((8U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209 
        = ((((7U == (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (~ (IData)(this->__PVT__spi_rg_transfer_done)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__spi_rg_receive_state_EN) {
            this->__PVT__spi_rg_receive_state = (((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5) 
                                                  | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_done))
                                                  ? 0U
                                                  : 
                                                 ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)
                                                   ? 1U
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                    ? 2U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3)
                                                     ? 3U
                                                     : 0U))));
        }
    } else {
        this->__PVT__spi_rg_receive_state = 0U;
    }
    this->__PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget 
        = ((IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233) 
           & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280 
        = (((IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273) 
            & ((7U > (IData)(this->__PVT__spi_rg_data_counter)) 
               | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
           | (((~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
               & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
              & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211 
        = (((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
              & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             | ((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | ((((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | ((IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209) 
                 & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop 
        = (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
              & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
             & ((~ (this->__PVT__spi_rg_spi_cfg_cr2 
                    >> 0xfU)) | (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                    >> 0x10U)))) & 
            (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319 
        = (((IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315) 
            & (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)) 
           | (((0U != (IData)(this->__PVT__spi_rg_receive_state)) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (IData)(this->__PVT__spi_wr_clk_en_wget)));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg 
        = ((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
           & (0U == (IData)(this->__PVT__spi_rg_receive_state)));
    this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx = ((IData)(this->__PVT__spi_wr_transfer_en_whas) 
                                                   & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                         >> 6U) 
                                                        & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
                                                       & (0U 
                                                          != (IData)(this->__PVT__spi_rg_transmit_state))) 
                                                      | ((((this->__PVT__spi_rg_spi_cfg_cr2 
                                                            >> 0xfU) 
                                                           | (this->__PVT__spi_rg_spi_cfg_cr2 
                                                              >> 0x10U)) 
                                                          & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                         & (0U 
                                                            != (IData)(this->__PVT__spi_rg_receive_state)))));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_done = 
        ((3U == (IData)(this->__PVT__spi_rg_receive_state)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
             & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4 
        = ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
           & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle = 
        (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & ((this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU) 
               | ((this->__PVT__spi_rg_spi_cfg_cr2 
                   >> 0x10U) & (IData)(this->__PVT__spi_rg_tx_rx_start)))) 
           & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_receive = 
        (((2U == (IData)(this->__PVT__spi_rg_receive_state)) 
          & (~ (IData)(this->__PVT__spi_rg_nss))) & 
         (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive 
        = (((1U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle 
        = ((((0U == (IData)(this->__PVT__spi_rg_transmit_state)) 
             & (~ (IData)(this->__PVT__spi_tx_data_en))) 
            & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start 
        = (((1U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit 
        = (((2U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
             & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rx_fifo_ENQ = (((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                       & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
                                      & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                     & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                    | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                                        & (7U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                                       & (7U == (IData)(this->__PVT__spi_rg_data_counter))));
    this->__PVT__spi_rg_receive_state_EN = ((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                  & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                                                 & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                                & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                                               | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                    & (0U 
                                                       != (IData)(this->__PVT__spi_rg_receive_state))) 
                                                   & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                                 & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_done)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control 
        = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                    >> 9U) & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive))) 
                  & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle))) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle))));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
           & (((this->__PVT__spi_rg_spi_cfg_cr1 >> 6U) 
               & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
              | ((IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156) 
                 & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                       >> 0x10U)))));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
            & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U)) 
           & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182));
    this->__PVT__spi_tx_fifo_DEQ = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                    & (IData)(this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223));
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214));
    this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish 
        = (((((((~ (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                & (0U == (IData)(this->__PVT__spi_rg_receive_state))) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start))) 
            & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx))) 
           & (~ (IData)(this->__PVT__spi_tx_data_en)));
    this->__PVT__spi_rg_data_tx_D_IN = ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                         ? (IData)(this->__PVT__spi_tx_fifo_D_OUT)
                                         : ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                             ? ((0x80U 
                                                 & this->__PVT__spi_rg_spi_cfg_cr1)
                                                 ? 
                                                (0x7fU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    >> 1U))
                                                 : 
                                                (0xfeU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    << 1U)))
                                             : ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9)
                                                 ? 
                                                ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217)
                                                  ? 
                                                 ((0x80U 
                                                   & this->__PVT__spi_rg_spi_cfg_cr1)
                                                   ? 
                                                  (0x7fU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      >> 1U))
                                                   : 
                                                  (0xfeU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      << 1U)))
                                                  : (IData)(this->__PVT__spi_tx_fifo_D_OUT))
                                                 : 0U)));
    this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
            & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0U != (IData)(this->__PVT__spi_rg_data_counter)));
}

VL_INLINE_OPT void VmkSoc_mkspi::_combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__12(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__12\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get) 
                << 6U) | (0x3fU & ((IData)(this->__PVT__spi_rg_data_rx) 
                                   >> 1U)));
        this->__PVT__v___05Fh8399 = (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get) 
                                      << 7U) | (0x7fU 
                                                & (IData)(this->__PVT__spi_rg_data_rx)));
    } else {
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = ((0xfcU & ((IData)(this->__PVT__spi_rg_data_rx) 
                         << 1U)) | ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get) 
                                    << 1U));
        this->__PVT__v___05Fh8399 = ((0xfeU & (IData)(this->__PVT__spi_rg_data_rx)) 
                                     | (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get));
    }
}
