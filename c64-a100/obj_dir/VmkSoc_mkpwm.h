// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VmkSoc.h for the primary calling header

#ifndef _VMKSOC_MKPWM_H_
#define _VMKSOC_MKPWM_H_  // guard

#include "verilated_heavy.h"

//==========

class VmkSoc__Syms;

//----------

VL_MODULE(VmkSoc_mkpwm) {
  public:
    
    // PORTS
    VL_IN8(CLK,0,0);
    VL_IN8(RST_N,0,0);
    VL_IN8(slave_m_awvalid_awvalid,0,0);
    VL_IN8(slave_m_awvalid_awsize,1,0);
    VL_IN8(slave_m_awvalid_awprot,2,0);
    VL_OUT8(slave_awready,0,0);
    VL_IN8(slave_m_wvalid_wvalid,0,0);
    VL_IN8(slave_m_wvalid_wstrb,3,0);
    VL_OUT8(slave_wready,0,0);
    VL_OUT8(slave_bvalid,0,0);
    VL_OUT8(slave_bresp,1,0);
    VL_IN8(slave_m_bready_bready,0,0);
    VL_IN8(slave_m_arvalid_arvalid,0,0);
    VL_IN8(slave_m_arvalid_arsize,1,0);
    VL_IN8(slave_m_arvalid_arprot,2,0);
    VL_OUT8(slave_arready,0,0);
    VL_OUT8(slave_rvalid,0,0);
    VL_OUT8(slave_rresp,1,0);
    VL_IN8(slave_m_rready_rready,0,0);
    VL_OUT8(io_pwm_o,0,0);
    VL_OUT8(sb_interrupt,0,0);
    VL_OUT8(__PVT__RDY_sb_interrupt,0,0);
    VL_IN(slave_m_awvalid_awaddr,31,0);
    VL_IN(slave_m_wvalid_wdata,31,0);
    VL_IN(slave_m_arvalid_araddr,31,0);
    VL_OUT(slave_rdata,31,0);
    
    // LOCAL SIGNALS
    // Anonymous structures to workaround compiler member-count bugs
    struct {
        CData/*0:0*/ __PVT__pwm_clock_divider_clock_selector_CLK_OUT;
        CData/*0:0*/ __PVT__pwm_overall_reset_RST_OUT;
        CData/*0:0*/ __PVT__pwm_control_reset__DOT__rst;
        CData/*0:0*/ __PVT__pwm_clock_divider_clk;
        CData/*0:0*/ __PVT__pwm_clock_divider_clk_D_IN;
        CData/*0:0*/ __PVT__pwm_clock_selector;
        CData/*0:0*/ __PVT__pwm_continous_once;
        CData/*0:0*/ __PVT__pwm_interrupt;
        CData/*0:0*/ __PVT__pwm_pwm_enable;
        CData/*0:0*/ __PVT__pwm_pwm_output;
        CData/*0:0*/ __PVT__pwm_pwm_output_enable;
        CData/*0:0*/ __PVT__pwm_pwm_start;
        CData/*0:0*/ __PVT__pwm_reset_counter;
        CData/*0:0*/ __PVT__pwm_clock_divisor_sync_sRDY;
        CData/*0:0*/ __PVT__pwm_sync_continous_once_dD_OUT;
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable_dD_OUT;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output_dD_OUT;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output_sRDY;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start_dD_OUT;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data_DEQ;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data_ENQ;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp_D_IN;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp_DEQ;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_read_request;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_write_request;
        CData/*6:0*/ __PVT__r1___05Fread___05Fh3256;
        CData/*0:0*/ __PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47;
        CData/*0:0*/ __PVT__pwm_clock_divider_clock_selector__DOT__sel_reg;
        CData/*0:0*/ __PVT__pwm_clock_divider_new_clock__DOT__current_clk;
        CData/*0:0*/ __PVT__pwm_clock_divider_new_clock__DOT__current_gate;
        CData/*0:0*/ __PVT__pwm_clock_divider_new_clock__DOT__new_gate;
        CData/*0:0*/ __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg2;
        CData/*0:0*/ __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dLastState;
        CData/*0:0*/ __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg;
        CData/*0:0*/ __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2;
        CData/*1:0*/ __PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold;
        CData/*2:0*/ __PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset;
        CData/*0:0*/ __PVT__pwm_sync_continous_once__DOT__sDataSyncIn;
        CData/*0:0*/ __PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_continous_once__DOT__sync__DOT__dLastState;
        CData/*0:0*/ __PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg;
        CData/*0:0*/ __PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dLastState;
        CData/*0:0*/ __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg;
        CData/*0:0*/ __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_period__DOT__sync__DOT__dLastState;
        CData/*0:0*/ __PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg;
        CData/*0:0*/ __PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable__DOT__sDataSyncIn;
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg2;
    };
    struct {
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dLastState;
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg;
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output__DOT__sDataSyncIn;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output__DOT__sync__DOT__dLastState;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start__DOT__sDataSyncIn;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg2;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start__DOT__sync__DOT__dLastState;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp__DOT__empty_reg;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp__DOT__data0_reg;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp__DOT__data1_reg;
        SData/*15:0*/ __PVT__pwm_clock_divider_rg_counter;
        SData/*15:0*/ __PVT__pwm_clock_divider_rg_counter_D_IN;
        SData/*15:0*/ __PVT__pwm_clock_divider_rg_divisor;
        SData/*15:0*/ __PVT__pwm_clock_divisor;
        SData/*15:0*/ __PVT__pwm_duty_cycle;
        SData/*15:0*/ __PVT__pwm_period;
        SData/*15:0*/ __PVT__pwm_rg_counter;
        SData/*15:0*/ __PVT__pwm_rg_counter_D_IN;
        SData/*15:0*/ __PVT__pwm_clock_divisor_sync_dD_OUT;
        SData/*15:0*/ __PVT__pwm_sync_duty_cycle_dD_OUT;
        SData/*15:0*/ __PVT__pwm_sync_period_dD_OUT;
        SData/*15:0*/ __PVT__pwm_clock_divisor_sync__DOT__sDataSyncIn;
        SData/*15:0*/ __PVT__pwm_sync_duty_cycle__DOT__sDataSyncIn;
        SData/*15:0*/ __PVT__pwm_sync_period__DOT__sDataSyncIn;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data_D_IN;
        QData/*36:0*/ __PVT__s_xactor_f_rd_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_f_rd_addr__DOT__data1_reg;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data__DOT__data0_reg;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data__DOT__data1_reg;
        QData/*36:0*/ __PVT__s_xactor_f_wr_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_f_wr_addr__DOT__data1_reg;
        QData/*35:0*/ __PVT__s_xactor_f_wr_data__DOT__data0_reg;
        QData/*35:0*/ __PVT__s_xactor_f_wr_data__DOT__data1_reg;
    };
    
    // LOCAL VARIABLES
    CData/*0:0*/ __Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    CData/*0:0*/ __Vdly__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg;
    CData/*0:0*/ __Vdly__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg;
    CData/*0:0*/ __Vdly__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg;
    CData/*0:0*/ __Vdly__pwm_sync_period__DOT__sync__DOT__sToggleReg;
    CData/*0:0*/ __Vdly__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg;
    CData/*0:0*/ __Vdly__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg;
    CData/*0:0*/ __Vdly__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg;
    QData/*36:0*/ __Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    QData/*35:0*/ __Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    
    // INTERNAL VARIABLES
  private:
    VmkSoc__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(VmkSoc_mkpwm);  ///< Copying not allowed
  public:
    VmkSoc_mkpwm(const char* name = "TOP");
    ~VmkSoc_mkpwm();
    
    // INTERNAL METHODS
    void __Vconfigure(VmkSoc__Syms* symsp, bool first);
    void _combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(VmkSoc__Syms* __restrict vlSymsp);
    void _combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(VmkSoc__Syms* __restrict vlSymsp);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
  public:
    void _initial__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__1(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__13(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__14(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__15(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__16(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__17(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__18(VmkSoc__Syms* __restrict vlSymsp);
    void _settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__31(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__32(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__33(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__34(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__35(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__36(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
