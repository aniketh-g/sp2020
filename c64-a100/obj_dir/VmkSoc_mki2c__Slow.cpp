// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc_mki2c.h"
#include "VmkSoc__Syms.h"

//==========

VL_CTOR_IMP(VmkSoc_mki2c) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VmkSoc_mki2c::__Vconfigure(VmkSoc__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

VmkSoc_mki2c::~VmkSoc_mki2c() {
}

void VmkSoc_mki2c::_initial__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__1(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mki2c::_initial__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__1\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg = 0x2aaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg = 0x2aaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg = 2U;
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg = 2U;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    this->__PVT__i2c_user_aas = 0U;
    this->__PVT__i2c_user_ack = 0U;
    this->__PVT__i2c_user_ad0_lrb = 0U;
    this->__PVT__i2c_user_bb = 0U;
    this->__PVT__i2c_user_ber = 0U;
    this->__PVT__i2c_user_cOutEn = 0U;
    this->__PVT__i2c_user_c_scl = 0xaaaaaaaaU;
    this->__PVT__i2c_user_coSCL = 0xaaaaaaaaU;
    this->__PVT__i2c_user_configchange = 0U;
    this->__PVT__i2c_user_cprescaler = 0xaaU;
    this->__PVT__i2c_user_cycwaste = 0x2aaU;
    this->__PVT__i2c_user_dOutEn = 0U;
    this->__PVT__i2c_user_dOutEn_delay_0 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_1 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_2 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_3 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_4 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_5 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_6 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_7 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_8 = 0U;
    this->__PVT__i2c_user_dOutEn_delay_9 = 0U;
    this->__PVT__i2c_user_dataBit = 0xaU;
    this->__PVT__i2c_user_eni = 0U;
    this->__PVT__i2c_user_eso = 0U;
    this->__PVT__i2c_user_i2ctime = 0xaaaaU;
    this->__PVT__i2c_user_i2ctimeout = 0x2aaaU;
    this->__PVT__i2c_user_lab = 0U;
    this->__PVT__i2c_user_last_byte_read = 0U;
    this->__PVT__i2c_user_mTransFSM = 0xaU;
    this->__PVT__i2c_user_mod_start = 0U;
    this->__PVT__i2c_user_operation = 0U;
    this->__PVT__i2c_user_pin = 0U;
    this->__PVT__i2c_user_reSCL = 0xaaaaaaaaU;
    this->__PVT__i2c_user_resetcount = 0x2aU;
    this->__PVT__i2c_user_rprescaler = 0xaaU;
    this->__PVT__i2c_user_rstsig = 0U;
    this->__PVT__i2c_user_s0 = 0xaaU;
    this->__PVT__i2c_user_s01 = 0xaaU;
    this->__PVT__i2c_user_s2 = 0xaaU;
    this->__PVT__i2c_user_s3 = 0xaaU;
    this->__PVT__i2c_user_scl_start = 0U;
    this->__PVT__i2c_user_sendInd = 2U;
    this->__PVT__i2c_user_st_toggle = 0U;
    this->__PVT__i2c_user_sta = 0U;
    this->__PVT__i2c_user_sto = 0U;
    this->__PVT__i2c_user_sts = 0U;
    this->__PVT__i2c_user_val_SCL = 0U;
    this->__PVT__i2c_user_val_SCL_in = 0U;
    this->__PVT__i2c_user_val_SDA = 0U;
    this->__PVT__i2c_user_val_SDA_in = 0U;
    this->__PVT__i2c_user_val_sda_delay_0 = 0U;
    this->__PVT__i2c_user_val_sda_delay_1 = 0U;
    this->__PVT__i2c_user_val_sda_delay_2 = 0U;
    this->__PVT__i2c_user_val_sda_delay_3 = 0U;
    this->__PVT__i2c_user_val_sda_delay_4 = 0U;
    this->__PVT__i2c_user_val_sda_delay_5 = 0U;
    this->__PVT__i2c_user_val_sda_delay_6 = 0U;
    this->__PVT__i2c_user_val_sda_delay_7 = 0U;
    this->__PVT__i2c_user_val_sda_delay_8 = 0U;
    this->__PVT__i2c_user_val_sda_delay_9 = 0U;
    this->__PVT__i2c_user_zero = 0U;
}

void VmkSoc_mki2c::_settle__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__7(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mki2c::_settle__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__7\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__i2c_user_cycwaste_D_IN = (0x3ffU & 
                                           ((0U == (IData)(this->__PVT__i2c_user_cycwaste))
                                             ? ((IData)(1U) 
                                                + (IData)(this->__PVT__i2c_user_cycwaste))
                                             : ((0x258U 
                                                 == (IData)(this->__PVT__i2c_user_cycwaste))
                                                 ? 0U
                                                 : 
                                                ((IData)(1U) 
                                                 + (IData)(this->__PVT__i2c_user_cycwaste)))));
    this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140 
        = ((IData)(this->__PVT__i2c_user_i2ctimeout) 
           <= (0x3fffU & (IData)(this->__PVT__i2c_user_i2ctime)));
    this->__PVT__s_xactor_f_wr_resp_D_IN = (((IData)(this->__PVT__i2c_user_ber) 
                                             | (((((((8U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U)))) 
                                                     & (0x20U 
                                                        != 
                                                        (0xffU 
                                                         & (IData)(
                                                                   (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                    >> 5U))))) 
                                                    & (0x10U 
                                                       != 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))) 
                                                   & (0U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U))))) 
                                                  & (0x28U 
                                                     != 
                                                     (0xffU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 & (0x38U 
                                                    != 
                                                    (0xffU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (0x30U 
                                                   != 
                                                   (0xffU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U))))))
                                             ? 2U : 0U);
    this->__PVT__x___05Fh10210 = (1U & ((IData)(this->__PVT__i2c_user_s0) 
                                        >> (7U & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                  - (IData)(2U)))));
    this->__PVT__x___05Fh6518 = (((IData)(this->__PVT__i2c_user_sta) 
                                  << 1U) | (IData)(this->__PVT__i2c_user_sto));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock 
        = ((IData)(this->__PVT__i2c_user_scl_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock 
        = ((IData)(this->__PVT__i2c_user_mod_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & (~ (IData)(this->__PVT__i2c_user_bb))) 
           | (((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343 
        = (((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U)))) & 
              (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323 
        = ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                    >> 5U)))) & (((
                                                   (6U 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                   | (0xcU 
                                                      == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                                                  | (2U 
                                                     != 
                                                     (3U 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & ((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               | (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
           & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale 
        = ((0U == (IData)(this->__PVT__i2c_user_cprescaler)) 
           & (0U != (IData)(this->__PVT__i2c_user_rprescaler)));
    this->__PVT__r1___05Fread___05Fh12450 = (((((IData)(this->__PVT__i2c_user_pin) 
                                                << 6U) 
                                               | (((IData)(this->__PVT__i2c_user_zero) 
                                                   << 5U) 
                                                  | ((IData)(this->__PVT__i2c_user_sts) 
                                                     << 4U))) 
                                              | (((IData)(this->__PVT__i2c_user_ber) 
                                                  << 3U) 
                                                 | ((IData)(this->__PVT__i2c_user_ad0_lrb) 
                                                    << 2U))) 
                                             | (((IData)(this->__PVT__i2c_user_aas) 
                                                 << 1U) 
                                                | (IData)(this->__PVT__i2c_user_lab)));
    this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U)))));
    this->__PVT__WILL_FIRE_RL_read_request = (((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                                              & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U))))) & 
           (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__i2c_user_i2ctime_D_IN = (0xffffU & 
                                          (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                            & (0x30U 
                                               == (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                            ? (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 4U))
                                            : (0x8000U 
                                               | (0x7fffU 
                                                  & (IData)(this->__PVT__i2c_user_i2ctime)))));
    this->__PVT__i2c_user_s0_D_IN = (0xffU & (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                               & (0x10U 
                                                  == 
                                                  (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                               ? (IData)(
                                                         (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 4U))
                                               : ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   | ((IData)(1U) 
                                                      << 
                                                      (0xfU 
                                                       & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U)))))
                                                   : 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   & (~ 
                                                      ((IData)(1U) 
                                                       << 
                                                       (0xfU 
                                                        & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U)))))))));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end 
        = ((((8U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_idler = ((1U 
                                                 == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178 
        = ((((1U != (IData)(this->__PVT__x___05Fh6518)) 
             & (0xaU != (IData)(this->__PVT__i2c_user_s3))) 
            & (IData)(this->__PVT__i2c_user_operation)) 
           & (~ (IData)(this->__PVT__i2c_user_ack)));
    this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165 
        = (((IData)(this->__PVT__i2c_user_pin) & (2U 
                                                  != (IData)(this->__PVT__x___05Fh6518))) 
           & (((1U == (IData)(this->__PVT__x___05Fh6518)) 
               | (0xaU == (IData)(this->__PVT__i2c_user_s3))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155 
        = (((IData)(this->__PVT__i2c_user_pin) & ((
                                                   (((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518)) 
                                                     | (1U 
                                                        == (IData)(this->__PVT__x___05Fh6518))) 
                                                    | (0xaU 
                                                       == (IData)(this->__PVT__i2c_user_s3))) 
                                                   | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
                                                  | (~ (IData)(this->__PVT__i2c_user_operation)))) 
           | ((~ (IData)(this->__PVT__i2c_user_pin)) 
              & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140)));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168 
        = ((1U != (IData)(this->__PVT__x___05Fh6518)) 
           & (((0xaU == (IData)(this->__PVT__i2c_user_s3)) 
               | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314));
    this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340));
    this->__PVT__i2c_user_cprescaler_EN = (((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock) 
                                            | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                           | (0U != (IData)(this->__PVT__i2c_user_cprescaler)));
    this->__PVT__i2c_user_cprescaler_D_IN = (0xffU 
                                             & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock)
                                                 ? (IData)(this->__PVT__i2c_user_s2)
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)
                                                  ? (IData)(this->__PVT__i2c_user_rprescaler)
                                                  : 
                                                 ((0U 
                                                   != (IData)(this->__PVT__i2c_user_cprescaler))
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_cprescaler) 
                                                   - (IData)(1U))
                                                   : 0U))));
    this->__PVT__CAN_FIRE_RL_i2c_user_count_scl = (
                                                   (((0U 
                                                      != this->__PVT__i2c_user_coSCL) 
                                                     & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                                    & (IData)(this->__PVT__i2c_user_eso)) 
                                                   & (IData)(this->__PVT__i2c_user_st_toggle));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (2U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl = 
        (((0U == this->__PVT__i2c_user_coSCL) & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
         & (0U != this->__PVT__i2c_user_reSCL));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg 
        = (((((((IData)(this->__PVT__i2c_user_configchange) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (1U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286 
        = (((((((((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
                 | (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                               >> 5U))))) 
                | (0x18U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                              >> 5U))))) 
               | (0x20U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
              | (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))) 
             | (0x30U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U))))) 
            | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                          >> 5U)))))
            ? ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                        >> 5U)))) ? 
               (((IData)(this->__PVT__i2c_user_s2) 
                 << 0x18U) | (((IData)(this->__PVT__i2c_user_s2) 
                               << 0x10U) | (((IData)(this->__PVT__i2c_user_s2) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s2))))
                : ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))
                    ? (((IData)(this->__PVT__i2c_user_pin) 
                        << 0x1fU) | (((IData)(this->__PVT__i2c_user_pin) 
                                      << 0x17U) | (
                                                   ((IData)(this->__PVT__i2c_user_pin) 
                                                    << 0xfU) 
                                                   | ((IData)(this->__PVT__i2c_user_pin) 
                                                      << 7U))))
                    : ((0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                   >> 5U))))
                        ? (((IData)(this->__PVT__i2c_user_s0) 
                            << 0x18U) | (((IData)(this->__PVT__i2c_user_s0) 
                                          << 0x10U) 
                                         | (((IData)(this->__PVT__i2c_user_s0) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s0))))
                        : ((0x18U == (0xffU & (IData)(
                                                      (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                       >> 5U))))
                            ? (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                << 0x19U) | (((IData)(this->__PVT__i2c_user_bb) 
                                              << 0x18U) 
                                             | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                 << 0x11U) 
                                                | (((IData)(this->__PVT__i2c_user_bb) 
                                                    << 0x10U) 
                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                       << 9U) 
                                                      | (((IData)(this->__PVT__i2c_user_bb) 
                                                          << 8U) 
                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                             << 1U) 
                                                            | (IData)(this->__PVT__i2c_user_bb))))))))
                            : ((0x20U == (0xffU & (IData)(
                                                          (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                           >> 5U))))
                                ? (((IData)(this->__PVT__i2c_user_s01) 
                                    << 0x18U) | (((IData)(this->__PVT__i2c_user_s01) 
                                                  << 0x10U) 
                                                 | (((IData)(this->__PVT__i2c_user_s01) 
                                                     << 8U) 
                                                    | (IData)(this->__PVT__i2c_user_s01))))
                                : ((0x28U == (0xffU 
                                              & (IData)(
                                                        (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                         >> 5U))))
                                    ? (((IData)(this->__PVT__i2c_user_s3) 
                                        << 0x18U) | 
                                       (((IData)(this->__PVT__i2c_user_s3) 
                                         << 0x10U) 
                                        | (((IData)(this->__PVT__i2c_user_s3) 
                                            << 8U) 
                                           | (IData)(this->__PVT__i2c_user_s3))))
                                    : ((0x30U == (0xffU 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                             >> 5U))))
                                        ? (((IData)(this->__PVT__i2c_user_i2ctime) 
                                            << 0x10U) 
                                           | (IData)(this->__PVT__i2c_user_i2ctime))
                                        : this->__PVT__i2c_user_c_scl)))))))
            : 0U);
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt 
        = ((((((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
           & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__i2c_user_coSCL_D_IN = ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock)
                                         ? this->__PVT__i2c_user_c_scl
                                         : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)
                                             ? this->__PVT__i2c_user_reSCL
                                             : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl)
                                                 ? 
                                                (this->__PVT__i2c_user_coSCL 
                                                 - (IData)(1U))
                                                 : 0U)));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113 
        = ((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__CAN_FIRE_RL_i2c_user_receive_data 
        = ((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & 
           (~ (IData)(this->__PVT__i2c_user_val_SCL)));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110 
        = (((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
              | (1U == (IData)(this->__PVT__i2c_user_dataBit))) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118 
        = ((((1U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (2U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
               | (IData)(this->__PVT__i2c_user_bb))) 
           & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
              | (1U == (IData)(this->__PVT__x___05Fh6518))));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack = 
        (((((0xbU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
            & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
           & (IData)(this->__PVT__i2c_user_eso)) & 
          (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition 
        = ((((((0xfU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (IData)(this->__PVT__i2c_user_val_SCL_in)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1 
        = ((((((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                 | (3U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (IData)(this->__PVT__i2c_user_val_SCL)) 
             & ((0U == (IData)(this->__PVT__i2c_user_dataBit)) 
                | (8U == (IData)(this->__PVT__i2c_user_dataBit)))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (4U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_data = 
        ((((((7U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_addr = 
        (((5U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_reset_state 
        = ((((((9U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (~ (IData)(this->__PVT__i2c_user_ber))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             (((((((((8U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                     & (0x18U 
                                                                        != 
                                                                        (0xffU 
                                                                         & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                    & (0x20U 
                                                                       != 
                                                                       (0xffU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   & (0x10U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (0U 
                                                                     != 
                                                                     (0xffU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                 & (0x28U 
                                                                    != 
                                                                    (0xffU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                & (0x30U 
                                                                   != 
                                                                   (0xffU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U))))) 
                                                               & (0x38U 
                                                                  != 
                                                                  (0xffU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))))
                                                               ? 2U
                                                               : 0U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
           & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165));
    this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
            & (~ (IData)(this->__PVT__i2c_user_pin))) 
           & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140));
    this->__PVT__i2c_user_val_SCL_D_IN = (1U & ((~ 
                                                 ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (~ (IData)(this->__PVT__i2c_user_pin)))) 
                                                & (~ (IData)(this->__PVT__i2c_user_val_SCL))));
    this->__PVT__i2c_user_i2ctimeout_D_IN = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                              & ((IData)(this->__PVT__i2c_user_pin) 
                                                 | ((IData)(this->__PVT__i2c_user_i2ctime) 
                                                    >> 0xeU)))
                                              ? ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 1U
                                                  : 
                                                 (0x3fffU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_i2ctimeout))))
                                              : 1U);
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
             & (IData)(this->__PVT__i2c_user_pin)) 
            & (2U != (IData)(this->__PVT__x___05Fh6518))) 
           & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168));
    this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
           & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
           & (0U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__i2c_user_last_byte_read_EN = (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (IData)(this->__PVT__i2c_user_pin)) 
                                                 & (2U 
                                                    != (IData)(this->__PVT__x___05Fh6518))) 
                                                & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178)) 
                                               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                   & (0U 
                                                      == (IData)(this->__PVT__i2c_user_dataBit))) 
                                                  & (IData)(this->__PVT__i2c_user_last_byte_read)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
            & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_last_byte_read));
    this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans));
    this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
           & (1U < (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (1U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113));
    this->__PVT__i2c_user_dataBit_EN = (((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                               & (1U 
                                                  < (IData)(this->__PVT__i2c_user_dataBit))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                                                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110))) 
                                            | ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin)) 
                                                & (2U 
                                                   != (IData)(this->__PVT__x___05Fh6518))) 
                                               & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168))) 
                                           | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                               & (0U 
                                                  == (IData)(this->__PVT__i2c_user_dataBit))) 
                                              & (IData)(this->__PVT__i2c_user_last_byte_read))) 
                                          | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                             & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340))) 
                                         | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                        | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack));
    this->__PVT__i2c_user_val_SDA_EN = (((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                                                & (IData)(this->__PVT__i2c_user_val_SDA)) 
                                               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
                                                  & (1U 
                                                     == (IData)(this->__PVT__x___05Fh6518)))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                                 & (1U 
                                                    < (IData)(this->__PVT__i2c_user_dataBit)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                & (0U 
                                                   == (IData)(this->__PVT__i2c_user_dataBit)))) 
                                            | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                                                & (IData)(this->__PVT__i2c_user_bb)) 
                                               & (2U 
                                                  == (IData)(this->__PVT__x___05Fh6518)))) 
                                           | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                                              & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                          | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                             & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113))) 
                                         | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                            & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165))) 
                                        | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118));
    this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter 
        = (1U & ((~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler))));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1 
        = ((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__i2c_user_sendInd_D_IN = (3U & (((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
                                                 & (IData)(this->__PVT__i2c_user_val_SDA))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_sendInd) 
                                                 - (IData)(1U))
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_sendInd) 
                                                  - (IData)(1U))
                                                  : 
                                                 (((((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3)) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1)) 
                                                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                      | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)))
                                                   ? 2U
                                                   : 0U))));
    this->__PVT__i2c_user_dataBit_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)) 
                                                | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)) 
                                              | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)) 
                                            | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)) 
                                           | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8))
                                           ? ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1)
                                               ? 9U
                                               : ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)
                                                   ? 8U
                                                   : 
                                                  (0xfU 
                                                   & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)
                                                       ? 
                                                      ((IData)(this->__PVT__i2c_user_dataBit) 
                                                       - (IData)(1U))
                                                       : 
                                                      ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_dataBit) 
                                                        - (IData)(1U))
                                                        : 
                                                       ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)
                                                         ? 
                                                        ((0xaU 
                                                          == (IData)(this->__PVT__i2c_user_s3))
                                                          ? 
                                                         ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U))
                                                          : 
                                                         ((IData)(this->__PVT__i2c_user_operation)
                                                           ? 8U
                                                           : 
                                                          ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U))))
                                                         : 
                                                        ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)
                                                          ? 9U
                                                          : 
                                                         ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)
                                                           ? 
                                                          ((1U 
                                                            >= (IData)(this->__PVT__i2c_user_dataBit))
                                                            ? 8U
                                                            : 
                                                           ((IData)(this->__PVT__i2c_user_dataBit) 
                                                            - (IData)(1U)))
                                                           : 9U))))))))
                                           : 0U);
    this->__PVT__i2c_user_resetcount_D_IN = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter)
                                              ? ((IData)(this->__PVT__i2c_user_rstsig)
                                                  ? 
                                                 (0x3fU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_resetcount)))
                                                  : 0U)
                                              : 0U);
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
            & (IData)(this->__PVT__i2c_user_rstsig)) 
           & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)));
    this->__PVT__i2c_user_mTransFSM_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                                                | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)) 
                                              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)) 
                                             | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7))
                                             ? ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1)
                                                 ? 9U
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)
                                                  ? 1U
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)
                                                   ? 0xfU
                                                   : 
                                                  ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)
                                                    ? 1U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)
                                                     ? 
                                                    ((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518))
                                                      ? 2U
                                                      : 0xfU)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)
                                                      ? 
                                                     ((8U 
                                                       == 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))
                                                       ? 
                                                      ((((2U 
                                                          == 
                                                          (3U 
                                                           & (IData)(
                                                                     (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                      >> 5U)))) 
                                                         & ((6U 
                                                             == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                            | (0xcU 
                                                               == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                                        & (~ (IData)(this->__PVT__i2c_user_bb)))
                                                        ? 4U
                                                        : 0xfU)
                                                       : 1U)
                                                      : 
                                                     ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)
                                                       ? 
                                                      ((0U 
                                                        == (IData)(this->__PVT__i2c_user_dataBit))
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_last_byte_read)
                                                         ? 3U
                                                         : 0xbU)
                                                        : 0xcU)
                                                       : 0xbU)))))))
                                             : (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 
                                                 ((1U 
                                                   == (IData)(this->__PVT__x___05Fh6518))
                                                   ? 0xfU
                                                   : 
                                                  ((2U 
                                                    == (IData)(this->__PVT__x___05Fh6518))
                                                    ? 9U
                                                    : 
                                                   ((0xaU 
                                                     == (IData)(this->__PVT__i2c_user_s3))
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__i2c_user_operation)
                                                      ? 8U
                                                      : 7U))))
                                                  : 0xfU)
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 0xcU
                                                   : 6U)
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)
                                                   ? 0xbU
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)
                                                    ? 6U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8)
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1)
                                                      ? 0x10U
                                                      : 0U)))))));
}

void VmkSoc_mki2c::_settle__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1__8(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mki2c::_settle__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1__8\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__i2c_user_cycwaste_D_IN = (0x3ffU & 
                                           ((0U == (IData)(this->__PVT__i2c_user_cycwaste))
                                             ? ((IData)(1U) 
                                                + (IData)(this->__PVT__i2c_user_cycwaste))
                                             : ((0x258U 
                                                 == (IData)(this->__PVT__i2c_user_cycwaste))
                                                 ? 0U
                                                 : 
                                                ((IData)(1U) 
                                                 + (IData)(this->__PVT__i2c_user_cycwaste)))));
    this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140 
        = ((IData)(this->__PVT__i2c_user_i2ctimeout) 
           <= (0x3fffU & (IData)(this->__PVT__i2c_user_i2ctime)));
    this->__PVT__s_xactor_f_wr_resp_D_IN = (((IData)(this->__PVT__i2c_user_ber) 
                                             | (((((((8U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U)))) 
                                                     & (0x20U 
                                                        != 
                                                        (0xffU 
                                                         & (IData)(
                                                                   (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                    >> 5U))))) 
                                                    & (0x10U 
                                                       != 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))) 
                                                   & (0U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U))))) 
                                                  & (0x28U 
                                                     != 
                                                     (0xffU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 & (0x38U 
                                                    != 
                                                    (0xffU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (0x30U 
                                                   != 
                                                   (0xffU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U))))))
                                             ? 2U : 0U);
    this->__PVT__x___05Fh10210 = (1U & ((IData)(this->__PVT__i2c_user_s0) 
                                        >> (7U & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                  - (IData)(2U)))));
    this->__PVT__x___05Fh6518 = (((IData)(this->__PVT__i2c_user_sta) 
                                  << 1U) | (IData)(this->__PVT__i2c_user_sto));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock 
        = ((IData)(this->__PVT__i2c_user_scl_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock 
        = ((IData)(this->__PVT__i2c_user_mod_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & (~ (IData)(this->__PVT__i2c_user_bb))) 
           | (((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343 
        = (((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U)))) & 
              (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323 
        = ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                    >> 5U)))) & (((
                                                   (6U 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                   | (0xcU 
                                                      == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                                                  | (2U 
                                                     != 
                                                     (3U 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & ((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               | (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
           & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale 
        = ((0U == (IData)(this->__PVT__i2c_user_cprescaler)) 
           & (0U != (IData)(this->__PVT__i2c_user_rprescaler)));
    this->__PVT__r1___05Fread___05Fh12450 = (((((IData)(this->__PVT__i2c_user_pin) 
                                                << 6U) 
                                               | (((IData)(this->__PVT__i2c_user_zero) 
                                                   << 5U) 
                                                  | ((IData)(this->__PVT__i2c_user_sts) 
                                                     << 4U))) 
                                              | (((IData)(this->__PVT__i2c_user_ber) 
                                                  << 3U) 
                                                 | ((IData)(this->__PVT__i2c_user_ad0_lrb) 
                                                    << 2U))) 
                                             | (((IData)(this->__PVT__i2c_user_aas) 
                                                 << 1U) 
                                                | (IData)(this->__PVT__i2c_user_lab)));
    this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U)))));
    this->__PVT__WILL_FIRE_RL_read_request = (((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                                              & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U))))) & 
           (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__i2c_user_i2ctime_D_IN = (0xffffU & 
                                          (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                            & (0x30U 
                                               == (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                            ? (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 4U))
                                            : (0x8000U 
                                               | (0x7fffU 
                                                  & (IData)(this->__PVT__i2c_user_i2ctime)))));
    this->__PVT__i2c_user_s0_D_IN = (0xffU & (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                               & (0x10U 
                                                  == 
                                                  (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                               ? (IData)(
                                                         (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 4U))
                                               : ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   | ((IData)(1U) 
                                                      << 
                                                      (0xfU 
                                                       & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U)))))
                                                   : 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   & (~ 
                                                      ((IData)(1U) 
                                                       << 
                                                       (0xfU 
                                                        & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U)))))))));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end 
        = ((((8U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_idler = ((1U 
                                                 == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178 
        = ((((1U != (IData)(this->__PVT__x___05Fh6518)) 
             & (0xaU != (IData)(this->__PVT__i2c_user_s3))) 
            & (IData)(this->__PVT__i2c_user_operation)) 
           & (~ (IData)(this->__PVT__i2c_user_ack)));
    this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165 
        = (((IData)(this->__PVT__i2c_user_pin) & (2U 
                                                  != (IData)(this->__PVT__x___05Fh6518))) 
           & (((1U == (IData)(this->__PVT__x___05Fh6518)) 
               | (0xaU == (IData)(this->__PVT__i2c_user_s3))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155 
        = (((IData)(this->__PVT__i2c_user_pin) & ((
                                                   (((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518)) 
                                                     | (1U 
                                                        == (IData)(this->__PVT__x___05Fh6518))) 
                                                    | (0xaU 
                                                       == (IData)(this->__PVT__i2c_user_s3))) 
                                                   | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
                                                  | (~ (IData)(this->__PVT__i2c_user_operation)))) 
           | ((~ (IData)(this->__PVT__i2c_user_pin)) 
              & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140)));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168 
        = ((1U != (IData)(this->__PVT__x___05Fh6518)) 
           & (((0xaU == (IData)(this->__PVT__i2c_user_s3)) 
               | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314));
    this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340));
    this->__PVT__i2c_user_cprescaler_EN = (((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock) 
                                            | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                           | (0U != (IData)(this->__PVT__i2c_user_cprescaler)));
    this->__PVT__i2c_user_cprescaler_D_IN = (0xffU 
                                             & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock)
                                                 ? (IData)(this->__PVT__i2c_user_s2)
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)
                                                  ? (IData)(this->__PVT__i2c_user_rprescaler)
                                                  : 
                                                 ((0U 
                                                   != (IData)(this->__PVT__i2c_user_cprescaler))
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_cprescaler) 
                                                   - (IData)(1U))
                                                   : 0U))));
    this->__PVT__CAN_FIRE_RL_i2c_user_count_scl = (
                                                   (((0U 
                                                      != this->__PVT__i2c_user_coSCL) 
                                                     & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                                    & (IData)(this->__PVT__i2c_user_eso)) 
                                                   & (IData)(this->__PVT__i2c_user_st_toggle));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (2U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl = 
        (((0U == this->__PVT__i2c_user_coSCL) & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
         & (0U != this->__PVT__i2c_user_reSCL));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg 
        = (((((((IData)(this->__PVT__i2c_user_configchange) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (1U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286 
        = (((((((((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
                 | (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                               >> 5U))))) 
                | (0x18U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                              >> 5U))))) 
               | (0x20U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
              | (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))) 
             | (0x30U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U))))) 
            | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                          >> 5U)))))
            ? ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                        >> 5U)))) ? 
               (((IData)(this->__PVT__i2c_user_s2) 
                 << 0x18U) | (((IData)(this->__PVT__i2c_user_s2) 
                               << 0x10U) | (((IData)(this->__PVT__i2c_user_s2) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s2))))
                : ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))
                    ? (((IData)(this->__PVT__i2c_user_pin) 
                        << 0x1fU) | (((IData)(this->__PVT__i2c_user_pin) 
                                      << 0x17U) | (
                                                   ((IData)(this->__PVT__i2c_user_pin) 
                                                    << 0xfU) 
                                                   | ((IData)(this->__PVT__i2c_user_pin) 
                                                      << 7U))))
                    : ((0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                   >> 5U))))
                        ? (((IData)(this->__PVT__i2c_user_s0) 
                            << 0x18U) | (((IData)(this->__PVT__i2c_user_s0) 
                                          << 0x10U) 
                                         | (((IData)(this->__PVT__i2c_user_s0) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s0))))
                        : ((0x18U == (0xffU & (IData)(
                                                      (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                       >> 5U))))
                            ? (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                << 0x19U) | (((IData)(this->__PVT__i2c_user_bb) 
                                              << 0x18U) 
                                             | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                 << 0x11U) 
                                                | (((IData)(this->__PVT__i2c_user_bb) 
                                                    << 0x10U) 
                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                       << 9U) 
                                                      | (((IData)(this->__PVT__i2c_user_bb) 
                                                          << 8U) 
                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                             << 1U) 
                                                            | (IData)(this->__PVT__i2c_user_bb))))))))
                            : ((0x20U == (0xffU & (IData)(
                                                          (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                           >> 5U))))
                                ? (((IData)(this->__PVT__i2c_user_s01) 
                                    << 0x18U) | (((IData)(this->__PVT__i2c_user_s01) 
                                                  << 0x10U) 
                                                 | (((IData)(this->__PVT__i2c_user_s01) 
                                                     << 8U) 
                                                    | (IData)(this->__PVT__i2c_user_s01))))
                                : ((0x28U == (0xffU 
                                              & (IData)(
                                                        (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                         >> 5U))))
                                    ? (((IData)(this->__PVT__i2c_user_s3) 
                                        << 0x18U) | 
                                       (((IData)(this->__PVT__i2c_user_s3) 
                                         << 0x10U) 
                                        | (((IData)(this->__PVT__i2c_user_s3) 
                                            << 8U) 
                                           | (IData)(this->__PVT__i2c_user_s3))))
                                    : ((0x30U == (0xffU 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                             >> 5U))))
                                        ? (((IData)(this->__PVT__i2c_user_i2ctime) 
                                            << 0x10U) 
                                           | (IData)(this->__PVT__i2c_user_i2ctime))
                                        : this->__PVT__i2c_user_c_scl)))))))
            : 0U);
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt 
        = ((((((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
           & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__i2c_user_coSCL_D_IN = ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock)
                                         ? this->__PVT__i2c_user_c_scl
                                         : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)
                                             ? this->__PVT__i2c_user_reSCL
                                             : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl)
                                                 ? 
                                                (this->__PVT__i2c_user_coSCL 
                                                 - (IData)(1U))
                                                 : 0U)));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113 
        = ((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__CAN_FIRE_RL_i2c_user_receive_data 
        = ((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & 
           (~ (IData)(this->__PVT__i2c_user_val_SCL)));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110 
        = (((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
              | (1U == (IData)(this->__PVT__i2c_user_dataBit))) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118 
        = ((((1U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (2U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
               | (IData)(this->__PVT__i2c_user_bb))) 
           & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
              | (1U == (IData)(this->__PVT__x___05Fh6518))));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack = 
        (((((0xbU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
            & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
           & (IData)(this->__PVT__i2c_user_eso)) & 
          (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition 
        = ((((((0xfU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (IData)(this->__PVT__i2c_user_val_SCL_in)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1 
        = ((((((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                 | (3U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (IData)(this->__PVT__i2c_user_val_SCL)) 
             & ((0U == (IData)(this->__PVT__i2c_user_dataBit)) 
                | (8U == (IData)(this->__PVT__i2c_user_dataBit)))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (4U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_data = 
        ((((((7U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_addr = 
        (((5U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_reset_state 
        = ((((((9U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (~ (IData)(this->__PVT__i2c_user_ber))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             (((((((((8U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                     & (0x18U 
                                                                        != 
                                                                        (0xffU 
                                                                         & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                    & (0x20U 
                                                                       != 
                                                                       (0xffU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   & (0x10U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (0U 
                                                                     != 
                                                                     (0xffU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                 & (0x28U 
                                                                    != 
                                                                    (0xffU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                & (0x30U 
                                                                   != 
                                                                   (0xffU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U))))) 
                                                               & (0x38U 
                                                                  != 
                                                                  (0xffU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))))
                                                               ? 2U
                                                               : 0U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
           & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165));
    this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
            & (~ (IData)(this->__PVT__i2c_user_pin))) 
           & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140));
    this->__PVT__i2c_user_val_SCL_D_IN = (1U & ((~ 
                                                 ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (~ (IData)(this->__PVT__i2c_user_pin)))) 
                                                & (~ (IData)(this->__PVT__i2c_user_val_SCL))));
    this->__PVT__i2c_user_i2ctimeout_D_IN = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                              & ((IData)(this->__PVT__i2c_user_pin) 
                                                 | ((IData)(this->__PVT__i2c_user_i2ctime) 
                                                    >> 0xeU)))
                                              ? ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 1U
                                                  : 
                                                 (0x3fffU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_i2ctimeout))))
                                              : 1U);
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
             & (IData)(this->__PVT__i2c_user_pin)) 
            & (2U != (IData)(this->__PVT__x___05Fh6518))) 
           & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168));
    this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
           & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
           & (0U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__i2c_user_last_byte_read_EN = (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (IData)(this->__PVT__i2c_user_pin)) 
                                                 & (2U 
                                                    != (IData)(this->__PVT__x___05Fh6518))) 
                                                & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178)) 
                                               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                   & (0U 
                                                      == (IData)(this->__PVT__i2c_user_dataBit))) 
                                                  & (IData)(this->__PVT__i2c_user_last_byte_read)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
            & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_last_byte_read));
    this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans));
    this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
           & (1U < (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (1U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113));
    this->__PVT__i2c_user_dataBit_EN = (((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                               & (1U 
                                                  < (IData)(this->__PVT__i2c_user_dataBit))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                                                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110))) 
                                            | ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin)) 
                                                & (2U 
                                                   != (IData)(this->__PVT__x___05Fh6518))) 
                                               & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168))) 
                                           | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                               & (0U 
                                                  == (IData)(this->__PVT__i2c_user_dataBit))) 
                                              & (IData)(this->__PVT__i2c_user_last_byte_read))) 
                                          | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                             & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340))) 
                                         | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                        | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack));
    this->__PVT__i2c_user_val_SDA_EN = (((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                                                & (IData)(this->__PVT__i2c_user_val_SDA)) 
                                               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
                                                  & (1U 
                                                     == (IData)(this->__PVT__x___05Fh6518)))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                                 & (1U 
                                                    < (IData)(this->__PVT__i2c_user_dataBit)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                & (0U 
                                                   == (IData)(this->__PVT__i2c_user_dataBit)))) 
                                            | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                                                & (IData)(this->__PVT__i2c_user_bb)) 
                                               & (2U 
                                                  == (IData)(this->__PVT__x___05Fh6518)))) 
                                           | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                                              & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                          | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                             & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113))) 
                                         | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                            & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165))) 
                                        | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118));
    this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter 
        = (1U & ((~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler))));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1 
        = ((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__i2c_user_sendInd_D_IN = (3U & (((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
                                                 & (IData)(this->__PVT__i2c_user_val_SDA))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_sendInd) 
                                                 - (IData)(1U))
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_sendInd) 
                                                  - (IData)(1U))
                                                  : 
                                                 (((((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3)) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1)) 
                                                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                      | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)))
                                                   ? 2U
                                                   : 0U))));
    this->__PVT__i2c_user_dataBit_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)) 
                                                | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)) 
                                              | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)) 
                                            | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)) 
                                           | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8))
                                           ? ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1)
                                               ? 9U
                                               : ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)
                                                   ? 8U
                                                   : 
                                                  (0xfU 
                                                   & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)
                                                       ? 
                                                      ((IData)(this->__PVT__i2c_user_dataBit) 
                                                       - (IData)(1U))
                                                       : 
                                                      ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_dataBit) 
                                                        - (IData)(1U))
                                                        : 
                                                       ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)
                                                         ? 
                                                        ((0xaU 
                                                          == (IData)(this->__PVT__i2c_user_s3))
                                                          ? 
                                                         ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U))
                                                          : 
                                                         ((IData)(this->__PVT__i2c_user_operation)
                                                           ? 8U
                                                           : 
                                                          ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U))))
                                                         : 
                                                        ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)
                                                          ? 9U
                                                          : 
                                                         ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)
                                                           ? 
                                                          ((1U 
                                                            >= (IData)(this->__PVT__i2c_user_dataBit))
                                                            ? 8U
                                                            : 
                                                           ((IData)(this->__PVT__i2c_user_dataBit) 
                                                            - (IData)(1U)))
                                                           : 9U))))))))
                                           : 0U);
    this->__PVT__i2c_user_resetcount_D_IN = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter)
                                              ? ((IData)(this->__PVT__i2c_user_rstsig)
                                                  ? 
                                                 (0x3fU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_resetcount)))
                                                  : 0U)
                                              : 0U);
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
            & (IData)(this->__PVT__i2c_user_rstsig)) 
           & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)));
    this->__PVT__i2c_user_mTransFSM_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                                                | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)) 
                                              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)) 
                                             | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7))
                                             ? ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1)
                                                 ? 9U
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)
                                                  ? 1U
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)
                                                   ? 0xfU
                                                   : 
                                                  ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)
                                                    ? 1U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)
                                                     ? 
                                                    ((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518))
                                                      ? 2U
                                                      : 0xfU)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)
                                                      ? 
                                                     ((8U 
                                                       == 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))
                                                       ? 
                                                      ((((2U 
                                                          == 
                                                          (3U 
                                                           & (IData)(
                                                                     (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                      >> 5U)))) 
                                                         & ((6U 
                                                             == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                            | (0xcU 
                                                               == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                                        & (~ (IData)(this->__PVT__i2c_user_bb)))
                                                        ? 4U
                                                        : 0xfU)
                                                       : 1U)
                                                      : 
                                                     ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)
                                                       ? 
                                                      ((0U 
                                                        == (IData)(this->__PVT__i2c_user_dataBit))
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_last_byte_read)
                                                         ? 3U
                                                         : 0xbU)
                                                        : 0xcU)
                                                       : 0xbU)))))))
                                             : (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 
                                                 ((1U 
                                                   == (IData)(this->__PVT__x___05Fh6518))
                                                   ? 0xfU
                                                   : 
                                                  ((2U 
                                                    == (IData)(this->__PVT__x___05Fh6518))
                                                    ? 9U
                                                    : 
                                                   ((0xaU 
                                                     == (IData)(this->__PVT__i2c_user_s3))
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__i2c_user_operation)
                                                      ? 8U
                                                      : 7U))))
                                                  : 0xfU)
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 0xcU
                                                   : 6U)
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)
                                                   ? 0xbU
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)
                                                    ? 6U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8)
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1)
                                                      ? 0x10U
                                                      : 0U)))))));
}

void VmkSoc_mki2c::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mki2c::_ctor_var_reset\n"); );
    // Body
    CLK = VL_RAND_RESET_I(1);
    RST_N = VL_RAND_RESET_I(1);
    slave_m_awvalid_awvalid = VL_RAND_RESET_I(1);
    slave_m_awvalid_awaddr = VL_RAND_RESET_I(32);
    slave_m_awvalid_awsize = VL_RAND_RESET_I(2);
    slave_m_awvalid_awprot = VL_RAND_RESET_I(3);
    slave_awready = VL_RAND_RESET_I(1);
    slave_m_wvalid_wvalid = VL_RAND_RESET_I(1);
    slave_m_wvalid_wdata = VL_RAND_RESET_I(32);
    slave_m_wvalid_wstrb = VL_RAND_RESET_I(4);
    slave_wready = VL_RAND_RESET_I(1);
    slave_bvalid = VL_RAND_RESET_I(1);
    slave_bresp = VL_RAND_RESET_I(2);
    slave_m_bready_bready = VL_RAND_RESET_I(1);
    slave_m_arvalid_arvalid = VL_RAND_RESET_I(1);
    slave_m_arvalid_araddr = VL_RAND_RESET_I(32);
    slave_m_arvalid_arsize = VL_RAND_RESET_I(2);
    slave_m_arvalid_arprot = VL_RAND_RESET_I(3);
    slave_arready = VL_RAND_RESET_I(1);
    slave_rvalid = VL_RAND_RESET_I(1);
    slave_rresp = VL_RAND_RESET_I(2);
    slave_rdata = VL_RAND_RESET_I(32);
    slave_m_rready_rready = VL_RAND_RESET_I(1);
    io_scl_out = VL_RAND_RESET_I(1);
    io_scl_in_in = VL_RAND_RESET_I(1);
    io_scl_out_en = VL_RAND_RESET_I(1);
    io_sda_out = VL_RAND_RESET_I(1);
    io_sda_in_in = VL_RAND_RESET_I(1);
    io_sda_out_en = VL_RAND_RESET_I(1);
    isint = VL_RAND_RESET_I(1);
    __PVT__RDY_isint = VL_RAND_RESET_I(1);
    __PVT__timerint = VL_RAND_RESET_I(1);
    __PVT__RDY_timerint = VL_RAND_RESET_I(1);
    __PVT__isber = VL_RAND_RESET_I(1);
    __PVT__RDY_isber = VL_RAND_RESET_I(1);
    __PVT__i2c_user_aas = VL_RAND_RESET_I(1);
    __PVT__i2c_user_ack = VL_RAND_RESET_I(1);
    __PVT__i2c_user_ad0_lrb = VL_RAND_RESET_I(1);
    __PVT__i2c_user_bb = VL_RAND_RESET_I(1);
    __PVT__i2c_user_ber = VL_RAND_RESET_I(1);
    __PVT__i2c_user_cOutEn = VL_RAND_RESET_I(1);
    __PVT__i2c_user_c_scl = VL_RAND_RESET_I(32);
    __PVT__i2c_user_coSCL = VL_RAND_RESET_I(32);
    __PVT__i2c_user_coSCL_D_IN = VL_RAND_RESET_I(32);
    __PVT__i2c_user_configchange = VL_RAND_RESET_I(1);
    __PVT__i2c_user_cprescaler = VL_RAND_RESET_I(8);
    __PVT__i2c_user_cprescaler_D_IN = VL_RAND_RESET_I(8);
    __PVT__i2c_user_cprescaler_EN = VL_RAND_RESET_I(1);
    __PVT__i2c_user_cycwaste = VL_RAND_RESET_I(10);
    __PVT__i2c_user_cycwaste_D_IN = VL_RAND_RESET_I(10);
    __PVT__i2c_user_dOutEn = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_0 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_1 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_2 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_3 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_4 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_5 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_6 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_7 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_8 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dOutEn_delay_9 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dataBit = VL_RAND_RESET_I(4);
    __PVT__i2c_user_dataBit_D_IN = VL_RAND_RESET_I(4);
    __PVT__i2c_user_dataBit_EN = VL_RAND_RESET_I(1);
    __PVT__i2c_user_eni = VL_RAND_RESET_I(1);
    __PVT__i2c_user_eso = VL_RAND_RESET_I(1);
    __PVT__i2c_user_i2ctime = VL_RAND_RESET_I(16);
    __PVT__i2c_user_i2ctime_D_IN = VL_RAND_RESET_I(16);
    __PVT__i2c_user_i2ctimeout = VL_RAND_RESET_I(14);
    __PVT__i2c_user_i2ctimeout_D_IN = VL_RAND_RESET_I(14);
    __PVT__i2c_user_lab = VL_RAND_RESET_I(1);
    __PVT__i2c_user_last_byte_read = VL_RAND_RESET_I(1);
    __PVT__i2c_user_last_byte_read_EN = VL_RAND_RESET_I(1);
    __PVT__i2c_user_mTransFSM = VL_RAND_RESET_I(5);
    __PVT__i2c_user_mTransFSM_D_IN = VL_RAND_RESET_I(5);
    __PVT__i2c_user_mod_start = VL_RAND_RESET_I(1);
    __PVT__i2c_user_operation = VL_RAND_RESET_I(1);
    __PVT__i2c_user_pin = VL_RAND_RESET_I(1);
    __PVT__i2c_user_reSCL = VL_RAND_RESET_I(32);
    __PVT__i2c_user_resetcount = VL_RAND_RESET_I(6);
    __PVT__i2c_user_resetcount_D_IN = VL_RAND_RESET_I(6);
    __PVT__i2c_user_rprescaler = VL_RAND_RESET_I(8);
    __PVT__i2c_user_rstsig = VL_RAND_RESET_I(1);
    __PVT__i2c_user_s0 = VL_RAND_RESET_I(8);
    __PVT__i2c_user_s0_D_IN = VL_RAND_RESET_I(8);
    __PVT__i2c_user_s01 = VL_RAND_RESET_I(8);
    __PVT__i2c_user_s2 = VL_RAND_RESET_I(8);
    __PVT__i2c_user_s3 = VL_RAND_RESET_I(8);
    __PVT__i2c_user_scl_start = VL_RAND_RESET_I(1);
    __PVT__i2c_user_sendInd = VL_RAND_RESET_I(2);
    __PVT__i2c_user_sendInd_D_IN = VL_RAND_RESET_I(2);
    __PVT__i2c_user_st_toggle = VL_RAND_RESET_I(1);
    __PVT__i2c_user_sta = VL_RAND_RESET_I(1);
    __PVT__i2c_user_sto = VL_RAND_RESET_I(1);
    __PVT__i2c_user_sts = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_SCL = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_SCL_D_IN = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_SCL_in = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_SDA = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_SDA_EN = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_SDA_in = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_0 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_1 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_2 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_3 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_4 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_5 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_6 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_7 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_8 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_val_sda_delay_9 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_zero = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_data_D_IN = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_f_rd_data_DEQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_data_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_resp_D_IN = VL_RAND_RESET_I(2);
    __PVT__s_xactor_f_wr_resp_DEQ = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_i2c_user_count_scl = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_i2c_user_receive_data = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_i2c_user_restore_prescale = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_i2c_user_restore_scl = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_i2c_user_set_scl_clock = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_write_request = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_check_Ack = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_check_control_reg = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_idler = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_receive_data1 = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_reset_state = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_resetfilter = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_send_addr = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_send_data = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_send_start_trans = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_send_stop_condition = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_wait_interrupt = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_read_request = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_aas_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_ack_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_bb_write_1___05FPSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_ber_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_ber_write_1___05FSEL_3 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_s3_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7 = VL_RAND_RESET_I(1);
    __PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8 = VL_RAND_RESET_I(1);
    __PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286 = VL_RAND_RESET_I(32);
    __PVT__r1___05Fread___05Fh12450 = VL_RAND_RESET_I(7);
    __PVT__x___05Fh6518 = VL_RAND_RESET_I(2);
    __PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113 = VL_RAND_RESET_I(1);
    __PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110 = VL_RAND_RESET_I(1);
    __PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168 = VL_RAND_RESET_I(1);
    __PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165 = VL_RAND_RESET_I(1);
    __PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155 = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314 = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323 = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340 = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343 = VL_RAND_RESET_I(1);
    __PVT__x___05Fh10210 = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_rd_addr__DOT__data1_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_rd_data__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_rd_data__DOT__data0_reg = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_f_rd_data__DOT__data1_reg = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_f_wr_addr__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_wr_addr__DOT__data1_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_f_wr_data__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_data__DOT__data0_reg = VL_RAND_RESET_Q(36);
    __PVT__s_xactor_f_wr_data__DOT__data1_reg = VL_RAND_RESET_Q(36);
    __PVT__s_xactor_f_wr_resp__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_resp__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_f_wr_resp__DOT__data0_reg = VL_RAND_RESET_I(2);
    __PVT__s_xactor_f_wr_resp__DOT__data1_reg = VL_RAND_RESET_I(2);
    __Vdly__i2c_user_last_byte_read = VL_RAND_RESET_I(1);
    __Vdly__i2c_user_val_SCL = VL_RAND_RESET_I(1);
    __Vdly__i2c_user_sendInd = VL_RAND_RESET_I(2);
    __Vdly__s_xactor_f_rd_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_rd_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __Vdly__s_xactor_f_rd_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_wr_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_wr_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __Vdly__s_xactor_f_wr_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_f_wr_data__DOT__data0_reg = VL_RAND_RESET_Q(36);
    __Vdly__s_xactor_f_wr_resp__DOT__empty_reg = VL_RAND_RESET_I(1);
}
