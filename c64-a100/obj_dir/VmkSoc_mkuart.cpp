// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc_mkuart.h"
#include "VmkSoc__Syms.h"

//==========

VL_INLINE_OPT void VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__4(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__4\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0 = 0U;
    this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0 = 0U;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__tail;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__head;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__tail;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__head;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart0.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)) 
                         & (IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart0.user_ifc_uart_fifoRecv.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart0.user_ifc_uart_fifoRecv.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart0.user_ifc_uart_fifoXmit.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full)) 
                         & (IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart0.user_ifc_uart_fifoXmit.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x10U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_delay_control 
                = (0xffffU & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                      >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_rg_delay_control = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_baud_value = (0xffffU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_baud_value = 0xa3U;
    }
    this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state 
        = ((IData)(vlTOPp->RST_N) ? ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)
                                      ? (0xffffU & 
                                         ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state) 
                                          + ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)
                                              ? 1U : 0U)))
                                      : 0U) : 0U);
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x18U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_interrupt_en = 
                (0xffU & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                  >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_rg_interrupt_en = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit) {
            this->__PVT__user_ifc_uart_rRecvParity 
                = this->__PVT__user_ifc_uart_rRecvData;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvParity = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
               & (0xcU == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
              & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit))) {
            this->__PVT__user_ifc_uart_error_status_register 
                = this->__PVT__user_ifc_uart_error_status_register_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_error_status_register = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                     & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
               | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit))) {
            this->__PVT__user_ifc_uart_rXmitState = this->__PVT__user_ifc_uart_rXmitState_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitState = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas) 
             | ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time) 
                  & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))) 
                 & ((~ (IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317)) 
                    | (0U != (IData)(this->__PVT__user_ifc_rg_parity)))) 
                & ((~ (IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317)) 
                   | (0U == (IData)(this->__PVT__user_ifc_rg_parity)))))) {
            this->__PVT__user_ifc_uart_rXmitBitCount 
                = this->__PVT__user_ifc_uart_rXmitBitCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitBitCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                   | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                  | (IData)(this->__PVT__user_ifc_uart_out_enable))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                 & (~ (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit))) {
            this->__PVT__user_ifc_uart_rg_delay_count 
                = this->__PVT__user_ifc_uart_rg_delay_count_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rg_delay_count = 0U;
    }
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                            << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                       << 2U) | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
        this->__PVT__user_ifc_uart_fifoRecv_D_OUT = 
            ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
              ? this->__PVT__user_ifc_uart_fifoRecv__DOT__arr
             [this->__PVT__user_ifc_uart_fifoRecv__DOT__head]
              : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
            this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                = this->__PVT__bitdata___05Fh17072;
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                  << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                    = ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
                        ? this->__PVT__user_ifc_uart_fifoRecv__DOT__arr
                       [this->__PVT__user_ifc_uart_fifoRecv__DOT__head]
                        : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                      << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                    this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                        = this->__PVT__bitdata___05Fh17072;
                }
            }
        }
    }
    if (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
         & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
             & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)) 
                & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
               & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full))))) {
        this->user_ifc_uart_fifoXmit__DOT____Vlvbound2 
            = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                       >> 4U));
        if ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))) {
            this->__Vdlyvval__user_ifc_uart_fifoXmit__DOT__arr__v0 
                = this->user_ifc_uart_fifoXmit__DOT____Vlvbound2;
            this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__user_ifc_uart_fifoXmit__DOT__arr__v0 
                = this->__PVT__user_ifc_uart_fifoXmit__DOT__tail;
        }
    }
    if (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
         & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
             & (~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas)) 
                & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)) 
               & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full))))) {
        this->user_ifc_uart_fifoRecv__DOT____Vlvbound2 
            = this->__PVT__bitdata___05Fh17072;
        if ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))) {
            this->__Vdlyvval__user_ifc_uart_fifoRecv__DOT__arr__v0 
                = this->user_ifc_uart_fifoRecv__DOT____Vlvbound2;
            this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__user_ifc_uart_fifoRecv__DOT__arr__v0 
                = this->__PVT__user_ifc_uart_fifoRecv__DOT__tail;
        }
    }
    if (vlTOPp->RST_N) {
        if ((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit)) 
               | (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_rRecvState = 
                ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)
                  ? ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                      ? 0U : 1U) : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell)
                                     ? ((4U == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))
                                         ? ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                                             ? 0U : 2U)
                                         : 1U) : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)
                                                   ? 
                                                  (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                    == (IData)(this->__PVT__user_ifc_rg_charsize))
                                                    ? 
                                                   ((0U 
                                                     == (IData)(this->__PVT__user_ifc_rg_parity))
                                                     ? 
                                                    ((0U 
                                                      == (IData)(this->__PVT__user_ifc_rg_stopbits))
                                                      ? 6U
                                                      : 5U)
                                                     : 4U)
                                                    : 
                                                   (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                     == 
                                                     (0x3fU 
                                                      & ((IData)(1U) 
                                                         + (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                     ? 
                                                    (((0U 
                                                       == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                      | (0U 
                                                         == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                      ? 6U
                                                      : 5U)
                                                     : 
                                                    (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                      == 
                                                      (0x3fU 
                                                       & ((IData)(2U) 
                                                          + (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                      ? 6U
                                                      : 3U)))
                                                   : 
                                                  ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit)
                                                    ? 
                                                   ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                                                     ? 2U
                                                     : 0U)
                                                    : 
                                                   ((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)
                                                     ? 0U
                                                     : 
                                                    (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit) 
                                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))
                                                      ? 2U
                                                      : 0U))))));
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvState = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                 << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__head = 0U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail = 0U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 1U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                    << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
                    = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail;
                this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
                    = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                      << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
                            = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head;
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty 
                            = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head) 
                               == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                            this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                                if (this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full) {
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
                                        = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail;
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 0U;
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail) 
                                           != (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__head = 0U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail = 0U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 1U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                 << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__head = 0U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail = 0U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 1U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                    << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
                    = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail;
                this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
                    = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                      << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
                            = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head;
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty 
                            = ((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head) 
                               == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                            this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                                if (this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full) {
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
                                        = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail;
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 0U;
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail) 
                                           != (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__head = 0U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail = 0U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 1U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_0 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_0 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2)) 
               | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command))) {
            this->__PVT__user_ifc_uart_rXmitDataOut 
                = (1U & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit) 
                          | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time))
                          ? (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_0)
                          : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)
                              ? ((1U == (IData)(this->__PVT__user_ifc_rg_parity))
                                  ? (~ (IData)(this->__PVT__user_ifc_uart_rXmitParity))
                                  : ((2U == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                     & (IData)(this->__PVT__user_ifc_uart_rXmitParity)))
                              : ((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                                 & ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2) 
                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
                                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
                                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command))))));
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitDataOut = 1U;
    }
    if (this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0) {
        this->__PVT__user_ifc_uart_fifoRecv__DOT__arr[this->__Vdlyvdim0__user_ifc_uart_fifoRecv__DOT__arr__v0] 
            = this->__Vdlyvval__user_ifc_uart_fifoRecv__DOT__arr__v0;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__tail 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__tail 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__head 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__head;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full;
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)))) {
            this->__PVT__user_ifc_uart_rXmitCellCount 
                = this->__PVT__user_ifc_uart_rXmitCellCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitCellCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata))) 
              & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control))) {
            this->__PVT__user_ifc_uart_out_enable = 
                (1U & (~ (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                           & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata))) 
                          & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297))));
        }
    } else {
        this->__PVT__user_ifc_uart_out_enable = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas) 
             | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift)))) {
            this->__PVT__user_ifc_uart_rRecvBitCount 
                = this->__PVT__user_ifc_uart_rRecvBitCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvBitCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_stopbits = (3U 
                                                 & (IData)(
                                                           (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 5U)));
        }
    } else {
        this->__PVT__user_ifc_rg_stopbits = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_charsize = (0x3fU 
                                                 & (IData)(
                                                           (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 9U)));
        }
    } else {
        this->__PVT__user_ifc_rg_charsize = 8U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)))) {
            this->__PVT__user_ifc_uart_rRecvCellCount 
                = this->__PVT__user_ifc_uart_rRecvCellCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvCellCount = 0U;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))));
    this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))));
    this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_1 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_parity = (3U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 7U)));
        }
    } else {
        this->__PVT__user_ifc_rg_parity = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) {
            this->__PVT__user_ifc_uart_rXmitParity 
                = (1U & ((((((((((((IData)(this->__PVT__z___05Fh27968) 
                                   ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                      >> 0x15U)) ^ 
                                  (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                   >> 0x16U)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0x17U)) 
                                ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                   >> 0x18U)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0x19U)) 
                              ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                 >> 0x1aU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                               >> 0x1bU)) 
                            ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                               >> 0x1cU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                             >> 0x1dU)) 
                          ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                             >> 0x1eU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                           >> 0x1fU)));
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitParity = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_0 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? this->__PVT__user_ifc_uart_fifoXmit_D_OUT
                    : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_1)));
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297 
        = ((IData)(this->__PVT__user_ifc_uart_rg_delay_count) 
           == (IData)(this->__PVT__user_ifc_rg_delay_control));
    this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30 
        = ((0xffffU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state))) 
           < (IData)(this->__PVT__user_ifc_baud_value));
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_capture_read_request = 
        ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
         & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_capture_write_request 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317 
        = ((IData)(this->__PVT__user_ifc_uart_rXmitBitCount) 
           == (0x3fU & ((IData)(this->__PVT__user_ifc_rg_charsize) 
                        - (IData)(1U))));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit 
        = ((4U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit 
        = ((5U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell 
        = ((1U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample 
        = (((2U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
            & (0xfU == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit 
        = ((4U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit 
        = ((2U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time 
        = ((3U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit 
        = ((8U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit 
        = ((5U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5 
        = ((6U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2 
        = ((7U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command 
        = ((0U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control 
        = ((1U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_2 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_2 = 0U;
    }
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((((4U 
                                                 == 
                                                 (0x1fU 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                             >> 5U)))) 
                                                | ((0U 
                                                    == 
                                                    (0x1fU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U)))) 
                                                   & (1U 
                                                      == 
                                                      (3U 
                                                       & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                               | ((0x10U 
                                                   == 
                                                   (0x1fU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  & (1U 
                                                     == 
                                                     (3U 
                                                      & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                              | ((0x14U 
                                                  == 
                                                  (0x1fU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))) 
                                                 & (1U 
                                                    == 
                                                    (3U 
                                                     & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                             | (((0x18U 
                                                  == 
                                                  (0x1fU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))) 
                                                 | (0xcU 
                                                    == 
                                                    (0x1fU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (0U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_1 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 1U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_2)));
    }
    this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
            & (8U == (0x1fU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                       >> 5U))))) & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata));
    this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit 
        = (((6U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
            & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)));
    this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
           | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command));
    this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
           & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297));
    this->__PVT__user_ifc_uart_rXmitBitCount_D_IN = 
        ((IData)(this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas)
          ? 0U : (0x3fU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rXmitBitCount))));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_3 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_3 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_2 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 2U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_3)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_4 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_4 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_3 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 3U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_4)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_5 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_5 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_4 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 4U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_5)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_6 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_6 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_5 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 5U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_6)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_7 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_7 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_6 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 6U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_7)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_8 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_8 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_7 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 7U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_8)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_9 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_9 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_8 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 8U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_9)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_10 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_10 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_9 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 9U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_10)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_11 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_11 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_10 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xaU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_11)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_12 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_12 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_11 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xbU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_12)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_13 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_13 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_12 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xcU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_13)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_14 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_14 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_13 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xdU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_14)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_15 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_15 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_14 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xeU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_15)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_16 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_16 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_15 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xfU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_16)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_17 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_17 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_16 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x10U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_17)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_18 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_18 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_17 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x11U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_18)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_19 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_19 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_18 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x12U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_19)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_20 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_20 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_19 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x13U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_20)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_21 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_21 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_20 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x14U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_21)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_22 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_22 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_21 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x15U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_22)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_23 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_23 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_22 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x16U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_23)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_24 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_24 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_23 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x17U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_24)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_25 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_25 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_24 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x18U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_25)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_26 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_26 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_25 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x19U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_26)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_27 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_27 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_26 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1aU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_27)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_28 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_28 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_27 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1bU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_28)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_29 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_29 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_28 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1cU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_29)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_30 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_30 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_29 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1dU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_30)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_31 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_rRecvData));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_31 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_30 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1eU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_31)));
    }
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift 
        = ((3U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit 
        = ((0U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__user_ifc_uart_rRecvCellCount_D_IN 
        = (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell) 
              & (4U == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)) 
            | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))));
    this->__PVT__bitdata___05Fh17072 = (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31) 
                                         << 0x1fU) 
                                        | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30) 
                                            << 0x1eU) 
                                           | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29) 
                                               << 0x1dU) 
                                              | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28) 
                                                  << 0x1cU) 
                                                 | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27) 
                                                     << 0x1bU) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26) 
                                                        << 0x1aU) 
                                                       | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25) 
                                                           << 0x19U) 
                                                          | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24) 
                                                              << 0x18U) 
                                                             | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23) 
                                                                 << 0x17U) 
                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22) 
                                                                    << 0x16U) 
                                                                   | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21) 
                                                                       << 0x15U) 
                                                                      | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20) 
                                                                          << 0x14U) 
                                                                         | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19) 
                                                                             << 0x13U) 
                                                                            | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18) 
                                                                                << 0x12U) 
                                                                               | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17) 
                                                                                << 0x11U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16) 
                                                                                << 0x10U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15) 
                                                                                << 0xfU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14) 
                                                                                << 0xeU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13) 
                                                                                << 0xdU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12) 
                                                                                << 0xcU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11) 
                                                                                << 0xbU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10) 
                                                                                << 0xaU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9) 
                                                                                << 9U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8) 
                                                                                << 8U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7) 
                                                                                << 7U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6) 
                                                                                << 6U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5) 
                                                                                << 5U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4) 
                                                                                << 4U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3) 
                                                                                << 3U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2) 
                                                                                << 2U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0))))))))))))))))))))))))))))))));
    this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169 
        = (((((((((((((((((((((((((((((((((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0) 
                                          ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1)) 
                                         ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2)) 
                                        ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3)) 
                                       ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4)) 
                                      ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5)) 
                                     ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6)) 
                                    ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7)) 
                                   ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8)) 
                                  ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9)) 
                                 ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10)) 
                                ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11)) 
                               ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12)) 
                              ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13)) 
                             ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14)) 
                            ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15)) 
                           ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16)) 
                          ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17)) 
                         ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18)) 
                        ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19)) 
                       ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20)) 
                      ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21)) 
                     ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22)) 
                    ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23)) 
                   ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24)) 
                  ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25)) 
                 ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26)) 
                ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27)) 
               ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28)) 
              ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29)) 
             ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30)) 
            ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31)) 
           ^ (IData)(this->__PVT__user_ifc_uart_rRecvParity));
    this->__PVT__user_ifc_uart_rRecvData = (1U & ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (IData)(vlTOPp->uart0_io_SIN)));
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_31 
            = (1U & ((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)) 
                     | (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                        >> 0x1fU)));
    }
    this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
           & (IData)(this->__PVT__user_ifc_uart_rRecvData));
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                            << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                       << 2U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
        this->__PVT__user_ifc_uart_fifoXmit_D_OUT = 
            ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
              ? this->__PVT__user_ifc_uart_fifoXmit__DOT__arr
             [this->__PVT__user_ifc_uart_fifoXmit__DOT__head]
              : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
            this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                           >> 4U));
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                  << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                    = ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
                        ? this->__PVT__user_ifc_uart_fifoXmit__DOT__arr
                       [this->__PVT__user_ifc_uart_fifoXmit__DOT__head]
                        : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                      << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                    this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                        = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                   >> 4U));
                }
            }
        }
    }
    this->__PVT__user_ifc_uart_rRecvBitCount_D_IN = 
        ((IData)(this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas)
          ? 0U : (0x3fU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rRecvBitCount))));
    this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
           & (4U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))));
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__head 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__head;
    if (this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0) {
        this->__PVT__user_ifc_uart_fifoXmit__DOT__arr[this->__Vdlyvdim0__user_ifc_uart_fifoXmit__DOT__arr__v0] 
            = this->__Vdlyvval__user_ifc_uart_fifoXmit__DOT__arr__v0;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata;
    this->__PVT__z___05Fh27968 = (1U & ((((((((((((
                                                   ((((((((this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                           ^ 
                                                           (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                            >> 1U)) 
                                                          ^ 
                                                          (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                           >> 2U)) 
                                                         ^ 
                                                         (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                          >> 3U)) 
                                                        ^ 
                                                        (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                         >> 4U)) 
                                                       ^ 
                                                       (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                        >> 5U)) 
                                                      ^ 
                                                      (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                       >> 6U)) 
                                                     ^ 
                                                     (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                      >> 7U)) 
                                                    ^ 
                                                    (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                     >> 8U)) 
                                                   ^ 
                                                   (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                    >> 9U)) 
                                                  ^ 
                                                  (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                   >> 0xaU)) 
                                                 ^ 
                                                 (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                  >> 0xbU)) 
                                                ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                   >> 0xcU)) 
                                               ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                  >> 0xdU)) 
                                              ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0xeU)) 
                                             ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                >> 0xfU)) 
                                            ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                               >> 0x10U)) 
                                           ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                              >> 0x11U)) 
                                          ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                             >> 0x12U)) 
                                         ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                            >> 0x13U)) 
                                        ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                           >> 0x14U)));
    this->__PVT__user_ifc_uart_error_status_register_D_IN 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (0xcU == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
            & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))
            ? ((IData)(this->__PVT__user_ifc_uart_error_status_register) 
               & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                          >> 8U))) : ((((0U == this->__PVT__bitdata___05Fh17072) 
                                        & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
                                       << 3U) | ((4U 
                                                  & ((~ (IData)(this->__PVT__user_ifc_uart_rRecvData)) 
                                                     << 2U)) 
                                                 | ((2U 
                                                     & ((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                                                        << 1U)) 
                                                    | (((1U 
                                                         == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                        & (~ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169))) 
                                                       | ((2U 
                                                           == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                          & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169)))))));
    this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))));
    this->__PVT__user_ifc_uart_rg_delay_count_D_IN 
        = (0xffffU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                       & (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                           | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                          | (IData)(this->__PVT__user_ifc_uart_out_enable)))
                       ? (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                           | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297))
                           ? 0U : ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rg_delay_count)))
                       : (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                           & (~ (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)))
                           ? ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rg_delay_count))
                           : 0U)));
    this->__PVT__user_ifc_uart_rXmitCellCount_D_IN 
        = (((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
               | (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2) 
                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                  & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5) 
                 & (7U == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)))) 
             | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                 & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                & (IData)(this->__PVT__user_ifc_uart_out_enable))) 
            | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit) 
                & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))) 
               & (((0U == (IData)(this->__PVT__user_ifc_rg_stopbits)) 
                   | (2U == (IData)(this->__PVT__user_ifc_rg_stopbits))) 
                  | (1U == (IData)(this->__PVT__user_ifc_rg_stopbits)))))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))));
    this->__PVT__user_ifc_uart_rXmitState_D_IN = ((
                                                   (((((((IData)(this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1) 
                                                         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)) 
                                                        | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                                                       | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                                                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
                                                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
                                                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2))
                                                   ? 
                                                  ((IData)(this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1)
                                                    ? 2U
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)
                                                     ? 
                                                    ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)
                                                      ? 
                                                     ((IData)(this->__PVT__user_ifc_uart_out_enable)
                                                       ? 2U
                                                       : 1U)
                                                      : 0U)
                                                     : 
                                                    ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)
                                                      ? 
                                                     ((0xfU 
                                                       == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                       ? 3U
                                                       : 2U)
                                                      : 
                                                     ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)
                                                       ? 
                                                      ((0xfU 
                                                        == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                        ? 
                                                       (((IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317) 
                                                         & (0U 
                                                            == (IData)(this->__PVT__user_ifc_rg_parity)))
                                                         ? 5U
                                                         : 
                                                        (((IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317) 
                                                          & (0U 
                                                             != (IData)(this->__PVT__user_ifc_rg_parity)))
                                                          ? 8U
                                                          : 4U))
                                                        : 3U)
                                                       : 
                                                      ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)
                                                        ? 
                                                       ((0xfU 
                                                         == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                         ? 5U
                                                         : 8U)
                                                        : 
                                                       ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)
                                                         ? 
                                                        (((0xfU 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                          & (0U 
                                                             == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                          ? 0U
                                                          : 
                                                         (((0xfU 
                                                            == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                           & (2U 
                                                              == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                           ? 7U
                                                           : 
                                                          (((0xfU 
                                                             == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                            & (1U 
                                                               == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                            ? 6U
                                                            : 5U)))
                                                         : 
                                                        ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)
                                                          ? 
                                                         ((7U 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                           ? 0U
                                                           : 6U)
                                                          : 
                                                         ((0xfU 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                           ? 0U
                                                           : 7U))))))))
                                                   : 
                                                  ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit)
                                                    ? 3U
                                                    : 0U));
    this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas 
        = (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
            & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
           | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
              & (IData)(this->__PVT__user_ifc_uart_out_enable)));
    this->__PVT__status___05Fh37064 = (((IData)(this->__PVT__user_ifc_uart_error_status_register) 
                                        << 4U) | ((8U 
                                                   & ((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                                                      << 3U)) 
                                                  | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                      << 2U) 
                                                     | ((2U 
                                                         & ((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full)) 
                                                            << 1U)) 
                                                        | ((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                                                           & (0U 
                                                              == (IData)(this->__PVT__user_ifc_uart_rXmitState)))))));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift 
        = ((~ (IData)(this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas)) 
           & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load 
        = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
           & (IData)(this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((((((0xcU 
                                                                     == 
                                                                     (0x1fU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    & (0U 
                                                                       == 
                                                                       (3U 
                                                                        & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                                   | (8U 
                                                                      == 
                                                                      (0x1fU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  | ((0x14U 
                                                                      == 
                                                                      (0x1fU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                     & (1U 
                                                                        == 
                                                                        (3U 
                                                                         & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))) 
                                                                 | (0U 
                                                                    == 
                                                                    (0x1fU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                | ((0x10U 
                                                                    == 
                                                                    (0x1fU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))) 
                                                               | ((0x18U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (0U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((0xcU 
                                                                 == 
                                                                 (0x1fU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__status___05Fh37064) 
                                                                 << 0x18U) 
                                                                | (((IData)(this->__PVT__status___05Fh37064) 
                                                                    << 0x10U) 
                                                                   | (((IData)(this->__PVT__status___05Fh37064) 
                                                                       << 8U) 
                                                                      | (IData)(this->__PVT__status___05Fh37064))))
                                                                : 
                                                               ((8U 
                                                                 == 
                                                                 (0x1fU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U))))
                                                                 ? 
                                                                ((0x1fU 
                                                                  >= 
                                                                  (0x3fU 
                                                                   & ((IData)(0x20U) 
                                                                      - (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)
                                                                    ? this->__PVT__user_ifc_uart_fifoRecv_D_OUT
                                                                    : 0U) 
                                                                  >> 
                                                                  (0x3fU 
                                                                   & ((IData)(0x20U) 
                                                                      - (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                                  : 0U)
                                                                 : 
                                                                (((0x14U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__user_ifc_rg_charsize) 
                                                                   << 0x15U) 
                                                                  | (((IData)(this->__PVT__user_ifc_rg_parity) 
                                                                      << 0x13U) 
                                                                     | (((IData)(this->__PVT__user_ifc_rg_stopbits) 
                                                                         << 0x11U) 
                                                                        | (((IData)(this->__PVT__user_ifc_rg_charsize) 
                                                                            << 5U) 
                                                                           | (((IData)(this->__PVT__user_ifc_rg_parity) 
                                                                               << 3U) 
                                                                              | ((IData)(this->__PVT__user_ifc_rg_stopbits) 
                                                                                << 1U))))))
                                                                  : 
                                                                 ((0U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__user_ifc_baud_value) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__user_ifc_baud_value))
                                                                   : 
                                                                  (((0x10U 
                                                                     == 
                                                                     (0x1fU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    & (1U 
                                                                       == 
                                                                       (3U 
                                                                        & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                    ? 
                                                                   (((IData)(this->__PVT__user_ifc_rg_delay_control) 
                                                                     << 0x10U) 
                                                                    | (IData)(this->__PVT__user_ifc_rg_delay_control))
                                                                    : 
                                                                   (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                     << 0x18U) 
                                                                    | (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                        << 0x10U) 
                                                                       | (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                           << 8U) 
                                                                          | (IData)(this->__PVT__user_ifc_rg_interrupt_en))))))))))));
}

VL_INLINE_OPT void VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__7(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__7\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_1.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_2.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_3.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_4.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_5.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_6.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_7.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_8.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_9.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_10.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_11.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_12.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_13.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_14.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_15.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_16.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_17.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_18.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_19.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_20.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_21.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_22.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_23.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_24.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_25.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_26.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_27.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_28.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_29.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_30.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                          & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)) 
                         & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))))) {
            VL_WRITEF("Error: \"devices/uart_v2/RS232_modified.bsv\", line 500, column 9: (R0002)\n  Conflict-free rules RL_user_ifc_uart_receive_buffer_shift and\n  RL_user_ifc_uart_receive_wait_for_start_bit called conflicting methods read\n  and write of module instance user_ifc_uart_vrRecvBuffer_31.\n\n");
        }
    }
}

VL_INLINE_OPT void VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart1__5(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart1__5\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0 = 0U;
    this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0 = 0U;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__tail;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__head;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__tail;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__head;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart1.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)) 
                         & (IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart1.user_ifc_uart_fifoRecv.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart1.user_ifc_uart_fifoRecv.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart1.user_ifc_uart_fifoXmit.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full)) 
                         & (IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart1.user_ifc_uart_fifoXmit.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x10U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_delay_control 
                = (0xffffU & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                      >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_rg_delay_control = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_baud_value = (0xffffU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_baud_value = 0xa3U;
    }
    this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state 
        = ((IData)(vlTOPp->RST_N) ? ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)
                                      ? (0xffffU & 
                                         ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state) 
                                          + ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)
                                              ? 1U : 0U)))
                                      : 0U) : 0U);
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x18U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_interrupt_en = 
                (0xffU & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                  >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_rg_interrupt_en = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit) {
            this->__PVT__user_ifc_uart_rRecvParity 
                = this->__PVT__user_ifc_uart_rRecvData;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvParity = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
               & (0xcU == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
              & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit))) {
            this->__PVT__user_ifc_uart_error_status_register 
                = this->__PVT__user_ifc_uart_error_status_register_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_error_status_register = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                     & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
               | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit))) {
            this->__PVT__user_ifc_uart_rXmitState = this->__PVT__user_ifc_uart_rXmitState_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitState = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas) 
             | ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time) 
                  & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))) 
                 & ((~ (IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317)) 
                    | (0U != (IData)(this->__PVT__user_ifc_rg_parity)))) 
                & ((~ (IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317)) 
                   | (0U == (IData)(this->__PVT__user_ifc_rg_parity)))))) {
            this->__PVT__user_ifc_uart_rXmitBitCount 
                = this->__PVT__user_ifc_uart_rXmitBitCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitBitCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                   | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                  | (IData)(this->__PVT__user_ifc_uart_out_enable))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                 & (~ (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit))) {
            this->__PVT__user_ifc_uart_rg_delay_count 
                = this->__PVT__user_ifc_uart_rg_delay_count_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rg_delay_count = 0U;
    }
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                            << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                       << 2U) | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
        this->__PVT__user_ifc_uart_fifoRecv_D_OUT = 
            ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
              ? this->__PVT__user_ifc_uart_fifoRecv__DOT__arr
             [this->__PVT__user_ifc_uart_fifoRecv__DOT__head]
              : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
            this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                = this->__PVT__bitdata___05Fh17072;
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                  << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                    = ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
                        ? this->__PVT__user_ifc_uart_fifoRecv__DOT__arr
                       [this->__PVT__user_ifc_uart_fifoRecv__DOT__head]
                        : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                      << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                    this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                        = this->__PVT__bitdata___05Fh17072;
                }
            }
        }
    }
    if (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
         & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
             & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)) 
                & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
               & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full))))) {
        this->user_ifc_uart_fifoXmit__DOT____Vlvbound2 
            = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                       >> 4U));
        if ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))) {
            this->__Vdlyvval__user_ifc_uart_fifoXmit__DOT__arr__v0 
                = this->user_ifc_uart_fifoXmit__DOT____Vlvbound2;
            this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__user_ifc_uart_fifoXmit__DOT__arr__v0 
                = this->__PVT__user_ifc_uart_fifoXmit__DOT__tail;
        }
    }
    if (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
         & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
             & (~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas)) 
                & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)) 
               & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full))))) {
        this->user_ifc_uart_fifoRecv__DOT____Vlvbound2 
            = this->__PVT__bitdata___05Fh17072;
        if ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))) {
            this->__Vdlyvval__user_ifc_uart_fifoRecv__DOT__arr__v0 
                = this->user_ifc_uart_fifoRecv__DOT____Vlvbound2;
            this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__user_ifc_uart_fifoRecv__DOT__arr__v0 
                = this->__PVT__user_ifc_uart_fifoRecv__DOT__tail;
        }
    }
    if (vlTOPp->RST_N) {
        if ((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit)) 
               | (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_rRecvState = 
                ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)
                  ? ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                      ? 0U : 1U) : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell)
                                     ? ((4U == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))
                                         ? ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                                             ? 0U : 2U)
                                         : 1U) : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)
                                                   ? 
                                                  (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                    == (IData)(this->__PVT__user_ifc_rg_charsize))
                                                    ? 
                                                   ((0U 
                                                     == (IData)(this->__PVT__user_ifc_rg_parity))
                                                     ? 
                                                    ((0U 
                                                      == (IData)(this->__PVT__user_ifc_rg_stopbits))
                                                      ? 6U
                                                      : 5U)
                                                     : 4U)
                                                    : 
                                                   (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                     == 
                                                     (0x3fU 
                                                      & ((IData)(1U) 
                                                         + (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                     ? 
                                                    (((0U 
                                                       == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                      | (0U 
                                                         == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                      ? 6U
                                                      : 5U)
                                                     : 
                                                    (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                      == 
                                                      (0x3fU 
                                                       & ((IData)(2U) 
                                                          + (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                      ? 6U
                                                      : 3U)))
                                                   : 
                                                  ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit)
                                                    ? 
                                                   ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                                                     ? 2U
                                                     : 0U)
                                                    : 
                                                   ((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)
                                                     ? 0U
                                                     : 
                                                    (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit) 
                                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))
                                                      ? 2U
                                                      : 0U))))));
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvState = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                 << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__head = 0U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail = 0U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 1U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                    << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
                    = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail;
                this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
                    = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                      << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
                            = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head;
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty 
                            = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head) 
                               == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                            this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                                if (this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full) {
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
                                        = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail;
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 0U;
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail) 
                                           != (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__head = 0U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail = 0U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 1U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                 << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__head = 0U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail = 0U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 1U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                    << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
                    = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail;
                this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
                    = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                      << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
                            = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head;
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty 
                            = ((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head) 
                               == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                            this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                                if (this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full) {
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
                                        = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail;
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 0U;
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail) 
                                           != (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__head = 0U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail = 0U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 1U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_0 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_0 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2)) 
               | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command))) {
            this->__PVT__user_ifc_uart_rXmitDataOut 
                = (1U & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit) 
                          | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time))
                          ? (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_0)
                          : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)
                              ? ((1U == (IData)(this->__PVT__user_ifc_rg_parity))
                                  ? (~ (IData)(this->__PVT__user_ifc_uart_rXmitParity))
                                  : ((2U == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                     & (IData)(this->__PVT__user_ifc_uart_rXmitParity)))
                              : ((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                                 & ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2) 
                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
                                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
                                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command))))));
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitDataOut = 1U;
    }
    if (this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0) {
        this->__PVT__user_ifc_uart_fifoRecv__DOT__arr[this->__Vdlyvdim0__user_ifc_uart_fifoRecv__DOT__arr__v0] 
            = this->__Vdlyvval__user_ifc_uart_fifoRecv__DOT__arr__v0;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__tail 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__tail 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__head 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__head;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full;
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)))) {
            this->__PVT__user_ifc_uart_rXmitCellCount 
                = this->__PVT__user_ifc_uart_rXmitCellCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitCellCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata))) 
              & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control))) {
            this->__PVT__user_ifc_uart_out_enable = 
                (1U & (~ (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                           & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata))) 
                          & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297))));
        }
    } else {
        this->__PVT__user_ifc_uart_out_enable = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas) 
             | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift)))) {
            this->__PVT__user_ifc_uart_rRecvBitCount 
                = this->__PVT__user_ifc_uart_rRecvBitCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvBitCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_stopbits = (3U 
                                                 & (IData)(
                                                           (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 5U)));
        }
    } else {
        this->__PVT__user_ifc_rg_stopbits = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_charsize = (0x3fU 
                                                 & (IData)(
                                                           (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 9U)));
        }
    } else {
        this->__PVT__user_ifc_rg_charsize = 8U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)))) {
            this->__PVT__user_ifc_uart_rRecvCellCount 
                = this->__PVT__user_ifc_uart_rRecvCellCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvCellCount = 0U;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))));
    this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))));
    this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_1 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_parity = (3U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 7U)));
        }
    } else {
        this->__PVT__user_ifc_rg_parity = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) {
            this->__PVT__user_ifc_uart_rXmitParity 
                = (1U & ((((((((((((IData)(this->__PVT__z___05Fh27968) 
                                   ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                      >> 0x15U)) ^ 
                                  (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                   >> 0x16U)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0x17U)) 
                                ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                   >> 0x18U)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0x19U)) 
                              ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                 >> 0x1aU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                               >> 0x1bU)) 
                            ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                               >> 0x1cU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                             >> 0x1dU)) 
                          ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                             >> 0x1eU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                           >> 0x1fU)));
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitParity = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_0 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? this->__PVT__user_ifc_uart_fifoXmit_D_OUT
                    : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_1)));
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297 
        = ((IData)(this->__PVT__user_ifc_uart_rg_delay_count) 
           == (IData)(this->__PVT__user_ifc_rg_delay_control));
    this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30 
        = ((0xffffU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state))) 
           < (IData)(this->__PVT__user_ifc_baud_value));
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_capture_read_request = 
        ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
         & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_capture_write_request 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317 
        = ((IData)(this->__PVT__user_ifc_uart_rXmitBitCount) 
           == (0x3fU & ((IData)(this->__PVT__user_ifc_rg_charsize) 
                        - (IData)(1U))));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit 
        = ((4U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit 
        = ((5U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell 
        = ((1U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample 
        = (((2U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
            & (0xfU == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit 
        = ((4U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit 
        = ((2U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time 
        = ((3U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit 
        = ((8U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit 
        = ((5U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5 
        = ((6U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2 
        = ((7U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command 
        = ((0U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control 
        = ((1U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_2 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_2 = 0U;
    }
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((((4U 
                                                 == 
                                                 (0x1fU 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                             >> 5U)))) 
                                                | ((0U 
                                                    == 
                                                    (0x1fU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U)))) 
                                                   & (1U 
                                                      == 
                                                      (3U 
                                                       & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                               | ((0x10U 
                                                   == 
                                                   (0x1fU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  & (1U 
                                                     == 
                                                     (3U 
                                                      & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                              | ((0x14U 
                                                  == 
                                                  (0x1fU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))) 
                                                 & (1U 
                                                    == 
                                                    (3U 
                                                     & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                             | (((0x18U 
                                                  == 
                                                  (0x1fU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))) 
                                                 | (0xcU 
                                                    == 
                                                    (0x1fU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (0U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_1 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 1U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_2)));
    }
    this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
            & (8U == (0x1fU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                       >> 5U))))) & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata));
    this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit 
        = (((6U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
            & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)));
    this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
           | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command));
    this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
           & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297));
    this->__PVT__user_ifc_uart_rXmitBitCount_D_IN = 
        ((IData)(this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas)
          ? 0U : (0x3fU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rXmitBitCount))));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_3 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_3 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_2 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 2U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_3)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_4 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_4 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_3 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 3U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_4)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_5 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_5 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_4 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 4U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_5)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_6 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_6 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_5 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 5U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_6)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_7 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_7 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_6 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 6U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_7)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_8 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_8 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_7 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 7U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_8)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_9 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_9 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_8 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 8U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_9)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_10 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_10 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_9 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 9U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_10)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_11 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_11 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_10 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xaU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_11)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_12 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_12 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_11 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xbU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_12)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_13 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_13 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_12 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xcU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_13)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_14 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_14 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_13 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xdU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_14)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_15 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_15 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_14 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xeU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_15)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_16 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_16 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_15 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xfU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_16)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_17 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_17 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_16 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x10U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_17)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_18 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_18 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_17 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x11U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_18)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_19 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_19 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_18 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x12U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_19)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_20 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_20 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_19 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x13U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_20)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_21 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_21 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_20 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x14U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_21)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_22 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_22 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_21 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x15U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_22)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_23 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_23 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_22 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x16U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_23)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_24 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_24 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_23 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x17U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_24)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_25 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_25 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_24 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x18U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_25)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_26 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_26 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_25 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x19U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_26)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_27 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_27 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_26 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1aU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_27)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_28 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_28 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_27 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1bU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_28)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_29 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_29 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_28 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1cU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_29)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_30 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_30 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_29 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1dU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_30)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_31 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_rRecvData));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_31 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_30 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1eU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_31)));
    }
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift 
        = ((3U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit 
        = ((0U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__user_ifc_uart_rRecvCellCount_D_IN 
        = (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell) 
              & (4U == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)) 
            | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))));
    this->__PVT__bitdata___05Fh17072 = (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31) 
                                         << 0x1fU) 
                                        | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30) 
                                            << 0x1eU) 
                                           | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29) 
                                               << 0x1dU) 
                                              | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28) 
                                                  << 0x1cU) 
                                                 | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27) 
                                                     << 0x1bU) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26) 
                                                        << 0x1aU) 
                                                       | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25) 
                                                           << 0x19U) 
                                                          | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24) 
                                                              << 0x18U) 
                                                             | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23) 
                                                                 << 0x17U) 
                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22) 
                                                                    << 0x16U) 
                                                                   | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21) 
                                                                       << 0x15U) 
                                                                      | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20) 
                                                                          << 0x14U) 
                                                                         | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19) 
                                                                             << 0x13U) 
                                                                            | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18) 
                                                                                << 0x12U) 
                                                                               | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17) 
                                                                                << 0x11U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16) 
                                                                                << 0x10U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15) 
                                                                                << 0xfU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14) 
                                                                                << 0xeU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13) 
                                                                                << 0xdU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12) 
                                                                                << 0xcU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11) 
                                                                                << 0xbU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10) 
                                                                                << 0xaU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9) 
                                                                                << 9U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8) 
                                                                                << 8U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7) 
                                                                                << 7U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6) 
                                                                                << 6U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5) 
                                                                                << 5U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4) 
                                                                                << 4U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3) 
                                                                                << 3U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2) 
                                                                                << 2U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0))))))))))))))))))))))))))))))));
    this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169 
        = (((((((((((((((((((((((((((((((((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0) 
                                          ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1)) 
                                         ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2)) 
                                        ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3)) 
                                       ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4)) 
                                      ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5)) 
                                     ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6)) 
                                    ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7)) 
                                   ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8)) 
                                  ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9)) 
                                 ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10)) 
                                ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11)) 
                               ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12)) 
                              ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13)) 
                             ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14)) 
                            ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15)) 
                           ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16)) 
                          ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17)) 
                         ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18)) 
                        ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19)) 
                       ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20)) 
                      ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21)) 
                     ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22)) 
                    ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23)) 
                   ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24)) 
                  ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25)) 
                 ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26)) 
                ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27)) 
               ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28)) 
              ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29)) 
             ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30)) 
            ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31)) 
           ^ (IData)(this->__PVT__user_ifc_uart_rRecvParity));
    this->__PVT__user_ifc_uart_rRecvData = (1U & ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (((((3U 
                                                         == 
                                                         (3U 
                                                          & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)) 
                                                        | (2U 
                                                           == 
                                                           (3U 
                                                            & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config))) 
                                                       | (0U 
                                                          == 
                                                          (3U 
                                                           & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config))) 
                                                      | (1U 
                                                         == 
                                                         (3U 
                                                          & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config))) 
                                                     & ((((3U 
                                                           == 
                                                           (3U 
                                                            & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)) 
                                                          | (2U 
                                                             == 
                                                             (3U 
                                                              & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config))) 
                                                         | (0U 
                                                            == 
                                                            (3U 
                                                             & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config))) 
                                                        | (IData)(vlTOPp->iocell_io_io7_cell_in_in)))));
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_31 
            = (1U & ((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)) 
                     | (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                        >> 0x1fU)));
    }
    this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
           & (IData)(this->__PVT__user_ifc_uart_rRecvData));
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                            << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                       << 2U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
        this->__PVT__user_ifc_uart_fifoXmit_D_OUT = 
            ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
              ? this->__PVT__user_ifc_uart_fifoXmit__DOT__arr
             [this->__PVT__user_ifc_uart_fifoXmit__DOT__head]
              : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
            this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                           >> 4U));
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                  << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                    = ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
                        ? this->__PVT__user_ifc_uart_fifoXmit__DOT__arr
                       [this->__PVT__user_ifc_uart_fifoXmit__DOT__head]
                        : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                      << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                    this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                        = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                   >> 4U));
                }
            }
        }
    }
    this->__PVT__user_ifc_uart_rRecvBitCount_D_IN = 
        ((IData)(this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas)
          ? 0U : (0x3fU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rRecvBitCount))));
    this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
           & (4U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))));
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__head 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__head;
    if (this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0) {
        this->__PVT__user_ifc_uart_fifoXmit__DOT__arr[this->__Vdlyvdim0__user_ifc_uart_fifoXmit__DOT__arr__v0] 
            = this->__Vdlyvval__user_ifc_uart_fifoXmit__DOT__arr__v0;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata;
    this->__PVT__z___05Fh27968 = (1U & ((((((((((((
                                                   ((((((((this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                           ^ 
                                                           (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                            >> 1U)) 
                                                          ^ 
                                                          (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                           >> 2U)) 
                                                         ^ 
                                                         (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                          >> 3U)) 
                                                        ^ 
                                                        (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                         >> 4U)) 
                                                       ^ 
                                                       (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                        >> 5U)) 
                                                      ^ 
                                                      (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                       >> 6U)) 
                                                     ^ 
                                                     (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                      >> 7U)) 
                                                    ^ 
                                                    (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                     >> 8U)) 
                                                   ^ 
                                                   (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                    >> 9U)) 
                                                  ^ 
                                                  (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                   >> 0xaU)) 
                                                 ^ 
                                                 (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                  >> 0xbU)) 
                                                ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                   >> 0xcU)) 
                                               ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                  >> 0xdU)) 
                                              ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0xeU)) 
                                             ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                >> 0xfU)) 
                                            ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                               >> 0x10U)) 
                                           ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                              >> 0x11U)) 
                                          ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                             >> 0x12U)) 
                                         ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                            >> 0x13U)) 
                                        ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                           >> 0x14U)));
    this->__PVT__user_ifc_uart_error_status_register_D_IN 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (0xcU == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
            & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))
            ? ((IData)(this->__PVT__user_ifc_uart_error_status_register) 
               & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                          >> 8U))) : ((((0U == this->__PVT__bitdata___05Fh17072) 
                                        & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
                                       << 3U) | ((4U 
                                                  & ((~ (IData)(this->__PVT__user_ifc_uart_rRecvData)) 
                                                     << 2U)) 
                                                 | ((2U 
                                                     & ((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                                                        << 1U)) 
                                                    | (((1U 
                                                         == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                        & (~ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169))) 
                                                       | ((2U 
                                                           == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                          & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169)))))));
    this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))));
    this->__PVT__user_ifc_uart_rg_delay_count_D_IN 
        = (0xffffU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                       & (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                           | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                          | (IData)(this->__PVT__user_ifc_uart_out_enable)))
                       ? (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                           | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297))
                           ? 0U : ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rg_delay_count)))
                       : (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                           & (~ (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)))
                           ? ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rg_delay_count))
                           : 0U)));
    this->__PVT__user_ifc_uart_rXmitCellCount_D_IN 
        = (((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
               | (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2) 
                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                  & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5) 
                 & (7U == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)))) 
             | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                 & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                & (IData)(this->__PVT__user_ifc_uart_out_enable))) 
            | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit) 
                & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))) 
               & (((0U == (IData)(this->__PVT__user_ifc_rg_stopbits)) 
                   | (2U == (IData)(this->__PVT__user_ifc_rg_stopbits))) 
                  | (1U == (IData)(this->__PVT__user_ifc_rg_stopbits)))))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))));
    this->__PVT__user_ifc_uart_rXmitState_D_IN = ((
                                                   (((((((IData)(this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1) 
                                                         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)) 
                                                        | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                                                       | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                                                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
                                                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
                                                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2))
                                                   ? 
                                                  ((IData)(this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1)
                                                    ? 2U
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)
                                                     ? 
                                                    ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)
                                                      ? 
                                                     ((IData)(this->__PVT__user_ifc_uart_out_enable)
                                                       ? 2U
                                                       : 1U)
                                                      : 0U)
                                                     : 
                                                    ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)
                                                      ? 
                                                     ((0xfU 
                                                       == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                       ? 3U
                                                       : 2U)
                                                      : 
                                                     ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)
                                                       ? 
                                                      ((0xfU 
                                                        == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                        ? 
                                                       (((IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317) 
                                                         & (0U 
                                                            == (IData)(this->__PVT__user_ifc_rg_parity)))
                                                         ? 5U
                                                         : 
                                                        (((IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317) 
                                                          & (0U 
                                                             != (IData)(this->__PVT__user_ifc_rg_parity)))
                                                          ? 8U
                                                          : 4U))
                                                        : 3U)
                                                       : 
                                                      ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)
                                                        ? 
                                                       ((0xfU 
                                                         == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                         ? 5U
                                                         : 8U)
                                                        : 
                                                       ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)
                                                         ? 
                                                        (((0xfU 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                          & (0U 
                                                             == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                          ? 0U
                                                          : 
                                                         (((0xfU 
                                                            == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                           & (2U 
                                                              == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                           ? 7U
                                                           : 
                                                          (((0xfU 
                                                             == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                            & (1U 
                                                               == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                            ? 6U
                                                            : 5U)))
                                                         : 
                                                        ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)
                                                          ? 
                                                         ((7U 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                           ? 0U
                                                           : 6U)
                                                          : 
                                                         ((0xfU 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                           ? 0U
                                                           : 7U))))))))
                                                   : 
                                                  ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit)
                                                    ? 3U
                                                    : 0U));
    this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas 
        = (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
            & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
           | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
              & (IData)(this->__PVT__user_ifc_uart_out_enable)));
    this->__PVT__status___05Fh37064 = (((IData)(this->__PVT__user_ifc_uart_error_status_register) 
                                        << 4U) | ((8U 
                                                   & ((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                                                      << 3U)) 
                                                  | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                      << 2U) 
                                                     | ((2U 
                                                         & ((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full)) 
                                                            << 1U)) 
                                                        | ((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                                                           & (0U 
                                                              == (IData)(this->__PVT__user_ifc_uart_rXmitState)))))));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift 
        = ((~ (IData)(this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas)) 
           & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load 
        = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
           & (IData)(this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((((((0xcU 
                                                                     == 
                                                                     (0x1fU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    & (0U 
                                                                       == 
                                                                       (3U 
                                                                        & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                                   | (8U 
                                                                      == 
                                                                      (0x1fU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  | ((0x14U 
                                                                      == 
                                                                      (0x1fU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                     & (1U 
                                                                        == 
                                                                        (3U 
                                                                         & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))) 
                                                                 | (0U 
                                                                    == 
                                                                    (0x1fU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                | ((0x10U 
                                                                    == 
                                                                    (0x1fU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))) 
                                                               | ((0x18U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (0U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((0xcU 
                                                                 == 
                                                                 (0x1fU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__status___05Fh37064) 
                                                                 << 0x18U) 
                                                                | (((IData)(this->__PVT__status___05Fh37064) 
                                                                    << 0x10U) 
                                                                   | (((IData)(this->__PVT__status___05Fh37064) 
                                                                       << 8U) 
                                                                      | (IData)(this->__PVT__status___05Fh37064))))
                                                                : 
                                                               ((8U 
                                                                 == 
                                                                 (0x1fU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U))))
                                                                 ? 
                                                                ((0x1fU 
                                                                  >= 
                                                                  (0x3fU 
                                                                   & ((IData)(0x20U) 
                                                                      - (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)
                                                                    ? this->__PVT__user_ifc_uart_fifoRecv_D_OUT
                                                                    : 0U) 
                                                                  >> 
                                                                  (0x3fU 
                                                                   & ((IData)(0x20U) 
                                                                      - (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                                  : 0U)
                                                                 : 
                                                                (((0x14U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__user_ifc_rg_charsize) 
                                                                   << 0x15U) 
                                                                  | (((IData)(this->__PVT__user_ifc_rg_parity) 
                                                                      << 0x13U) 
                                                                     | (((IData)(this->__PVT__user_ifc_rg_stopbits) 
                                                                         << 0x11U) 
                                                                        | (((IData)(this->__PVT__user_ifc_rg_charsize) 
                                                                            << 5U) 
                                                                           | (((IData)(this->__PVT__user_ifc_rg_parity) 
                                                                               << 3U) 
                                                                              | ((IData)(this->__PVT__user_ifc_rg_stopbits) 
                                                                                << 1U))))))
                                                                  : 
                                                                 ((0U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__user_ifc_baud_value) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__user_ifc_baud_value))
                                                                   : 
                                                                  (((0x10U 
                                                                     == 
                                                                     (0x1fU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    & (1U 
                                                                       == 
                                                                       (3U 
                                                                        & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                    ? 
                                                                   (((IData)(this->__PVT__user_ifc_rg_delay_control) 
                                                                     << 0x10U) 
                                                                    | (IData)(this->__PVT__user_ifc_rg_delay_control))
                                                                    : 
                                                                   (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                     << 0x18U) 
                                                                    | (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                        << 0x10U) 
                                                                       | (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                           << 8U) 
                                                                          | (IData)(this->__PVT__user_ifc_rg_interrupt_en))))))))))));
}

VL_INLINE_OPT void VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart2__6(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkuart::_sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart2__6\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0 = 0U;
    this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0 = 0U;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__tail;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__head;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__tail;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__head;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata;
    this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full 
        = this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata;
    this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full 
        = this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.uart_cluster.uart2.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)) 
                         & (IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart2.user_ifc_uart_fifoRecv.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart2.user_ifc_uart_fifoRecv.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart2.user_ifc_uart_fifoXmit.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full)) 
                         & (IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas)))) {
            VL_WRITEF("Warning: SizedFIFO: %NmkSoc.uart_cluster.uart2.user_ifc_uart_fifoXmit.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x10U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_delay_control 
                = (0xffffU & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                      >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_rg_delay_control = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_baud_value = (0xffffU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_baud_value = 0xa3U;
    }
    this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state 
        = ((IData)(vlTOPp->RST_N) ? ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)
                                      ? (0xffffU & 
                                         ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state) 
                                          + ((IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)
                                              ? 1U : 0U)))
                                      : 0U) : 0U);
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x18U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_interrupt_en = 
                (0xffU & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                  >> 4U)));
        }
    } else {
        this->__PVT__user_ifc_rg_interrupt_en = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit) {
            this->__PVT__user_ifc_uart_rRecvParity 
                = this->__PVT__user_ifc_uart_rRecvData;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvParity = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
               & (0xcU == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
              & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit))) {
            this->__PVT__user_ifc_uart_error_status_register 
                = this->__PVT__user_ifc_uart_error_status_register_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_error_status_register = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                     & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
               | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit))) {
            this->__PVT__user_ifc_uart_rXmitState = this->__PVT__user_ifc_uart_rXmitState_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitState = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas) 
             | ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time) 
                  & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))) 
                 & ((~ (IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317)) 
                    | (0U != (IData)(this->__PVT__user_ifc_rg_parity)))) 
                & ((~ (IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317)) 
                   | (0U == (IData)(this->__PVT__user_ifc_rg_parity)))))) {
            this->__PVT__user_ifc_uart_rXmitBitCount 
                = this->__PVT__user_ifc_uart_rXmitBitCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitBitCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                   | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                  | (IData)(this->__PVT__user_ifc_uart_out_enable))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                 & (~ (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit))) {
            this->__PVT__user_ifc_uart_rg_delay_count 
                = this->__PVT__user_ifc_uart_rg_delay_count_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rg_delay_count = 0U;
    }
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                            << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                       << 2U) | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
        this->__PVT__user_ifc_uart_fifoRecv_D_OUT = 
            ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
              ? this->__PVT__user_ifc_uart_fifoRecv__DOT__arr
             [this->__PVT__user_ifc_uart_fifoRecv__DOT__head]
              : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
            this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                = this->__PVT__bitdata___05Fh17072;
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                  << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                    = ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
                        ? this->__PVT__user_ifc_uart_fifoRecv__DOT__arr
                       [this->__PVT__user_ifc_uart_fifoRecv__DOT__head]
                        : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                      << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                    this->__PVT__user_ifc_uart_fifoRecv_D_OUT 
                        = this->__PVT__bitdata___05Fh17072;
                }
            }
        }
    }
    if (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
         & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
             & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)) 
                & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
               & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full))))) {
        this->user_ifc_uart_fifoXmit__DOT____Vlvbound2 
            = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                       >> 4U));
        if ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))) {
            this->__Vdlyvval__user_ifc_uart_fifoXmit__DOT__arr__v0 
                = this->user_ifc_uart_fifoXmit__DOT____Vlvbound2;
            this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__user_ifc_uart_fifoXmit__DOT__arr__v0 
                = this->__PVT__user_ifc_uart_fifoXmit__DOT__tail;
        }
    }
    if (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
         & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
             & (~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))) 
            | (((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas)) 
                & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)) 
               & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full))))) {
        this->user_ifc_uart_fifoRecv__DOT____Vlvbound2 
            = this->__PVT__bitdata___05Fh17072;
        if ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))) {
            this->__Vdlyvval__user_ifc_uart_fifoRecv__DOT__arr__v0 
                = this->user_ifc_uart_fifoRecv__DOT____Vlvbound2;
            this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0 = 1U;
            this->__Vdlyvdim0__user_ifc_uart_fifoRecv__DOT__arr__v0 
                = this->__PVT__user_ifc_uart_fifoRecv__DOT__tail;
        }
    }
    if (vlTOPp->RST_N) {
        if ((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit)) 
               | (IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_rRecvState = 
                ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit)
                  ? ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                      ? 0U : 1U) : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell)
                                     ? ((4U == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))
                                         ? ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                                             ? 0U : 2U)
                                         : 1U) : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)
                                                   ? 
                                                  (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                    == (IData)(this->__PVT__user_ifc_rg_charsize))
                                                    ? 
                                                   ((0U 
                                                     == (IData)(this->__PVT__user_ifc_rg_parity))
                                                     ? 
                                                    ((0U 
                                                      == (IData)(this->__PVT__user_ifc_rg_stopbits))
                                                      ? 6U
                                                      : 5U)
                                                     : 4U)
                                                    : 
                                                   (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                     == 
                                                     (0x3fU 
                                                      & ((IData)(1U) 
                                                         + (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                     ? 
                                                    (((0U 
                                                       == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                      | (0U 
                                                         == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                      ? 6U
                                                      : 5U)
                                                     : 
                                                    (((IData)(this->__PVT__user_ifc_uart_rRecvBitCount) 
                                                      == 
                                                      (0x3fU 
                                                       & ((IData)(2U) 
                                                          + (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                      ? 6U
                                                      : 3U)))
                                                   : 
                                                  ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit)
                                                    ? 
                                                   ((IData)(this->__PVT__user_ifc_uart_rRecvData)
                                                     ? 2U
                                                     : 0U)
                                                    : 
                                                   ((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit)
                                                     ? 0U
                                                     : 
                                                    (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit) 
                                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))
                                                      ? 2U
                                                      : 0U))))));
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvState = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                 << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__head = 0U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail = 0U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 1U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
            this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                    << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
                    = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail;
                this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
                    = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                      << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__head 
                            = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head;
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
                        this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty 
                            = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head) 
                               == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                            this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                                if (this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full) {
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail 
                                        = this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail;
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 0U;
                                    this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail) 
                                           != (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__head = 0U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail = 0U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty = 1U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full = 1U;
        this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((0x10U == (0x10U & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                 << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                            << 2U) 
                                           | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                               << 1U) 
                                              | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__head = 0U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail = 0U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 1U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
            this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
        } else {
            if ((0xcU == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                    << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                               << 2U) 
                                              | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
                    = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail;
                this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
                    = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head;
            } else {
                if ((9U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                      << 3U) | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
                } else {
                    if ((8U == (0x1dU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                          << 3U) | 
                                         (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__head 
                            = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head;
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
                        this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty 
                            = ((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head) 
                               == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail));
                    } else {
                        if ((4U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                              << 3U) 
                                             | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                            this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 1U;
                        } else {
                            if ((6U == (0x1eU & (((IData)(this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas) 
                                                  << 3U) 
                                                 | (((IData)(this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit) 
                                                     << 2U) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                        << 1U) 
                                                       | (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty))))))) {
                                if (this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full) {
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail 
                                        = this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail;
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 0U;
                                    this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full 
                                        = ((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail) 
                                           != (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head));
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__head = 0U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail = 0U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty = 1U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full = 1U;
        this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_0 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_0 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                  | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2)) 
               | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
              | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command))) {
            this->__PVT__user_ifc_uart_rXmitDataOut 
                = (1U & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit) 
                          | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time))
                          ? (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_0)
                          : ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)
                              ? ((1U == (IData)(this->__PVT__user_ifc_rg_parity))
                                  ? (~ (IData)(this->__PVT__user_ifc_uart_rXmitParity))
                                  : ((2U == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                     & (IData)(this->__PVT__user_ifc_uart_rXmitParity)))
                              : ((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                                 & ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2) 
                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
                                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
                                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command))))));
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitDataOut = 1U;
    }
    if (this->__Vdlyvset__user_ifc_uart_fifoRecv__DOT__arr__v0) {
        this->__PVT__user_ifc_uart_fifoRecv__DOT__arr[this->__Vdlyvdim0__user_ifc_uart_fifoRecv__DOT__arr__v0] 
            = this->__Vdlyvval__user_ifc_uart_fifoRecv__DOT__arr__v0;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__tail 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__tail;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__not_ring_full;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__ring_empty 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__ring_empty;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__tail 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__tail;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__head 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__head;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__hasodata;
    this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full 
        = this->__Vdly__user_ifc_uart_fifoRecv__DOT__not_ring_full;
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)))) {
            this->__PVT__user_ifc_uart_rXmitCellCount 
                = this->__PVT__user_ifc_uart_rXmitCellCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitCellCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata))) 
              & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control))) {
            this->__PVT__user_ifc_uart_out_enable = 
                (1U & (~ (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                           & (~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata))) 
                          & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297))));
        }
    } else {
        this->__PVT__user_ifc_uart_out_enable = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas) 
             | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit) 
                 | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit)) 
                | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift)))) {
            this->__PVT__user_ifc_uart_rRecvBitCount 
                = this->__PVT__user_ifc_uart_rRecvBitCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvBitCount = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_stopbits = (3U 
                                                 & (IData)(
                                                           (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 5U)));
        }
    } else {
        this->__PVT__user_ifc_rg_stopbits = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_charsize = (0x3fU 
                                                 & (IData)(
                                                           (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 9U)));
        }
    } else {
        this->__PVT__user_ifc_rg_charsize = 8U;
    }
    if (vlTOPp->RST_N) {
        if ((1U & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)))) {
            this->__PVT__user_ifc_uart_rRecvCellCount 
                = this->__PVT__user_ifc_uart_rRecvCellCount_D_IN;
        }
    } else {
        this->__PVT__user_ifc_uart_rRecvCellCount = 0U;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__next_tail 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__tail))));
    this->__PVT__user_ifc_uart_fifoRecv__DOT__next_tail 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__tail))));
    this->__PVT__user_ifc_uart_fifoRecv__DOT__next_head 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__head))));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_1 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
              & (0x14U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__user_ifc_rg_parity = (3U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 7U)));
        }
    } else {
        this->__PVT__user_ifc_rg_parity = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) {
            this->__PVT__user_ifc_uart_rXmitParity 
                = (1U & ((((((((((((IData)(this->__PVT__z___05Fh27968) 
                                   ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                      >> 0x15U)) ^ 
                                  (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                   >> 0x16U)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0x17U)) 
                                ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                   >> 0x18U)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0x19U)) 
                              ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                 >> 0x1aU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                               >> 0x1bU)) 
                            ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                               >> 0x1cU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                             >> 0x1dU)) 
                          ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                             >> 0x1eU)) ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                           >> 0x1fU)));
        }
    } else {
        this->__PVT__user_ifc_uart_rXmitParity = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_0 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? this->__PVT__user_ifc_uart_fifoXmit_D_OUT
                    : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_1)));
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297 
        = ((IData)(this->__PVT__user_ifc_uart_rg_delay_count) 
           == (IData)(this->__PVT__user_ifc_rg_delay_control));
    this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30 
        = ((0xffffU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter__DOT__q_state))) 
           < (IData)(this->__PVT__user_ifc_baud_value));
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_capture_read_request = 
        ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
         & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__uart_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_capture_write_request 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317 
        = ((IData)(this->__PVT__user_ifc_uart_rXmitBitCount) 
           == (0x3fU & ((IData)(this->__PVT__user_ifc_rg_charsize) 
                        - (IData)(1U))));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_parity_bit 
        = ((4U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_stop_first_bit 
        = ((5U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell 
        = ((1U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample 
        = (((2U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
            & (0xfU == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit 
        = ((4U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit 
        = ((2U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time 
        = ((3U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit 
        = ((8U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit 
        = ((5U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5 
        = ((6U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2 
        = ((7U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command 
        = ((0U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control 
        = ((1U == (IData)(this->__PVT__user_ifc_uart_rXmitState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_2 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_2 = 0U;
    }
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((((4U 
                                                 == 
                                                 (0x1fU 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                             >> 5U)))) 
                                                | ((0U 
                                                    == 
                                                    (0x1fU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U)))) 
                                                   & (1U 
                                                      == 
                                                      (3U 
                                                       & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                               | ((0x10U 
                                                   == 
                                                   (0x1fU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  & (1U 
                                                     == 
                                                     (3U 
                                                      & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                              | ((0x14U 
                                                  == 
                                                  (0x1fU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))) 
                                                 & (1U 
                                                    == 
                                                    (3U 
                                                     & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) 
                                             | (((0x18U 
                                                  == 
                                                  (0x1fU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))) 
                                                 | (0xcU 
                                                    == 
                                                    (0x1fU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (0U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_1 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 1U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_2)));
    }
    this->__PVT__user_ifc_uart_fifoRecv_r_deq_whas 
        = (((IData)(this->__PVT__CAN_FIRE_RL_capture_read_request) 
            & (8U == (0x1fU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                       >> 5U))))) & (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata));
    this->__PVT__WILL_FIRE_RL_user_ifc_uart_receive_stop_last_bit 
        = (((6U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
            & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_capture_read_request)));
    this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
           | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command));
    this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
           & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297));
    this->__PVT__user_ifc_uart_rXmitBitCount_D_IN = 
        ((IData)(this->__PVT__user_ifc_uart_pwXmitResetBitCount_whas)
          ? 0U : (0x3fU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rXmitBitCount))));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_3 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_3 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_2 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 2U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_3)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_4 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_4 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_3 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 3U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_4)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_5 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_5 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_4 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 4U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_5)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_6 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_6 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_5 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 5U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_6)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_7 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_7 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_6 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 6U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_7)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_8 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_8 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_7 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 7U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_8)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_9 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_9 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_8 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 8U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_9)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_10 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_10 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_9 = 
            (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                    ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                       >> 9U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_10)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_11 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_11 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_10 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xaU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_11)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_12 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_12 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_11 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xbU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_12)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_13 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_13 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_12 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xcU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_13)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_14 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_14 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_13 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xdU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_14)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_15 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_15 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_14 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xeU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_15)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_16 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_16 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_15 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0xfU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_16)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_17 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_17 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_16 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x10U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_17)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_18 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_18 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_17 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x11U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_18)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_19 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_19 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_18 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x12U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_19)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_20 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_20 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_19 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x13U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_20)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_21 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_21 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_20 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x14U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_21)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_22 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_22 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_21 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x15U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_22)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_23 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_23 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_22 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x16U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_23)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_24 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_24 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_23 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x17U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_24)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_25 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_25 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_24 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x18U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_25)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_26 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_26 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_25 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x19U) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_26)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_27 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_27 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_26 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1aU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_27)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_28 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_28 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_27 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1bU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_28)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_29 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_29 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_28 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1cU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_29)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_30 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_30 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_29 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1dU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_30)));
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
              & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift))) {
            this->__PVT__user_ifc_uart_vrRecvBuffer_31 
                = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift) 
                   & (IData)(this->__PVT__user_ifc_uart_rRecvData));
        }
    } else {
        this->__PVT__user_ifc_uart_vrRecvBuffer_31 = 0U;
    }
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_30 
            = (1U & ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)
                      ? (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                         >> 0x1eU) : (IData)(this->__PVT__user_ifc_uart_vrXmitBuffer_31)));
    }
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_buffer_shift 
        = ((3U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit 
        = ((0U == (IData)(this->__PVT__user_ifc_uart_rRecvState)) 
           & (~ (IData)(this->__PVT__user_ifc_uart_baudGen_rBaudCounter_value_PLUS___05FETC___05F_d30)));
    this->__PVT__user_ifc_uart_rRecvCellCount_D_IN 
        = (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_find_center_of_bit_cell) 
              & (4U == (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_bit_cell_time_for_sample)) 
            | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rRecvCellCount))));
    this->__PVT__bitdata___05Fh17072 = (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31) 
                                         << 0x1fU) 
                                        | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30) 
                                            << 0x1eU) 
                                           | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29) 
                                               << 0x1dU) 
                                              | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28) 
                                                  << 0x1cU) 
                                                 | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27) 
                                                     << 0x1bU) 
                                                    | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26) 
                                                        << 0x1aU) 
                                                       | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25) 
                                                           << 0x19U) 
                                                          | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24) 
                                                              << 0x18U) 
                                                             | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23) 
                                                                 << 0x17U) 
                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22) 
                                                                    << 0x16U) 
                                                                   | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21) 
                                                                       << 0x15U) 
                                                                      | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20) 
                                                                          << 0x14U) 
                                                                         | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19) 
                                                                             << 0x13U) 
                                                                            | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18) 
                                                                                << 0x12U) 
                                                                               | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17) 
                                                                                << 0x11U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16) 
                                                                                << 0x10U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15) 
                                                                                << 0xfU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14) 
                                                                                << 0xeU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13) 
                                                                                << 0xdU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12) 
                                                                                << 0xcU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11) 
                                                                                << 0xbU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10) 
                                                                                << 0xaU) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9) 
                                                                                << 9U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8) 
                                                                                << 8U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7) 
                                                                                << 7U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6) 
                                                                                << 6U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5) 
                                                                                << 5U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4) 
                                                                                << 4U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3) 
                                                                                << 3U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2) 
                                                                                << 2U) 
                                                                                | (((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0))))))))))))))))))))))))))))))));
    this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169 
        = (((((((((((((((((((((((((((((((((IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0) 
                                          ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_1)) 
                                         ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_2)) 
                                        ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_3)) 
                                       ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_4)) 
                                      ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_5)) 
                                     ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_6)) 
                                    ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_7)) 
                                   ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_8)) 
                                  ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_9)) 
                                 ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_10)) 
                                ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_11)) 
                               ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_12)) 
                              ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_13)) 
                             ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_14)) 
                            ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_15)) 
                           ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_16)) 
                          ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_17)) 
                         ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_18)) 
                        ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_19)) 
                       ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_20)) 
                      ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_21)) 
                     ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_22)) 
                    ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_23)) 
                   ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_24)) 
                  ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_25)) 
                 ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_26)) 
                ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_27)) 
               ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_28)) 
              ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_29)) 
             ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_30)) 
            ^ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_31)) 
           ^ (IData)(this->__PVT__user_ifc_uart_rRecvParity));
    this->__PVT__user_ifc_uart_rRecvData = (1U & ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (((((3U 
                                                         == 
                                                         (3U 
                                                          & (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                             >> 4U))) 
                                                        | (2U 
                                                           == 
                                                           (3U 
                                                            & (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                               >> 4U)))) 
                                                       | (0U 
                                                          == 
                                                          (3U 
                                                           & (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                              >> 4U)))) 
                                                      | (1U 
                                                         == 
                                                         (3U 
                                                          & (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                             >> 4U)))) 
                                                     & ((((3U 
                                                           == 
                                                           (3U 
                                                            & (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                               >> 4U))) 
                                                          | (2U 
                                                             == 
                                                             (3U 
                                                              & (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                                 >> 4U)))) 
                                                         | (0U 
                                                            == 
                                                            (3U 
                                                             & (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                                >> 4U)))) 
                                                        | (IData)(vlTOPp->iocell_io_io9_cell_in_in)))));
    if (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift))) {
        this->__PVT__user_ifc_uart_vrXmitBuffer_31 
            = (1U & ((~ (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load)) 
                     | (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                        >> 0x1fU)));
    }
    this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_receive_wait_for_start_bit) 
           & (IData)(this->__PVT__user_ifc_uart_rRecvData));
    if ((0xcU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                            << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                       << 2U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                  << 1U) 
                                                 | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
        this->__PVT__user_ifc_uart_fifoXmit_D_OUT = 
            ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
              ? this->__PVT__user_ifc_uart_fifoXmit__DOT__arr
             [this->__PVT__user_ifc_uart_fifoXmit__DOT__head]
              : 0U);
    } else {
        if ((0xdU == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                           << 2U) | 
                                          (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                            << 1U) 
                                           | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
            this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                           >> 4U));
        } else {
            if ((8U == (0x1dU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                  << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                             << 2U) 
                                            | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                << 1U) 
                                               | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                    = ((0xeU >= (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
                        ? this->__PVT__user_ifc_uart_fifoXmit__DOT__arr
                       [this->__PVT__user_ifc_uart_fifoXmit__DOT__head]
                        : 0U);
            } else {
                if ((4U == (0x1eU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load) 
                                      << 3U) | (((IData)(this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas) 
                                                 << 2U) 
                                                | (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                                                    << 1U) 
                                                   | (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty))))))) {
                    this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                        = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                   >> 4U));
                }
            }
        }
    }
    this->__PVT__user_ifc_uart_rRecvBitCount_D_IN = 
        ((IData)(this->__PVT__user_ifc_uart_pwRecvResetBitCount_whas)
          ? 0U : (0x3fU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rRecvBitCount))));
    this->__PVT__user_ifc_uart_fifoXmit_r_enq_whas 
        = ((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
           & (4U == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))));
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__ring_empty 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__ring_empty;
    this->__PVT__user_ifc_uart_fifoXmit__DOT__head 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__head;
    if (this->__Vdlyvset__user_ifc_uart_fifoXmit__DOT__arr__v0) {
        this->__PVT__user_ifc_uart_fifoXmit__DOT__arr[this->__Vdlyvdim0__user_ifc_uart_fifoXmit__DOT__arr__v0] 
            = this->__Vdlyvval__user_ifc_uart_fifoXmit__DOT__arr__v0;
    }
    this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata 
        = this->__Vdly__user_ifc_uart_fifoXmit__DOT__hasodata;
    this->__PVT__z___05Fh27968 = (1U & ((((((((((((
                                                   ((((((((this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                           ^ 
                                                           (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                            >> 1U)) 
                                                          ^ 
                                                          (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                           >> 2U)) 
                                                         ^ 
                                                         (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                          >> 3U)) 
                                                        ^ 
                                                        (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                         >> 4U)) 
                                                       ^ 
                                                       (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                        >> 5U)) 
                                                      ^ 
                                                      (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                       >> 6U)) 
                                                     ^ 
                                                     (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                      >> 7U)) 
                                                    ^ 
                                                    (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                     >> 8U)) 
                                                   ^ 
                                                   (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                    >> 9U)) 
                                                  ^ 
                                                  (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                   >> 0xaU)) 
                                                 ^ 
                                                 (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                  >> 0xbU)) 
                                                ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                   >> 0xcU)) 
                                               ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                  >> 0xdU)) 
                                              ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                 >> 0xeU)) 
                                             ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                                >> 0xfU)) 
                                            ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                               >> 0x10U)) 
                                           ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                              >> 0x11U)) 
                                          ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                             >> 0x12U)) 
                                         ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                            >> 0x13U)) 
                                        ^ (this->__PVT__user_ifc_uart_fifoXmit_D_OUT 
                                           >> 0x14U)));
    this->__PVT__user_ifc_uart_error_status_register_D_IN 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_capture_write_request) 
             & (0xcU == (0x1fU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
            & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))
            ? ((IData)(this->__PVT__user_ifc_uart_error_status_register) 
               & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                          >> 8U))) : ((((0U == this->__PVT__bitdata___05Fh17072) 
                                        & (~ (IData)(this->__PVT__user_ifc_uart_rRecvData))) 
                                       << 3U) | ((4U 
                                                  & ((~ (IData)(this->__PVT__user_ifc_uart_rRecvData)) 
                                                     << 2U)) 
                                                 | ((2U 
                                                     & ((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                                                        << 1U)) 
                                                    | (((1U 
                                                         == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                        & (~ (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169))) 
                                                       | ((2U 
                                                           == (IData)(this->__PVT__user_ifc_rg_parity)) 
                                                          & (IData)(this->__PVT__user_ifc_uart_vrRecvBuffer_0_30_XOR_user_ifc_u_ETC___05F_d169)))))));
    this->__PVT__user_ifc_uart_fifoXmit__DOT__next_head 
        = ((0xeU == (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__head))));
    this->__PVT__user_ifc_uart_rg_delay_count_D_IN 
        = (0xffffU & (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                       & (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                           | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
                          | (IData)(this->__PVT__user_ifc_uart_out_enable)))
                       ? (((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
                           | (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297))
                           ? 0U : ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rg_delay_count)))
                       : (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                           & (~ (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)))
                           ? ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rg_delay_count))
                           : 0U)));
    this->__PVT__user_ifc_uart_rXmitCellCount_D_IN 
        = (((((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
                & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
               | (((((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2) 
                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                  & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5) 
                 & (7U == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)))) 
             | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
                 & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                & (IData)(this->__PVT__user_ifc_uart_out_enable))) 
            | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit) 
                & (0xfU == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))) 
               & (((0U == (IData)(this->__PVT__user_ifc_rg_stopbits)) 
                   | (2U == (IData)(this->__PVT__user_ifc_rg_stopbits))) 
                  | (1U == (IData)(this->__PVT__user_ifc_rg_stopbits)))))
            ? 0U : (0xfU & ((IData)(1U) + (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))));
    this->__PVT__user_ifc_uart_rXmitState_D_IN = ((
                                                   (((((((IData)(this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1) 
                                                         | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)) 
                                                        | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)) 
                                                       | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)) 
                                                      | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)) 
                                                     | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)) 
                                                    | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)) 
                                                   | (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit2))
                                                   ? 
                                                  ((IData)(this->__PVT__MUX_user_ifc_uart_rXmitState_write_1___05FSEL_1)
                                                    ? 2U
                                                    : 
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command)
                                                     ? 
                                                    ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)
                                                      ? 
                                                     ((IData)(this->__PVT__user_ifc_uart_out_enable)
                                                       ? 2U
                                                       : 1U)
                                                      : 0U)
                                                     : 
                                                    ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_start_bit)
                                                      ? 
                                                     ((0xfU 
                                                       == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                       ? 3U
                                                       : 2U)
                                                      : 
                                                     ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_1_bit_cell_time)
                                                       ? 
                                                      ((0xfU 
                                                        == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                        ? 
                                                       (((IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317) 
                                                         & (0U 
                                                            == (IData)(this->__PVT__user_ifc_rg_parity)))
                                                         ? 5U
                                                         : 
                                                        (((IData)(this->__PVT__user_ifc_uart_rXmitBitCount_84_EQ_user_ifc_rg___05FETC___05F_d317) 
                                                          & (0U 
                                                             != (IData)(this->__PVT__user_ifc_rg_parity)))
                                                          ? 8U
                                                          : 4U))
                                                        : 3U)
                                                       : 
                                                      ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_parity_bit)
                                                        ? 
                                                       ((0xfU 
                                                         == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                         ? 5U
                                                         : 8U)
                                                        : 
                                                       ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit)
                                                         ? 
                                                        (((0xfU 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                          & (0U 
                                                             == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                          ? 0U
                                                          : 
                                                         (((0xfU 
                                                            == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                           & (2U 
                                                              == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                           ? 7U
                                                           : 
                                                          (((0xfU 
                                                             == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount)) 
                                                            & (1U 
                                                               == (IData)(this->__PVT__user_ifc_rg_stopbits)))
                                                            ? 6U
                                                            : 5U)))
                                                         : 
                                                        ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_send_stop_bit1_5)
                                                          ? 
                                                         ((7U 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                           ? 0U
                                                           : 6U)
                                                          : 
                                                         ((0xfU 
                                                           == (IData)(this->__PVT__user_ifc_uart_rXmitCellCount))
                                                           ? 0U
                                                           : 7U))))))))
                                                   : 
                                                  ((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit)
                                                    ? 3U
                                                    : 0U));
    this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas 
        = (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_rl_delay_control) 
            & (IData)(this->__PVT__user_ifc_uart_rg_delay_count_95_EQ_user_ifc_rg_ETC___05F_d297)) 
           | (((IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_wait_for_start_command) 
               & (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
              & (IData)(this->__PVT__user_ifc_uart_out_enable)));
    this->__PVT__status___05Fh37064 = (((IData)(this->__PVT__user_ifc_uart_error_status_register) 
                                        << 4U) | ((8U 
                                                   & ((~ (IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__not_ring_full)) 
                                                      << 3U)) 
                                                  | (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata) 
                                                      << 2U) 
                                                     | ((2U 
                                                         & ((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__not_ring_full)) 
                                                            << 1U)) 
                                                        | ((~ (IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata)) 
                                                           & (0U 
                                                              == (IData)(this->__PVT__user_ifc_uart_rXmitState)))))));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_shift 
        = ((~ (IData)(this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas)) 
           & (IData)(this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_shift_next_bit));
    this->__PVT__CAN_FIRE_RL_user_ifc_uart_transmit_buffer_load 
        = ((IData)(this->__PVT__user_ifc_uart_fifoXmit__DOT__hasodata) 
           & (IData)(this->__PVT__user_ifc_uart_pwXmitLoadBuffer_whas));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((((((0xcU 
                                                                     == 
                                                                     (0x1fU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    & (0U 
                                                                       == 
                                                                       (3U 
                                                                        & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                                   | (8U 
                                                                      == 
                                                                      (0x1fU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  | ((0x14U 
                                                                      == 
                                                                      (0x1fU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                     & (1U 
                                                                        == 
                                                                        (3U 
                                                                         & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))) 
                                                                 | (0U 
                                                                    == 
                                                                    (0x1fU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                | ((0x10U 
                                                                    == 
                                                                    (0x1fU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))) 
                                                               | ((0x18U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (0U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((0xcU 
                                                                 == 
                                                                 (0x1fU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__status___05Fh37064) 
                                                                 << 0x18U) 
                                                                | (((IData)(this->__PVT__status___05Fh37064) 
                                                                    << 0x10U) 
                                                                   | (((IData)(this->__PVT__status___05Fh37064) 
                                                                       << 8U) 
                                                                      | (IData)(this->__PVT__status___05Fh37064))))
                                                                : 
                                                               ((8U 
                                                                 == 
                                                                 (0x1fU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U))))
                                                                 ? 
                                                                ((0x1fU 
                                                                  >= 
                                                                  (0x3fU 
                                                                   & ((IData)(0x20U) 
                                                                      - (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__user_ifc_uart_fifoRecv__DOT__hasodata)
                                                                    ? this->__PVT__user_ifc_uart_fifoRecv_D_OUT
                                                                    : 0U) 
                                                                  >> 
                                                                  (0x3fU 
                                                                   & ((IData)(0x20U) 
                                                                      - (IData)(this->__PVT__user_ifc_rg_charsize))))
                                                                  : 0U)
                                                                 : 
                                                                (((0x14U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__user_ifc_rg_charsize) 
                                                                   << 0x15U) 
                                                                  | (((IData)(this->__PVT__user_ifc_rg_parity) 
                                                                      << 0x13U) 
                                                                     | (((IData)(this->__PVT__user_ifc_rg_stopbits) 
                                                                         << 0x11U) 
                                                                        | (((IData)(this->__PVT__user_ifc_rg_charsize) 
                                                                            << 5U) 
                                                                           | (((IData)(this->__PVT__user_ifc_rg_parity) 
                                                                               << 3U) 
                                                                              | ((IData)(this->__PVT__user_ifc_rg_stopbits) 
                                                                                << 1U))))))
                                                                  : 
                                                                 ((0U 
                                                                   == 
                                                                   (0x1fU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__user_ifc_baud_value) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__user_ifc_baud_value))
                                                                   : 
                                                                  (((0x10U 
                                                                     == 
                                                                     (0x1fU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    & (1U 
                                                                       == 
                                                                       (3U 
                                                                        & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                    ? 
                                                                   (((IData)(this->__PVT__user_ifc_rg_delay_control) 
                                                                     << 0x10U) 
                                                                    | (IData)(this->__PVT__user_ifc_rg_delay_control))
                                                                    : 
                                                                   (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                     << 0x18U) 
                                                                    | (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                        << 0x10U) 
                                                                       | (((IData)(this->__PVT__user_ifc_rg_interrupt_en) 
                                                                           << 8U) 
                                                                          | (IData)(this->__PVT__user_ifc_rg_interrupt_en))))))))))));
}
