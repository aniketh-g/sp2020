// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc.h"
#include "VmkSoc__Syms.h"

VL_INLINE_OPT void VmkSoc::_sequent__TOP__9(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__9\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*95:0*/ __Vtemp985[3];
    WData/*95:0*/ __Vtemp993[3];
    WData/*95:0*/ __Vtemp1001[3];
    WData/*95:0*/ __Vtemp1009[3];
    // Body
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (8U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_8 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (8U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_8 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (7U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_7 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (7U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_7 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x3fU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_63 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x3fU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_63 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x3eU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_62 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x3eU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_62 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x3dU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_61 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x3dU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_61 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x3cU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_60 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x3cU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_60 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (6U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_6 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (6U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_6 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x3bU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_59 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x3bU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_59 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x3aU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_58 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x3aU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_58 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x1aU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_26 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x1aU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_26 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x19U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_25 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x19U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_25 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x18U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_24 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x18U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_24 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x17U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_23 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x17U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_23 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x16U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_22 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x16U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_22 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x15U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_21 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x15U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_21 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x14U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_20 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x14U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_20 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (2U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_2 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (2U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_2 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x13U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_19 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x13U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_19 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x39U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_57 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x39U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_57 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x12U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_18 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x12U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_18 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x38U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_56 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x38U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_56 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x11U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_17 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x11U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_17 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x37U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_55 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x37U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_55 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x10U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_16 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x10U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_16 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x36U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_54 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x36U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_54 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0xfU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                     << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                  >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_15 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0xfU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                          << 0x19U) 
                                         | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                            >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_15 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x35U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_53 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x35U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_53 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0xeU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                     << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                  >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_14 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0xeU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                          << 0x19U) 
                                         | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                            >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_14 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x34U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_52 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x34U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_52 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0xdU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                     << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                  >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_13 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0xdU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                          << 0x19U) 
                                         | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                            >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_13 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x2cU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_44 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x2cU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_44 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x2bU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_43 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x2bU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_43 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x2eU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_46 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x2eU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_46 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x33U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_51 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x33U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_51 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x2dU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_45 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x2dU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_45 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x2fU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_47 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x2fU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_47 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_0 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_0 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x30U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_48 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x30U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_48 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (1U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_1 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (1U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x31U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_49 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x31U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_49 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0xaU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                     << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                  >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_10 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0xaU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                          << 0x19U) 
                                         | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                            >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_10 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (5U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_5 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (5U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_5 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0xbU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                     << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                  >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_11 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0xbU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                          << 0x19U) 
                                         | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                            >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_11 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x32U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_50 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x32U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_50 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x2aU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_42 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x2aU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_42 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0xcU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                     << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                  >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_12 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0xcU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                          << 0x19U) 
                                         | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                            >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_12 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x1bU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_27 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x1bU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_27 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x1cU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_28 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x1cU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_28 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x1dU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_29 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x1dU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_29 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (3U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_3 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (3U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_3 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x1eU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_30 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x1eU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_30 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x1fU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_31 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x1fU == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_31 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x20U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_32 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x20U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_32 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x21U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_33 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x21U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_33 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x22U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_34 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x22U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_34 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x23U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_35 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x23U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_35 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x24U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_36 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x24U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_36 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x25U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_37 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x25U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_37 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x26U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_38 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x26U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_38 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x27U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_39 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x27U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_39 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (4U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                   << 0x19U) | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_4 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (4U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                        << 0x19U) | 
                                       (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                        >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_4 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x28U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_40 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x28U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_40 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
               & (0x29U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                      << 0x19U) | (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                                   >> 7U))))) 
              & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fence_operation))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_41 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (0x29U == (0x3fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[1U] 
                                           << 0x19U) 
                                          | (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U] 
                                             >> 7U))))) 
                   & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U]));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__v_reg_valid_41 = 0U;
    }
    if (vlTOPp->RST_N) {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[0U] 
            = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_EN_commit_rd) 
                 & ((0x1fU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
                              >> 2U)) == (0x1fU & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[2U]))) 
                & (0U != (0x1fU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
                                   >> 2U)))) ? ((0xfffffffcU 
                                                 & ((IData)(
                                                            (((QData)((IData)(
                                                                              vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[1U])) 
                                                              << 0x20U) 
                                                             | (QData)((IData)(
                                                                               vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[0U])))) 
                                                    << 2U)) 
                                                | (3U 
                                                   & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[0U]))
                : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[0U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[1U] 
            = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_EN_commit_rd) 
                 & ((0x1fU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
                              >> 2U)) == (0x1fU & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[2U]))) 
                & (0U != (0x1fU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
                                   >> 2U)))) ? ((3U 
                                                 & ((IData)(
                                                            (((QData)((IData)(
                                                                              vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[1U])) 
                                                              << 0x20U) 
                                                             | (QData)((IData)(
                                                                               vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[0U])))) 
                                                    >> 0x1eU)) 
                                                | (0xfffffffcU 
                                                   & ((IData)(
                                                              ((((QData)((IData)(
                                                                                vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[1U])) 
                                                                 << 0x20U) 
                                                                | (QData)((IData)(
                                                                                vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[0U]))) 
                                                               >> 0x20U)) 
                                                      << 2U)))
                : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[1U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[2U] 
            = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_EN_commit_rd) 
                 & ((0x1fU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
                              >> 2U)) == (0x1fU & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[2U]))) 
                & (0U != (0x1fU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
                                   >> 2U)))) ? ((0x7cU 
                                                 & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U]) 
                                                | (3U 
                                                   & ((IData)(
                                                              ((((QData)((IData)(
                                                                                vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[1U])) 
                                                                 << 0x20U) 
                                                                | (QData)((IData)(
                                                                                vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_commit_rd[0U]))) 
                                                               >> 0x20U)) 
                                                      >> 0x1eU)))
                : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U]);
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__CAN_FIRE_RL_m1_mkConnectionAVtoAf) 
             & (0x800U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__ff_fwd_request__DOT__data0_reg[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_ienable 
                = (1U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__x___05Fh4071));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_ienable = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__CAN_FIRE_RL_m1_mkConnectionAVtoAf) 
             & (0x800U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__ff_fwd_request__DOT__data0_reg[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_bpuenable 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__x___05Fh4071 
                                 >> 2U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_bpuenable = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__CAN_FIRE_RL_m1_mkConnectionAVtoAf) 
             & (0x800U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__ff_fwd_request__DOT__data0_reg[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_denable 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__x___05Fh4071 
                                 >> 1U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_denable = 1U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_addr_1 
                = vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_address;
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_addr_1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_DEQ)))) {
            vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__empty_reg = 1U;
        } else {
            if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_DEQ) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request)))) {
                vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__empty_reg 
                    = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__full_reg)));
            }
        }
    } else {
        vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fill_from_memory) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer__DOT__rg_fb_enables 
                = ((2U & vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])
                    ? 0U : (0xffffU & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer__DOT__w___05Fh8378) 
                                       | ((IData)(1U) 
                                          << (0xfU 
                                              & ((IData)(1U) 
                                                 + (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh6597)))))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer__DOT__rg_fb_enables = 0U;
    }
    if (vlTOPp->RST_N) {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv[0U] 
            = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_capture_io_response) 
                | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv[1U] 
            = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_capture_io_response) 
                | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv[2U] 
            = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_capture_io_response) 
                | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__ras_stack_top_index 
            = (7U & ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu_EN_mav_prediction_response) 
                         & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2_mv_cacheenable) 
                            >> 2U)) & (0U != vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT___theResult___05F___05F_7___05Fh40216)) 
                       & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_mav_pred_ETC___05F_d506)) 
                      | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_initialize))
                      ? ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__MUX_ras_stack_top_index_port1___05Fwrite_1___05FSEL_1)
                          ? ((IData)(1U) + (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__i___05Fh410552))
                          : 0U) : (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__i___05Fh410552)));
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv[2U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__ras_stack_top_index = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__rg_performing_replay_EN) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__rg_performing_replay 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                    & (~ vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info[0U])) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mv_release_info___05F80_BITS_12_TO_7___05FETC___05F_d594));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__rg_performing_replay = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_response_to_core) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__rg_handling_miss 
                = vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request;
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__rg_handling_miss = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_9[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_9[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_9[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_9[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_9[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_9[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_8[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_8[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_8[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_8[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_8[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_8[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_7[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_7[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_7[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_7[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_7[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_7[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_6[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_6[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_6[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_6[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_6[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_6[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_5[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_5[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_5[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_5[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_5[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_5[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_4[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_4[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_4[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_4[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_4[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_4[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_3[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_3[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_3[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_3[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_3[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_3[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_2[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_2[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_2[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_2[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_2[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_2[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_1[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_1[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_1[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_1[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_1[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_1[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x1fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x1fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_31[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_31[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_31[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_31[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_31[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_31[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x1eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x1eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_30[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_30[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_30[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_30[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_30[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_30[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x1dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x1dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_29[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_29[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_29[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_29[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_29[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_29[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x1cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x1cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_28[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_28[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_28[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_28[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_28[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_28[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x1bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x1bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_27[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_27[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_27[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_27[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_27[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_27[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x1aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x1aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_26[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_26[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_26[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_26[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_26[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_26[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x19U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x19U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_25[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_25[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_25[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_25[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_25[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_25[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x18U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x18U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_24[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_24[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_24[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_24[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_24[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_24[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x17U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x17U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_23[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_23[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_23[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_23[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_23[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_23[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x16U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x16U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_22[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_22[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_22[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_22[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_22[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_22[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x15U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x15U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_21[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_21[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_21[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_21[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_21[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_21[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x14U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x14U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_20[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_20[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_20[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_20[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_20[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_20[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x13U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x13U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_19[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_19[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_19[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_19[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_19[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_19[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x12U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x12U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_18[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_18[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_18[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_18[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_18[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_18[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x11U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x11U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_17[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_17[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_17[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_17[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_17[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_17[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0x10U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0x10U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_16[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_16[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_16[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_16[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_16[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_16[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0xfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0xfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_15[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_15[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_15[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_15[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_15[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_15[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0xeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0xeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_14[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_14[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_14[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_14[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_14[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_14[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0xdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0xdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_13[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_13[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_13[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_13[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_13[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_13[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0xcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0xcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_12[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_12[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_12[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_12[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_12[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_12[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0xbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0xbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_11[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_11[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_11[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_11[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_11[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_11[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
             & (((0xaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__x___05Fh429503)) 
                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_train_ETC___05F_d7534)) 
                | ((0xaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_allocate)) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__NOT_v_reg_btb_tag_0_61_BITS_62_TO_1_62_EQ_ma_t_ETC___05F_d7613))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_10[0U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[0U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_10[1U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[1U];
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_10[2U] 
                = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_0_D_IN[2U];
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_10[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_10[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__v_reg_btb_entry_10[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_response_to_core) 
             | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__rg_handling_miss 
                = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_response_to_core)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__rg_handling_miss = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__rg_performing_replay_EN) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__rg_performing_replay 
                = ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__WILL_FIRE_RL_rl_release_from_fillbuffer) 
                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__NOT_m_fillbuffer_mv_release_info___05F53_BIT_1_432_ETC___05F_d1578));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__rg_performing_replay = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fillbuffer_check) {
            vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__rg_polling_mode 
                = (1U & ((IData)((vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mav_polling_response 
                                  >> 1U)) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__m_fillbuffer_mav_polling_response))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__rg_polling_mode = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x103U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_sideleg 
                = (0xfffU & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh15982));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_sideleg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__CAN_FIRE_RL_m1_mkConnectionAVtoAf) 
             & ((2U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__ff_fwd_request__DOT__data0_reg[2U] 
                                  >> 4U))) | (3U == 
                                              (0xfffU 
                                               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__ff_fwd_request__DOT__data0_reg[2U] 
                                                  >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_frm 
                = (7U & ((2U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__ff_fwd_request__DOT__data0_reg[2U] 
                                           >> 4U)))
                          ? (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__x___05Fh4071)
                          : (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__x___05Fh4071 
                                     >> 5U))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp2__DOT__rg_frm = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x303U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mideleg 
                = (0xfffU & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh15982));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mideleg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)))) {
            vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ)))) {
                vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_m_storebuffer_rg_sb_busy_write_1___05FSEL_1) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_storebuffer_rg_atomic_op 
                = (0x1fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request__DOT__data0_reg[1U] 
                             << 0x1fU) | (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request__DOT__data0_reg[0U] 
                                          >> 1U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_storebuffer_rg_atomic_op = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___05FEN_ma_inputs) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_upperbits 
                = (1U & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                         >> 0x1dU));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_upperbits = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___05FEN_ma_inputs) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_complement 
                = (1U & ((4U == (7U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[1U] 
                                        << 4U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                                                  >> 0x1cU))))
                          ? ((IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_ma_inputs_word32_THEN_IF_ma_inputs_funct3_B_ETC___05F_d52 
                                      >> 0x3fU)) ^ (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_ma_inputs_word32_THEN_IF_ma_inputs_funct3_B_ETC___05F_d64 
                                                            >> 0x3fU)))
                          : (6U == (7U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[1U] 
                                           << 4U) | 
                                          (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                                           >> 0x1cU))))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_complement = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___05FEN_ma_inputs) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__quotient_remainder 
                = (1U & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                         >> 0x1dU));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__quotient_remainder = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___05FEN_ma_inputs) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_word 
                = (1U & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                         >> 0xdU));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_word = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___05FEN_ma_inputs) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_fn3 
                = (7U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[1U] 
                          << 4U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                                    >> 0x1cU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_fn3 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_valid_1_whas) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_result 
                = ((0ULL == vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_op2)
                    ? ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_word)
                        ? (((QData)((IData)((- (IData)(
                                                       (1U 
                                                        & (IData)(
                                                                  (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_quotient_remainder_8_THEN_rg_in1_9_ELSE_184_ETC___05F_d20 
                                                                   >> 0x1fU))))))) 
                            << 0x20U) | (QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_quotient_remainder_8_THEN_rg_in1_9_ELSE_184_ETC___05F_d20)))
                        : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_quotient_remainder_8_THEN_rg_in1_9_ELSE_184_ETC___05F_d20)
                    : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_word)
                        ? (((QData)((IData)((- (IData)(
                                                       (1U 
                                                        & (IData)(
                                                                  (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_rg_upperbits_4_AND_rg_complement_5_6_AND_NO_ETC___05F_d39 
                                                                   >> 0x1fU))))))) 
                            << 0x20U) | (QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_rg_upperbits_4_AND_rg_complement_5_6_AND_NO_ETC___05F_d39)))
                        : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__IF_rg_upperbits_4_AND_rg_complement_5_6_AND_NO_ETC___05F_d39));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__div___DOT__rg_result = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_addr_0 
                = vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_address;
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_addr_0 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___05FEN_ma_inputs) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op1_0[0U] 
                = (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__arg1___05Fh2978);
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op1_0[1U] 
                = (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__arg1___05Fh2978 
                           >> 0x20U));
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op1_0[2U] 
                = (1U & (((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                           >> 0x1dU) ^ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                                        >> 0x1cU)) 
                         & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__arg1___05Fh2978 
                                    >> 0x3fU))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op1_0[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op1_0[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op1_0[2U] = 0U;
    }
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_mispredict_ghr 
        = ((IData)(vlTOPp->RST_N) ? ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__CAN_FIRE_RL_rl_capture_latest_ops) 
                                         & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__rg_eEpoch_read___05F6_CONCAT_rg_wEpoch_7_8_EQ_IF_r_ETC___05F_d53)) 
                                        & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__NOT_IF_fwding_read_rs1_1_BIT_64_2_AND_fwding_r_ETC___05F_d138)) 
                                       & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__fn_alu___05F_d166[0U] 
                                          >> 1U)) & 
                                      (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__fn_alu___05F_d166[4U] 
                                          >> 2U))) ? 
                                     (0x200U | ((0x100U 
                                                 & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                                                    >> 1U)) 
                                                | (0xffU 
                                                   & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U])))
                                      : 0U) : 0U);
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___05FEN_ma_inputs) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op2_0[0U] 
                = (IData)((((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__fwding_read_rs2[1U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__fwding_read_rs2[0U]))));
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op2_0[1U] 
                = (IData)(((((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__fwding_read_rs2[1U])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__fwding_read_rs2[0U]))) 
                           >> 0x20U));
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op2_0[2U] 
                = ((1U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[1U] 
                                  << 4U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_D_OUT[0U] 
                                            >> 0x1cU)))) 
                   & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__fwding_read_rs2[1U] 
                      >> 0x1fU));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op2_0[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op2_0[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__multicycle_alu__DOT__mbox__DOT__mul___DOT__rg_op2_0[2U] = 0U;
    }
    vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg 
        = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
                          & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__empty_reg))) 
                         | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
                             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_DEQ)) 
                            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__full_reg))))) 
             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_D_IN)) 
            | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_DEQ) 
                           & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__full_reg))))) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_DEQ)) 
                                  & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request))) 
                                 | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_DEQ)) 
                                    & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__empty_reg))) 
                                | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request)) 
                                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__full_reg)))))) 
              & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg)));
    vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data1_reg 
        = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__empty_reg))
            ? (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req_D_IN)
            : (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data1_reg));
    if (vlTOPp->RST_N) {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv[0U] 
            = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_capture_io_response) 
                | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fill_from_memory))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv_port1___05Fread[0U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv[1U] 
            = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_capture_io_response) 
                | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fill_from_memory))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv_port1___05Fread[1U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv[2U] 
            = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_capture_io_response) 
                | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_fill_from_memory))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv_port1___05Fread[2U]);
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_read_mem_response_rv[2U] = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_DEQ)))) {
            vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__empty_reg = 1U;
        } else {
            if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_DEQ) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request)))) {
                vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__empty_reg 
                    = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__full_reg)));
            }
        }
    } else {
        vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__empty_reg = 0U;
    }
    __Vtemp985[1U] = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
                                     & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg))) 
                                    | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
                                        & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                       & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg))))) 
                        & vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_1_f_rd_data__DOT__data0_reg[1U]) 
                       | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp) 
                                      & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg))))) 
                          & vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[1U])) 
                      | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ))) 
                                            | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg))) 
                                           | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ)) 
                                              & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg)))))) 
                         & vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data0_reg[1U]));
    __Vtemp985[2U] = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
                                     & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg))) 
                                    | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
                                        & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                       & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg))))) 
                        & vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_1_f_rd_data__DOT__data0_reg[2U]) 
                       | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp) 
                                      & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg))))) 
                          & vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[2U])) 
                      | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ))) 
                                            | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg))) 
                                           | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ)) 
                                              & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg)))))) 
                         & vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data0_reg[2U]));
    vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data0_reg[0U] 
        = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
                          & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg))) 
                         | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
                             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg))))) 
             & vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_1_f_rd_data__DOT__data0_reg[0U]) 
            | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp) 
                           & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg))))) 
               & vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[0U])) 
           | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                  & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ))) 
                                 | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_dmem_line_resp)) 
                                    & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg))) 
                                | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ)) 
                                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__full_reg)))))) 
              & vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data0_reg[0U]));
    vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data0_reg[1U] 
        = __Vtemp985[1U];
    vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data0_reg[2U] 
        = __Vtemp985[2U];
    if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data_ENQ) 
         & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__empty_reg))) {
        __Vtemp993[1U] = vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_1_f_rd_data__DOT__data0_reg[1U];
        __Vtemp993[2U] = vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_1_f_rd_data__DOT__data0_reg[2U];
        vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[0U] 
            = vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_1_f_rd_data__DOT__data0_reg[0U];
    } else {
        __Vtemp993[1U] = vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[1U];
        __Vtemp993[2U] = vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[2U];
        vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[0U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[0U];
    }
    vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[1U] 
        = __Vtemp993[1U];
    vlTOPp->mkSoc__DOT__ccore__DOT__memory_xactor_f_rd_data__DOT__data1_reg[2U] 
        = __Vtemp993[2U];
    if ((1U & (~ (IData)(vlTOPp->RST_N)))) {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_nmip = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_stoptime 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 9U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_stoptime = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_stopcount 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 0xaU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_stopcount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_ebreakm 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 0xfU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_ebreakm = 1U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_ebreaks 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 0xdU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_ebreaks = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_ebreaku 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_ebreaku = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_mprven 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 4U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_mprven = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_meie 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                 >> 0xbU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_meie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_msie 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                 >> 3U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_msie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mtie 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                 >> 7U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mtie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x344U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_usip 
                = (1U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_usip = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x344U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_stip 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                 >> 5U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_stip = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & ((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                      >> 4U))) | (0x104U 
                                                  == 
                                                  (0xfffU 
                                                   & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                      >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_stie 
                = (1U & ((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                     >> 5U)) : (IData)(
                                                       (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                                        >> 5U))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_stie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & ((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                      >> 4U))) | (0x104U 
                                                  == 
                                                  (0xfffU 
                                                   & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                      >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ssie 
                = (1U & ((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                     >> 1U)) : (IData)(
                                                       (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                                        >> 1U))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ssie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & ((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                      >> 4U))) | (0x104U 
                                                  == 
                                                  (0xfffU 
                                                   & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                      >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_seie 
                = (1U & ((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                     >> 9U)) : (IData)(
                                                       (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                                        >> 9U))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_seie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & ((0x344U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                      >> 4U))) | (0x144U 
                                                  == 
                                                  (0xfffU 
                                                   & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                      >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ssip 
                = (1U & ((0x344U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                     >> 1U)) : (IData)(
                                                       (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                                        >> 1U))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ssip = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                       >> 4U))) | (0x104U 
                                                   == 
                                                   (0xfffU 
                                                    & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                       >> 4U)))) 
                | (4U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                    >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ueie 
                = (1U & ((0x104U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                     >> 8U)) : ((0x304U 
                                                 == 
                                                 (0xfffU 
                                                  & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                     >> 4U)))
                                                 ? (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                                            >> 8U))
                                                 : (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh14357 
                                                            >> 8U)))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ueie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                       >> 4U))) | (0x104U 
                                                   == 
                                                   (0xfffU 
                                                    & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                       >> 4U)))) 
                | (4U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                    >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_utie 
                = (1U & ((0x104U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                     >> 4U)) : ((0x304U 
                                                 == 
                                                 (0xfffU 
                                                  & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                     >> 4U)))
                                                 ? (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                                            >> 4U))
                                                 : (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh14357 
                                                            >> 4U)))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_utie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (((0x344U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                       >> 4U))) | (0x144U 
                                                   == 
                                                   (0xfffU 
                                                    & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                       >> 4U)))) 
                | (0x44U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                       >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_utip 
                = (1U & ((0x144U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                     >> 4U)) : ((0x344U 
                                                 == 
                                                 (0xfffU 
                                                  & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                     >> 4U)))
                                                 ? (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                                            >> 4U))
                                                 : (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh14357 
                                                            >> 4U)))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_utip = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (((((0x304U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                         >> 4U))) | 
                   (0x104U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                         >> 4U)))) 
                  | (0x144U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                          >> 4U)))) 
                 | (4U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U)))) | (0x44U 
                                                  == 
                                                  (0xfffU 
                                                   & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                      >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_usie 
                = (1U & (((0x104U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                >> 4U))) 
                          | (0x144U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                  >> 4U))))
                          ? (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258)
                          : ((0x304U == (0xfffU & (
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                   >> 4U)))
                              ? (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938)
                              : (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh14357))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_usie = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_step 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 2U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_step = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_dcsr_prv_write_1___05FSEL_2) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_stepie 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__x___05Fh3986 
                                 >> 0xbU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_stepie = 0U;
    }
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_msip 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__clint_clint_msip));
    if (vlTOPp->RST_N) {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv[0U] 
            = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__CAN_FIRE_RL_check_instruction) 
                 & (0U == (3U & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe3type__DOT__data0_reg[2U] 
                                 >> 0x11U)))) & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv_port1___05Fread[2U] 
                                                 >> 8U))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv_port1___05Fread[0U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv[1U] 
            = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__CAN_FIRE_RL_check_instruction) 
                 & (0U == (3U & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe3type__DOT__data0_reg[2U] 
                                 >> 0x11U)))) & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv_port1___05Fread[2U] 
                                                 >> 8U))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv_port1___05Fread[1U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv[2U] 
            = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__CAN_FIRE_RL_check_instruction) 
                 & (0U == (3U & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe3type__DOT__data0_reg[2U] 
                                 >> 0x11U)))) & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv_port1___05Fread[2U] 
                                                 >> 8U))
                ? 0U : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv_port1___05Fread[2U]);
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mtip 
            = ((IData)(vlTOPp->mkSoc__DOT__clint_clint_mtip) 
               & 1U);
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv[0U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv[1U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage4__DOT__ff_memory_response_rv[2U] = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mtip = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__MUX_rg_csr_dpc_write_1___05FSEL_1) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_cause 
                = (7U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr_take_trap_type_cause));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_cause = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)))) {
            vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ)))) {
                vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache_EN_put_core_req_put) 
             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request_DEQ)))) {
            vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request__DOT__empty_reg = 1U;
        } else {
            if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request_DEQ) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache_EN_put_core_req_put)))) {
                vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request__DOT__empty_reg 
                    = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request__DOT__full_reg)));
            }
        }
    } else {
        vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_core_request__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache_EN_put_core_req_put) 
             & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_core_request_DEQ)))) {
            vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_core_request__DOT__empty_reg = 1U;
        } else {
            if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_core_request_DEQ) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache_EN_put_core_req_put)))) {
                vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_core_request__DOT__empty_reg 
                    = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_core_request__DOT__full_reg)));
            }
        }
    } else {
        vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_core_request__DOT__empty_reg = 0U;
    }
    vlTOPp->__Vdly__mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__data0_reg 
        = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
                          & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__empty_reg))) 
                         | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
                             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_DEQ)) 
                            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__full_reg))))) 
             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_D_IN)) 
            | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_DEQ) 
                           & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__full_reg))))) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_DEQ)) 
                                  & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request))) 
                                 | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_DEQ)) 
                                    & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__empty_reg))) 
                                | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request)) 
                                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__full_reg)))))) 
              & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__data0_reg)));
    vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__data1_reg 
        = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__CAN_FIRE_RL_rl_send_memory_request) 
            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__empty_reg))
            ? (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req_D_IN)
            : (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__imem__DOT__icache__DOT__ff_pending_req__DOT__data1_reg));
    if (vlTOPp->RST_N) {
        if ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
              & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))) 
             | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
                & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_dirty_0 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))) 
                   | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_dirty_0_write_1___05FVAL_2));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_dirty_0 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
              & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)) 
             | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_dirty_1 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                    & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)) 
                   | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_dirty_0_write_1___05FVAL_2));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_dirty_1 = 0U;
    }
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ext_seip = 0U;
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (0x344U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                     >> 4U))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_soft_seip 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                 >> 9U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_soft_seip = 0U;
    }
    if ((1U & (~ (IData)(vlTOPp->RST_N)))) {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ext_ueip = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_EN_ma_core_req) 
             & (((0x344U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                       >> 4U))) | (0x144U 
                                                   == 
                                                   (0xfffU 
                                                    & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                       >> 4U)))) 
                | (0x44U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                       >> 4U)))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_soft_ueip 
                = (1U & ((0x144U == (0xfffU & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                               >> 4U)))
                          ? (IData)((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh11258 
                                     >> 8U)) : ((0x344U 
                                                 == 
                                                 (0xfffU 
                                                  & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile_ma_core_req_req[2U] 
                                                     >> 4U)))
                                                 ? (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh6938 
                                                            >> 8U))
                                                 : (IData)(
                                                           (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__x___05Fh14357 
                                                            >> 8U)))));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_soft_ueip = 0U;
    }
    __Vtemp1001[1U] = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
                                      & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg))) 
                                     | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
                                         & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                        & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg))))) 
                         & vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_2_f_rd_data__DOT__data0_reg[1U]) 
                        | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp) 
                                       & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg))))) 
                           & vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[1U])) 
                       | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                              & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ))) 
                                             | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg))) 
                                            | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ)) 
                                               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg)))))) 
                          & vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data0_reg[1U]));
    __Vtemp1001[2U] = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
                                      & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg))) 
                                     | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
                                         & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                        & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg))))) 
                         & vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_2_f_rd_data__DOT__data0_reg[2U]) 
                        | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp) 
                                       & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg))))) 
                           & vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[2U])) 
                       | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                              & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ))) 
                                             | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg))) 
                                            | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ)) 
                                               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg)))))) 
                          & vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data0_reg[2U]));
    vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data0_reg[0U] 
        = ((((- (IData)((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
                          & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg))) 
                         | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
                             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg))))) 
             & vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_2_f_rd_data__DOT__data0_reg[0U]) 
            | ((- (IData)(((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp) 
                           & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg))))) 
               & vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[0U])) 
           | ((- (IData)((1U & ((((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                  & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ))) 
                                 | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__CAN_FIRE_RL_rl_handle_imem_line_resp)) 
                                    & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg))) 
                                | ((~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ)) 
                                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__full_reg)))))) 
              & vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data0_reg[0U]));
    vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data0_reg[1U] 
        = __Vtemp1001[1U];
    vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data0_reg[2U] 
        = __Vtemp1001[2U];
    if (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data_ENQ) 
         & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__empty_reg))) {
        __Vtemp1009[1U] = vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_2_f_rd_data__DOT__data0_reg[1U];
        __Vtemp1009[2U] = vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_2_f_rd_data__DOT__data0_reg[2U];
        vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[0U] 
            = vlTOPp->mkSoc__DOT__fabric_xactors_from_masters_2_f_rd_data__DOT__data0_reg[0U];
    } else {
        __Vtemp1009[1U] = vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[1U];
        __Vtemp1009[2U] = vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[2U];
        vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[0U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[0U];
    }
    vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[1U] 
        = __Vtemp1009[1U];
    vlTOPp->mkSoc__DOT__ccore__DOT__fetch_xactor_f_rd_data__DOT__data1_reg[2U] 
        = __Vtemp1009[2U];
    if (vlTOPp->RST_N) {
        if ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
              & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
                                & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_err_0 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                    & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                          >> 1U))) & vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U]);
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_err_0 = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
              & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                 >> 1U)) | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
                            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_err_1 
                = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                    & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)) & vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U]);
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_err_1 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (5U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_5 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (5U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xbU])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xaU])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_5 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (4U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_4 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (4U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[9U])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[8U])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_4 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (3U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_3 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (3U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[7U])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[6U])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_3 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (2U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_2 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (2U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[5U])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[4U])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_2 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (0U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_0 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (0U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[1U])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0U])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_0 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (1U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_1 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (1U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[3U])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[2U])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_1 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (3U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_3 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (3U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[7U])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[6U])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_3 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (6U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_6 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (6U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xdU])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xcU])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_6 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail))) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                       >> 1U)))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                     & (7U == (7U & 
                                               (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                >> 3U)))) 
                                    & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_7 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (7U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                              >> 1U))) ? (((QData)((IData)(
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                           << 0x3eU) 
                                          | (((QData)((IData)(
                                                              vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                              << 0x1eU) 
                                             | ((QData)((IData)(
                                                                vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                                >> 2U)))
                        : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_0_write_1___05FSEL_2)
                            ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xfU])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xeU])))
                            : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_0_7 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (0U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_0 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (0U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[1U])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0U])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_0 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (1U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_1 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (1U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[3U])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[2U])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_1 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (2U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_2 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (2U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[5U])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[4U])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_2 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (4U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_4 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (4U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[9U])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[8U])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_4 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (5U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_5 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (5U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xbU])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xaU])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_5 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (6U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_6 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (6U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xdU])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xcU])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_6 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_EN_mav_allocate_line) 
               & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__rg_fbtail)) 
              | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                  & (7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                 & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                    >> 1U))) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                                 & (7U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                                 >> 3U)))) 
                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex)))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_7 
                = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__MUX_rg_globaldirty_write_1___05FSEL_2) 
                     & (7U == (7U & (vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__address___05Fh120552 
                                     >> 3U)))) & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_ma_from_storebuffer_fbindex))
                    ? vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_data_0_0_write_1___05FVAL_1
                    : ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__CAN_FIRE_RL_rl_fill_from_memory) 
                         & (7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__lv_current_bank___05Fh4668))) 
                        & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_pending_req__DOT__data0_reg) 
                           >> 1U)) ? (((QData)((IData)(
                                                       vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[2U])) 
                                       << 0x3eU) | 
                                      (((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[1U])) 
                                        << 0x1eU) | 
                                       ((QData)((IData)(
                                                        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__ff_read_mem_response_rv_port1___05Fread[0U])) 
                                        >> 2U))) : 
                       ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__MUX_v_fb_addr_valid_1_write_1___05FSEL_2)
                         ? (((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xfU])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer_mav_allocate_line_dataline[0xeU])))
                         : 0ULL)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__dmem__DOT__dcache__DOT__m_fillbuffer__DOT__v_fb_data_1_7 = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x1cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_28 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_28 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x1bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_27 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_27 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x1aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_26 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_26 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xffU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_255 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_255 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xfeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_254 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_254 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xfdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_253 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_253 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xfcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_252 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_252 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xfbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_251 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_251 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xfaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_250 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_250 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x19U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_25 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_25 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_249 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_249 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_248 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_248 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_247 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_247 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_246 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_246 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_245 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_245 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_244 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_244 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_243 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_243 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_242 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_242 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_241 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_241 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xf0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_240 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_240 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x18U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_24 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_24 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xefU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_239 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_239 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xeeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_238 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_238 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xedU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_237 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_237 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xecU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_236 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_236 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xebU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_235 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_235 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xeaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_234 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_234 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_233 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_233 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_232 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_232 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_231 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_231 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_230 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_230 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x17U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_23 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_23 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_229 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_229 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_228 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_228 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_227 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_227 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_226 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_226 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_225 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_225 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xe0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_224 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_224 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xdfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_223 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_223 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xdeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_222 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_222 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xddU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_221 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_221 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xdcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_220 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_220 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x16U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_22 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_22 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xdbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_219 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_219 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xdaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_218 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_218 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_217 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_217 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_216 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_216 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_215 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_215 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_214 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_214 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_213 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_213 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_212 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_212 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_211 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_211 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x56U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_86 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_86 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x55U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_85 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_85 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x54U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_84 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_84 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x53U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_83 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_83 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x52U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_82 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_82 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x51U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_81 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_81 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x50U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_80 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_80 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_8 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_8 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x4fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_79 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_79 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x4eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_78 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_78 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x4dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_77 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_77 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x4cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_76 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_76 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x4bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_75 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_75 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x4aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_74 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_74 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x49U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_73 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_73 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x48U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_72 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_72 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x47U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_71 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_71 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x46U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_70 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_70 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_7 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_7 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x45U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_69 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_69 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x44U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_68 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_68 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x43U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_67 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_67 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x85U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_133 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_133 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x42U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_66 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_66 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x84U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_132 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_132 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x41U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_65 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_65 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x83U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_131 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_131 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x40U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_64 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_64 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x82U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_130 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_130 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x3fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_63 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_63 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_13 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_13 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x3eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_62 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_62 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x81U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_129 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_129 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x3dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_61 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_61 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x80U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_128 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_128 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x3cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_60 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_60 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_127 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_127 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_6 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_6 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_126 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_126 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x3bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_59 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_59 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_125 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_125 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x3aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_58 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_58 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_124 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_124 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x39U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_57 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_57 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_123 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_123 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x38U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_56 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_56 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_122 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_122 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_108 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_108 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_107 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_107 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_106 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_106 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x69U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_105 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_105 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x68U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_104 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_104 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x37U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_55 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_55 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_142 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_142 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x25U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_37 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_37 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x67U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_103 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_103 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x36U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_54 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_54 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_141 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_141 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x24U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_36 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_36 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x66U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_102 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_102 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x35U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_53 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_53 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_140 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_140 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x23U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_35 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_35 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x65U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_101 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_101 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x34U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_52 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_52 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x86U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_134 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_134 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x1dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_29 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_29 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x93U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_147 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_147 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x29U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_41 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_41 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_10 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_10 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x32U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_50 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_50 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_143 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_143 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x26U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_38 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_38 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x69U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_105 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_105 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x98U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_152 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_152 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x2fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_47 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_47 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x87U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_135 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_135 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_3 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_3 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x64U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_100 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_100 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x33U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_51 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_51 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x90U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_144 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_144 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x27U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_39 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_39 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_106 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_106 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x30U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_48 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_48 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x88U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_136 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_136 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x1eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_30 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_30 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x91U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_145 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_145 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_4 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_4 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_0 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_0 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_107 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_107 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x31U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_49 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_49 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x89U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_137 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_137 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x1fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_31 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_31 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x92U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_146 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_146 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x28U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_40 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_40 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_1 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_1 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_108 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_108 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_5 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_5 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_138 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_138 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x20U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_32 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_32 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_139 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_139 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x21U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_33 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_33 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_14 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_14 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x22U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_34 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_34 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_109 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_109 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x94U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_148 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_148 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x2aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_42 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_42 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_11 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_11 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x95U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_149 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_149 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x2bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_43 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_43 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_110 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_110 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_15 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_15 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x2cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_44 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_44 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_111 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_111 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x96U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_150 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_150 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x2dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_45 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_45 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x70U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_112 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_112 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x97U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_151 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_151 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x2eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_46 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_46 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x71U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_113 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_113 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x72U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_114 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_114 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x73U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_115 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_115 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x74U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_116 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_116 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x75U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_117 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_117 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x76U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_118 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_118 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x77U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_119 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_119 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_12 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_12 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x78U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_120 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_120 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x79U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_121 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_121 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x99U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_153 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_153 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x57U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_87 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_87 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_154 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_154 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x58U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_88 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_88 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_155 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_155 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x59U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_89 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_89 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_156 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_156 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_9 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_9 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_157 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_157 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x5aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_90 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_90 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_158 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_158 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x5bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_91 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_91 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_159 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_159 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x5cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_92 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_92 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x10U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_16 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_16 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x5dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_93 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_93 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_160 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_160 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x5eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_94 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_94 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_161 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_161 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x5fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_95 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_95 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_162 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_162 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x60U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_96 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_96 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_163 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_163 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x61U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_97 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_97 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_164 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_164 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x62U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_98 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_98 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_165 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_165 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x63U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_99 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_99 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_166 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_166 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_0 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_0 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_167 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_167 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_1 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_1 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_168 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_168 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_10 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_10 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_169 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_169 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x64U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_100 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_100 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x11U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_17 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_17 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x65U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_101 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_101 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xaaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_170 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_170 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x66U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_102 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_102 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xabU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_171 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_171 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x67U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_103 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_103 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xacU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_172 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_172 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x68U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_104 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_104 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xadU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_173 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_173 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xaeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_174 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_174 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xafU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_175 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_175 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_176 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_176 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_177 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_177 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_178 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_178 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_179 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_179 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x12U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_18 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_18 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_180 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_180 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_181 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_181 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_182 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_182 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_183 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_183 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_184 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_184 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_185 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_185 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_186 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_186 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_187 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_187 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_188 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_188 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_189 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_189 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x13U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_19 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_19 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_190 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_190 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_191 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_191 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_192 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_192 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_193 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_193 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_194 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_194 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_195 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_195 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_196 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_196 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_197 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_197 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_198 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_198 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_199 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_199 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_2 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_2 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x14U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_20 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_20 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_200 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_200 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_201 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_201 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_202 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_202 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_203 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_203 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xccU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_204 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_204 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_205 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_205 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xceU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_206 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_206 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_207 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_207 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_208 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_208 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_209 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_209 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x15U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_21 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_21 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                     >> 0xfU))) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                                 << 0x16U) 
                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                                   >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_210 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_0_210 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_109 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_109 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_11 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_11 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_110 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_110 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x6fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_111 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_111 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x70U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_112 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_112 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x71U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_113 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_113 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x72U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_114 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_114 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x73U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_115 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_115 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x74U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_116 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_116 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x75U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_117 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_117 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x76U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_118 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_118 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x77U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_119 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_119 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_12 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_12 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x78U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_120 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_120 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x79U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_121 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_121 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_122 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_122 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_123 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_123 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_124 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_124 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_125 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_125 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_126 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_126 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x7fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_127 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_127 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x80U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_128 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_128 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x81U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_129 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_129 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_13 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_13 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x82U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_130 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_130 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x83U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_131 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_131 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x84U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_132 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_132 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x85U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_133 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_133 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x86U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_134 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_134 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x87U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_135 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_135 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x88U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_136 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_136 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x89U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_137 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_137 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_138 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_138 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_139 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_139 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_14 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_14 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_140 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_140 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_141 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_141 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_142 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_142 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x8fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_143 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_143 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x90U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_144 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_144 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x91U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_145 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_145 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x92U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_146 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_146 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x93U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_147 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_147 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x94U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_148 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_148 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x95U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_149 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_149 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_15 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_15 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x96U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_150 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_150 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x97U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_151 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_151 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x98U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_152 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_152 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x99U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_153 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_153 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9aU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_154 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_154 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9bU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_155 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_155 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9cU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_156 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_156 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9dU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_157 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_157 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9eU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_158 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_158 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x9fU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_159 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_159 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x10U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_16 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_16 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_160 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_160 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_161 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_161 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_162 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_162 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_163 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_163 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_164 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_164 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_165 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_165 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_166 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_166 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_167 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_167 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_168 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_168 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xa9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_169 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_169 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x11U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_17 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_17 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xaaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_170 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_170 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xabU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_171 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_171 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xacU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_172 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_172 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xadU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_173 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_173 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xaeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_174 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_174 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xafU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_175 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_175 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_176 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_176 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_177 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_177 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_178 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_178 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_179 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_179 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x12U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_18 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_18 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_180 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_180 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_181 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_181 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_182 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_182 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_183 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_183 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_184 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_184 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xb9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_185 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_185 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_186 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_186 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_187 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_187 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbcU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_188 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_188 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_189 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_189 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x13U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_19 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_19 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbeU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_190 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_190 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xbfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_191 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_191 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_192 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_192 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_193 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_193 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_194 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_194 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_195 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_195 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_196 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_196 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_197 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_197 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_198 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_198 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_199 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_199 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_2 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_2 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x14U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_20 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_20 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_200 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_200 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xc9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_201 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_201 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcaU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_202 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_202 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcbU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_203 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_203 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xccU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_204 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_204 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcdU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_205 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_205 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xceU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_206 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_206 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xcfU == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_207 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_207 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_208 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_208 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd1U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_209 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_209 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0x15U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_21 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_21 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd2U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_210 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_210 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd3U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_211 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_211 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd4U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_212 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_212 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd5U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_213 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_213 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_214 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_214 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_215 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_215 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd8U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_216 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_216 = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_mkConnectionVtoAf) 
                & (0xd9U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__bht_index___05F_h433635))) 
               & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[2U] 
                  >> 0xfU)) & (0U == (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                               >> 0xaU))))) 
             & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                >> 9U))) {
            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_217 
                = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[1U] 
                          << 0x14U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3__DOT__wr_training_data[0U] 
                                       >> 0xcU)));
        }
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage0__DOT__bpu__DOT__rg_bht_arr_1_217 = 1U;
    }
}
