// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc_mki2c.h"
#include "VmkSoc__Syms.h"

//==========

VL_INLINE_OPT void VmkSoc_mki2c::_sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__3(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mki2c::_sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__3\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_rd_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__i2c_user_sendInd = this->__PVT__i2c_user_sendInd;
    this->__Vdly__i2c_user_last_byte_read = this->__PVT__i2c_user_last_byte_read;
    this->__Vdly__i2c_user_val_SCL = this->__PVT__i2c_user_val_SCL;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c0.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    this->__PVT__i2c_user_val_SCL_in = (1U & ((~ (IData)(vlTOPp->RST_N)) 
                                              | (IData)(vlTOPp->i2c0_out_scl_in_in)));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock) 
              | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl))) {
            this->__PVT__i2c_user_coSCL = this->__PVT__i2c_user_coSCL_D_IN;
        }
    } else {
        this->__PVT__i2c_user_coSCL = 0U;
    }
    this->__Vdly__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_cprescaler_EN) {
            this->__PVT__i2c_user_cprescaler = this->__PVT__i2c_user_cprescaler_D_IN;
        }
    } else {
        this->__PVT__i2c_user_cprescaler = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (0x20U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))))) {
            this->__PVT__i2c_user_s01 = (0xffU & (IData)(
                                                         (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 4U)));
        }
    } else {
        this->__PVT__i2c_user_s01 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
               & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U)))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_zero = 0U;
        }
    } else {
        this->__PVT__i2c_user_zero = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
              & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_lab = 0U;
        }
    } else {
        this->__PVT__i2c_user_lab = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
              & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_sts = 0U;
        }
    } else {
        this->__PVT__i2c_user_sts = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
              & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_aas = 0U;
        }
    } else {
        this->__PVT__i2c_user_aas = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_eni = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                    >> 7U)));
        }
    } else {
        this->__PVT__i2c_user_eni = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
               & (IData)(this->__PVT__i2c_user_val_SCL)) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                 & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_ad0_lrb = ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1) 
                                             & (IData)(this->__PVT__i2c_user_val_SDA_in));
        }
    } else {
        this->__PVT__i2c_user_ad0_lrb = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
               & (IData)(this->__PVT__i2c_user_val_SDA)) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_sto = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                    >> 5U)));
        }
    } else {
        this->__PVT__i2c_user_sto = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                  | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_sta = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                    >> 6U)));
        }
    } else {
        this->__PVT__i2c_user_sta = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
                & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                        >> 5U))))) 
               & (~ (IData)(this->__PVT__i2c_user_bb))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                  & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
                     | (IData)(this->__PVT__i2c_user_bb))) 
                 & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
                    | (1U == (IData)(this->__PVT__x___05Fh6518))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_cOutEn = (1U & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)));
        }
    } else {
        this->__PVT__i2c_user_cOutEn = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                    | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                   & (IData)(this->__PVT__i2c_user_val_SDA)) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                     & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                    & (1U == (IData)(this->__PVT__x___05Fh6518)))) 
                | ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                     & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                >> 5U))))) 
                    & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                            >> 5U))))) 
                   & (~ (IData)(this->__PVT__i2c_user_bb)))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                  & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__Vdly__i2c_user_sendInd = this->__PVT__i2c_user_sendInd_D_IN;
        }
    } else {
        this->__Vdly__i2c_user_sendInd = 2U;
    }
    this->__PVT__i2c_user_val_sda_delay_9 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_8));
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
               & (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) {
            this->__PVT__i2c_user_configchange = ((
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                                    & (8U 
                                                       == 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))) 
                                                   & (6U 
                                                      != (IData)(this->__PVT__i2c_user_mTransFSM))) 
                                                  & (0xcU 
                                                     != (IData)(this->__PVT__i2c_user_mTransFSM)));
        }
    } else {
        this->__PVT__i2c_user_configchange = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_ack = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & ((((2U == 
                                               (3U 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & ((6U 
                                                  == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                 | (0xcU 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                             & (~ (IData)(this->__PVT__i2c_user_bb))) 
                                            | (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 4U))));
        }
    } else {
        this->__PVT__i2c_user_ack = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                >> 5U)))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock))) {
            this->__PVT__i2c_user_scl_start = this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1;
        }
    } else {
        this->__PVT__i2c_user_scl_start = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                >> 5U)))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock))) {
            this->__PVT__i2c_user_mod_start = this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1;
        }
    } else {
        this->__PVT__i2c_user_mod_start = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                             >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock))) {
            this->__PVT__i2c_user_reSCL = ((IData)(this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1)
                                            ? 0U : this->__PVT__i2c_user_c_scl);
        }
    } else {
        this->__PVT__i2c_user_reSCL = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & (0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock))) {
            this->__PVT__i2c_user_rprescaler = ((IData)(this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1)
                                                 ? 0U
                                                 : (IData)(this->__PVT__i2c_user_s2));
        }
    } else {
        this->__PVT__i2c_user_rprescaler = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_last_byte_read_EN) {
            this->__Vdly__i2c_user_last_byte_read = 
                ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                   & (IData)(this->__PVT__i2c_user_pin)) 
                  & (2U != (IData)(this->__PVT__x___05Fh6518))) 
                 & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178));
        }
    } else {
        this->__Vdly__i2c_user_last_byte_read = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                    & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                      & (1U == (IData)(this->__PVT__x___05Fh6518)))) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                     & (0x18U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                   >> 5U)))))) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                     & (IData)(this->__PVT__i2c_user_val_SDA_in)) 
                    & (IData)(this->__PVT__i2c_user_val_SCL))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                    & (IData)(this->__PVT__i2c_user_pin)) 
                   & (2U == (IData)(this->__PVT__x___05Fh6518)))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
                   & (IData)(this->__PVT__i2c_user_rstsig)) 
                  & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_ber = ((~ ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) 
                                         & ((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3) 
                                              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                                 & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343))) 
                                             | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin)) 
                                                & (2U 
                                                   == (IData)(this->__PVT__x___05Fh6518)))) 
                                            | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                                                & (IData)(this->__PVT__i2c_user_val_SDA_in)) 
                                               & (IData)(this->__PVT__i2c_user_val_SCL))));
        }
    } else {
        this->__PVT__i2c_user_ber = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                 & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                   & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                  & ((IData)(this->__PVT__i2c_user_pin) 
                     | ((IData)(this->__PVT__i2c_user_i2ctime) 
                        >> 0xeU)))) | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_i2ctimeout = this->__PVT__i2c_user_i2ctimeout_D_IN;
        }
    } else {
        this->__PVT__i2c_user_i2ctimeout = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
              & (~ (IData)(this->__PVT__i2c_user_pin))) 
             | (((IData)(this->__PVT__i2c_user_st_toggle) 
                 & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
                & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt))))) {
            this->__Vdly__i2c_user_val_SCL = this->__PVT__i2c_user_val_SCL_D_IN;
        }
    } else {
        this->__Vdly__i2c_user_val_SCL = 1U;
    }
    this->__PVT__i2c_user_dOutEn_delay_9 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_8));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    this->__PVT__i2c_user_val_sda_delay_8 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_7));
    if (vlTOPp->RST_N) {
        if (this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1) {
            this->__PVT__i2c_user_c_scl = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                   >> 4U));
        }
    } else {
        this->__PVT__i2c_user_c_scl = 0U;
    }
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale 
        = ((0U == (IData)(this->__PVT__i2c_user_cprescaler)) 
           & (0U != (IData)(this->__PVT__i2c_user_rprescaler)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1) {
            this->__PVT__i2c_user_s2 = (0xffU & (IData)(
                                                        (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                         >> 4U)));
        }
    } else {
        this->__PVT__i2c_user_s2 = 0xc0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_eso = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & ((((2U == 
                                               (3U 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & ((6U 
                                                  == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                 | (0xcU 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                             & (~ (IData)(this->__PVT__i2c_user_bb))) 
                                            | (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 0xaU))));
        }
    } else {
        this->__PVT__i2c_user_eso = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (0x30U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                 & (~ (IData)(this->__PVT__i2c_user_pin))) 
                & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140)))) {
            this->__PVT__i2c_user_i2ctime = this->__PVT__i2c_user_i2ctime_D_IN;
        }
    } else {
        this->__PVT__i2c_user_i2ctime = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                  & (IData)(this->__PVT__i2c_user_val_SDA)) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                     & (IData)(this->__PVT__i2c_user_bb)) 
                    & (2U == (IData)(this->__PVT__x___05Fh6518)))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                   & ((IData)(this->__PVT__i2c_user_pin) 
                      | (~ (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140))))) 
               | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                  & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_st_toggle = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                & ((IData)(this->__PVT__i2c_user_pin) 
                                                   | (~ (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140))))
                                                ? (IData)(this->__PVT__i2c_user_pin)
                                                : ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end)
                                                    ? 
                                                   (1U 
                                                    == (IData)(this->__PVT__x___05Fh6518))
                                                    : 
                                                   ((~ 
                                                     ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2) 
                                                      | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) 
                                                    & ((IData)(this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2) 
                                                       | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1)))));
        }
    } else {
        this->__PVT__i2c_user_st_toggle = 0U;
    }
    this->__PVT__i2c_user_dOutEn_delay_8 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_7));
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl = 
        (((0U == this->__PVT__i2c_user_coSCL) & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
         & (0U != this->__PVT__i2c_user_reSCL));
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__i2c_user_val_sda_delay_7 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_6));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock 
        = ((IData)(this->__PVT__i2c_user_scl_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock 
        = ((IData)(this->__PVT__i2c_user_mod_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__CAN_FIRE_RL_i2c_user_count_scl = (
                                                   (((0U 
                                                      != this->__PVT__i2c_user_coSCL) 
                                                     & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                                    & (IData)(this->__PVT__i2c_user_eso)) 
                                                   & (IData)(this->__PVT__i2c_user_st_toggle));
    this->__PVT__i2c_user_dOutEn_delay_7 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_6));
    this->__PVT__i2c_user_cprescaler_EN = (((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock) 
                                            | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                           | (0U != (IData)(this->__PVT__i2c_user_cprescaler)));
    this->__PVT__i2c_user_cprescaler_D_IN = (0xffU 
                                             & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock)
                                                 ? (IData)(this->__PVT__i2c_user_s2)
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)
                                                  ? (IData)(this->__PVT__i2c_user_rprescaler)
                                                  : 
                                                 ((0U 
                                                   != (IData)(this->__PVT__i2c_user_cprescaler))
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_cprescaler) 
                                                   - (IData)(1U))
                                                   : 0U))));
    this->__PVT__i2c_user_coSCL_D_IN = ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock)
                                         ? this->__PVT__i2c_user_c_scl
                                         : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)
                                             ? this->__PVT__i2c_user_reSCL
                                             : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl)
                                                 ? 
                                                (this->__PVT__i2c_user_coSCL 
                                                 - (IData)(1U))
                                                 : 0U)));
    this->__PVT__i2c_user_val_sda_delay_6 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_5));
    this->__PVT__i2c_user_dOutEn_delay_6 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_5));
    this->__PVT__i2c_user_val_sda_delay_5 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_4));
    this->__PVT__i2c_user_dOutEn_delay_5 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_4));
    this->__PVT__i2c_user_val_sda_delay_4 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_3));
    this->__PVT__i2c_user_dOutEn_delay_4 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_3));
    this->__PVT__i2c_user_val_sda_delay_3 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_2));
    this->__PVT__i2c_user_dOutEn_delay_3 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_2));
    this->__PVT__i2c_user_val_sda_delay_2 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_1));
    this->__PVT__i2c_user_dOutEn_delay_2 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_1));
    this->__PVT__i2c_user_val_sda_delay_1 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_0));
    this->__PVT__i2c_user_dOutEn_delay_1 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_0));
    this->__PVT__i2c_user_val_sda_delay_0 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__i2c_user_dOutEn_delay_0 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn));
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                    & (IData)(this->__PVT__i2c_user_val_SCL)) 
                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                      & (0U == (IData)(this->__PVT__i2c_user_dataBit)))) 
                  | ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                       & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                  >> 5U))))) 
                      & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                              >> 5U))))) 
                     & (~ (IData)(this->__PVT__i2c_user_bb)))) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                     & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
                        | (IData)(this->__PVT__i2c_user_bb))) 
                    & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
                       | (1U == (IData)(this->__PVT__x___05Fh6518))))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                    & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                   & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118))) 
               | ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                    & (IData)(this->__PVT__i2c_user_pin)) 
                   & (2U != (IData)(this->__PVT__x___05Fh6518))) 
                  & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                  & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                 & (1U == (IData)(this->__PVT__i2c_user_dataBit)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_dOutEn = (1U & ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)
                                                   ? 
                                                  ((0xaU 
                                                    == (IData)(this->__PVT__i2c_user_s3)) 
                                                   | (~ (IData)(this->__PVT__i2c_user_operation)))
                                                   : 
                                                  ((~ 
                                                    (((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5) 
                                                      | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7)) 
                                                     | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) 
                                                   & ((((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1) 
                                                        | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2)) 
                                                       | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1)) 
                                                      | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)))));
        }
    } else {
        this->__PVT__i2c_user_dOutEn = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5) {
            this->__PVT__i2c_user_operation = (1U & (IData)(this->__PVT__i2c_user_s0));
        }
    } else {
        this->__PVT__i2c_user_operation = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                     & (IData)(this->__PVT__i2c_user_val_SCL)) 
                    | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                       & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
                   | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                      & (0x28U == (0xffU & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                    >> 5U)))))) 
                  | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                      & (~ (IData)(this->__PVT__i2c_user_pin))) 
                     & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                    & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                   & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                   & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                  & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
                  & (IData)(this->__PVT__i2c_user_rstsig)) 
                 & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)))) 
             | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                 & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
                & (IData)(this->__PVT__i2c_user_last_byte_read)))) {
            this->__PVT__i2c_user_s3 = (0xffU & (((
                                                   ((((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                                        | (IData)(this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2)) 
                                                       | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)) 
                                                      | (IData)(this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2)) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8))
                                                  ? 
                                                 ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1)
                                                   ? 7U
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2)
                                                    ? (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 4U))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)
                                                     ? 8U
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2)
                                                      ? 0xcU
                                                      : 
                                                     ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)
                                                       ? 
                                                      ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                        ? 5U
                                                        : 6U)
                                                       : 
                                                      ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)
                                                        ? 1U
                                                        : 
                                                       ((IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)
                                                         ? 0xaU
                                                         : 0xbU)))))))
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1)
                                                   ? 9U
                                                   : 0U)));
        }
    } else {
        this->__PVT__i2c_user_s3 = 9U;
    }
    this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140 
        = ((IData)(this->__PVT__i2c_user_i2ctimeout) 
           <= (0x3fffU & (IData)(this->__PVT__i2c_user_i2ctime)));
    if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
          & (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
         | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data))) {
        this->__PVT__i2c_user_s0 = this->__PVT__i2c_user_s0_D_IN;
    }
    if (vlTOPp->RST_N) {
        if ((((((((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                  & (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                >> 5U))))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                    & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                    & (~ (IData)(this->__PVT__i2c_user_val_SDA_in))) 
                   & (IData)(this->__PVT__i2c_user_val_SCL))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                   & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
                  & (IData)(this->__PVT__i2c_user_last_byte_read))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & ((IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323) 
                    | (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                  >> 5U))))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_pin = (1U & ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state) 
                                               | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                                   & ((IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323) 
                                                      | (0x10U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                     >> 5U))))))
                                                   ? 
                                                  (((8U 
                                                     != 
                                                     (0xffU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U)))) 
                                                    | (((2U 
                                                         == 
                                                         (3U 
                                                          & (IData)(
                                                                    (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                     >> 5U)))) 
                                                        & ((6U 
                                                            == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                           | (0xcU 
                                                              == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                                       & (~ (IData)(this->__PVT__i2c_user_bb)))) 
                                                   | (IData)(
                                                             (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 0xbU)))
                                                   : 
                                                  ((~ 
                                                    (((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2) 
                                                      | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                                                          & (~ (IData)(this->__PVT__i2c_user_val_SDA_in))) 
                                                         & (IData)(this->__PVT__i2c_user_val_SCL))) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2))) 
                                                   & ((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                                      & (0x10U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                     >> 5U)))))))));
        }
    } else {
        this->__PVT__i2c_user_pin = 1U;
    }
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__data0_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__PVT__i2c_user_val_SDA_in = (1U & ((~ (IData)(vlTOPp->RST_N)) 
                                              | (IData)(vlTOPp->i2c0_out_sda_in_in)));
    this->__PVT__s_xactor_f_wr_resp_D_IN = (((IData)(this->__PVT__i2c_user_ber) 
                                             | (((((((8U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U)))) 
                                                     & (0x20U 
                                                        != 
                                                        (0xffU 
                                                         & (IData)(
                                                                   (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                    >> 5U))))) 
                                                    & (0x10U 
                                                       != 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))) 
                                                   & (0U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U))))) 
                                                  & (0x28U 
                                                     != 
                                                     (0xffU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 & (0x38U 
                                                    != 
                                                    (0xffU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (0x30U 
                                                   != 
                                                   (0xffU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U))))))
                                             ? 2U : 0U);
    this->__PVT__r1___05Fread___05Fh12450 = (((((IData)(this->__PVT__i2c_user_pin) 
                                                << 6U) 
                                               | (((IData)(this->__PVT__i2c_user_zero) 
                                                   << 5U) 
                                                  | ((IData)(this->__PVT__i2c_user_sts) 
                                                     << 4U))) 
                                              | (((IData)(this->__PVT__i2c_user_ber) 
                                                  << 3U) 
                                                 | ((IData)(this->__PVT__i2c_user_ad0_lrb) 
                                                    << 2U))) 
                                             | (((IData)(this->__PVT__i2c_user_aas) 
                                                 << 1U) 
                                                | (IData)(this->__PVT__i2c_user_lab)));
    if (vlTOPp->RST_N) {
        if ((((((((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                          & (IData)(this->__PVT__i2c_user_val_SCL)) 
                         | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                            & (IData)(this->__PVT__i2c_user_val_SDA))) 
                        | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                           & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
                       | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
                          & (1U == (IData)(this->__PVT__x___05Fh6518)))) 
                      | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                          & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
                             | (IData)(this->__PVT__i2c_user_bb))) 
                         & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
                            | (1U == (IData)(this->__PVT__x___05Fh6518))))) 
                     | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                        & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                    | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                       & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                   | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                       & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                      & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118))) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                     & (IData)(this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155))) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                     & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                    & (1U == (IData)(this->__PVT__i2c_user_dataBit)))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
                    & (IData)(this->__PVT__i2c_user_rstsig)) 
                   & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)))) 
               | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                  & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_mTransFSM = this->__PVT__i2c_user_mTransFSM_D_IN;
        }
    } else {
        this->__PVT__i2c_user_mTransFSM = 1U;
    }
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__i2c_user_val_SCL = this->__Vdly__i2c_user_val_SCL;
    if ((1U & (~ (IData)(vlTOPp->RST_N)))) {
        this->__PVT__i2c_user_rstsig = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_dataBit_EN) {
            this->__PVT__i2c_user_dataBit = this->__PVT__i2c_user_dataBit_D_IN;
        }
    } else {
        this->__PVT__i2c_user_dataBit = 9U;
    }
    this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U)))));
    this->__PVT__WILL_FIRE_RL_read_request = (((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                                              & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U))))) & 
           (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__i2c_user_i2ctime_D_IN = (0xffffU & 
                                          (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                            & (0x30U 
                                               == (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                            ? (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 4U))
                                            : (0x8000U 
                                               | (0x7fffU 
                                                  & (IData)(this->__PVT__i2c_user_i2ctime)))));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg 
        = (((((((IData)(this->__PVT__i2c_user_configchange) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (1U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__CAN_FIRE_RL_i2c_user_receive_data 
        = ((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & 
           (~ (IData)(this->__PVT__i2c_user_val_SCL)));
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
               & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_resetcount = this->__PVT__i2c_user_resetcount_D_IN;
        }
    } else {
        this->__PVT__i2c_user_resetcount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
               & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                  | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_bb = (1U & (~ ((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
                                                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))));
        }
    } else {
        this->__PVT__i2c_user_bb = 1U;
    }
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt 
        = ((((((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack = 
        (((((0xbU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
            & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
           & (IData)(this->__PVT__i2c_user_eso)) & 
          (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition 
        = ((((((0xfU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (IData)(this->__PVT__i2c_user_val_SCL_in)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_data = 
        ((((((7U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_addr = 
        (((5U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (4U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_reset_state 
        = ((((((9U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (~ (IData)(this->__PVT__i2c_user_ber))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__i2c_user_s0_D_IN = (0xffU & (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                               & (0x10U 
                                                  == 
                                                  (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                               ? (IData)(
                                                         (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 4U))
                                               : ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   | ((IData)(1U) 
                                                      << 
                                                      (0xfU 
                                                       & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U)))))
                                                   : 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   & (~ 
                                                      ((IData)(1U) 
                                                       << 
                                                       (0xfU 
                                                        & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U)))))))));
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end 
        = ((((8U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113 
        = ((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110 
        = (((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
              | (1U == (IData)(this->__PVT__i2c_user_dataBit))) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118 
        = ((((1U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1 
        = ((((((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                 | (3U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (IData)(this->__PVT__i2c_user_val_SCL)) 
             & ((0U == (IData)(this->__PVT__i2c_user_dataBit)) 
                | (8U == (IData)(this->__PVT__i2c_user_dataBit)))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
            & (~ (IData)(this->__PVT__i2c_user_pin))) 
           & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140));
    this->__PVT__i2c_user_val_SCL_D_IN = (1U & ((~ 
                                                 ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (~ (IData)(this->__PVT__i2c_user_pin)))) 
                                                & (~ (IData)(this->__PVT__i2c_user_val_SCL))));
    this->__PVT__i2c_user_i2ctimeout_D_IN = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                              & ((IData)(this->__PVT__i2c_user_pin) 
                                                 | ((IData)(this->__PVT__i2c_user_i2ctime) 
                                                    >> 0xeU)))
                                              ? ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 1U
                                                  : 
                                                 (0x3fffU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_i2ctimeout))))
                                              : 1U);
    this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
           & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (1U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343 
        = (((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U)))) & 
              (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323 
        = ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                    >> 5U)))) & (((
                                                   (6U 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                   | (0xcU 
                                                      == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                                                  | (2U 
                                                     != 
                                                     (3U 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & (~ (IData)(this->__PVT__i2c_user_bb))) 
           | (((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & ((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               | (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
           & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286 
        = (((((((((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
                 | (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                               >> 5U))))) 
                | (0x18U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                              >> 5U))))) 
               | (0x20U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
              | (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))) 
             | (0x30U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U))))) 
            | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                          >> 5U)))))
            ? ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                        >> 5U)))) ? 
               (((IData)(this->__PVT__i2c_user_s2) 
                 << 0x18U) | (((IData)(this->__PVT__i2c_user_s2) 
                               << 0x10U) | (((IData)(this->__PVT__i2c_user_s2) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s2))))
                : ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))
                    ? (((IData)(this->__PVT__i2c_user_pin) 
                        << 0x1fU) | (((IData)(this->__PVT__i2c_user_pin) 
                                      << 0x17U) | (
                                                   ((IData)(this->__PVT__i2c_user_pin) 
                                                    << 0xfU) 
                                                   | ((IData)(this->__PVT__i2c_user_pin) 
                                                      << 7U))))
                    : ((0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                   >> 5U))))
                        ? (((IData)(this->__PVT__i2c_user_s0) 
                            << 0x18U) | (((IData)(this->__PVT__i2c_user_s0) 
                                          << 0x10U) 
                                         | (((IData)(this->__PVT__i2c_user_s0) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s0))))
                        : ((0x18U == (0xffU & (IData)(
                                                      (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                       >> 5U))))
                            ? (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                << 0x19U) | (((IData)(this->__PVT__i2c_user_bb) 
                                              << 0x18U) 
                                             | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                 << 0x11U) 
                                                | (((IData)(this->__PVT__i2c_user_bb) 
                                                    << 0x10U) 
                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                       << 9U) 
                                                      | (((IData)(this->__PVT__i2c_user_bb) 
                                                          << 8U) 
                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                             << 1U) 
                                                            | (IData)(this->__PVT__i2c_user_bb))))))))
                            : ((0x20U == (0xffU & (IData)(
                                                          (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                           >> 5U))))
                                ? (((IData)(this->__PVT__i2c_user_s01) 
                                    << 0x18U) | (((IData)(this->__PVT__i2c_user_s01) 
                                                  << 0x10U) 
                                                 | (((IData)(this->__PVT__i2c_user_s01) 
                                                     << 8U) 
                                                    | (IData)(this->__PVT__i2c_user_s01))))
                                : ((0x28U == (0xffU 
                                              & (IData)(
                                                        (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                         >> 5U))))
                                    ? (((IData)(this->__PVT__i2c_user_s3) 
                                        << 0x18U) | 
                                       (((IData)(this->__PVT__i2c_user_s3) 
                                         << 0x10U) 
                                        | (((IData)(this->__PVT__i2c_user_s3) 
                                            << 8U) 
                                           | (IData)(this->__PVT__i2c_user_s3))))
                                    : ((0x30U == (0xffU 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                             >> 5U))))
                                        ? (((IData)(this->__PVT__i2c_user_i2ctime) 
                                            << 0x10U) 
                                           | (IData)(this->__PVT__i2c_user_i2ctime))
                                        : this->__PVT__i2c_user_c_scl)))))))
            : 0U);
    this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (2U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__WILL_FIRE_RL_i2c_user_idler) {
            this->__PVT__i2c_user_cycwaste = this->__PVT__i2c_user_cycwaste_D_IN;
        }
    } else {
        this->__PVT__i2c_user_cycwaste = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_val_SDA_EN) {
            this->__PVT__i2c_user_val_SDA = (((((((
                                                   ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1)) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7)) 
                                                | (IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)) 
                                              | ((IData)(this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1))) 
                                             & ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2)
                                                 ? (IData)(this->__PVT__i2c_user_last_byte_read)
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1)
                                                  ? 
                                                 ((2U 
                                                   >= (IData)(this->__PVT__i2c_user_sendInd)) 
                                                  & (6U 
                                                     >> (IData)(this->__PVT__i2c_user_sendInd)))
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)
                                                   ? (IData)(this->__PVT__x___05Fh10210)
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)
                                                    ? 
                                                   ((2U 
                                                     >= (IData)(this->__PVT__i2c_user_sendInd)) 
                                                    & (1U 
                                                       >> (IData)(this->__PVT__i2c_user_sendInd)))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7)
                                                     ? (IData)(this->__PVT__x___05Fh10210)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8)
                                                      ? 
                                                     ((1U 
                                                       != (IData)(this->__PVT__x___05Fh6518)) 
                                                      & (IData)(this->__PVT__x___05Fh10210))
                                                      : 
                                                     (~ (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)))))))));
        }
    } else {
        this->__PVT__i2c_user_val_SDA = 1U;
    }
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118));
    this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             (((((((((8U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                     & (0x18U 
                                                                        != 
                                                                        (0xffU 
                                                                         & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                    & (0x20U 
                                                                       != 
                                                                       (0xffU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   & (0x10U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (0U 
                                                                     != 
                                                                     (0xffU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                 & (0x28U 
                                                                    != 
                                                                    (0xffU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                & (0x30U 
                                                                   != 
                                                                   (0xffU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U))))) 
                                                               & (0x38U 
                                                                  != 
                                                                  (0xffU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))))
                                                               ? 2U
                                                               : 0U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286)));
    this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans));
    this->__PVT__WILL_FIRE_RL_i2c_user_idler = ((1U 
                                                 == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113));
    this->__PVT__x___05Fh10210 = (1U & ((IData)(this->__PVT__i2c_user_s0) 
                                        >> (7U & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                  - (IData)(2U)))));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
           & (0U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
           & (1U < (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340));
    this->__PVT__x___05Fh6518 = (((IData)(this->__PVT__i2c_user_sta) 
                                  << 1U) | (IData)(this->__PVT__i2c_user_sto));
    this->__PVT__i2c_user_last_byte_read = this->__Vdly__i2c_user_last_byte_read;
    this->__PVT__i2c_user_sendInd = this->__Vdly__i2c_user_sendInd;
    this->__PVT__i2c_user_cycwaste_D_IN = (0x3ffU & 
                                           ((0U == (IData)(this->__PVT__i2c_user_cycwaste))
                                             ? ((IData)(1U) 
                                                + (IData)(this->__PVT__i2c_user_cycwaste))
                                             : ((0x258U 
                                                 == (IData)(this->__PVT__i2c_user_cycwaste))
                                                 ? 0U
                                                 : 
                                                ((IData)(1U) 
                                                 + (IData)(this->__PVT__i2c_user_cycwaste)))));
    this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
           & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)));
    this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter 
        = (1U & ((~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler))));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1 
        = ((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178 
        = ((((1U != (IData)(this->__PVT__x___05Fh6518)) 
             & (0xaU != (IData)(this->__PVT__i2c_user_s3))) 
            & (IData)(this->__PVT__i2c_user_operation)) 
           & (~ (IData)(this->__PVT__i2c_user_ack)));
    this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165 
        = (((IData)(this->__PVT__i2c_user_pin) & (2U 
                                                  != (IData)(this->__PVT__x___05Fh6518))) 
           & (((1U == (IData)(this->__PVT__x___05Fh6518)) 
               | (0xaU == (IData)(this->__PVT__i2c_user_s3))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155 
        = (((IData)(this->__PVT__i2c_user_pin) & ((
                                                   (((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518)) 
                                                     | (1U 
                                                        == (IData)(this->__PVT__x___05Fh6518))) 
                                                    | (0xaU 
                                                       == (IData)(this->__PVT__i2c_user_s3))) 
                                                   | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
                                                  | (~ (IData)(this->__PVT__i2c_user_operation)))) 
           | ((~ (IData)(this->__PVT__i2c_user_pin)) 
              & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140)));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168 
        = ((1U != (IData)(this->__PVT__x___05Fh6518)) 
           & (((0xaU == (IData)(this->__PVT__i2c_user_s3)) 
               | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (2U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
               | (IData)(this->__PVT__i2c_user_bb))) 
           & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
              | (1U == (IData)(this->__PVT__x___05Fh6518))));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
            & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_last_byte_read));
    this->__PVT__i2c_user_resetcount_D_IN = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter)
                                              ? ((IData)(this->__PVT__i2c_user_rstsig)
                                                  ? 
                                                 (0x3fU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_resetcount)))
                                                  : 0U)
                                              : 0U);
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
            & (IData)(this->__PVT__i2c_user_rstsig)) 
           & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)));
    this->__PVT__i2c_user_last_byte_read_EN = (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (IData)(this->__PVT__i2c_user_pin)) 
                                                 & (2U 
                                                    != (IData)(this->__PVT__x___05Fh6518))) 
                                                & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178)) 
                                               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                   & (0U 
                                                      == (IData)(this->__PVT__i2c_user_dataBit))) 
                                                  & (IData)(this->__PVT__i2c_user_last_byte_read)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
           & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165));
    this->__PVT__i2c_user_val_SDA_EN = (((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                                                & (IData)(this->__PVT__i2c_user_val_SDA)) 
                                               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
                                                  & (1U 
                                                     == (IData)(this->__PVT__x___05Fh6518)))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                                 & (1U 
                                                    < (IData)(this->__PVT__i2c_user_dataBit)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                & (0U 
                                                   == (IData)(this->__PVT__i2c_user_dataBit)))) 
                                            | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                                                & (IData)(this->__PVT__i2c_user_bb)) 
                                               & (2U 
                                                  == (IData)(this->__PVT__x___05Fh6518)))) 
                                           | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                                              & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                          | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                             & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113))) 
                                         | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                            & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165))) 
                                        | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340)));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
             & (IData)(this->__PVT__i2c_user_pin)) 
            & (2U != (IData)(this->__PVT__x___05Fh6518))) 
           & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168));
    this->__PVT__i2c_user_dataBit_EN = (((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                               & (1U 
                                                  < (IData)(this->__PVT__i2c_user_dataBit))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                                                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110))) 
                                            | ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin)) 
                                                & (2U 
                                                   != (IData)(this->__PVT__x___05Fh6518))) 
                                               & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168))) 
                                           | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                               & (0U 
                                                  == (IData)(this->__PVT__i2c_user_dataBit))) 
                                              & (IData)(this->__PVT__i2c_user_last_byte_read))) 
                                          | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                             & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340))) 
                                         | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                        | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack));
    this->__PVT__i2c_user_sendInd_D_IN = (3U & (((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
                                                 & (IData)(this->__PVT__i2c_user_val_SDA))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_sendInd) 
                                                 - (IData)(1U))
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_sendInd) 
                                                  - (IData)(1U))
                                                  : 
                                                 (((((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3)) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1)) 
                                                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                      | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)))
                                                   ? 2U
                                                   : 0U))));
    this->__PVT__i2c_user_mTransFSM_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                                                | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)) 
                                              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)) 
                                             | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7))
                                             ? ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1)
                                                 ? 9U
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)
                                                  ? 1U
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)
                                                   ? 0xfU
                                                   : 
                                                  ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)
                                                    ? 1U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)
                                                     ? 
                                                    ((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518))
                                                      ? 2U
                                                      : 0xfU)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)
                                                      ? 
                                                     ((8U 
                                                       == 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))
                                                       ? 
                                                      ((((2U 
                                                          == 
                                                          (3U 
                                                           & (IData)(
                                                                     (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                      >> 5U)))) 
                                                         & ((6U 
                                                             == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                            | (0xcU 
                                                               == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                                        & (~ (IData)(this->__PVT__i2c_user_bb)))
                                                        ? 4U
                                                        : 0xfU)
                                                       : 1U)
                                                      : 
                                                     ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)
                                                       ? 
                                                      ((0U 
                                                        == (IData)(this->__PVT__i2c_user_dataBit))
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_last_byte_read)
                                                         ? 3U
                                                         : 0xbU)
                                                        : 0xcU)
                                                       : 0xbU)))))))
                                             : (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 
                                                 ((1U 
                                                   == (IData)(this->__PVT__x___05Fh6518))
                                                   ? 0xfU
                                                   : 
                                                  ((2U 
                                                    == (IData)(this->__PVT__x___05Fh6518))
                                                    ? 9U
                                                    : 
                                                   ((0xaU 
                                                     == (IData)(this->__PVT__i2c_user_s3))
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__i2c_user_operation)
                                                      ? 8U
                                                      : 7U))))
                                                  : 0xfU)
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 0xcU
                                                   : 6U)
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)
                                                   ? 0xbU
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)
                                                    ? 6U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8)
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1)
                                                      ? 0x10U
                                                      : 0U)))))));
    this->__PVT__i2c_user_dataBit_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)) 
                                                | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)) 
                                              | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)) 
                                            | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)) 
                                           | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8))
                                           ? ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1)
                                               ? 9U
                                               : ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)
                                                   ? 8U
                                                   : 
                                                  (0xfU 
                                                   & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)
                                                       ? 
                                                      ((IData)(this->__PVT__i2c_user_dataBit) 
                                                       - (IData)(1U))
                                                       : 
                                                      ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_dataBit) 
                                                        - (IData)(1U))
                                                        : 
                                                       ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)
                                                         ? 
                                                        ((0xaU 
                                                          == (IData)(this->__PVT__i2c_user_s3))
                                                          ? 
                                                         ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U))
                                                          : 
                                                         ((IData)(this->__PVT__i2c_user_operation)
                                                           ? 8U
                                                           : 
                                                          ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U))))
                                                         : 
                                                        ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)
                                                          ? 9U
                                                          : 
                                                         ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)
                                                           ? 
                                                          ((1U 
                                                            >= (IData)(this->__PVT__i2c_user_dataBit))
                                                            ? 8U
                                                            : 
                                                           ((IData)(this->__PVT__i2c_user_dataBit) 
                                                            - (IData)(1U)))
                                                           : 9U))))))))
                                           : 0U);
}

VL_INLINE_OPT void VmkSoc_mki2c::_sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__5(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mki2c::_sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__5\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 382, column 32: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_set_scl_clock] and\n  [RL_i2c_user_restore_scl] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 382, column 32: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_set_scl_clock] and\n  [RL_i2c_user_count_scl] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 382, column 46: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_count_scl] and\n  [RL_i2c_user_restore_scl] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 381, column 52: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_send_addr] and\n  [RL_i2c_user_receive_data] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 381, column 42: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_check_Ack] and\n  [RL_i2c_user_receive_data] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 381, column 42: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_check_Ack] and\n  [RL_i2c_user_send_addr] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 381, column 42: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_check_Ack] and\n  [RL_i2c_user_send_data] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 381, column 32: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_send_data] and\n  [RL_i2c_user_receive_data] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 381, column 32: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_send_data] and\n  [RL_i2c_user_send_addr] ) fired in the same clock cycle.\n\n");
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)))) {
            VL_WRITEF("Error: \"devices/i2c//i2c.bsv\", line 482, column 32: (R0001)\n  Mutually exclusive rules (from the ME sets [RL_i2c_user_check_control_reg]\n  and [RL_i2c_user_send_start_trans] ) fired in the same clock cycle.\n\n");
        }
    }
}

VL_INLINE_OPT void VmkSoc_mki2c::_sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1__4(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mki2c::_sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1__4\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_rd_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__i2c_user_sendInd = this->__PVT__i2c_user_sendInd;
    this->__Vdly__i2c_user_last_byte_read = this->__PVT__i2c_user_last_byte_read;
    this->__Vdly__i2c_user_val_SCL = this->__PVT__i2c_user_val_SCL;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.mixed_cluster.i2c1.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    this->__PVT__i2c_user_val_SCL_in = (1U & ((~ (IData)(vlTOPp->RST_N)) 
                                              | (IData)(vlTOPp->i2c1_out_scl_in_in)));
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock) 
              | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl))) {
            this->__PVT__i2c_user_coSCL = this->__PVT__i2c_user_coSCL_D_IN;
        }
    } else {
        this->__PVT__i2c_user_coSCL = 0U;
    }
    this->__Vdly__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_cprescaler_EN) {
            this->__PVT__i2c_user_cprescaler = this->__PVT__i2c_user_cprescaler_D_IN;
        }
    } else {
        this->__PVT__i2c_user_cprescaler = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (0x20U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))))) {
            this->__PVT__i2c_user_s01 = (0xffU & (IData)(
                                                         (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 4U)));
        }
    } else {
        this->__PVT__i2c_user_s01 = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
               & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U)))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_zero = 0U;
        }
    } else {
        this->__PVT__i2c_user_zero = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
              & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_lab = 0U;
        }
    } else {
        this->__PVT__i2c_user_lab = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
              & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_sts = 0U;
        }
    } else {
        this->__PVT__i2c_user_sts = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
              & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_aas = 0U;
        }
    } else {
        this->__PVT__i2c_user_aas = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_eni = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                    >> 7U)));
        }
    } else {
        this->__PVT__i2c_user_eni = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
               & (IData)(this->__PVT__i2c_user_val_SCL)) 
              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                 & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_ad0_lrb = ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1) 
                                             & (IData)(this->__PVT__i2c_user_val_SDA_in));
        }
    } else {
        this->__PVT__i2c_user_ad0_lrb = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
               & (IData)(this->__PVT__i2c_user_val_SDA)) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_sto = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                    >> 5U)));
        }
    } else {
        this->__PVT__i2c_user_sto = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                  | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_sta = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                    >> 6U)));
        }
    } else {
        this->__PVT__i2c_user_sta = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
                & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                        >> 5U))))) 
               & (~ (IData)(this->__PVT__i2c_user_bb))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                  & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
                     | (IData)(this->__PVT__i2c_user_bb))) 
                 & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
                    | (1U == (IData)(this->__PVT__x___05Fh6518))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_cOutEn = (1U & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)));
        }
    } else {
        this->__PVT__i2c_user_cOutEn = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                    | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                   & (IData)(this->__PVT__i2c_user_val_SDA)) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                     & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                    & (1U == (IData)(this->__PVT__x___05Fh6518)))) 
                | ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                     & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                >> 5U))))) 
                    & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                            >> 5U))))) 
                   & (~ (IData)(this->__PVT__i2c_user_bb)))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                  & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__Vdly__i2c_user_sendInd = this->__PVT__i2c_user_sendInd_D_IN;
        }
    } else {
        this->__Vdly__i2c_user_sendInd = 2U;
    }
    this->__PVT__i2c_user_val_sda_delay_9 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_8));
    if (vlTOPp->RST_N) {
        if ((((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
               & (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) {
            this->__PVT__i2c_user_configchange = ((
                                                   ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                                    & (8U 
                                                       == 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))) 
                                                   & (6U 
                                                      != (IData)(this->__PVT__i2c_user_mTransFSM))) 
                                                  & (0xcU 
                                                     != (IData)(this->__PVT__i2c_user_mTransFSM)));
        }
    } else {
        this->__PVT__i2c_user_configchange = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_ack = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & ((((2U == 
                                               (3U 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & ((6U 
                                                  == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                 | (0xcU 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                             & (~ (IData)(this->__PVT__i2c_user_bb))) 
                                            | (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 4U))));
        }
    } else {
        this->__PVT__i2c_user_ack = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                >> 5U)))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock))) {
            this->__PVT__i2c_user_scl_start = this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1;
        }
    } else {
        this->__PVT__i2c_user_scl_start = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                >> 5U)))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock))) {
            this->__PVT__i2c_user_mod_start = this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1;
        }
    } else {
        this->__PVT__i2c_user_mod_start = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                             >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock))) {
            this->__PVT__i2c_user_reSCL = ((IData)(this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1)
                                            ? 0U : this->__PVT__i2c_user_c_scl);
        }
    } else {
        this->__PVT__i2c_user_reSCL = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
               & (0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))) 
             | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock))) {
            this->__PVT__i2c_user_rprescaler = ((IData)(this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1)
                                                 ? 0U
                                                 : (IData)(this->__PVT__i2c_user_s2));
        }
    } else {
        this->__PVT__i2c_user_rprescaler = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_last_byte_read_EN) {
            this->__Vdly__i2c_user_last_byte_read = 
                ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                   & (IData)(this->__PVT__i2c_user_pin)) 
                  & (2U != (IData)(this->__PVT__x___05Fh6518))) 
                 & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178));
        }
    } else {
        this->__Vdly__i2c_user_last_byte_read = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                    & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                      & (1U == (IData)(this->__PVT__x___05Fh6518)))) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                     & (0x18U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                   >> 5U)))))) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                     & (IData)(this->__PVT__i2c_user_val_SDA_in)) 
                    & (IData)(this->__PVT__i2c_user_val_SCL))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                    & (IData)(this->__PVT__i2c_user_pin)) 
                   & (2U == (IData)(this->__PVT__x___05Fh6518)))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
                   & (IData)(this->__PVT__i2c_user_rstsig)) 
                  & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_ber = ((~ ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) 
                                         & ((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3) 
                                              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                                 & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343))) 
                                             | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin)) 
                                                & (2U 
                                                   == (IData)(this->__PVT__x___05Fh6518)))) 
                                            | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                                                & (IData)(this->__PVT__i2c_user_val_SDA_in)) 
                                               & (IData)(this->__PVT__i2c_user_val_SCL))));
        }
    } else {
        this->__PVT__i2c_user_ber = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                 & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                   & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                  & ((IData)(this->__PVT__i2c_user_pin) 
                     | ((IData)(this->__PVT__i2c_user_i2ctime) 
                        >> 0xeU)))) | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_i2ctimeout = this->__PVT__i2c_user_i2ctimeout_D_IN;
        }
    } else {
        this->__PVT__i2c_user_i2ctimeout = 1U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
              & (~ (IData)(this->__PVT__i2c_user_pin))) 
             | (((IData)(this->__PVT__i2c_user_st_toggle) 
                 & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
                & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt))))) {
            this->__Vdly__i2c_user_val_SCL = this->__PVT__i2c_user_val_SCL_D_IN;
        }
    } else {
        this->__Vdly__i2c_user_val_SCL = 1U;
    }
    this->__PVT__i2c_user_dOutEn_delay_9 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_8));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    this->__PVT__i2c_user_val_sda_delay_8 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_7));
    if (vlTOPp->RST_N) {
        if (this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1) {
            this->__PVT__i2c_user_c_scl = (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                   >> 4U));
        }
    } else {
        this->__PVT__i2c_user_c_scl = 0U;
    }
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale 
        = ((0U == (IData)(this->__PVT__i2c_user_cprescaler)) 
           & (0U != (IData)(this->__PVT__i2c_user_rprescaler)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1) {
            this->__PVT__i2c_user_s2 = (0xffU & (IData)(
                                                        (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                         >> 4U)));
        }
    } else {
        this->__PVT__i2c_user_s2 = 0xc0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_eso = ((IData)(this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1) 
                                         & ((((2U == 
                                               (3U 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & ((6U 
                                                  == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                 | (0xcU 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                             & (~ (IData)(this->__PVT__i2c_user_bb))) 
                                            | (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 0xaU))));
        }
    } else {
        this->__PVT__i2c_user_eso = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (0x30U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U))))) 
             | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                 & (~ (IData)(this->__PVT__i2c_user_pin))) 
                & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140)))) {
            this->__PVT__i2c_user_i2ctime = this->__PVT__i2c_user_i2ctime_D_IN;
        }
    } else {
        this->__PVT__i2c_user_i2ctime = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                  & (IData)(this->__PVT__i2c_user_val_SDA)) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                     & (IData)(this->__PVT__i2c_user_bb)) 
                    & (2U == (IData)(this->__PVT__x___05Fh6518)))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                   & ((IData)(this->__PVT__i2c_user_pin) 
                      | (~ (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140))))) 
               | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                  & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_st_toggle = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                & ((IData)(this->__PVT__i2c_user_pin) 
                                                   | (~ (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140))))
                                                ? (IData)(this->__PVT__i2c_user_pin)
                                                : ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end)
                                                    ? 
                                                   (1U 
                                                    == (IData)(this->__PVT__x___05Fh6518))
                                                    : 
                                                   ((~ 
                                                     ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2) 
                                                      | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) 
                                                    & ((IData)(this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2) 
                                                       | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1)))));
        }
    } else {
        this->__PVT__i2c_user_st_toggle = 0U;
    }
    this->__PVT__i2c_user_dOutEn_delay_8 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_7));
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl = 
        (((0U == this->__PVT__i2c_user_coSCL) & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
         & (0U != this->__PVT__i2c_user_reSCL));
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__i2c_user_val_sda_delay_7 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_6));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock 
        = ((IData)(this->__PVT__i2c_user_scl_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock 
        = ((IData)(this->__PVT__i2c_user_mod_start) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__CAN_FIRE_RL_i2c_user_count_scl = (
                                                   (((0U 
                                                      != this->__PVT__i2c_user_coSCL) 
                                                     & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                                    & (IData)(this->__PVT__i2c_user_eso)) 
                                                   & (IData)(this->__PVT__i2c_user_st_toggle));
    this->__PVT__i2c_user_dOutEn_delay_7 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_6));
    this->__PVT__i2c_user_cprescaler_EN = (((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock) 
                                            | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
                                           | (0U != (IData)(this->__PVT__i2c_user_cprescaler)));
    this->__PVT__i2c_user_cprescaler_D_IN = (0xffU 
                                             & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock)
                                                 ? (IData)(this->__PVT__i2c_user_s2)
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)
                                                  ? (IData)(this->__PVT__i2c_user_rprescaler)
                                                  : 
                                                 ((0U 
                                                   != (IData)(this->__PVT__i2c_user_cprescaler))
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_cprescaler) 
                                                   - (IData)(1U))
                                                   : 0U))));
    this->__PVT__i2c_user_coSCL_D_IN = ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_set_scl_clock)
                                         ? this->__PVT__i2c_user_c_scl
                                         : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)
                                             ? this->__PVT__i2c_user_reSCL
                                             : ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_count_scl)
                                                 ? 
                                                (this->__PVT__i2c_user_coSCL 
                                                 - (IData)(1U))
                                                 : 0U)));
    this->__PVT__i2c_user_val_sda_delay_6 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_5));
    this->__PVT__i2c_user_dOutEn_delay_6 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_5));
    this->__PVT__i2c_user_val_sda_delay_5 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_4));
    this->__PVT__i2c_user_dOutEn_delay_5 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_4));
    this->__PVT__i2c_user_val_sda_delay_4 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_3));
    this->__PVT__i2c_user_dOutEn_delay_4 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_3));
    this->__PVT__i2c_user_val_sda_delay_3 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_2));
    this->__PVT__i2c_user_dOutEn_delay_3 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_2));
    this->__PVT__i2c_user_val_sda_delay_2 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_1));
    this->__PVT__i2c_user_dOutEn_delay_2 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_1));
    this->__PVT__i2c_user_val_sda_delay_1 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_sda_delay_0));
    this->__PVT__i2c_user_dOutEn_delay_1 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn_delay_0));
    this->__PVT__i2c_user_val_sda_delay_0 = ((IData)(vlTOPp->RST_N) 
                                             & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__i2c_user_dOutEn_delay_0 = ((IData)(vlTOPp->RST_N) 
                                            & (IData)(this->__PVT__i2c_user_dOutEn));
    if (vlTOPp->RST_N) {
        if ((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                    & (IData)(this->__PVT__i2c_user_val_SCL)) 
                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                      & (0U == (IData)(this->__PVT__i2c_user_dataBit)))) 
                  | ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                       & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                  >> 5U))))) 
                      & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                              >> 5U))))) 
                     & (~ (IData)(this->__PVT__i2c_user_bb)))) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                     & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
                        | (IData)(this->__PVT__i2c_user_bb))) 
                    & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
                       | (1U == (IData)(this->__PVT__x___05Fh6518))))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                    & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                   & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118))) 
               | ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                    & (IData)(this->__PVT__i2c_user_pin)) 
                   & (2U != (IData)(this->__PVT__x___05Fh6518))) 
                  & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                  & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                 & (1U == (IData)(this->__PVT__i2c_user_dataBit)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_dOutEn = (1U & ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)
                                                   ? 
                                                  ((0xaU 
                                                    == (IData)(this->__PVT__i2c_user_s3)) 
                                                   | (~ (IData)(this->__PVT__i2c_user_operation)))
                                                   : 
                                                  ((~ 
                                                    (((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5) 
                                                      | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7)) 
                                                     | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) 
                                                   & ((((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1) 
                                                        | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2)) 
                                                       | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1)) 
                                                      | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)))));
        }
    } else {
        this->__PVT__i2c_user_dOutEn = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5) {
            this->__PVT__i2c_user_operation = (1U & (IData)(this->__PVT__i2c_user_s0));
        }
    } else {
        this->__PVT__i2c_user_operation = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                     & (IData)(this->__PVT__i2c_user_val_SCL)) 
                    | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                       & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
                   | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                      & (0x28U == (0xffU & (IData)(
                                                   (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                    >> 5U)))))) 
                  | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                      & (~ (IData)(this->__PVT__i2c_user_pin))) 
                     & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                    & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                   & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                   & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                  & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
                  & (IData)(this->__PVT__i2c_user_rstsig)) 
                 & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)))) 
             | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                 & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
                & (IData)(this->__PVT__i2c_user_last_byte_read)))) {
            this->__PVT__i2c_user_s3 = (0xffU & (((
                                                   ((((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                                        | (IData)(this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2)) 
                                                       | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)) 
                                                      | (IData)(this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2)) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8))
                                                  ? 
                                                 ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1)
                                                   ? 7U
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2)
                                                    ? (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 4U))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)
                                                     ? 8U
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2)
                                                      ? 0xcU
                                                      : 
                                                     ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)
                                                       ? 
                                                      ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                        ? 5U
                                                        : 6U)
                                                       : 
                                                      ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)
                                                        ? 1U
                                                        : 
                                                       ((IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)
                                                         ? 0xaU
                                                         : 0xbU)))))))
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1)
                                                   ? 9U
                                                   : 0U)));
        }
    } else {
        this->__PVT__i2c_user_s3 = 9U;
    }
    this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140 
        = ((IData)(this->__PVT__i2c_user_i2ctimeout) 
           <= (0x3fffU & (IData)(this->__PVT__i2c_user_i2ctime)));
    if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
          & (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
         | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data))) {
        this->__PVT__i2c_user_s0 = this->__PVT__i2c_user_s0_D_IN;
    }
    if (vlTOPp->RST_N) {
        if ((((((((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                  & (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                >> 5U))))) 
                 | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                    & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                    & (~ (IData)(this->__PVT__i2c_user_val_SDA_in))) 
                   & (IData)(this->__PVT__i2c_user_val_SCL))) 
               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                   & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
                  & (IData)(this->__PVT__i2c_user_last_byte_read))) 
              | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & ((IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323) 
                    | (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                  >> 5U))))))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_pin = (1U & ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state) 
                                               | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                                   & ((IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323) 
                                                      | (0x10U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                     >> 5U))))))
                                                   ? 
                                                  (((8U 
                                                     != 
                                                     (0xffU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U)))) 
                                                    | (((2U 
                                                         == 
                                                         (3U 
                                                          & (IData)(
                                                                    (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                     >> 5U)))) 
                                                        & ((6U 
                                                            == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                           | (0xcU 
                                                              == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                                       & (~ (IData)(this->__PVT__i2c_user_bb)))) 
                                                   | (IData)(
                                                             (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 0xbU)))
                                                   : 
                                                  ((~ 
                                                    (((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2) 
                                                      | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                                                          & (~ (IData)(this->__PVT__i2c_user_val_SDA_in))) 
                                                         & (IData)(this->__PVT__i2c_user_val_SCL))) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2))) 
                                                   & ((IData)(this->__PVT__WILL_FIRE_RL_read_request) 
                                                      & (0x10U 
                                                         == 
                                                         (0xffU 
                                                          & (IData)(
                                                                    (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                     >> 5U)))))))));
        }
    } else {
        this->__PVT__i2c_user_pin = 1U;
    }
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__data0_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__PVT__i2c_user_val_SDA_in = (1U & ((~ (IData)(vlTOPp->RST_N)) 
                                              | (IData)(vlTOPp->i2c1_out_sda_in_in)));
    this->__PVT__s_xactor_f_wr_resp_D_IN = (((IData)(this->__PVT__i2c_user_ber) 
                                             | (((((((8U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U)))) 
                                                     & (0x20U 
                                                        != 
                                                        (0xffU 
                                                         & (IData)(
                                                                   (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                    >> 5U))))) 
                                                    & (0x10U 
                                                       != 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))) 
                                                   & (0U 
                                                      != 
                                                      (0xffU 
                                                       & (IData)(
                                                                 (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                  >> 5U))))) 
                                                  & (0x28U 
                                                     != 
                                                     (0xffU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 & (0x38U 
                                                    != 
                                                    (0xffU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (0x30U 
                                                   != 
                                                   (0xffU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U))))))
                                             ? 2U : 0U);
    this->__PVT__r1___05Fread___05Fh12450 = (((((IData)(this->__PVT__i2c_user_pin) 
                                                << 6U) 
                                               | (((IData)(this->__PVT__i2c_user_zero) 
                                                   << 5U) 
                                                  | ((IData)(this->__PVT__i2c_user_sts) 
                                                     << 4U))) 
                                              | (((IData)(this->__PVT__i2c_user_ber) 
                                                  << 3U) 
                                                 | ((IData)(this->__PVT__i2c_user_ad0_lrb) 
                                                    << 2U))) 
                                             | (((IData)(this->__PVT__i2c_user_aas) 
                                                 << 1U) 
                                                | (IData)(this->__PVT__i2c_user_lab)));
    if (vlTOPp->RST_N) {
        if ((((((((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
                          & (IData)(this->__PVT__i2c_user_val_SCL)) 
                         | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                            & (IData)(this->__PVT__i2c_user_val_SDA))) 
                        | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
                           & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)))) 
                       | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
                          & (1U == (IData)(this->__PVT__x___05Fh6518)))) 
                      | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                          & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
                             | (IData)(this->__PVT__i2c_user_bb))) 
                         & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
                            | (1U == (IData)(this->__PVT__x___05Fh6518))))) 
                     | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                        & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                    | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                       & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                   | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                       & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                      & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118))) 
                  | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                     & (IData)(this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155))) 
                 | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                     & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
                    & (1U == (IData)(this->__PVT__i2c_user_dataBit)))) 
                | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
                    & (IData)(this->__PVT__i2c_user_rstsig)) 
                   & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)))) 
               | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                  & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_mTransFSM = this->__PVT__i2c_user_mTransFSM_D_IN;
        }
    } else {
        this->__PVT__i2c_user_mTransFSM = 1U;
    }
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__i2c_user_val_SCL = this->__Vdly__i2c_user_val_SCL;
    if ((1U & (~ (IData)(vlTOPp->RST_N)))) {
        this->__PVT__i2c_user_rstsig = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_dataBit_EN) {
            this->__PVT__i2c_user_dataBit = this->__PVT__i2c_user_dataBit_D_IN;
        }
    } else {
        this->__PVT__i2c_user_dataBit = 9U;
    }
    this->__PVT__MUX_i2c_user_s3_write_1___05FSEL_2 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U)))));
    this->__PVT__WILL_FIRE_RL_read_request = (((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                                              & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
           & (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U))))) & 
           (~ (IData)(this->__PVT__i2c_user_eso)));
    this->__PVT__i2c_user_i2ctime_D_IN = (0xffffU & 
                                          (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                            & (0x30U 
                                               == (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                            ? (IData)(
                                                      (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 4U))
                                            : (0x8000U 
                                               | (0x7fffU 
                                                  & (IData)(this->__PVT__i2c_user_i2ctime)))));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg 
        = (((((((IData)(this->__PVT__i2c_user_configchange) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (1U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__CAN_FIRE_RL_i2c_user_receive_data 
        = ((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & 
           (~ (IData)(this->__PVT__i2c_user_val_SCL)));
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
               & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter)) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_resetcount = this->__PVT__i2c_user_resetcount_D_IN;
        }
    } else {
        this->__PVT__i2c_user_resetcount = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
               & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste))) 
              | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                  | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state))) {
            this->__PVT__i2c_user_bb = (1U & (~ ((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
                                                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))));
        }
    } else {
        this->__PVT__i2c_user_bb = 1U;
    }
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt 
        = ((((((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack = 
        (((((0xbU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
            & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
           & (IData)(this->__PVT__i2c_user_eso)) & 
          (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition 
        = ((((((0xfU == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (IData)(this->__PVT__i2c_user_eso)) 
             & (IData)(this->__PVT__i2c_user_val_SCL_in)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_data = 
        ((((((7U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_addr = 
        (((5U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
          & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (4U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__WILL_FIRE_RL_i2c_user_reset_state 
        = ((((((9U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
              & (~ (IData)(this->__PVT__i2c_user_ber))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_read_request))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__i2c_user_s0_D_IN = (0xffU & (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                               & (0x10U 
                                                  == 
                                                  (0xffU 
                                                   & (IData)(
                                                             (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                              >> 5U)))))
                                               ? (IData)(
                                                         (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 4U))
                                               : ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   | ((IData)(1U) 
                                                      << 
                                                      (0xfU 
                                                       & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U)))))
                                                   : 
                                                  ((IData)(this->__PVT__i2c_user_s0) 
                                                   & (~ 
                                                      ((IData)(1U) 
                                                       << 
                                                       (0xfU 
                                                        & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U)))))))));
    this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end 
        = ((((8U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (~ (IData)(this->__PVT__i2c_user_val_SCL_in))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113 
        = ((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110 
        = (((((1U < (IData)(this->__PVT__i2c_user_dataBit)) 
              | (1U == (IData)(this->__PVT__i2c_user_dataBit))) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118 
        = ((((1U == (IData)(this->__PVT__i2c_user_dataBit)) 
             & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
            & (IData)(this->__PVT__i2c_user_eso)) & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1 
        = ((((((((8U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                 | (3U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_scl)) 
               & (IData)(this->__PVT__i2c_user_eso)) 
              & (IData)(this->__PVT__i2c_user_val_SCL)) 
             & ((0U == (IData)(this->__PVT__i2c_user_dataBit)) 
                | (8U == (IData)(this->__PVT__i2c_user_dataBit)))) 
            & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
            & (~ (IData)(this->__PVT__i2c_user_pin))) 
           & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140));
    this->__PVT__i2c_user_val_SCL_D_IN = (1U & ((~ 
                                                 ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (~ (IData)(this->__PVT__i2c_user_pin)))) 
                                                & (~ (IData)(this->__PVT__i2c_user_val_SCL))));
    this->__PVT__i2c_user_i2ctimeout_D_IN = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                              & ((IData)(this->__PVT__i2c_user_pin) 
                                                 | ((IData)(this->__PVT__i2c_user_i2ctime) 
                                                    >> 0xeU)))
                                              ? ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 1U
                                                  : 
                                                 (0x3fffU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_i2ctimeout))))
                                              : 1U);
    this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack) 
           & (IData)(this->__PVT__i2c_user_val_SCL));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (1U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343 
        = (((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U)))) & 
              (6U != (IData)(this->__PVT__i2c_user_mTransFSM))) 
             & (0xcU != (IData)(this->__PVT__i2c_user_mTransFSM))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323 
        = ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                    >> 5U)))) & (((
                                                   (6U 
                                                    == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                   | (0xcU 
                                                      == (IData)(this->__PVT__i2c_user_mTransFSM))) 
                                                  | (2U 
                                                     != 
                                                     (3U 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
            & (2U == (3U & (IData)((this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 5U))))) & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & (~ (IData)(this->__PVT__i2c_user_bb))) 
           | (((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U)))) | 
               (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
              & (~ (IData)(this->__PVT__i2c_user_eso))));
    this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340 
        = ((((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                      >> 5U)))) & (2U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 5U))))) 
            & ((6U == (IData)(this->__PVT__i2c_user_mTransFSM)) 
               | (0xcU == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
           & (~ (IData)(this->__PVT__i2c_user_bb)));
    this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286 
        = (((((((((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U)))) 
                  | (8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
                 | (0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                               >> 5U))))) 
                | (0x18U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                              >> 5U))))) 
               | (0x20U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                             >> 5U))))) 
              | (0x28U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))) 
             | (0x30U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                           >> 5U))))) 
            | (0x38U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                          >> 5U)))))
            ? ((0U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                        >> 5U)))) ? 
               (((IData)(this->__PVT__i2c_user_s2) 
                 << 0x18U) | (((IData)(this->__PVT__i2c_user_s2) 
                               << 0x10U) | (((IData)(this->__PVT__i2c_user_s2) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s2))))
                : ((8U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                            >> 5U))))
                    ? (((IData)(this->__PVT__i2c_user_pin) 
                        << 0x1fU) | (((IData)(this->__PVT__i2c_user_pin) 
                                      << 0x17U) | (
                                                   ((IData)(this->__PVT__i2c_user_pin) 
                                                    << 0xfU) 
                                                   | ((IData)(this->__PVT__i2c_user_pin) 
                                                      << 7U))))
                    : ((0x10U == (0xffU & (IData)((this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                   >> 5U))))
                        ? (((IData)(this->__PVT__i2c_user_s0) 
                            << 0x18U) | (((IData)(this->__PVT__i2c_user_s0) 
                                          << 0x10U) 
                                         | (((IData)(this->__PVT__i2c_user_s0) 
                                             << 8U) 
                                            | (IData)(this->__PVT__i2c_user_s0))))
                        : ((0x18U == (0xffU & (IData)(
                                                      (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                       >> 5U))))
                            ? (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                << 0x19U) | (((IData)(this->__PVT__i2c_user_bb) 
                                              << 0x18U) 
                                             | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                 << 0x11U) 
                                                | (((IData)(this->__PVT__i2c_user_bb) 
                                                    << 0x10U) 
                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                       << 9U) 
                                                      | (((IData)(this->__PVT__i2c_user_bb) 
                                                          << 8U) 
                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh12450) 
                                                             << 1U) 
                                                            | (IData)(this->__PVT__i2c_user_bb))))))))
                            : ((0x20U == (0xffU & (IData)(
                                                          (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                           >> 5U))))
                                ? (((IData)(this->__PVT__i2c_user_s01) 
                                    << 0x18U) | (((IData)(this->__PVT__i2c_user_s01) 
                                                  << 0x10U) 
                                                 | (((IData)(this->__PVT__i2c_user_s01) 
                                                     << 8U) 
                                                    | (IData)(this->__PVT__i2c_user_s01))))
                                : ((0x28U == (0xffU 
                                              & (IData)(
                                                        (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                         >> 5U))))
                                    ? (((IData)(this->__PVT__i2c_user_s3) 
                                        << 0x18U) | 
                                       (((IData)(this->__PVT__i2c_user_s3) 
                                         << 0x10U) 
                                        | (((IData)(this->__PVT__i2c_user_s3) 
                                            << 8U) 
                                           | (IData)(this->__PVT__i2c_user_s3))))
                                    : ((0x30U == (0xffU 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                             >> 5U))))
                                        ? (((IData)(this->__PVT__i2c_user_i2ctime) 
                                            << 0x10U) 
                                           | (IData)(this->__PVT__i2c_user_i2ctime))
                                        : this->__PVT__i2c_user_c_scl)))))))
            : 0U);
    this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans 
        = ((((((IData)(this->__PVT__i2c_user_val_SCL_in) 
               & (2U == (IData)(this->__PVT__i2c_user_mTransFSM))) 
              & (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_restore_prescale)) 
             & (IData)(this->__PVT__i2c_user_eso)) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    if (vlTOPp->RST_N) {
        if (this->__PVT__WILL_FIRE_RL_i2c_user_idler) {
            this->__PVT__i2c_user_cycwaste = this->__PVT__i2c_user_cycwaste_D_IN;
        }
    } else {
        this->__PVT__i2c_user_cycwaste = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__i2c_user_val_SDA_EN) {
            this->__PVT__i2c_user_val_SDA = (((((((
                                                   ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1)) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7)) 
                                                | (IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)) 
                                              | ((IData)(this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1))) 
                                             & ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2)
                                                 ? (IData)(this->__PVT__i2c_user_last_byte_read)
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1)
                                                  ? 
                                                 ((2U 
                                                   >= (IData)(this->__PVT__i2c_user_sendInd)) 
                                                  & (6U 
                                                     >> (IData)(this->__PVT__i2c_user_sendInd)))
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)
                                                   ? (IData)(this->__PVT__x___05Fh10210)
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)
                                                    ? 
                                                   ((2U 
                                                     >= (IData)(this->__PVT__i2c_user_sendInd)) 
                                                    & (1U 
                                                       >> (IData)(this->__PVT__i2c_user_sendInd)))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7)
                                                     ? (IData)(this->__PVT__x___05Fh10210)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8)
                                                      ? 
                                                     ((1U 
                                                       != (IData)(this->__PVT__x___05Fh6518)) 
                                                      & (IData)(this->__PVT__x___05Fh10210))
                                                      : 
                                                     (~ (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)))))))));
        }
    } else {
        this->__PVT__i2c_user_val_SDA = 1U;
    }
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
            & (1U >= (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118));
    this->__PVT__MUX_i2c_user_ack_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314));
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             (((((((((8U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                     & (0x18U 
                                                                        != 
                                                                        (0xffU 
                                                                         & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                    & (0x20U 
                                                                       != 
                                                                       (0xffU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   & (0x10U 
                                                                      != 
                                                                      (0xffU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (0U 
                                                                     != 
                                                                     (0xffU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                 & (0x28U 
                                                                    != 
                                                                    (0xffU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                & (0x30U 
                                                                   != 
                                                                   (0xffU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U))))) 
                                                               & (0x38U 
                                                                  != 
                                                                  (0xffU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))))
                                                               ? 2U
                                                               : 0U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(this->__PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286)));
    this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans));
    this->__PVT__WILL_FIRE_RL_i2c_user_idler = ((1U 
                                                 == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
           & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113));
    this->__PVT__x___05Fh10210 = (1U & ((IData)(this->__PVT__i2c_user_s0) 
                                        >> (7U & ((IData)(this->__PVT__i2c_user_dataBit) 
                                                  - (IData)(2U)))));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
           & (0U == (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
           & (1U < (IData)(this->__PVT__i2c_user_dataBit)));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1 
        = ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340));
    this->__PVT__x___05Fh6518 = (((IData)(this->__PVT__i2c_user_sta) 
                                  << 1U) | (IData)(this->__PVT__i2c_user_sto));
    this->__PVT__i2c_user_last_byte_read = this->__Vdly__i2c_user_last_byte_read;
    this->__PVT__i2c_user_sendInd = this->__Vdly__i2c_user_sendInd;
    this->__PVT__i2c_user_cycwaste_D_IN = (0x3ffU & 
                                           ((0U == (IData)(this->__PVT__i2c_user_cycwaste))
                                             ? ((IData)(1U) 
                                                + (IData)(this->__PVT__i2c_user_cycwaste))
                                             : ((0x258U 
                                                 == (IData)(this->__PVT__i2c_user_cycwaste))
                                                 ? 0U
                                                 : 
                                                ((IData)(1U) 
                                                 + (IData)(this->__PVT__i2c_user_cycwaste)))));
    this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler) 
           & (0x258U == (IData)(this->__PVT__i2c_user_cycwaste)));
    this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter 
        = (1U & ((~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_idler))));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
           & (~ (IData)(this->__PVT__i2c_user_val_SDA)));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1 
        = ((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
           & (IData)(this->__PVT__i2c_user_val_SDA));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178 
        = ((((1U != (IData)(this->__PVT__x___05Fh6518)) 
             & (0xaU != (IData)(this->__PVT__i2c_user_s3))) 
            & (IData)(this->__PVT__i2c_user_operation)) 
           & (~ (IData)(this->__PVT__i2c_user_ack)));
    this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165 
        = (((IData)(this->__PVT__i2c_user_pin) & (2U 
                                                  != (IData)(this->__PVT__x___05Fh6518))) 
           & (((1U == (IData)(this->__PVT__x___05Fh6518)) 
               | (0xaU == (IData)(this->__PVT__i2c_user_s3))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155 
        = (((IData)(this->__PVT__i2c_user_pin) & ((
                                                   (((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518)) 
                                                     | (1U 
                                                        == (IData)(this->__PVT__x___05Fh6518))) 
                                                    | (0xaU 
                                                       == (IData)(this->__PVT__i2c_user_s3))) 
                                                   | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
                                                  | (~ (IData)(this->__PVT__i2c_user_operation)))) 
           | ((~ (IData)(this->__PVT__i2c_user_pin)) 
              & (IData)(this->__PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140)));
    this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168 
        = ((1U != (IData)(this->__PVT__x___05Fh6518)) 
           & (((0xaU == (IData)(this->__PVT__i2c_user_s3)) 
               | (~ (IData)(this->__PVT__i2c_user_val_SCL))) 
              | (~ (IData)(this->__PVT__i2c_user_operation))));
    this->__PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & (IData)(this->__PVT__i2c_user_bb)) & 
           (2U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
           & (1U == (IData)(this->__PVT__x___05Fh6518)));
    this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
            & ((2U != (IData)(this->__PVT__x___05Fh6518)) 
               | (IData)(this->__PVT__i2c_user_bb))) 
           & ((2U == (IData)(this->__PVT__x___05Fh6518)) 
              | (1U == (IData)(this->__PVT__x___05Fh6518))));
    this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
            & (0U == (IData)(this->__PVT__i2c_user_dataBit))) 
           & (IData)(this->__PVT__i2c_user_last_byte_read));
    this->__PVT__i2c_user_resetcount_D_IN = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter)
                                              ? ((IData)(this->__PVT__i2c_user_rstsig)
                                                  ? 
                                                 (0x3fU 
                                                  & ((IData)(1U) 
                                                     + (IData)(this->__PVT__i2c_user_resetcount)))
                                                  : 0U)
                                              : 0U);
    this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_resetfilter) 
            & (IData)(this->__PVT__i2c_user_rstsig)) 
           & (0x3bU == (IData)(this->__PVT__i2c_user_resetcount)));
    this->__PVT__i2c_user_last_byte_read_EN = (((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                  & (IData)(this->__PVT__i2c_user_pin)) 
                                                 & (2U 
                                                    != (IData)(this->__PVT__x___05Fh6518))) 
                                                & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178)) 
                                               | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                   & (0U 
                                                      == (IData)(this->__PVT__i2c_user_dataBit))) 
                                                  & (IData)(this->__PVT__i2c_user_last_byte_read)));
    this->__PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
           & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165));
    this->__PVT__i2c_user_val_SDA_EN = (((((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans)) 
                                                & (IData)(this->__PVT__i2c_user_val_SDA)) 
                                               | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end) 
                                                  & (1U 
                                                     == (IData)(this->__PVT__x___05Fh6518)))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                                 & (1U 
                                                    < (IData)(this->__PVT__i2c_user_dataBit)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                                & (0U 
                                                   == (IData)(this->__PVT__i2c_user_dataBit)))) 
                                            | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_control_reg) 
                                                & (IData)(this->__PVT__i2c_user_bb)) 
                                               & (2U 
                                                  == (IData)(this->__PVT__x___05Fh6518)))) 
                                           | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_stop_condition) 
                                              & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                          | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                             & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113))) 
                                         | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                            & (IData)(this->__PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165))) 
                                        | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340)));
    this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
             & (IData)(this->__PVT__i2c_user_pin)) 
            & (2U != (IData)(this->__PVT__x___05Fh6518))) 
           & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168));
    this->__PVT__i2c_user_dataBit_EN = (((((((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_data) 
                                               & (1U 
                                                  < (IData)(this->__PVT__i2c_user_dataBit))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_start_trans) 
                                                 & (~ (IData)(this->__PVT__i2c_user_val_SDA)))) 
                                             | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                & (IData)(this->__PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110))) 
                                            | ((((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin)) 
                                                & (2U 
                                                   != (IData)(this->__PVT__x___05Fh6518))) 
                                               & (IData)(this->__PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168))) 
                                           | (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1) 
                                               & (0U 
                                                  == (IData)(this->__PVT__i2c_user_dataBit))) 
                                              & (IData)(this->__PVT__i2c_user_last_byte_read))) 
                                          | ((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                             & (IData)(this->__PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340))) 
                                         | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                        | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack));
    this->__PVT__i2c_user_sendInd_D_IN = (3U & (((IData)(this->__PVT__MUX_i2c_user_bb_write_1___05FPSEL_2) 
                                                 & (IData)(this->__PVT__i2c_user_val_SDA))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_sendInd) 
                                                 - (IData)(1U))
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_sendInd) 
                                                  - (IData)(1U))
                                                  : 
                                                 (((((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1) 
                                                     | (IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_3)) 
                                                    | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1)) 
                                                   | ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_send_addr) 
                                                      | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)))
                                                   ? 2U
                                                   : 0U))));
    this->__PVT__i2c_user_mTransFSM_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1) 
                                                   | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)) 
                                                  | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)) 
                                                 | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)) 
                                                | (IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)) 
                                              | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)) 
                                             | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7))
                                             ? ((IData)(this->__PVT__MUX_i2c_user_ber_write_1___05FSEL_1)
                                                 ? 9U
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2)
                                                  ? 1U
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3)
                                                   ? 0xfU
                                                   : 
                                                  ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_reset_state)
                                                    ? 1U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2)
                                                     ? 
                                                    ((2U 
                                                      == (IData)(this->__PVT__x___05Fh6518))
                                                      ? 2U
                                                      : 0xfU)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6)
                                                      ? 
                                                     ((8U 
                                                       == 
                                                       (0xffU 
                                                        & (IData)(
                                                                  (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                   >> 5U))))
                                                       ? 
                                                      ((((2U 
                                                          == 
                                                          (3U 
                                                           & (IData)(
                                                                     (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                                      >> 5U)))) 
                                                         & ((6U 
                                                             == (IData)(this->__PVT__i2c_user_mTransFSM)) 
                                                            | (0xcU 
                                                               == (IData)(this->__PVT__i2c_user_mTransFSM)))) 
                                                        & (~ (IData)(this->__PVT__i2c_user_bb)))
                                                        ? 4U
                                                        : 0xfU)
                                                       : 1U)
                                                      : 
                                                     ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_receive_data1)
                                                       ? 
                                                      ((0U 
                                                        == (IData)(this->__PVT__i2c_user_dataBit))
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_last_byte_read)
                                                         ? 3U
                                                         : 0xbU)
                                                        : 0xcU)
                                                       : 0xbU)))))))
                                             : (((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_wait_interrupt) 
                                                 & (IData)(this->__PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155))
                                                 ? 
                                                ((IData)(this->__PVT__i2c_user_pin)
                                                  ? 
                                                 ((1U 
                                                   == (IData)(this->__PVT__x___05Fh6518))
                                                   ? 0xfU
                                                   : 
                                                  ((2U 
                                                    == (IData)(this->__PVT__x___05Fh6518))
                                                    ? 9U
                                                    : 
                                                   ((0xaU 
                                                     == (IData)(this->__PVT__i2c_user_s3))
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__i2c_user_operation)
                                                      ? 8U
                                                      : 7U))))
                                                  : 0xfU)
                                                 : 
                                                ((IData)(this->__PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1)
                                                  ? 
                                                 ((IData)(this->__PVT__i2c_user_val_SDA_in)
                                                   ? 0xcU
                                                   : 6U)
                                                  : 
                                                 ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5)
                                                   ? 0xbU
                                                   : 
                                                  ((IData)(this->__PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2)
                                                    ? 6U
                                                    : 
                                                   ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8)
                                                     ? 5U
                                                     : 
                                                    ((IData)(this->__PVT__MUX_i2c_user_aas_write_1___05FSEL_1)
                                                      ? 0x10U
                                                      : 0U)))))));
    this->__PVT__i2c_user_dataBit_D_IN = (((((((((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1) 
                                                 | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)) 
                                                | (IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)) 
                                               | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)) 
                                              | (IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)) 
                                            | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)) 
                                           | (IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8))
                                           ? ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1)
                                               ? 9U
                                               : ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2)
                                                   ? 8U
                                                   : 
                                                  (0xfU 
                                                   & ((IData)(this->__PVT__CAN_FIRE_RL_i2c_user_receive_data)
                                                       ? 
                                                      ((IData)(this->__PVT__i2c_user_dataBit) 
                                                       - (IData)(1U))
                                                       : 
                                                      ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4)
                                                        ? 
                                                       ((IData)(this->__PVT__i2c_user_dataBit) 
                                                        - (IData)(1U))
                                                        : 
                                                       ((IData)(this->__PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6)
                                                         ? 
                                                        ((0xaU 
                                                          == (IData)(this->__PVT__i2c_user_s3))
                                                          ? 
                                                         ((IData)(this->__PVT__i2c_user_dataBit) 
                                                          - (IData)(1U))
                                                          : 
                                                         ((IData)(this->__PVT__i2c_user_operation)
                                                           ? 8U
                                                           : 
                                                          ((IData)(this->__PVT__i2c_user_dataBit) 
                                                           - (IData)(1U))))
                                                         : 
                                                        ((IData)(this->__PVT__WILL_FIRE_RL_i2c_user_check_Ack)
                                                          ? 9U
                                                          : 
                                                         ((IData)(this->__PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7)
                                                           ? 
                                                          ((1U 
                                                            >= (IData)(this->__PVT__i2c_user_dataBit))
                                                            ? 8U
                                                            : 
                                                           ((IData)(this->__PVT__i2c_user_dataBit) 
                                                            - (IData)(1U)))
                                                           : 9U))))))))
                                           : 0U);
}
