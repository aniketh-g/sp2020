# Verilated -*- Makefile -*-
# DESCRIPTION: Verilator output: Make include file with class lists
#
# This file lists generated Verilated files, for including in higher level makefiles.
# See VmkSoc.mk for the caller.

### Switches...
# C11 constructs required?  0/1 (always on now)
VM_C11 = 1
# Coverage output mode?  0/1 (from --coverage)
VM_COVERAGE = 0
# Parallel builds?  0/1 (from --output-split)
VM_PARALLEL_BUILDS = 1
# Threaded output mode?  0/1/N threads (from --threads)
VM_THREADS = 0
# Tracing output mode?  0/1 (from --trace/--trace-fst)
VM_TRACE = 0
# Tracing output mode in FST format?  0/1 (from --trace-fst)
VM_TRACE_FST = 0
# Tracing threaded output mode?  0/1/N threads (from --trace-thread)
VM_TRACE_THREADS = 0
# Separate FST writer thread? 0/1 (from --trace-fst with --trace-thread > 0)
VM_TRACE_FST_WRITER_THREAD = 0

### Object file lists...
# Generated module classes, fast-path, compile with highest optimization
VM_CLASSES_FAST += \
	VmkSoc \
	VmkSoc__1 \
	VmkSoc__2 \
	VmkSoc__3 \
	VmkSoc__4 \
	VmkSoc__5 \
	VmkSoc__6 \
	VmkSoc__7 \
	VmkSoc__8 \
	VmkSoc__9 \
	VmkSoc__10 \
	VmkSoc__11 \
	VmkSoc__12 \
	VmkSoc_mki2c \
	VmkSoc_mkpwm \
	VmkSoc_mkspi \
	VmkSoc_mkuart \

# Generated module classes, non-fast-path, compile with low/medium optimization
VM_CLASSES_SLOW += \
	VmkSoc__Slow \
	VmkSoc__1__Slow \
	VmkSoc__2__Slow \
	VmkSoc__3__Slow \
	VmkSoc__4__Slow \
	VmkSoc__5__Slow \
	VmkSoc__6__Slow \
	VmkSoc__7__Slow \
	VmkSoc_mki2c__Slow \
	VmkSoc_mkpwm__Slow \
	VmkSoc_mkspi__Slow \
	VmkSoc_mkuart__Slow \

# Generated support classes, fast-path, compile with highest optimization
VM_SUPPORT_FAST += \

# Generated support classes, non-fast-path, compile with low/medium optimization
VM_SUPPORT_SLOW += \
	VmkSoc__Syms \

# Global classes, need linked once per executable, fast-path, compile with highest optimization
VM_GLOBAL_FAST += \
	verilated \

# Global classes, need linked once per executable, non-fast-path, compile with low/medium optimization
VM_GLOBAL_SLOW += \


# Verilated -*- Makefile -*-
