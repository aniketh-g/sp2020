// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VmkSoc.h for the primary calling header

#ifndef _VMKSOC_MKI2C_H_
#define _VMKSOC_MKI2C_H_  // guard

#include "verilated_heavy.h"

//==========

class VmkSoc__Syms;

//----------

VL_MODULE(VmkSoc_mki2c) {
  public:
    
    // PORTS
    VL_IN8(CLK,0,0);
    VL_IN8(RST_N,0,0);
    VL_IN8(slave_m_awvalid_awvalid,0,0);
    VL_IN8(slave_m_awvalid_awsize,1,0);
    VL_IN8(slave_m_awvalid_awprot,2,0);
    VL_OUT8(slave_awready,0,0);
    VL_IN8(slave_m_wvalid_wvalid,0,0);
    VL_IN8(slave_m_wvalid_wstrb,3,0);
    VL_OUT8(slave_wready,0,0);
    VL_OUT8(slave_bvalid,0,0);
    VL_OUT8(slave_bresp,1,0);
    VL_IN8(slave_m_bready_bready,0,0);
    VL_IN8(slave_m_arvalid_arvalid,0,0);
    VL_IN8(slave_m_arvalid_arsize,1,0);
    VL_IN8(slave_m_arvalid_arprot,2,0);
    VL_OUT8(slave_arready,0,0);
    VL_OUT8(slave_rvalid,0,0);
    VL_OUT8(slave_rresp,1,0);
    VL_IN8(slave_m_rready_rready,0,0);
    VL_OUT8(io_scl_out,0,0);
    VL_IN8(io_scl_in_in,0,0);
    VL_OUT8(io_scl_out_en,0,0);
    VL_OUT8(io_sda_out,0,0);
    VL_IN8(io_sda_in_in,0,0);
    VL_OUT8(io_sda_out_en,0,0);
    VL_OUT8(isint,0,0);
    VL_OUT8(__PVT__RDY_isint,0,0);
    VL_OUT8(__PVT__timerint,0,0);
    VL_OUT8(__PVT__RDY_timerint,0,0);
    VL_OUT8(__PVT__isber,0,0);
    VL_OUT8(__PVT__RDY_isber,0,0);
    VL_IN(slave_m_awvalid_awaddr,31,0);
    VL_IN(slave_m_wvalid_wdata,31,0);
    VL_IN(slave_m_arvalid_araddr,31,0);
    VL_OUT(slave_rdata,31,0);
    
    // LOCAL SIGNALS
    // Anonymous structures to workaround compiler member-count bugs
    struct {
        CData/*0:0*/ __PVT__i2c_user_aas;
        CData/*0:0*/ __PVT__i2c_user_ack;
        CData/*0:0*/ __PVT__i2c_user_ad0_lrb;
        CData/*0:0*/ __PVT__i2c_user_bb;
        CData/*0:0*/ __PVT__i2c_user_ber;
        CData/*0:0*/ __PVT__i2c_user_cOutEn;
        CData/*0:0*/ __PVT__i2c_user_configchange;
        CData/*7:0*/ __PVT__i2c_user_cprescaler;
        CData/*7:0*/ __PVT__i2c_user_cprescaler_D_IN;
        CData/*0:0*/ __PVT__i2c_user_cprescaler_EN;
        CData/*0:0*/ __PVT__i2c_user_dOutEn;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_0;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_1;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_2;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_3;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_4;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_5;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_6;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_7;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_8;
        CData/*0:0*/ __PVT__i2c_user_dOutEn_delay_9;
        CData/*3:0*/ __PVT__i2c_user_dataBit;
        CData/*3:0*/ __PVT__i2c_user_dataBit_D_IN;
        CData/*0:0*/ __PVT__i2c_user_dataBit_EN;
        CData/*0:0*/ __PVT__i2c_user_eni;
        CData/*0:0*/ __PVT__i2c_user_eso;
        CData/*0:0*/ __PVT__i2c_user_lab;
        CData/*0:0*/ __PVT__i2c_user_last_byte_read;
        CData/*0:0*/ __PVT__i2c_user_last_byte_read_EN;
        CData/*4:0*/ __PVT__i2c_user_mTransFSM;
        CData/*4:0*/ __PVT__i2c_user_mTransFSM_D_IN;
        CData/*0:0*/ __PVT__i2c_user_mod_start;
        CData/*0:0*/ __PVT__i2c_user_operation;
        CData/*0:0*/ __PVT__i2c_user_pin;
        CData/*5:0*/ __PVT__i2c_user_resetcount;
        CData/*5:0*/ __PVT__i2c_user_resetcount_D_IN;
        CData/*7:0*/ __PVT__i2c_user_rprescaler;
        CData/*0:0*/ __PVT__i2c_user_rstsig;
        CData/*7:0*/ __PVT__i2c_user_s0;
        CData/*7:0*/ __PVT__i2c_user_s0_D_IN;
        CData/*7:0*/ __PVT__i2c_user_s01;
        CData/*7:0*/ __PVT__i2c_user_s2;
        CData/*7:0*/ __PVT__i2c_user_s3;
        CData/*0:0*/ __PVT__i2c_user_scl_start;
        CData/*1:0*/ __PVT__i2c_user_sendInd;
        CData/*1:0*/ __PVT__i2c_user_sendInd_D_IN;
        CData/*0:0*/ __PVT__i2c_user_st_toggle;
        CData/*0:0*/ __PVT__i2c_user_sta;
        CData/*0:0*/ __PVT__i2c_user_sto;
        CData/*0:0*/ __PVT__i2c_user_sts;
        CData/*0:0*/ __PVT__i2c_user_val_SCL;
        CData/*0:0*/ __PVT__i2c_user_val_SCL_D_IN;
        CData/*0:0*/ __PVT__i2c_user_val_SCL_in;
        CData/*0:0*/ __PVT__i2c_user_val_SDA;
        CData/*0:0*/ __PVT__i2c_user_val_SDA_EN;
        CData/*0:0*/ __PVT__i2c_user_val_SDA_in;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_0;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_1;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_2;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_3;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_4;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_5;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_6;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_7;
    };
    struct {
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_8;
        CData/*0:0*/ __PVT__i2c_user_val_sda_delay_9;
        CData/*0:0*/ __PVT__i2c_user_zero;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data_DEQ;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data_ENQ;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp_D_IN;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp_DEQ;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_i2c_user_count_scl;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_i2c_user_receive_data;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_i2c_user_restore_prescale;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_i2c_user_restore_scl;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_i2c_user_set_i2c_clock;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_i2c_user_set_scl_clock;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_write_request;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_check_Ack;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_check_control_reg;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_idler;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_receive_data1;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_reset_state;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_resetfilter;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_send_addr;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_send_data;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_send_rtstart_trans;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_send_start_trans;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_send_stop_condition;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_wait_interrupt;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_i2c_user_wait_interrupt_receive_end;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_read_request;
        CData/*0:0*/ __PVT__MUX_i2c_user_aas_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_ack_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_ad0_lrb_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_bb_write_1___05FPSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_ber_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_ber_write_1___05FSEL_3;
        CData/*0:0*/ __PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_cOutEn_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_5;
        CData/*0:0*/ __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_6;
        CData/*0:0*/ __PVT__MUX_i2c_user_dOutEn_write_1___05FSEL_7;
        CData/*0:0*/ __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_4;
        CData/*0:0*/ __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_7;
        CData/*0:0*/ __PVT__MUX_i2c_user_dataBit_write_1___05FSEL_8;
        CData/*0:0*/ __PVT__MUX_i2c_user_i2ctime_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_i2ctimeout_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_3;
        CData/*0:0*/ __PVT__MUX_i2c_user_mTransFSM_write_1___05FSEL_6;
        CData/*0:0*/ __PVT__MUX_i2c_user_mod_start_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_reSCL_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_rprescaler_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_s3_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_sendInd_write_1___05FSEL_5;
        CData/*0:0*/ __PVT__MUX_i2c_user_st_toggle_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_7;
        CData/*0:0*/ __PVT__MUX_i2c_user_val_SDA_write_1___05FSEL_8;
        CData/*6:0*/ __PVT__r1___05Fread___05Fh12450;
        CData/*1:0*/ __PVT__x___05Fh6518;
        CData/*0:0*/ __PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_AND_i2c_us_ETC___05F_d113;
    };
    struct {
        CData/*0:0*/ __PVT__NOT_i2c_user_dataBit_03_ULE_1_04_05_OR_i2c_use_ETC___05F_d110;
        CData/*0:0*/ __PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d168;
        CData/*0:0*/ __PVT__NOT_i2c_user_sta_3_CONCAT_i2c_user_sto_4_5_EQ___05FETC___05F_d178;
        CData/*0:0*/ __PVT__i2c_user_dataBit_03_EQ_1_06_AND_i2c_user_pwSCL_ETC___05F_d118;
        CData/*0:0*/ __PVT__i2c_user_i2ctimeout_36_ULE_i2c_user_i2ctime_33_ETC___05F_d140;
        CData/*0:0*/ __PVT__i2c_user_pin_32_AND_NOT_i2c_user_sta_3_CONCAT___05FETC___05F_d165;
        CData/*0:0*/ __PVT__i2c_user_pin_32_AND_i2c_user_sta_3_CONCAT_i2c___05FETC___05F_d155;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d314;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d323;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d340;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr_first___05F93_BITS_12_TO_5_94_E_ETC___05F_d343;
        CData/*0:0*/ __PVT__x___05Fh10210;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_rd_data__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_data__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_f_wr_resp__DOT__empty_reg;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp__DOT__data0_reg;
        CData/*1:0*/ __PVT__s_xactor_f_wr_resp__DOT__data1_reg;
        SData/*9:0*/ __PVT__i2c_user_cycwaste;
        SData/*9:0*/ __PVT__i2c_user_cycwaste_D_IN;
        SData/*15:0*/ __PVT__i2c_user_i2ctime;
        SData/*15:0*/ __PVT__i2c_user_i2ctime_D_IN;
        SData/*13:0*/ __PVT__i2c_user_i2ctimeout;
        SData/*13:0*/ __PVT__i2c_user_i2ctimeout_D_IN;
        IData/*31:0*/ __PVT__i2c_user_c_scl;
        IData/*31:0*/ __PVT__i2c_user_coSCL;
        IData/*31:0*/ __PVT__i2c_user_coSCL_D_IN;
        IData/*31:0*/ __PVT__i2c_user_reSCL;
        IData/*31:0*/ __PVT__IF_s_xactor_f_rd_addr_first___05F20_BITS_12_TO_5_2_ETC___05F_d286;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data_D_IN;
        QData/*36:0*/ __PVT__s_xactor_f_rd_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_f_rd_addr__DOT__data1_reg;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data__DOT__data0_reg;
        QData/*33:0*/ __PVT__s_xactor_f_rd_data__DOT__data1_reg;
        QData/*36:0*/ __PVT__s_xactor_f_wr_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_f_wr_addr__DOT__data1_reg;
        QData/*35:0*/ __PVT__s_xactor_f_wr_data__DOT__data0_reg;
        QData/*35:0*/ __PVT__s_xactor_f_wr_data__DOT__data1_reg;
    };
    
    // LOCAL VARIABLES
    CData/*0:0*/ __Vdly__i2c_user_last_byte_read;
    CData/*0:0*/ __Vdly__i2c_user_val_SCL;
    CData/*1:0*/ __Vdly__i2c_user_sendInd;
    CData/*0:0*/ __Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    QData/*36:0*/ __Vdly__s_xactor_f_rd_addr__DOT__data0_reg;
    QData/*36:0*/ __Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    QData/*35:0*/ __Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    
    // INTERNAL VARIABLES
  private:
    VmkSoc__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(VmkSoc_mki2c);  ///< Copying not allowed
  public:
    VmkSoc_mki2c(const char* name = "TOP");
    ~VmkSoc_mki2c();
    
    // INTERNAL METHODS
    void __Vconfigure(VmkSoc__Syms* symsp, bool first);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
  public:
    void _initial__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__1(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__3(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__5(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1__4(VmkSoc__Syms* __restrict vlSymsp);
    void _settle__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__7(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1__8(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
