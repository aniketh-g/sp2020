// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VmkSoc.h for the primary calling header

#ifndef _VMKSOC_MKSPI_H_
#define _VMKSOC_MKSPI_H_  // guard

#include "verilated_heavy.h"

//==========

class VmkSoc__Syms;

//----------

VL_MODULE(VmkSoc_mkspi) {
  public:
    
    // PORTS
    VL_IN8(CLK,0,0);
    VL_IN8(RST_N,0,0);
    VL_OUT8(io_mosi,0,0);
    VL_OUT8(io_sclk,0,0);
    VL_OUT8(io_nss,0,0);
    VL_IN8(io_miso_dat,0,0);
    VL_IN8(slave_m_awvalid_awvalid,0,0);
    VL_IN8(slave_m_awvalid_awsize,1,0);
    VL_IN8(slave_m_awvalid_awprot,2,0);
    VL_OUT8(slave_awready,0,0);
    VL_IN8(slave_m_wvalid_wvalid,0,0);
    VL_IN8(slave_m_wvalid_wstrb,3,0);
    VL_OUT8(slave_wready,0,0);
    VL_OUT8(slave_bvalid,0,0);
    VL_OUT8(slave_bresp,1,0);
    VL_IN8(slave_m_bready_bready,0,0);
    VL_IN8(slave_m_arvalid_arvalid,0,0);
    VL_IN8(slave_m_arvalid_arsize,1,0);
    VL_IN8(slave_m_arvalid_arprot,2,0);
    VL_OUT8(slave_arready,0,0);
    VL_OUT8(slave_rvalid,0,0);
    VL_OUT8(slave_rresp,1,0);
    VL_IN8(slave_m_rready_rready,0,0);
    VL_IN(slave_m_awvalid_awaddr,31,0);
    VL_IN(slave_m_wvalid_wdata,31,0);
    VL_IN(slave_m_arvalid_araddr,31,0);
    VL_OUT(slave_rdata,31,0);
    
    // LOCAL SIGNALS
    // Anonymous structures to workaround compiler member-count bugs
    struct {
        CData/*0:0*/ __PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget;
        CData/*0:0*/ __PVT__spi_wr_clk_en_wget;
        CData/*0:0*/ __PVT__spi_wr_transfer_en_whas;
        CData/*7:0*/ __PVT__spi_rg_bit_count;
        CData/*7:0*/ __PVT__spi_rg_bit_count_D_IN;
        CData/*0:0*/ __PVT__spi_rg_clk;
        CData/*0:0*/ __PVT__spi_rg_clk_D_IN;
        CData/*2:0*/ __PVT__spi_rg_clk_counter;
        CData/*2:0*/ __PVT__spi_rg_clk_counter_D_IN;
        CData/*7:0*/ __PVT__spi_rg_data_counter;
        CData/*7:0*/ __PVT__spi_rg_data_counter_D_IN;
        CData/*0:0*/ __PVT__spi_rg_data_counter_EN;
        CData/*7:0*/ __PVT__spi_rg_data_rx;
        CData/*7:0*/ __PVT__spi_rg_data_tx;
        CData/*7:0*/ __PVT__spi_rg_data_tx_D_IN;
        CData/*0:0*/ __PVT__spi_rg_nss;
        CData/*1:0*/ __PVT__spi_rg_receive_state;
        CData/*0:0*/ __PVT__spi_rg_receive_state_EN;
        CData/*0:0*/ __PVT__spi_rg_transfer_done;
        CData/*1:0*/ __PVT__spi_rg_transmit_state;
        CData/*0:0*/ __PVT__spi_rg_tx_rx_start;
        CData/*0:0*/ __PVT__spi_tx_data_en;
        CData/*0:0*/ __PVT__spi_tx_data_en_EN;
        CData/*0:0*/ __PVT__spi_wr_spi_out_io1;
        CData/*0:0*/ __PVT__ff_rd_req_dEMPTY_N;
        CData/*0:0*/ __PVT__ff_wr_req_dEMPTY_N;
        CData/*0:0*/ __PVT__s_xactor_spi_f_rd_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_spi_f_rd_data_DEQ;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_addr_ENQ;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_data_ENQ;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_resp_DEQ;
        CData/*7:0*/ __PVT__spi_rx_fifo_D_OUT;
        CData/*0:0*/ __PVT__spi_rx_fifo_ENQ;
        CData/*7:0*/ __PVT__spi_tx_fifo_D_OUT;
        CData/*0:0*/ __PVT__spi_tx_fifo_CLR;
        CData/*0:0*/ __PVT__spi_tx_fifo_DEQ;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_rl_read_request_from_core;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_rl_read_response_from_controller;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_rl_read_response_to_core;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_rl_write_request_from_core;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg;
        CData/*0:0*/ __PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_chip_select_control;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_data_receive;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_data_transmit;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_receive_done;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_receive_idle;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_receive_start_receive;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_transmit_idle;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_transmit_start;
        CData/*0:0*/ __PVT__WILL_FIRE_RL_spi_rl_write_to_cfg;
        CData/*7:0*/ __PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6;
        CData/*7:0*/ __PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1;
        CData/*0:0*/ __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4;
        CData/*0:0*/ __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6;
        CData/*0:0*/ __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7;
        CData/*0:0*/ __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8;
        CData/*0:0*/ __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11;
        CData/*0:0*/ __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5;
        CData/*0:0*/ __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9;
    };
    struct {
        CData/*0:0*/ __PVT__MUX_spi_rg_nss_write_1___05FSEL_1;
        CData/*0:0*/ __PVT__MUX_spi_rg_nss_write_1___05FSEL_3;
        CData/*0:0*/ __PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2;
        CData/*0:0*/ __PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6;
        CData/*0:0*/ __PVT__MUX_spi_tx_data_en_write_1___05FSEL_1;
        CData/*7:0*/ __PVT__v___05Fh2759;
        CData/*7:0*/ __PVT__v___05Fh8399;
        CData/*0:0*/ __PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223;
        CData/*0:0*/ __PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315;
        CData/*0:0*/ __PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319;
        CData/*0:0*/ __PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233;
        CData/*0:0*/ __PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156;
        CData/*0:0*/ __PVT_____05Fduses58;
        CData/*0:0*/ __PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299;
        CData/*0:0*/ __PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306;
        CData/*0:0*/ __PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209;
        CData/*0:0*/ __PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211;
        CData/*0:0*/ __PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214;
        CData/*0:0*/ __PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217;
        CData/*0:0*/ __PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273;
        CData/*0:0*/ __PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280;
        CData/*0:0*/ __PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130;
        CData/*0:0*/ __PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182;
        CData/*0:0*/ __PVT__v___05Fh5419;
        CData/*0:0*/ __PVT__ff_rd_req__DOT__sEnqToggle;
        CData/*0:0*/ __PVT__ff_rd_req__DOT__sDeqToggle;
        CData/*0:0*/ __PVT__ff_rd_req__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__ff_rd_req__DOT__dEnqToggle;
        CData/*0:0*/ __PVT__ff_rd_req__DOT__dDeqToggle;
        CData/*0:0*/ __PVT__ff_rd_req__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__ff_sync_rd_resp__DOT__sEnqToggle;
        CData/*0:0*/ __PVT__ff_sync_rd_resp__DOT__sDeqToggle;
        CData/*0:0*/ __PVT__ff_sync_rd_resp__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__ff_sync_rd_resp__DOT__dEnqToggle;
        CData/*0:0*/ __PVT__ff_sync_rd_resp__DOT__dDeqToggle;
        CData/*0:0*/ __PVT__ff_sync_rd_resp__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__ff_wr_req__DOT__sEnqToggle;
        CData/*0:0*/ __PVT__ff_wr_req__DOT__sDeqToggle;
        CData/*0:0*/ __PVT__ff_wr_req__DOT__sSyncReg1;
        CData/*0:0*/ __PVT__ff_wr_req__DOT__dEnqToggle;
        CData/*0:0*/ __PVT__ff_wr_req__DOT__dDeqToggle;
        CData/*0:0*/ __PVT__ff_wr_req__DOT__dSyncReg1;
        CData/*0:0*/ __PVT__s_xactor_spi_f_rd_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_rd_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_rd_data__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_addr__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_data__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_data__DOT__empty_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_resp__DOT__full_reg;
        CData/*0:0*/ __PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg;
        CData/*1:0*/ __PVT__s_xactor_spi_f_wr_resp__DOT__data0_reg;
        CData/*1:0*/ __PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg;
        CData/*0:0*/ __PVT__spi_rx_fifo__DOT__not_ring_full;
        CData/*0:0*/ __PVT__spi_rx_fifo__DOT__ring_empty;
        CData/*4:0*/ __PVT__spi_rx_fifo__DOT__head;
        CData/*4:0*/ __PVT__spi_rx_fifo__DOT__next_head;
        CData/*4:0*/ __PVT__spi_rx_fifo__DOT__tail;
        CData/*4:0*/ __PVT__spi_rx_fifo__DOT__next_tail;
        CData/*0:0*/ __PVT__spi_rx_fifo__DOT__hasodata;
        CData/*0:0*/ __PVT__spi_tx_fifo__DOT__not_ring_full;
        CData/*0:0*/ __PVT__spi_tx_fifo__DOT__ring_empty;
        CData/*4:0*/ __PVT__spi_tx_fifo__DOT__head;
    };
    struct {
        CData/*4:0*/ __PVT__spi_tx_fifo__DOT__next_head;
        CData/*4:0*/ __PVT__spi_tx_fifo__DOT__tail;
        CData/*4:0*/ __PVT__spi_tx_fifo__DOT__next_tail;
        CData/*0:0*/ __PVT__spi_tx_fifo__DOT__hasodata;
        IData/*31:0*/ __PVT__spi_wr_rd_data_wget;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_cr1;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_cr1_D_IN;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_cr2;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_crcpr;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_dr1;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_dr1_D_IN;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_dr2;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_dr3;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_dr4;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_dr5;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_dr5_D_IN;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_rxcrcr;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_sr;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_sr_D_IN;
        IData/*31:0*/ __PVT__spi_rg_spi_cfg_txcrcr;
        IData/*31:0*/ __PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4;
        IData/*31:0*/ __PVT__v___05Fh11853;
        IData/*31:0*/ __PVT__v___05Fh3428;
        IData/*31:0*/ __PVT__v___05Fh5769;
        IData/*31:0*/ __PVT__v___05Fh6289;
        IData/*31:0*/ __PVT__v___05Fh6816;
        IData/*31:0*/ __PVT__v___05Fh7130;
        IData/*31:0*/ __PVT__v___05Fh7329;
        IData/*31:0*/ __PVT__v___05Fh7628;
        IData/*31:0*/ __PVT__v___05Fh8248;
        IData/*31:0*/ __PVT__v___05Fh8744;
        IData/*31:0*/ __PVT__v___05Fh8863;
        IData/*31:0*/ __PVT__v___05Fh9087;
        IData/*31:0*/ __PVT__v___05Fh9152;
        IData/*31:0*/ __PVT__v___05Fh10919;
        IData/*31:0*/ __PVT__v___05Fh9738;
        IData/*31:0*/ __PVT__v___05Fh9992;
        IData/*31:0*/ __PVT__v___05Fh7823;
        IData/*31:0*/ __PVT__v___05Fh4119;
        IData/*31:0*/ __PVT__v___05Fh5228;
        IData/*31:0*/ __PVT__v___05Fh5330;
        WData/*159:0*/ __PVT___theResult___05F___05Fh10026[5];
        IData/*31:0*/ __PVT__x___05Fh2909;
        IData/*31:0*/ __PVT__x___05Fh2934;
        IData/*31:0*/ __PVT__x___05Fh2959;
        IData/*31:0*/ __PVT__x___05Fh2984;
        IData/*31:0*/ __PVT__x___05Fh3086;
        IData/*31:0*/ __PVT__x___05Fh3121;
        IData/*31:0*/ __PVT__x___05Fh3146;
        IData/*31:0*/ __PVT__x___05Fh3171;
        IData/*31:0*/ __PVT__ff_sync_rd_resp__DOT__syncFIFO1Data;
        WData/*66:0*/ __PVT__ff_wr_req__DOT__syncFIFO1Data[3];
        QData/*34:0*/ __PVT__ff_rd_req__DOT__syncFIFO1Data;
        QData/*36:0*/ __PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg;
        QData/*33:0*/ __PVT__s_xactor_spi_f_rd_data__DOT__data0_reg;
        QData/*33:0*/ __PVT__s_xactor_spi_f_rd_data__DOT__data1_reg;
        QData/*36:0*/ __PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg;
        QData/*36:0*/ __PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg;
        QData/*35:0*/ __PVT__s_xactor_spi_f_wr_data__DOT__data0_reg;
        QData/*35:0*/ __PVT__s_xactor_spi_f_wr_data__DOT__data1_reg;
        CData/*7:0*/ __PVT__spi_rx_fifo__DOT__arr[19];
        CData/*7:0*/ __PVT__spi_tx_fifo__DOT__arr[19];
    };
    
    // LOCAL VARIABLES
    CData/*7:0*/ spi_rx_fifo__DOT____Vlvbound2;
    CData/*7:0*/ spi_tx_fifo__DOT____Vlvbound2;
    CData/*0:0*/ __Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg;
    CData/*0:0*/ __Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg;
    CData/*4:0*/ __Vdly__spi_rx_fifo__DOT__head;
    CData/*4:0*/ __Vdly__spi_rx_fifo__DOT__tail;
    CData/*0:0*/ __Vdly__spi_rx_fifo__DOT__ring_empty;
    CData/*0:0*/ __Vdly__spi_rx_fifo__DOT__not_ring_full;
    CData/*0:0*/ __Vdly__spi_rx_fifo__DOT__hasodata;
    CData/*4:0*/ __Vdlyvdim0__spi_rx_fifo__DOT__arr__v0;
    CData/*7:0*/ __Vdlyvval__spi_rx_fifo__DOT__arr__v0;
    CData/*0:0*/ __Vdlyvset__spi_rx_fifo__DOT__arr__v0;
    CData/*4:0*/ __Vdly__spi_tx_fifo__DOT__head;
    CData/*4:0*/ __Vdly__spi_tx_fifo__DOT__tail;
    CData/*0:0*/ __Vdly__spi_tx_fifo__DOT__ring_empty;
    CData/*0:0*/ __Vdly__spi_tx_fifo__DOT__not_ring_full;
    CData/*0:0*/ __Vdly__spi_tx_fifo__DOT__hasodata;
    CData/*4:0*/ __Vdlyvdim0__spi_tx_fifo__DOT__arr__v0;
    CData/*7:0*/ __Vdlyvval__spi_tx_fifo__DOT__arr__v0;
    CData/*0:0*/ __Vdlyvset__spi_tx_fifo__DOT__arr__v0;
    CData/*0:0*/ __Vdly__ff_rd_req__DOT__sEnqToggle;
    CData/*0:0*/ __Vdly__ff_sync_rd_resp__DOT__sEnqToggle;
    CData/*0:0*/ __Vdly__ff_rd_req__DOT__dDeqToggle;
    CData/*0:0*/ __Vdly__ff_sync_rd_resp__DOT__dDeqToggle;
    CData/*0:0*/ __Vdly__ff_wr_req__DOT__dDeqToggle;
    QData/*36:0*/ __Vdly__s_xactor_spi_f_rd_addr__DOT__data0_reg;
    QData/*36:0*/ __Vdly__s_xactor_spi_f_wr_addr__DOT__data0_reg;
    QData/*35:0*/ __Vdly__s_xactor_spi_f_wr_data__DOT__data0_reg;
    
    // INTERNAL VARIABLES
  private:
    VmkSoc__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(VmkSoc_mkspi);  ///< Copying not allowed
  public:
    VmkSoc_mkspi(const char* name = "TOP");
    ~VmkSoc_mkspi();
    
    // INTERNAL METHODS
    void __Vconfigure(VmkSoc__Syms* symsp, bool first);
    void _combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__11(VmkSoc__Syms* __restrict vlSymsp);
    void _combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__12(VmkSoc__Syms* __restrict vlSymsp);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
  public:
    void _initial__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__1(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__13(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__15(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__17(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__3(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__5(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__7(VmkSoc__Syms* __restrict vlSymsp);
    void _sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__4(VmkSoc__Syms* __restrict vlSymsp);
    void _settle__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__9(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__10(VmkSoc__Syms* __restrict vlSymsp) VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
