// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc_mkpwm.h"
#include "VmkSoc__Syms.h"

//==========

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__13(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__13\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm0.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_clock_selector = (1U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 4U)));
        }
    } else {
        this->__PVT__pwm_clock_selector = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_output_enable = (1U 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 8U)));
        }
    } else {
        this->__PVT__pwm_pwm_output_enable = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__PVT__pwm_interrupt = ((IData)(vlTOPp->RST_N) 
                                  & ((~ (IData)(this->__PVT__pwm_pwm_enable)) 
                                     & (IData)(this->__PVT__pwm_sync_pwm_output_dD_OUT)));
    this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg 
        = (0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg 
        = this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg;
    this->__Vdly__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg 
        = this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg;
    this->__Vdly__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg 
        = this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg;
    this->__Vdly__pwm_sync_period__DOT__sync__DOT__sToggleReg 
        = this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg;
    this->__Vdly__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg 
        = this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg;
    this->__Vdly__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg 
        = this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg;
    this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_clock_divider_clk));
    if (vlTOPp->RST_N) {
        if (this->__PVT__pwm_clock_divisor_sync_sRDY) {
            this->__Vdly__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg 
                = (1U & (~ (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg)));
        }
    } else {
        this->__Vdly__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__Vdly__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg 
                = (1U & (~ (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
        }
    } else {
        this->__Vdly__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__Vdly__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg 
                = (1U & (~ (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg)));
        }
    } else {
        this->__Vdly__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__Vdly__pwm_sync_period__DOT__sync__DOT__sToggleReg 
                = (1U & (~ (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg)));
        }
    } else {
        this->__Vdly__pwm_sync_period__DOT__sync__DOT__sToggleReg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__Vdly__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg 
                = (1U & (~ (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg)));
        }
    } else {
        this->__Vdly__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__Vdly__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg 
                = (1U & (~ (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)));
        }
    } else {
        this->__Vdly__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg = 0U;
    }
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg1)));
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg1)));
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg1)));
    this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg1)));
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg1)));
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg1)));
    if ((1U & (~ (IData)(vlTOPp->RST_N)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate = 1U;
    }
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg1 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dLastState)));
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg1 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dLastState)));
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg1 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dLastState)));
    this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg1 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__dLastState)));
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg1 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dLastState)));
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg1 
        = (1U & ((~ (IData)(vlTOPp->RST_N)) | (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dLastState)));
    if ((1U & (~ (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk)))) {
        this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate 
            = this->__PVT__pwm_clock_divider_new_clock__DOT__new_gate;
    }
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold 
        = ((IData)(this->__PVT__pwm_control_reset__DOT__rst)
            ? (3U & (IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset))
            : 0U);
    this->__PVT__pwm_control_reset__DOT__rstSync__DOT__next_reset 
        = (1U | ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                 << 1U));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = ((IData)(this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg)
            ? ((IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_clk) 
               & (IData)(this->__PVT__pwm_clock_divider_new_clock__DOT__current_gate))
            : (IData)(vlTOPp->CLK));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (this->__PVT__pwm_overall_reset_RST_OUT) {
        if (((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg2) 
             != (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dLastState))) {
            this->__PVT__pwm_sync_pwm_output_dD_OUT 
                = this->__PVT__pwm_sync_pwm_output__DOT__sDataSyncIn;
        }
    } else {
        this->__PVT__pwm_sync_pwm_output_dD_OUT = 0U;
    }
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
             & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
                >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))) {
            this->__PVT__pwm_clock_divider_clk = this->__PVT__pwm_clock_divider_clk_D_IN;
        }
    } else {
        this->__PVT__pwm_clock_divider_clk = 0U;
    }
    this->__PVT__pwm_clock_divider_clk_D_IN = (1U & 
                                               (~ (IData)(this->__PVT__pwm_clock_divider_clk)));
    if (vlTOPp->RST_N) {
        this->__PVT__pwm_clock_divider_rg_counter = this->__PVT__pwm_clock_divider_rg_counter_D_IN;
        this->__PVT__pwm_clock_divider_rg_divisor = 
            (0xffffU & ((0U == (IData)(this->__PVT__pwm_clock_divisor_sync_dD_OUT))
                         ? (IData)(this->__PVT__pwm_clock_divisor_sync_dD_OUT)
                         : ((IData)(this->__PVT__pwm_clock_divisor_sync_dD_OUT) 
                            - (IData)(1U))));
    } else {
        this->__PVT__pwm_clock_divider_rg_counter = 0U;
        this->__PVT__pwm_clock_divider_rg_divisor = 0U;
    }
    this->__PVT__pwm_clock_divider_rg_counter_D_IN 
        = (((0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor)) 
            & ((IData)(this->__PVT__pwm_clock_divider_rg_counter) 
               >= (IData)(this->__PVT__pwm_clock_divider_rg_divisor)))
            ? 0U : (0xffffU & ((IData)(1U) + (IData)(this->__PVT__pwm_clock_divider_rg_counter))));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_control_reset__DOT__rst = (1U 
                                                & ((~ (IData)(vlTOPp->RST_N)) 
                                                   | (~ (IData)(this->__PVT__pwm_reset_counter))));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg2) 
             != (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dLastState))) {
            this->__PVT__pwm_clock_divisor_sync_dD_OUT 
                = this->__PVT__pwm_clock_divisor_sync__DOT__sDataSyncIn;
        }
    } else {
        this->__PVT__pwm_clock_divisor_sync_dD_OUT = 0U;
    }
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg2));
    if (vlTOPp->RST_N) {
        if (this->__PVT__pwm_clock_divisor_sync_sRDY) {
            this->__PVT__pwm_clock_divisor_sync__DOT__sDataSyncIn 
                = this->__PVT__pwm_clock_divisor;
        }
    } else {
        this->__PVT__pwm_clock_divisor_sync__DOT__sDataSyncIn = 0U;
    }
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg1));
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg 
        = this->__Vdly__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg;
    this->__PVT__pwm_clock_divisor_sync_sRDY = ((IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sSyncReg2) 
                                                == (IData)(this->__PVT__pwm_clock_divisor_sync__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg 
        = this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg;
    if (this->__PVT__pwm_overall_reset_RST_OUT) {
        if (this->__PVT__pwm_sync_pwm_output_sRDY) {
            this->__Vdly__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg 
                = (1U & (~ (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg)));
        }
    } else {
        this->__Vdly__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg = 0U;
    }
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2 
        = (1U & ((~ (IData)(this->__PVT__pwm_overall_reset_RST_OUT)) 
                 | (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg1)));
    if (this->__PVT__pwm_overall_reset_RST_OUT) {
        if (this->__PVT__pwm_sync_pwm_output_sRDY) {
            this->__PVT__pwm_sync_pwm_output__DOT__sDataSyncIn 
                = this->__PVT__pwm_pwm_output;
        }
    } else {
        this->__PVT__pwm_sync_pwm_output__DOT__sDataSyncIn = 0U;
    }
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg1 
        = (1U & ((~ (IData)(this->__PVT__pwm_overall_reset_RST_OUT)) 
                 | (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dLastState)));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg2) 
             != (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__dLastState))) {
            this->__PVT__pwm_sync_period_dD_OUT = this->__PVT__pwm_sync_period__DOT__sDataSyncIn;
        }
    } else {
        this->__PVT__pwm_sync_period_dD_OUT = 0U;
    }
    this->__PVT__pwm_sync_period__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg2));
    this->__PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg1));
    this->__PVT__pwm_sync_period__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (this->__PVT__pwm_overall_reset_RST_OUT) {
        if (this->__PVT__pwm_sync_pwm_start_dD_OUT) {
            this->__PVT__pwm_pwm_output = (1U & ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT)
                                                  ? 
                                                 ((IData)(this->__PVT__pwm_rg_counter) 
                                                  < (IData)(this->__PVT__pwm_sync_duty_cycle_dD_OUT))
                                                  : 
                                                 (~ (IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47))));
        }
    } else {
        this->__PVT__pwm_pwm_output = 0U;
    }
    if (this->__PVT__pwm_overall_reset_RST_OUT) {
        if (((IData)(this->__PVT__pwm_sync_pwm_start_dD_OUT) 
             & (((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                 | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT)) 
                | (IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)))) {
            this->__PVT__pwm_rg_counter = this->__PVT__pwm_rg_counter_D_IN;
        }
    } else {
        this->__PVT__pwm_rg_counter = 0U;
    }
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_reset_counter = (1U & (IData)(
                                                           (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0xbU)));
        }
    } else {
        this->__PVT__pwm_reset_counter = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (0xcU == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_clock_divisor = (0xffffU 
                                              & (IData)(
                                                        (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                         >> 4U)));
        }
    } else {
        this->__PVT__pwm_clock_divisor = 0U;
    }
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dLastState 
        = ((IData)(this->__PVT__pwm_overall_reset_RST_OUT) 
           & (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg2));
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg2 
        = ((IData)(this->__PVT__pwm_overall_reset_RST_OUT) 
           & (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg1));
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__dSyncReg1 
        = ((IData)(this->__PVT__pwm_overall_reset_RST_OUT) 
           & (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg 
        = this->__Vdly__pwm_sync_period__DOT__sync__DOT__sToggleReg;
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__PVT__pwm_sync_period__DOT__sDataSyncIn 
                = this->__PVT__pwm_period;
        }
    } else {
        this->__PVT__pwm_sync_period__DOT__sDataSyncIn = 0U;
    }
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47 
        = ((IData)(this->__PVT__pwm_rg_counter) < (0xffffU 
                                                   & ((0U 
                                                       == (IData)(this->__PVT__pwm_sync_period_dD_OUT))
                                                       ? (IData)(this->__PVT__pwm_sync_period_dD_OUT)
                                                       : 
                                                      ((IData)(this->__PVT__pwm_sync_period_dD_OUT) 
                                                       - (IData)(1U)))));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg2) 
             != (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dLastState))) {
            this->__PVT__pwm_sync_duty_cycle_dD_OUT 
                = this->__PVT__pwm_sync_duty_cycle__DOT__sDataSyncIn;
        }
    } else {
        this->__PVT__pwm_sync_duty_cycle_dD_OUT = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg2) 
             != (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dLastState))) {
            this->__PVT__pwm_sync_pwm_start_dD_OUT 
                = this->__PVT__pwm_sync_pwm_start__DOT__sDataSyncIn;
        }
    } else {
        this->__PVT__pwm_sync_pwm_start_dD_OUT = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg2) 
             != (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dLastState))) {
            this->__PVT__pwm_sync_pwm_enable_dD_OUT 
                = this->__PVT__pwm_sync_pwm_enable__DOT__sDataSyncIn;
        }
    } else {
        this->__PVT__pwm_sync_pwm_enable_dD_OUT = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg2) 
             != (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dLastState))) {
            this->__PVT__pwm_sync_continous_once_dD_OUT 
                = this->__PVT__pwm_sync_continous_once__DOT__sDataSyncIn;
        }
    } else {
        this->__PVT__pwm_sync_continous_once_dD_OUT = 0U;
    }
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg2));
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg2));
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg2));
    this->__PVT__pwm_rg_counter_D_IN = (0xffffU & (
                                                   ((IData)(this->__PVT__pwm_sync_pwm_enable_dD_OUT) 
                                                    | (IData)(this->__PVT__pwm_sync_continous_once_dD_OUT))
                                                    ? 
                                                   ((IData)(this->__PVT__pwm_rg_counter_2_ULT_IF_pwm_sync_period_read___05F_ETC___05F_d47)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__pwm_rg_counter))
                                                     : 0U)
                                                    : 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__pwm_rg_counter))));
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg2));
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg1));
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg1));
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg1));
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg1));
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg));
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_overall_reset_RST_OUT = (1U & 
                                              (~ ((~ (IData)(vlTOPp->RST_N)) 
                                                  | (~ 
                                                     ((IData)(this->__PVT__pwm_control_reset__DOT__rstSync__DOT__reset_hold) 
                                                      >> 1U)))));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg 
        = this->__Vdly__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg;
    this->__PVT__pwm_sync_pwm_output_sRDY = ((IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sSyncReg2) 
                                             == (IData)(this->__PVT__pwm_sync_pwm_output__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (0U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_period = (0xffffU & (IData)(
                                                         (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 4U)));
        }
    } else {
        this->__PVT__pwm_period = 0U;
    }
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg 
        = this->__Vdly__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg;
    this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg 
        = this->__Vdly__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg;
    this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg 
        = this->__Vdly__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg;
    this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg 
        = this->__Vdly__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg;
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__PVT__pwm_sync_duty_cycle__DOT__sDataSyncIn 
                = this->__PVT__pwm_duty_cycle;
        }
    } else {
        this->__PVT__pwm_sync_duty_cycle__DOT__sDataSyncIn = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__PVT__pwm_sync_pwm_start__DOT__sDataSyncIn 
                = this->__PVT__pwm_pwm_start;
        }
    } else {
        this->__PVT__pwm_sync_pwm_start__DOT__sDataSyncIn = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__PVT__pwm_sync_pwm_enable__DOT__sDataSyncIn 
                = this->__PVT__pwm_pwm_enable;
        }
    } else {
        this->__PVT__pwm_sync_pwm_enable__DOT__sDataSyncIn = 0U;
    }
    if (vlTOPp->RST_N) {
        if (this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock) {
            this->__PVT__pwm_sync_continous_once__DOT__sDataSyncIn 
                = this->__PVT__pwm_continous_once;
        }
    } else {
        this->__PVT__pwm_sync_continous_once__DOT__sDataSyncIn = 0U;
    }
    this->__PVT__CAN_FIRE_RL_pwm_sync_from_default_to_downclock 
        = ((((((IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_continous_once__DOT__sync__DOT__sToggleReg)) 
              & ((IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sSyncReg2) 
                 == (IData)(this->__PVT__pwm_sync_duty_cycle__DOT__sync__DOT__sToggleReg))) 
             & ((IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sSyncReg2) 
                == (IData)(this->__PVT__pwm_sync_period__DOT__sync__DOT__sToggleReg))) 
            & ((IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sSyncReg2) 
               == (IData)(this->__PVT__pwm_sync_pwm_enable__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sSyncReg2) 
              == (IData)(this->__PVT__pwm_sync_pwm_start__DOT__sync__DOT__sToggleReg)));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (4U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (1U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_duty_cycle = (0xffffU 
                                           & (IData)(
                                                     (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                      >> 4U)));
        }
    } else {
        this->__PVT__pwm_duty_cycle = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_start = (1U & (IData)(
                                                       (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                        >> 6U)));
        }
    } else {
        this->__PVT__pwm_pwm_start = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_enable = (1U & (IData)(
                                                        (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                         >> 5U)));
        }
    } else {
        this->__PVT__pwm_pwm_enable = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_continous_once = (1U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 7U)));
        }
    } else {
        this->__PVT__pwm_continous_once = 0U;
    }
    this->__PVT__CAN_FIRE_RL_write_request = (((IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                                              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__PVT__r1___05Fread___05Fh3256 = (((((IData)(this->__PVT__pwm_reset_counter) 
                                               << 6U) 
                                              | ((IData)(this->__PVT__pwm_interrupt) 
                                                 << 4U)) 
                                             | (((IData)(this->__PVT__pwm_pwm_output_enable) 
                                                 << 3U) 
                                                | ((IData)(this->__PVT__pwm_continous_once) 
                                                   << 2U))) 
                                            | (((IData)(this->__PVT__pwm_pwm_start) 
                                                << 1U) 
                                               | (IData)(this->__PVT__pwm_pwm_enable)));
    this->__PVT__s_xactor_f_wr_resp_D_IN = ((((8U == 
                                               (0xfU 
                                                & (IData)(
                                                          (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                              & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                             | ((((0U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                               >> 5U)))) 
                                                  | (4U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                 >> 5U))))) 
                                                 | (0xcU 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                                                >> 5U))))) 
                                                & (1U 
                                                   == 
                                                   (3U 
                                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg)))))
                                             ? 0U : 2U);
    this->__PVT__s_xactor_f_rd_data_D_IN = (((QData)((IData)(
                                                             ((((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))) 
                                                               | ((((0U 
                                                                     == 
                                                                     (0xfU 
                                                                      & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                    | (4U 
                                                                       == 
                                                                       (0xfU 
                                                                        & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                   | (0xcU 
                                                                      == 
                                                                      (0xfU 
                                                                       & (IData)(
                                                                                (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U))))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg)))))
                                                               ? 0U
                                                               : 2U))) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              (((8U 
                                                                 == 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                             >> 5U)))) 
                                                                & (0U 
                                                                   == 
                                                                   (3U 
                                                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                ? 
                                                               (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                 << 0x19U) 
                                                                | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                    << 0x18U) 
                                                                   | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                       << 0x11U) 
                                                                      | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                          << 0x10U) 
                                                                         | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                             << 9U) 
                                                                            | (((IData)(this->__PVT__pwm_clock_selector) 
                                                                                << 8U) 
                                                                               | (((IData)(this->__PVT__r1___05Fread___05Fh3256) 
                                                                                << 1U) 
                                                                                | (IData)(this->__PVT__pwm_clock_selector))))))))
                                                                : 
                                                               (((0U 
                                                                  == 
                                                                  (0xfU 
                                                                   & (IData)(
                                                                             (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                              >> 5U)))) 
                                                                 & (1U 
                                                                    == 
                                                                    (3U 
                                                                     & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                 ? 
                                                                (((IData)(this->__PVT__pwm_period) 
                                                                  << 0x10U) 
                                                                 | (IData)(this->__PVT__pwm_period))
                                                                 : 
                                                                (((4U 
                                                                   == 
                                                                   (0xfU 
                                                                    & (IData)(
                                                                              (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                               >> 5U)))) 
                                                                  & (1U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                  ? 
                                                                 (((IData)(this->__PVT__pwm_duty_cycle) 
                                                                   << 0x10U) 
                                                                  | (IData)(this->__PVT__pwm_duty_cycle))
                                                                  : 
                                                                 (((0xcU 
                                                                    == 
                                                                    (0xfU 
                                                                     & (IData)(
                                                                               (this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
                                                                                >> 5U)))) 
                                                                   & (1U 
                                                                      == 
                                                                      (3U 
                                                                       & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg))))
                                                                   ? 
                                                                  (((IData)(this->__PVT__pwm_clock_divisor) 
                                                                    << 0x10U) 
                                                                   | (IData)(this->__PVT__pwm_clock_divisor))
                                                                   : 0U)))))));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__14(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__14\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm1.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_clock_selector = (1U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 4U)));
        }
    } else {
        this->__PVT__pwm_clock_selector = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_output_enable = (1U 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 8U)));
        }
    } else {
        this->__PVT__pwm_pwm_output_enable = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__PVT__pwm_interrupt = ((IData)(vlTOPp->RST_N) 
                                  & ((~ (IData)(this->__PVT__pwm_pwm_enable)) 
                                     & (IData)(this->__PVT__pwm_sync_pwm_output_dD_OUT)));
    this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg 
        = (0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__15(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__15\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm2.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_clock_selector = (1U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 4U)));
        }
    } else {
        this->__PVT__pwm_clock_selector = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_output_enable = (1U 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 8U)));
        }
    } else {
        this->__PVT__pwm_pwm_output_enable = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__PVT__pwm_interrupt = ((IData)(vlTOPp->RST_N) 
                                  & ((~ (IData)(this->__PVT__pwm_pwm_enable)) 
                                     & (IData)(this->__PVT__pwm_sync_pwm_output_dD_OUT)));
    this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg 
        = (0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_2_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__16(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__16\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm3.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_clock_selector = (1U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 4U)));
        }
    } else {
        this->__PVT__pwm_clock_selector = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_output_enable = (1U 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 8U)));
        }
    } else {
        this->__PVT__pwm_pwm_output_enable = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__PVT__pwm_interrupt = ((IData)(vlTOPp->RST_N) 
                                  & ((~ (IData)(this->__PVT__pwm_pwm_enable)) 
                                     & (IData)(this->__PVT__pwm_sync_pwm_output_dD_OUT)));
    this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg 
        = (0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_3_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__17(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__17\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm4.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_clock_selector = (1U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 4U)));
        }
    } else {
        this->__PVT__pwm_clock_selector = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_output_enable = (1U 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 8U)));
        }
    } else {
        this->__PVT__pwm_pwm_output_enable = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__PVT__pwm_interrupt = ((IData)(vlTOPp->RST_N) 
                                  & ((~ (IData)(this->__PVT__pwm_pwm_enable)) 
                                     & (IData)(this->__PVT__pwm_sync_pwm_output_dD_OUT)));
    this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg 
        = (0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_4_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
}

VL_INLINE_OPT void VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__18(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkpwm::_sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__18\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__data0_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg;
    this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_data__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg;
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_rd_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_rd_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_rd_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_rd_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_wr_resp.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_wr_resp.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_wr_data.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_wr_data.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)) 
                         & (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_wr_addr.error_checks -- Dequeuing from empty fifo\n",
                      vlSymsp->name());
        }
        if (VL_UNLIKELY(((~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)) 
                         & (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
            VL_WRITEF("Warning: FIFO2: %NmkSoc.pwm_cluster.pwm5.s_xactor_f_wr_addr.error_checks -- Enqueuing to a full fifo\n",
                      vlSymsp->name());
        }
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_clock_selector = (1U & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 4U)));
        }
    } else {
        this->__PVT__pwm_clock_selector = 0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
              & (8U == (0xfU & (IData)((this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
             & (0U == (3U & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg))))) {
            this->__PVT__pwm_pwm_output_enable = (1U 
                                                  & (IData)(
                                                            (this->__PVT__s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 8U)));
        }
    } else {
        this->__PVT__pwm_pwm_output_enable = 0U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                     & (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
             & this->__PVT__s_xactor_f_rd_data_D_IN) 
            | ((- (QData)((IData)(((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                          & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request))) 
                                         | ((~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_data__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
            & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg))
            ? this->__PVT__s_xactor_f_rd_data_D_IN : this->__PVT__s_xactor_f_rd_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg 
        = ((((- (IData)((((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                          & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                         | (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                             & (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
             & (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)) 
            | ((- (IData)(((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                           & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg))))) 
               & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg))) 
           | ((- (IData)((1U & ((((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                  & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request))) 
                                 | ((~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))) 
                                | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                   & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)))))) 
              & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data0_reg)));
    this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg 
        = (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
            & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg))
            ? (IData)(this->__PVT__s_xactor_f_wr_resp_D_IN)
            : (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__data1_reg));
    this->__Vdly__s_xactor_f_wr_data__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_data__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_data__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_data__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_data__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_data__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_data__DOT__data1_reg);
    this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_rd_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_rd_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_rd_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_rd_addr__DOT__data1_reg);
    this->__Vdly__s_xactor_f_wr_addr__DOT__data0_reg 
        = ((((- (QData)((IData)((((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                  & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                 | (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
                                     & (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                    & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
             & vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_addr__DOT__data0_reg) 
            | ((- (QData)((IData)(((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                                   & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)))))) 
               & this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg)) 
           | ((- (QData)((IData)((1U & ((((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                          & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ))) 
                                         | ((~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)) 
                                            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))) 
                                        | ((~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg))))))) 
              & this->__PVT__s_xactor_f_wr_addr__DOT__data0_reg));
    this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg 
        = (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
            & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg))
            ? vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_addr__DOT__data0_reg
            : this->__PVT__s_xactor_f_wr_addr__DOT__data1_reg);
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 1U;
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg 
                    = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg)));
            }
        }
    } else {
        this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg = 0U;
    }
    this->__PVT__pwm_interrupt = ((IData)(vlTOPp->RST_N) 
                                  & ((~ (IData)(this->__PVT__pwm_pwm_enable)) 
                                     & (IData)(this->__PVT__pwm_sync_pwm_output_dD_OUT)));
    this->__PVT__pwm_clock_divider_clock_selector__DOT__sel_reg 
        = (0U != (IData)(this->__PVT__pwm_clock_divider_rg_divisor));
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_rd_data_DEQ)))) {
            this->__PVT__s_xactor_f_rd_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_rd_data_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
                this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_rd_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_read_request)))) {
            this->__PVT__s_xactor_f_rd_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_read_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_rd_addr_ENQ)))) {
                this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_rd_addr__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_data_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_data__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_data__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_data_ENQ)))) {
                this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_data__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
             & (~ (IData)(this->__PVT__s_xactor_f_wr_resp_DEQ)))) {
            this->__PVT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
                this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(this->__PVT__s_xactor_f_wr_addr_ENQ) 
             & (~ (IData)(this->__PVT__CAN_FIRE_RL_write_request)))) {
            this->__PVT__s_xactor_f_wr_addr__DOT__full_reg 
                = (1U & (~ (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg)));
        } else {
            if (((IData)(this->__PVT__CAN_FIRE_RL_write_request) 
                 & (~ (IData)(this->__PVT__s_xactor_f_wr_addr_ENQ)))) {
                this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
            }
        }
    } else {
        this->__PVT__s_xactor_f_wr_addr__DOT__full_reg = 1U;
    }
    this->__PVT__s_xactor_f_rd_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_rd_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_data__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_data__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_resp__DOT__empty_reg;
    this->__PVT__s_xactor_f_wr_addr__DOT__empty_reg 
        = this->__Vdly__s_xactor_f_wr_addr__DOT__empty_reg;
    this->__PVT__s_xactor_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_rd_data__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_rd_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_rd_addr__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_read_request = ((IData)(this->__PVT__s_xactor_f_rd_addr__DOT__empty_reg) 
                                             & (IData)(this->__PVT__s_xactor_f_rd_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_data__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_data__DOT__full_reg));
    this->__PVT__s_xactor_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_resp__DOT__full_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_resp__DOT__empty_reg));
    this->__PVT__s_xactor_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__pwm_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_addr__DOT__empty_reg) 
                                           & (IData)(this->__PVT__s_xactor_f_wr_addr__DOT__full_reg));
}
