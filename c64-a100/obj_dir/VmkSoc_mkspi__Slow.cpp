// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc_mkspi.h"
#include "VmkSoc__Syms.h"

//==========

VL_CTOR_IMP(VmkSoc_mkspi) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VmkSoc_mkspi::__Vconfigure(VmkSoc__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

VmkSoc_mkspi::~VmkSoc_mkspi() {
}

void VmkSoc_mkspi::_initial__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__1(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_initial__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__1\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg = 1U;
    this->__PVT__s_xactor_spi_f_rd_data__DOT__data0_reg = 0x2aaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_rd_data__DOT__data1_reg = 0x2aaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg = 1U;
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__data0_reg = 2U;
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg = 2U;
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg = 1U;
    this->__PVT__ff_rd_req__DOT__syncFIFO1Data = 0x2aaaaaaaaULL;
    this->__PVT__ff_rd_req__DOT__sEnqToggle = 0U;
    this->__PVT__ff_rd_req__DOT__sDeqToggle = 0U;
    this->__PVT__ff_rd_req__DOT__sSyncReg1 = 0U;
    this->__PVT__ff_rd_req__DOT__dEnqToggle = 0U;
    this->__PVT__ff_rd_req__DOT__dDeqToggle = 0U;
    this->__PVT__ff_rd_req__DOT__dSyncReg1 = 0U;
    this->__PVT__spi_tx_fifo_D_OUT = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__ring_empty = 1U;
    this->__PVT__spi_tx_fifo__DOT__not_ring_full = 1U;
    this->__PVT__spi_tx_fifo__DOT__hasodata = 0U;
    this->__PVT__spi_tx_fifo__DOT__head = 0U;
    this->__PVT__spi_tx_fifo__DOT__tail = 0U;
    this->__PVT__spi_tx_fifo__DOT__arr[0U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[1U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[2U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[3U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[4U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[5U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[6U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[7U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[8U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[9U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0xaU] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0xbU] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0xcU] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0xdU] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0xeU] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0xfU] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0x10U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0x11U] = 0xaaU;
    this->__PVT__spi_tx_fifo__DOT__arr[0x12U] = 0xaaU;
    this->__PVT__ff_sync_rd_resp__DOT__syncFIFO1Data = 0xaaaaaaaaU;
    this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle = 0U;
    this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle = 0U;
    this->__PVT__ff_sync_rd_resp__DOT__sSyncReg1 = 0U;
    this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle = 0U;
    this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle = 0U;
    this->__PVT__ff_sync_rd_resp__DOT__dSyncReg1 = 0U;
    this->__PVT__spi_rx_fifo_D_OUT = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__ring_empty = 1U;
    this->__PVT__spi_rx_fifo__DOT__not_ring_full = 1U;
    this->__PVT__spi_rx_fifo__DOT__hasodata = 0U;
    this->__PVT__spi_rx_fifo__DOT__head = 0U;
    this->__PVT__spi_rx_fifo__DOT__tail = 0U;
    this->__PVT__spi_rx_fifo__DOT__arr[0U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[1U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[2U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[3U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[4U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[5U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[6U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[7U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[8U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[9U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0xaU] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0xbU] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0xcU] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0xdU] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0xeU] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0xfU] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0x10U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0x11U] = 0xaaU;
    this->__PVT__spi_rx_fifo__DOT__arr[0x12U] = 0xaaU;
    this->__PVT__spi_rg_bit_count = 0xaaU;
    this->__PVT__spi_rg_clk = 0U;
    this->__PVT__spi_rg_clk_counter = 2U;
    this->__PVT__spi_rg_data_counter = 0xaaU;
    this->__PVT__spi_rg_data_rx = 0xaaU;
    this->__PVT__spi_rg_data_tx = 0xaaU;
    this->__PVT__spi_rg_nss = 0U;
    this->__PVT__spi_rg_receive_state = 2U;
    this->__PVT__spi_rg_spi_cfg_cr1 = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_cr2 = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_crcpr = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_dr1 = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_dr2 = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_dr3 = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_dr4 = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_dr5 = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_rxcrcr = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_sr = 0xaaaaaaaaU;
    this->__PVT__spi_rg_spi_cfg_txcrcr = 0xaaaaaaaaU;
    this->__PVT__spi_rg_transfer_done = 0U;
    this->__PVT__spi_rg_transmit_state = 2U;
    this->__PVT__spi_rg_tx_rx_start = 0U;
    this->__PVT__spi_tx_data_en = 0U;
    this->__PVT__spi_wr_spi_out_io1 = 0U;
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg = 1U;
    this->__PVT__s_xactor_spi_f_wr_data__DOT__data0_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_wr_data__DOT__data1_reg = 0xaaaaaaaaaULL;
    this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg = 0U;
    this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg = 1U;
    this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U] = 0xaaaaaaaaU;
    this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] = 0xaaaaaaaaU;
    this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] = 2U;
    this->__PVT__ff_wr_req__DOT__sEnqToggle = 0U;
    this->__PVT__ff_wr_req__DOT__sDeqToggle = 0U;
    this->__PVT__ff_wr_req__DOT__sSyncReg1 = 0U;
    this->__PVT__ff_wr_req__DOT__dEnqToggle = 0U;
    this->__PVT__ff_wr_req__DOT__dDeqToggle = 0U;
    this->__PVT__ff_wr_req__DOT__dSyncReg1 = 0U;
}

void VmkSoc_mkspi::_settle__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__9(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_settle__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__9\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_spi_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_rd_data__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_spi_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_resp__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_rl_read_request_from_core 
        = (((IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle) 
            == (IData)(this->__PVT__ff_rd_req__DOT__sDeqToggle)) 
           & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg));
    this->__PVT__ff_rd_req_dEMPTY_N = ((IData)(this->__PVT__ff_rd_req__DOT__dEnqToggle) 
                                       != (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle));
    this->__PVT__spi_tx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__head))));
    this->__PVT__spi_tx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__tail))));
    this->__PVT__CAN_FIRE_RL_rl_read_response_to_core 
        = (((IData)(this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle) 
            != (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle)) 
           & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg));
    this->__PVT__spi_rx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__head))));
    this->__PVT__spi_rx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__tail))));
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4 
        = (0xffffff7fU & this->__PVT__spi_rg_spi_cfg_sr);
    this->__PVT__x___05Fh2934 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 >> 8U)));
    this->__PVT__x___05Fh3121 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2959 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 >> 8U)));
    this->__PVT__x___05Fh3146 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2909 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 >> 8U)));
    this->__PVT__x___05Fh3086 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                             >> 0x18U)));
    this->__PVT__x___05Fh3171 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2984 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                 >> 8U)));
    this->__PVT__MUX_spi_tx_data_en_write_1___05FSEL_1 
        = (((IData)(this->__PVT__spi_tx_data_en) & 
            (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6 
        = ((0x13U > (IData)(this->__PVT__spi_rg_data_counter))
            ? (0xffU & ((IData)(1U) + (IData)(this->__PVT__spi_rg_data_counter)))
            : 0U);
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11 
        = ((IData)(this->__PVT__spi_tx_data_en) & (0x14U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter)));
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT__v___05Fh5419 = (1U & (IData)(this->__PVT__spi_rg_data_tx));
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = (((IData)(vlTOPp->spi0_io_miso_dat) << 6U) 
               | (0x3fU & ((IData)(this->__PVT__spi_rg_data_rx) 
                           >> 1U)));
    } else {
        this->__PVT__v___05Fh5419 = (1U & ((IData)(this->__PVT__spi_rg_data_tx) 
                                           >> 7U));
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = ((0xfcU & ((IData)(this->__PVT__spi_rg_data_rx) 
                         << 1U)) | ((IData)(vlTOPp->spi0_io_miso_dat) 
                                    << 1U));
    }
    this->__PVT__spi_rg_clk_D_IN = (1U & ((IData)(this->__PVT__spi_rg_nss)
                                           ? (this->__PVT__spi_rg_spi_cfg_cr1 
                                              >> 1U)
                                           : (~ (IData)(this->__PVT__spi_rg_clk))));
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT__v___05Fh2759 = (0xffU & this->__PVT__spi_rg_spi_cfg_dr5);
        this->__PVT__v___05Fh8399 = (((IData)(vlTOPp->spi0_io_miso_dat) 
                                      << 7U) | (0x7fU 
                                                & (IData)(this->__PVT__spi_rg_data_rx)));
    } else {
        this->__PVT__v___05Fh2759 = (0xffU & (this->__PVT__spi_rg_spi_cfg_dr1 
                                              >> 0x18U));
        this->__PVT__v___05Fh8399 = ((0xfeU & (IData)(this->__PVT__spi_rg_data_rx)) 
                                     | (IData)(vlTOPp->spi0_io_miso_dat));
    }
    this->__PVT__spi_wr_rd_data_wget = ((1U & (IData)(
                                                      (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                       >> 0xaU)))
                                         ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                         : ((1U & (IData)(
                                                          (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                           >> 9U)))
                                             ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                             : ((1U 
                                                 & (IData)(
                                                           (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                            >> 8U)))
                                                 ? 
                                                ((1U 
                                                  & (IData)(
                                                            (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                             >> 7U)))
                                                  ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_rxcrcr))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_crcpr)))))
                                                 : 
                                                ((1U 
                                                  & (IData)(
                                                            (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                             >> 7U)))
                                                  ? 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr5))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr4)))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr3))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr2))))
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr1))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_sr)))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_cr2))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_cr1))))))));
    this->__PVT___theResult___05F___05Fh10026[0U] = 
        ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1) ? this->__PVT__spi_rg_spi_cfg_dr5
          : ((0xffffff00U & this->__PVT__spi_rg_spi_cfg_dr5) 
             | (IData)(this->__PVT__spi_rx_fifo_D_OUT)));
    this->__PVT___theResult___05F___05Fh10026[1U] = this->__PVT__spi_rg_spi_cfg_dr4;
    this->__PVT___theResult___05F___05Fh10026[2U] = this->__PVT__spi_rg_spi_cfg_dr3;
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                          << 0x18U) 
                                         | (0xffffffU 
                                            & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                           << 0x18U) 
                                          | (0xffffffU 
                                             & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    } else {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    }
    this->__PVT__spi_wr_clk_en_wget = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                   >> 1U)) 
                                               & (~ (IData)(this->__PVT__spi_rg_clk))) 
                                              | ((this->__PVT__spi_rg_spi_cfg_cr1 
                                                  & (~ 
                                                     (this->__PVT__spi_rg_spi_cfg_cr1 
                                                      >> 1U))) 
                                                 & (IData)(this->__PVT__spi_rg_clk))) 
                                             | ((~ this->__PVT__spi_rg_spi_cfg_cr1) 
                                                & (((this->__PVT__spi_rg_spi_cfg_cr1 
                                                     >> 1U) 
                                                    & (IData)(this->__PVT__spi_rg_clk)) 
                                                   | ((~ 
                                                       (this->__PVT__spi_rg_spi_cfg_cr1 
                                                        >> 1U)) 
                                                      & (~ (IData)(this->__PVT__spi_rg_clk)))))));
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x10U) - (IData)(1U))));
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x18U) - (IData)(1U))));
    this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130 
        = ((7U & (this->__PVT__spi_rg_spi_cfg_cr1 >> 3U)) 
           == (IData)(this->__PVT__spi_rg_clk_counter));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg 
        = ((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
           & (0U == (IData)(this->__PVT__spi_rg_receive_state)));
    this->__PVT__s_xactor_spi_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_0_f_wr_data__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_rl_write_request_from_core 
        = (((((IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle) 
              == (IData)(this->__PVT__ff_wr_req__DOT__sDeqToggle)) 
             & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg)) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg)) 
           & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg));
    this->__PVT__ff_wr_req_dEMPTY_N = ((IData)(this->__PVT__ff_wr_req__DOT__dEnqToggle) 
                                       != (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle));
    this->__PVT__CAN_FIRE_RL_rl_read_response_from_controller 
        = (((IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle) 
            == (IData)(this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle)) 
           & (IData)(this->__PVT__ff_rd_req_dEMPTY_N));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315 
        = ((((~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU)) 
             & (0U != (IData)(this->__PVT__spi_rg_transmit_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
           & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)));
    if (this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130) {
        this->__PVT__spi_rg_clk_counter_D_IN = 0U;
        this->__PVT__spi_wr_transfer_en_whas = ((~ (IData)(this->__PVT__spi_rg_nss)) 
                                                & 1U);
    } else {
        this->__PVT__spi_rg_clk_counter_D_IN = (7U 
                                                & ((IData)(1U) 
                                                   + (IData)(this->__PVT__spi_rg_clk_counter)));
        this->__PVT__spi_wr_transfer_en_whas = 0U;
    }
    this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg = 
        (((IData)(this->__PVT__ff_wr_req_dEMPTY_N) 
          & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) 
         & (~ (IData)(this->__PVT__spi_tx_data_en)));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319 
        = (((IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315) 
            & (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)) 
           | (((0U != (IData)(this->__PVT__spi_rg_receive_state)) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (IData)(this->__PVT__spi_wr_clk_en_wget)));
    this->__PVT_____05Fduses58 = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                    & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                          >> 0x10U))) 
                                   & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                  & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop 
        = (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
              & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
             & ((~ (this->__PVT__spi_rg_spi_cfg_cr2 
                    >> 0xfU)) | (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                    >> 0x10U)))) & 
            (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223 
        = ((((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
             & (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156 
        = (((~ (IData)(this->__PVT__spi_wr_clk_en_wget)) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             | (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233 
        = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (IData)(this->__PVT__spi_rg_transfer_done));
    this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182 
        = ((IData)(this->__PVT__spi_wr_clk_en_wget) 
           | (this->__PVT__spi_rg_spi_cfg_cr1 & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273 
        = ((((8U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209 
        = ((((7U == (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (~ (IData)(this->__PVT__spi_rg_transfer_done)));
    this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx = ((IData)(this->__PVT__spi_wr_transfer_en_whas) 
                                                   & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                         >> 6U) 
                                                        & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
                                                       & (0U 
                                                          != (IData)(this->__PVT__spi_rg_transmit_state))) 
                                                      | ((((this->__PVT__spi_rg_spi_cfg_cr2 
                                                            >> 0xfU) 
                                                           | (this->__PVT__spi_rg_spi_cfg_cr2 
                                                              >> 0x10U)) 
                                                          & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                         & (0U 
                                                            != (IData)(this->__PVT__spi_rg_receive_state)))));
    this->__PVT__spi_tx_data_en_EN = ((((IData)(this->__PVT__spi_tx_data_en) 
                                        & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
                                       & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                                      | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                         & (0x1cU == 
                                            (0xffU 
                                             & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                 << 0x1dU) 
                                                | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                   >> 3U))))));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
           & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                  << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U)))));
    this->__PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget 
        = ((IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233) 
           & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280 
        = (((IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273) 
            & ((7U > (IData)(this->__PVT__spi_rg_data_counter)) 
               | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
           | (((~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
               & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
              & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211 
        = (((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
              & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             | ((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | ((((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | ((IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209) 
                 & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_done = 
        ((3U == (IData)(this->__PVT__spi_rg_receive_state)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
             & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4 
        = ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
           & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle = 
        (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & ((this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU) 
               | ((this->__PVT__spi_rg_spi_cfg_cr2 
                   >> 0x10U) & (IData)(this->__PVT__spi_rg_tx_rx_start)))) 
           & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_receive = 
        (((2U == (IData)(this->__PVT__spi_rg_receive_state)) 
          & (~ (IData)(this->__PVT__spi_rg_nss))) & 
         (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive 
        = (((1U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle 
        = ((((0U == (IData)(this->__PVT__spi_rg_transmit_state)) 
             & (~ (IData)(this->__PVT__spi_tx_data_en))) 
            & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start 
        = (((1U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit 
        = (((2U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__spi_tx_fifo_CLR = (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                     & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)) 
                                    | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                       & (0x1cU == 
                                          (0xffU & 
                                           ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                             << 0x1dU) 
                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U))))));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
             & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rx_fifo_ENQ = (((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                       & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
                                      & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                     & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                    | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                                        & (7U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                                       & (7U == (IData)(this->__PVT__spi_rg_data_counter))));
    this->__PVT__spi_rg_receive_state_EN = ((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                  & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                                                 & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                                & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                                               | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                    & (0U 
                                                       != (IData)(this->__PVT__spi_rg_receive_state))) 
                                                   & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                                 & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_done)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control 
        = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                    >> 9U) & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive))) 
                  & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle))) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle))));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
           & (((this->__PVT__spi_rg_spi_cfg_cr1 >> 6U) 
               & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
              | ((IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156) 
                 & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                       >> 0x10U)))));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
            & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U)) 
           & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata));
    this->__PVT__spi_rg_spi_cfg_cr1_D_IN = (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                               & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                                              & (IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156)) 
                                             & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                                   >> 0x10U)))
                                             ? (0xffffffbfU 
                                                & this->__PVT__spi_rg_spi_cfg_cr1)
                                             : this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]);
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182));
    this->__PVT__spi_tx_fifo_DEQ = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                    & (IData)(this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223));
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214));
    this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish 
        = (((((((~ (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                & (0U == (IData)(this->__PVT__spi_rg_receive_state))) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start))) 
            & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx))) 
           & (~ (IData)(this->__PVT__spi_tx_data_en)));
    this->__PVT__spi_rg_data_tx_D_IN = ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                         ? (IData)(this->__PVT__spi_tx_fifo_D_OUT)
                                         : ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                             ? ((0x80U 
                                                 & this->__PVT__spi_rg_spi_cfg_cr1)
                                                 ? 
                                                (0x7fU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    >> 1U))
                                                 : 
                                                (0xfeU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    << 1U)))
                                             : ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9)
                                                 ? 
                                                ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217)
                                                  ? 
                                                 ((0x80U 
                                                   & this->__PVT__spi_rg_spi_cfg_cr1)
                                                   ? 
                                                  (0x7fU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      >> 1U))
                                                   : 
                                                  (0xfeU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      << 1U)))
                                                  : (IData)(this->__PVT__spi_tx_fifo_D_OUT))
                                                 : 0U)));
    this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
            & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0U != (IData)(this->__PVT__spi_rg_data_counter)));
    this->__PVT__spi_rg_spi_cfg_sr_D_IN = ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                            ? (0x80U 
                                               | (0xffffff7fU 
                                                  & this->__PVT__spi_rg_spi_cfg_sr))
                                            : (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (8U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)
                                                    ? 
                                                   (1U 
                                                    | (0xfffffffeU 
                                                       & this->__PVT__spi_rg_spi_cfg_sr))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4)
                                                     ? this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4
                                                     : 
                                                    (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                                      & (0x13U 
                                                         <= (IData)(this->__PVT__spi_rg_data_counter)))
                                                      ? 
                                                     (1U 
                                                      | (0xfffffffeU 
                                                         & this->__PVT__spi_rg_spi_cfg_sr))
                                                      : 
                                                     ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6)
                                                       ? this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4
                                                       : 0U))))));
    this->__PVT__spi_rg_data_counter_EN = (((((((((
                                                   (((IData)(this->__PVT__spi_tx_data_en) 
                                                     & (0x14U 
                                                        > (IData)(this->__PVT__spi_rg_data_counter))) 
                                                    | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                                        & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                           >> 6U)) 
                                                       & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                                                   | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                        & (0U 
                                                           != (IData)(this->__PVT__spi_rg_receive_state))) 
                                                       & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                      & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                                  | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                     & (0x1cU 
                                                        == 
                                                        (0xffU 
                                                         & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                             << 0x1dU) 
                                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                               >> 3U)))))) 
                                                 | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                                                    & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
                                                | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214))) 
                                               | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                                                   & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                                  & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                 & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280))) 
                                             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                           | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle));
    this->__PVT__spi_rg_bit_count_D_IN = (((((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                           | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4))
                                           ? 0U : (0xffU 
                                                   & ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                        & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                                                       & ((7U 
                                                           > (IData)(this->__PVT__spi_rg_data_counter)) 
                                                          | (7U 
                                                             == (IData)(this->__PVT__spi_rg_data_counter))))
                                                       ? 
                                                      ((IData)(1U) 
                                                       + (IData)(this->__PVT__spi_rg_bit_count))
                                                       : 
                                                      ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                        ? 
                                                       ((IData)(1U) 
                                                        + (IData)(this->__PVT__spi_rg_bit_count))
                                                        : 
                                                       ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7)
                                                         ? 
                                                        ((IData)(1U) 
                                                         + (IData)(this->__PVT__spi_rg_bit_count))
                                                         : 
                                                        ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                          ? 
                                                         ((IData)(1U) 
                                                          + (IData)(this->__PVT__spi_rg_bit_count))
                                                          : 0U))))));
    this->__PVT__spi_rg_data_counter_D_IN = ((((((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1) 
                                                 | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1)) 
                                                | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                               | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                              | (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5))
                                              ? 0U : 
                                             (0xffU 
                                              & ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6)
                                                  : 
                                                 (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280))
                                                   ? 
                                                  ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)
                                                    ? 
                                                   ((7U 
                                                     > (IData)(this->__PVT__spi_rg_data_counter))
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__spi_rg_data_counter))
                                                     : 0U)
                                                    : 0U)
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                    ? 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rg_data_counter))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9)
                                                     ? 
                                                    ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217)
                                                      ? 
                                                     ((IData)(1U) 
                                                      + (IData)(this->__PVT__spi_rg_data_counter))
                                                      : 0U)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                      ? 
                                                     ((IData)(1U) 
                                                      + (IData)(this->__PVT__spi_rg_data_counter))
                                                      : 
                                                     ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                       ? (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6)
                                                       : 0U))))))));
    this->__PVT__spi_rg_spi_cfg_dr1_D_IN = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                             & (0xcU 
                                                == 
                                                (0xffU 
                                                 & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                     << 0x1dU) 
                                                    | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                       >> 3U)))))
                                             ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                             : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                 ? 
                                                ((0x80U 
                                                  & this->__PVT__spi_rg_spi_cfg_cr1)
                                                  ? this->__PVT__x___05Fh3086
                                                  : 
                                                 (0xffffffU 
                                                  & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                     >> 8U)))
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? 
                                                 ((0x13U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter))
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   (0xffffffU 
                                                    & (this->__PVT___theResult___05F___05Fh10026[4U] 
                                                       >> 8U))
                                                    : 
                                                   ((this->__PVT___theResult___05F___05Fh10026[4U] 
                                                     << 8U) 
                                                    | (this->__PVT___theResult___05F___05Fh10026[3U] 
                                                       >> 0x18U)))
                                                   : 
                                                  this->__PVT___theResult___05F___05Fh10026[4U])
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   (0xffffffU 
                                                    & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                       >> 8U))
                                                    : this->__PVT__x___05Fh3086)
                                                   : 0U))));
    this->__PVT__spi_rg_spi_cfg_dr5_D_IN = ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1)
                                             ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                             : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                 ? 
                                                ((0x80U 
                                                  & this->__PVT__spi_rg_spi_cfg_cr1)
                                                  ? 
                                                 (0xffffff00U 
                                                  & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                     << 8U))
                                                  : this->__PVT__x___05Fh2984)
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? 
                                                 ((0x13U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter))
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   ((this->__PVT___theResult___05F___05Fh10026[1U] 
                                                     << 0x18U) 
                                                    | (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                       >> 8U))
                                                    : 
                                                   (0xffffff00U 
                                                    & (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                       << 8U)))
                                                   : 
                                                  this->__PVT___theResult___05F___05Fh10026[0U])
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? this->__PVT__x___05Fh2984
                                                    : 
                                                   (0xffffff00U 
                                                    & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                       << 8U)))
                                                   : 0U))));
}

void VmkSoc_mkspi::_settle__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__10(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_settle__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__10\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__s_xactor_spi_f_rd_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_rd_data_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_rd_data__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__empty_reg));
    this->__PVT__s_xactor_spi_f_wr_resp_DEQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_resp__DOT__full_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg));
    this->__PVT__CAN_FIRE_RL_rl_read_request_from_core 
        = (((IData)(this->__PVT__ff_rd_req__DOT__sEnqToggle) 
            == (IData)(this->__PVT__ff_rd_req__DOT__sDeqToggle)) 
           & (IData)(this->__PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg));
    this->__PVT__ff_rd_req_dEMPTY_N = ((IData)(this->__PVT__ff_rd_req__DOT__dEnqToggle) 
                                       != (IData)(this->__PVT__ff_rd_req__DOT__dDeqToggle));
    this->__PVT__spi_tx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__head))));
    this->__PVT__spi_tx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_tx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_tx_fifo__DOT__tail))));
    this->__PVT__CAN_FIRE_RL_rl_read_response_to_core 
        = (((IData)(this->__PVT__ff_sync_rd_resp__DOT__dEnqToggle) 
            != (IData)(this->__PVT__ff_sync_rd_resp__DOT__dDeqToggle)) 
           & (IData)(this->__PVT__s_xactor_spi_f_rd_data__DOT__full_reg));
    this->__PVT__spi_rx_fifo__DOT__next_head = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__head))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__head))));
    this->__PVT__spi_rx_fifo__DOT__next_tail = ((0x12U 
                                                 == (IData)(this->__PVT__spi_rx_fifo__DOT__tail))
                                                 ? 0U
                                                 : 
                                                (0x1fU 
                                                 & ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rx_fifo__DOT__tail))));
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4 
        = (0xffffff7fU & this->__PVT__spi_rg_spi_cfg_sr);
    this->__PVT__x___05Fh2934 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 >> 8U)));
    this->__PVT__x___05Fh3121 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr3 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2959 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 >> 8U)));
    this->__PVT__x___05Fh3146 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr3 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr4 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2909 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                                 >> 8U)));
    this->__PVT__x___05Fh3086 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr2 
                                             >> 0x18U)));
    this->__PVT__x___05Fh3171 = ((0xffffff00U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 8U)) 
                                 | (0xffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                             >> 0x18U)));
    this->__PVT__x___05Fh2984 = ((0xff000000U & (this->__PVT__spi_rg_spi_cfg_dr4 
                                                 << 0x18U)) 
                                 | (0xffffffU & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                 >> 8U)));
    this->__PVT__MUX_spi_tx_data_en_write_1___05FSEL_1 
        = (((IData)(this->__PVT__spi_tx_data_en) & 
            (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6 
        = ((0x13U > (IData)(this->__PVT__spi_rg_data_counter))
            ? (0xffU & ((IData)(1U) + (IData)(this->__PVT__spi_rg_data_counter)))
            : 0U);
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11 
        = ((IData)(this->__PVT__spi_tx_data_en) & (0x14U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter)));
    this->__PVT__v___05Fh5419 = (1U & ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)
                                        ? (IData)(this->__PVT__spi_rg_data_tx)
                                        : ((IData)(this->__PVT__spi_rg_data_tx) 
                                           >> 7U)));
    this->__PVT__spi_rg_clk_D_IN = (1U & ((IData)(this->__PVT__spi_rg_nss)
                                           ? (this->__PVT__spi_rg_spi_cfg_cr1 
                                              >> 1U)
                                           : (~ (IData)(this->__PVT__spi_rg_clk))));
    this->__PVT__v___05Fh2759 = (0xffU & ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)
                                           ? this->__PVT__spi_rg_spi_cfg_dr5
                                           : (this->__PVT__spi_rg_spi_cfg_dr1 
                                              >> 0x18U)));
    this->__PVT__spi_wr_rd_data_wget = ((1U & (IData)(
                                                      (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                       >> 0xaU)))
                                         ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                         : ((1U & (IData)(
                                                          (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                           >> 9U)))
                                             ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                             : ((1U 
                                                 & (IData)(
                                                           (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                            >> 8U)))
                                                 ? 
                                                ((1U 
                                                  & (IData)(
                                                            (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                             >> 7U)))
                                                  ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_rxcrcr))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_crcpr)))))
                                                 : 
                                                ((1U 
                                                  & (IData)(
                                                            (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                             >> 7U)))
                                                  ? 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr5))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr4)))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr3))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr2))))
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                              >> 6U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_dr1))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_sr)))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                               >> 5U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_cr2))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                >> 4U)))
                                                     ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                     : 
                                                    ((1U 
                                                      & (IData)(
                                                                (this->__PVT__ff_rd_req__DOT__syncFIFO1Data 
                                                                 >> 3U)))
                                                      ? this->__PVT__spi_rg_spi_cfg_txcrcr
                                                      : this->__PVT__spi_rg_spi_cfg_cr1))))))));
    this->__PVT___theResult___05F___05Fh10026[0U] = 
        ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1) ? this->__PVT__spi_rg_spi_cfg_dr5
          : ((0xffffff00U & this->__PVT__spi_rg_spi_cfg_dr5) 
             | (IData)(this->__PVT__spi_rx_fifo_D_OUT)));
    this->__PVT___theResult___05F___05Fh10026[1U] = this->__PVT__spi_rg_spi_cfg_dr4;
    this->__PVT___theResult___05F___05Fh10026[2U] = this->__PVT__spi_rg_spi_cfg_dr3;
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                          << 0x18U) 
                                         | (0xffffffU 
                                            & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)((((IData)(this->__PVT__spi_rx_fifo_D_OUT) 
                                           << 0x18U) 
                                          | (0xffffffU 
                                             & this->__PVT__spi_rg_spi_cfg_dr1)))) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    } else {
        this->__PVT___theResult___05F___05Fh10026[3U] 
            = (IData)((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                        << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))));
        this->__PVT___theResult___05F___05Fh10026[4U] 
            = (IData)(((((QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr1)) 
                         << 0x20U) | (QData)((IData)(this->__PVT__spi_rg_spi_cfg_dr2))) 
                       >> 0x20U));
    }
    this->__PVT__spi_wr_clk_en_wget = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                   >> 1U)) 
                                               & (~ (IData)(this->__PVT__spi_rg_clk))) 
                                              | ((this->__PVT__spi_rg_spi_cfg_cr1 
                                                  & (~ 
                                                     (this->__PVT__spi_rg_spi_cfg_cr1 
                                                      >> 1U))) 
                                                 & (IData)(this->__PVT__spi_rg_clk))) 
                                             | ((~ this->__PVT__spi_rg_spi_cfg_cr1) 
                                                & (((this->__PVT__spi_rg_spi_cfg_cr1 
                                                     >> 1U) 
                                                    & (IData)(this->__PVT__spi_rg_clk)) 
                                                   | ((~ 
                                                       (this->__PVT__spi_rg_spi_cfg_cr1 
                                                        >> 1U)) 
                                                      & (~ (IData)(this->__PVT__spi_rg_clk)))))));
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x10U) - (IData)(1U))));
    this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306 
        = ((IData)(this->__PVT__spi_rg_bit_count) == 
           (0xffU & ((this->__PVT__spi_rg_spi_cfg_cr1 
                      >> 0x18U) - (IData)(1U))));
    this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130 
        = ((7U & (this->__PVT__spi_rg_spi_cfg_cr1 >> 3U)) 
           == (IData)(this->__PVT__spi_rg_clk_counter));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg 
        = ((IData)(this->__PVT__spi_rx_fifo__DOT__hasodata) 
           & (0U == (IData)(this->__PVT__spi_rg_receive_state)));
    this->__PVT__s_xactor_spi_f_wr_addr_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_addr__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__full_reg));
    this->__PVT__s_xactor_spi_f_wr_data_ENQ = ((IData)(vlTOPp->mkSoc__DOT__spi_cluster__DOT__fabric_xactors_to_slaves_1_f_wr_data__DOT__empty_reg) 
                                               & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__full_reg));
    this->__PVT__CAN_FIRE_RL_rl_write_request_from_core 
        = (((((IData)(this->__PVT__ff_wr_req__DOT__sEnqToggle) 
              == (IData)(this->__PVT__ff_wr_req__DOT__sDeqToggle)) 
             & (IData)(this->__PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg)) 
            & (IData)(this->__PVT__s_xactor_spi_f_wr_data__DOT__empty_reg)) 
           & (IData)(this->__PVT__s_xactor_spi_f_wr_resp__DOT__full_reg));
    this->__PVT__ff_wr_req_dEMPTY_N = ((IData)(this->__PVT__ff_wr_req__DOT__dEnqToggle) 
                                       != (IData)(this->__PVT__ff_wr_req__DOT__dDeqToggle));
    if ((0x80U & this->__PVT__spi_rg_spi_cfg_cr1)) {
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get) 
                << 6U) | (0x3fU & ((IData)(this->__PVT__spi_rg_data_rx) 
                                   >> 1U)));
        this->__PVT__v___05Fh8399 = (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get) 
                                      << 7U) | (0x7fU 
                                                & (IData)(this->__PVT__spi_rg_data_rx)));
    } else {
        this->__PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 
            = ((0xfcU & ((IData)(this->__PVT__spi_rg_data_rx) 
                         << 1U)) | ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get) 
                                    << 1U));
        this->__PVT__v___05Fh8399 = ((0xfeU & (IData)(this->__PVT__spi_rg_data_rx)) 
                                     | (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_peripheral_side_mspi_miso_get));
    }
    this->__PVT__CAN_FIRE_RL_rl_read_response_from_controller 
        = (((IData)(this->__PVT__ff_sync_rd_resp__DOT__sEnqToggle) 
            == (IData)(this->__PVT__ff_sync_rd_resp__DOT__sDeqToggle)) 
           & (IData)(this->__PVT__ff_rd_req_dEMPTY_N));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315 
        = ((((~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU)) 
             & (0U != (IData)(this->__PVT__spi_rg_transmit_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
           & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)));
    if (this->__PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130) {
        this->__PVT__spi_rg_clk_counter_D_IN = 0U;
        this->__PVT__spi_wr_transfer_en_whas = ((~ (IData)(this->__PVT__spi_rg_nss)) 
                                                & 1U);
    } else {
        this->__PVT__spi_rg_clk_counter_D_IN = (7U 
                                                & ((IData)(1U) 
                                                   + (IData)(this->__PVT__spi_rg_clk_counter)));
        this->__PVT__spi_wr_transfer_en_whas = 0U;
    }
    this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg = 
        (((IData)(this->__PVT__ff_wr_req_dEMPTY_N) 
          & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg))) 
         & (~ (IData)(this->__PVT__spi_tx_data_en)));
    this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319 
        = (((IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315) 
            & (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)) 
           | (((0U != (IData)(this->__PVT__spi_rg_receive_state)) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (IData)(this->__PVT__spi_wr_clk_en_wget)));
    this->__PVT_____05Fduses58 = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
                                    & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                          >> 0x10U))) 
                                   & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                  & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop 
        = (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
              & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
             & ((~ (this->__PVT__spi_rg_spi_cfg_cr2 
                    >> 0xfU)) | (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                    >> 0x10U)))) & 
            (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223 
        = ((((6U <= (IData)(this->__PVT__spi_rg_data_counter)) 
             & (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156 
        = (((~ (IData)(this->__PVT__spi_wr_clk_en_wget)) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             | (6U == (IData)(this->__PVT__spi_rg_data_counter))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233 
        = ((((~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (IData)(this->__PVT__spi_rg_transfer_done));
    this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182 
        = ((IData)(this->__PVT__spi_wr_clk_en_wget) 
           | (this->__PVT__spi_rg_spi_cfg_cr1 & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273 
        = ((((8U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209 
        = ((((7U == (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           & (~ (IData)(this->__PVT__spi_rg_transfer_done)));
    this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx = ((IData)(this->__PVT__spi_wr_transfer_en_whas) 
                                                   & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                                                         >> 6U) 
                                                        & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299)) 
                                                       & (0U 
                                                          != (IData)(this->__PVT__spi_rg_transmit_state))) 
                                                      | ((((this->__PVT__spi_rg_spi_cfg_cr2 
                                                            >> 0xfU) 
                                                           | (this->__PVT__spi_rg_spi_cfg_cr2 
                                                              >> 0x10U)) 
                                                          & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                         & (0U 
                                                            != (IData)(this->__PVT__spi_rg_receive_state)))));
    this->__PVT__spi_tx_data_en_EN = ((((IData)(this->__PVT__spi_tx_data_en) 
                                        & (0x14U > (IData)(this->__PVT__spi_rg_data_counter))) 
                                       & (0x13U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                                      | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                         & (0x1cU == 
                                            (0xffU 
                                             & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                 << 0x1dU) 
                                                | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                   >> 3U))))));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
           & (0x1cU == (0xffU & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                  << 0x1dU) | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U)))));
    this->__PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget 
        = ((IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233) 
           & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0x10U)));
    this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280 
        = (((IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273) 
            & ((7U > (IData)(this->__PVT__spi_rg_data_counter)) 
               | (7U == (IData)(this->__PVT__spi_rg_data_counter)))) 
           | (((~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full)) 
               & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
              & (IData)(this->__PVT__spi_wr_transfer_en_whas)));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211 
        = (((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
              & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
             | ((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget)))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | (IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209));
    this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214 
        = ((((6U > (IData)(this->__PVT__spi_rg_data_counter)) 
             & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
            & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
           | ((((6U == (IData)(this->__PVT__spi_rg_data_counter)) 
                & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
               & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
              | ((IData)(this->__PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209) 
                 & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_done = 
        ((3U == (IData)(this->__PVT__spi_rg_receive_state)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5 
        = ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
             & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
            & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4 
        = ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
           & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle = 
        (((((0U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & ((this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU) 
               | ((this->__PVT__spi_rg_spi_cfg_cr2 
                   >> 0x10U) & (IData)(this->__PVT__spi_rg_tx_rx_start)))) 
           & (0U == (IData)(this->__PVT__spi_rg_transmit_state))) 
          & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
         & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_receive = 
        (((2U == (IData)(this->__PVT__spi_rg_receive_state)) 
          & (~ (IData)(this->__PVT__spi_rg_nss))) & 
         (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive 
        = (((1U == (IData)(this->__PVT__spi_rg_receive_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle 
        = ((((0U == (IData)(this->__PVT__spi_rg_transmit_state)) 
             & (~ (IData)(this->__PVT__spi_tx_data_en))) 
            & (~ (this->__PVT__spi_rg_spi_cfg_cr2 >> 0xfU))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start 
        = (((1U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit 
        = (((2U == (IData)(this->__PVT__spi_rg_transmit_state)) 
            & (~ (IData)(this->__PVT__spi_rg_nss))) 
           & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx)));
    this->__PVT__spi_tx_fifo_CLR = (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                     & (IData)(this->__PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315)) 
                                    | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                       & (0x1cU == 
                                          (0xffU & 
                                           ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                             << 0x1dU) 
                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                               >> 3U))))));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_3 
        = ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
             & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
            & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__spi_rx_fifo_ENQ = (((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                       & (0U != (IData)(this->__PVT__spi_rg_receive_state))) 
                                      & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                     & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                    | ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                         & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                                        & (7U <= (IData)(this->__PVT__spi_rg_data_counter))) 
                                       & (7U == (IData)(this->__PVT__spi_rg_data_counter))));
    this->__PVT__spi_rg_receive_state_EN = ((((((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                  & (~ (IData)(this->__PVT__spi_rx_fifo__DOT__not_ring_full))) 
                                                 & (~ (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                                & (IData)(this->__PVT__spi_wr_transfer_en_whas)) 
                                               | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                    & (0U 
                                                       != (IData)(this->__PVT__spi_rg_receive_state))) 
                                                   & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                              | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                                                  & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                                 & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_done)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
            & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
           & (IData)(this->__PVT__spi_wr_transfer_en_whas));
    this->__PVT__WILL_FIRE_RL_spi_rl_chip_select_control 
        = (1U & ((((this->__PVT__spi_rg_spi_cfg_cr1 
                    >> 9U) & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive))) 
                  & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle))) 
                 & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle))));
    this->__PVT__MUX_spi_rg_nss_write_1___05FSEL_1 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
           & (((this->__PVT__spi_rg_spi_cfg_cr1 >> 6U) 
               & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata)) 
              | ((IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156) 
                 & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                       >> 0x10U)))));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
            & (this->__PVT__spi_rg_spi_cfg_cr1 >> 6U)) 
           & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata));
    this->__PVT__spi_rg_spi_cfg_cr1_D_IN = (((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                               & (~ (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                                              & (IData)(this->__PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156)) 
                                             & (~ (this->__PVT__spi_rg_spi_cfg_cr2 
                                                   >> 0x10U)))
                                             ? (0xffffffbfU 
                                                & this->__PVT__spi_rg_spi_cfg_cr1)
                                             : this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]);
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
           & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182));
    this->__PVT__spi_tx_fifo_DEQ = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                    & (IData)(this->__PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223));
    this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233));
    this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211));
    this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9 
        = ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
           & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214));
    this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish 
        = (((((((~ (IData)(this->__PVT__spi_rx_fifo__DOT__hasodata)) 
                & (0U == (IData)(this->__PVT__spi_rg_receive_state))) 
               & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
              & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit))) 
             & (~ (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start))) 
            & (~ (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx))) 
           & (~ (IData)(this->__PVT__spi_tx_data_en)));
    this->__PVT__spi_rg_data_tx_D_IN = ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                         ? (IData)(this->__PVT__spi_tx_fifo_D_OUT)
                                         : ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                             ? ((0x80U 
                                                 & this->__PVT__spi_rg_spi_cfg_cr1)
                                                 ? 
                                                (0x7fU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    >> 1U))
                                                 : 
                                                (0xfeU 
                                                 & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                    << 1U)))
                                             : ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9)
                                                 ? 
                                                ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217)
                                                  ? 
                                                 ((0x80U 
                                                   & this->__PVT__spi_rg_spi_cfg_cr1)
                                                   ? 
                                                  (0x7fU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      >> 1U))
                                                   : 
                                                  (0xfeU 
                                                   & ((IData)(this->__PVT__spi_rg_data_tx) 
                                                      << 1U)))
                                                  : (IData)(this->__PVT__spi_tx_fifo_D_OUT))
                                                 : 0U)));
    this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2 
        = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish) 
            & (0x13U > (IData)(this->__PVT__spi_rg_data_counter))) 
           & (0U != (IData)(this->__PVT__spi_rg_data_counter)));
    this->__PVT__spi_rg_spi_cfg_sr_D_IN = ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1)
                                            ? (0x80U 
                                               | (0xffffff7fU 
                                                  & this->__PVT__spi_rg_spi_cfg_sr))
                                            : (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                & (8U 
                                                   == 
                                                   (0xffU 
                                                    & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                        << 0x1dU) 
                                                       | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                          >> 3U)))))
                                                ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                                : ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)
                                                    ? 
                                                   (1U 
                                                    | (0xfffffffeU 
                                                       & this->__PVT__spi_rg_spi_cfg_sr))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4)
                                                     ? this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4
                                                     : 
                                                    (((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg) 
                                                      & (0x13U 
                                                         <= (IData)(this->__PVT__spi_rg_data_counter)))
                                                      ? 
                                                     (1U 
                                                      | (0xfffffffeU 
                                                         & this->__PVT__spi_rg_spi_cfg_sr))
                                                      : 
                                                     ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6)
                                                       ? this->__PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4
                                                       : 0U))))));
    this->__PVT__spi_rg_data_counter_EN = (((((((((
                                                   (((IData)(this->__PVT__spi_tx_data_en) 
                                                     & (0x14U 
                                                        > (IData)(this->__PVT__spi_rg_data_counter))) 
                                                    | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_idle) 
                                                        & (this->__PVT__spi_rg_spi_cfg_cr1 
                                                           >> 6U)) 
                                                       & (IData)(this->__PVT__spi_tx_fifo__DOT__hasodata))) 
                                                   | ((((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx) 
                                                        & (0U 
                                                           != (IData)(this->__PVT__spi_rg_receive_state))) 
                                                       & (IData)(this->__PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306)) 
                                                      & (IData)(this->__PVT__spi_wr_clk_en_wget))) 
                                                  | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                                     & (0x1cU 
                                                        == 
                                                        (0xffU 
                                                         & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                             << 0x1dU) 
                                                            | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                               >> 3U)))))) 
                                                 | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_transmit_start) 
                                                    & (IData)(this->__PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182))) 
                                                | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_transmit) 
                                                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214))) 
                                               | (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_start_receive) 
                                                   & (IData)(this->__PVT__spi_wr_clk_en_wget)) 
                                                  & (IData)(this->__PVT__spi_wr_transfer_en_whas))) 
                                              | ((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                 & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280))) 
                                             | (IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                           | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle));
    this->__PVT__spi_rg_bit_count_D_IN = (((((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1) 
                                             | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                            | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                           | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4))
                                           ? 0U : (0xffU 
                                                   & ((((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                        & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)) 
                                                       & ((7U 
                                                           > (IData)(this->__PVT__spi_rg_data_counter)) 
                                                          | (7U 
                                                             == (IData)(this->__PVT__spi_rg_data_counter))))
                                                       ? 
                                                      ((IData)(1U) 
                                                       + (IData)(this->__PVT__spi_rg_bit_count))
                                                       : 
                                                      ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                        ? 
                                                       ((IData)(1U) 
                                                        + (IData)(this->__PVT__spi_rg_bit_count))
                                                        : 
                                                       ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7)
                                                         ? 
                                                        ((IData)(1U) 
                                                         + (IData)(this->__PVT__spi_rg_bit_count))
                                                         : 
                                                        ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                          ? 
                                                         ((IData)(1U) 
                                                          + (IData)(this->__PVT__spi_rg_bit_count))
                                                          : 0U))))));
    this->__PVT__spi_rg_data_counter_D_IN = ((((((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1) 
                                                 | (IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1)) 
                                                | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_receive_idle)) 
                                               | (IData)(this->__PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish)) 
                                              | (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5))
                                              ? 0U : 
                                             (0xffU 
                                              & ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6)
                                                  : 
                                                 (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_data_receive) 
                                                   & (IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280))
                                                   ? 
                                                  ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273)
                                                    ? 
                                                   ((7U 
                                                     > (IData)(this->__PVT__spi_rg_data_counter))
                                                     ? 
                                                    ((IData)(1U) 
                                                     + (IData)(this->__PVT__spi_rg_data_counter))
                                                     : 0U)
                                                    : 0U)
                                                   : 
                                                  ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6)
                                                    ? 
                                                   ((IData)(1U) 
                                                    + (IData)(this->__PVT__spi_rg_data_counter))
                                                    : 
                                                   ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9)
                                                     ? 
                                                    ((IData)(this->__PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217)
                                                      ? 
                                                     ((IData)(1U) 
                                                      + (IData)(this->__PVT__spi_rg_data_counter))
                                                      : 0U)
                                                     : 
                                                    ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8)
                                                      ? 
                                                     ((IData)(1U) 
                                                      + (IData)(this->__PVT__spi_rg_data_counter))
                                                      : 
                                                     ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                       ? (IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6)
                                                       : 0U))))))));
    this->__PVT__spi_rg_spi_cfg_dr1_D_IN = (((IData)(this->__PVT__WILL_FIRE_RL_spi_rl_write_to_cfg) 
                                             & (0xcU 
                                                == 
                                                (0xffU 
                                                 & ((this->__PVT__ff_wr_req__DOT__syncFIFO1Data[2U] 
                                                     << 0x1dU) 
                                                    | (this->__PVT__ff_wr_req__DOT__syncFIFO1Data[1U] 
                                                       >> 3U)))))
                                             ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                             : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                 ? 
                                                ((0x80U 
                                                  & this->__PVT__spi_rg_spi_cfg_cr1)
                                                  ? this->__PVT__x___05Fh3086
                                                  : 
                                                 (0xffffffU 
                                                  & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                     >> 8U)))
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? 
                                                 ((0x13U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter))
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   (0xffffffU 
                                                    & (this->__PVT___theResult___05F___05Fh10026[4U] 
                                                       >> 8U))
                                                    : 
                                                   ((this->__PVT___theResult___05F___05Fh10026[4U] 
                                                     << 8U) 
                                                    | (this->__PVT___theResult___05F___05Fh10026[3U] 
                                                       >> 0x18U)))
                                                   : 
                                                  this->__PVT___theResult___05F___05Fh10026[4U])
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   (0xffffffU 
                                                    & (this->__PVT__spi_rg_spi_cfg_dr1 
                                                       >> 8U))
                                                    : this->__PVT__x___05Fh3086)
                                                   : 0U))));
    this->__PVT__spi_rg_spi_cfg_dr5_D_IN = ((IData)(this->__PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1)
                                             ? this->__PVT__ff_wr_req__DOT__syncFIFO1Data[0U]
                                             : ((IData)(this->__PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2)
                                                 ? 
                                                ((0x80U 
                                                  & this->__PVT__spi_rg_spi_cfg_cr1)
                                                  ? 
                                                 (0xffffff00U 
                                                  & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                     << 8U))
                                                  : this->__PVT__x___05Fh2984)
                                                 : 
                                                ((IData)(this->__PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg)
                                                  ? 
                                                 ((0x13U 
                                                   > (IData)(this->__PVT__spi_rg_data_counter))
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? 
                                                   ((this->__PVT___theResult___05F___05Fh10026[1U] 
                                                     << 0x18U) 
                                                    | (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                       >> 8U))
                                                    : 
                                                   (0xffffff00U 
                                                    & (this->__PVT___theResult___05F___05Fh10026[0U] 
                                                       << 8U)))
                                                   : 
                                                  this->__PVT___theResult___05F___05Fh10026[0U])
                                                  : 
                                                 ((IData)(this->__PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11)
                                                   ? 
                                                  ((0x80U 
                                                    & this->__PVT__spi_rg_spi_cfg_cr1)
                                                    ? this->__PVT__x___05Fh2984
                                                    : 
                                                   (0xffffff00U 
                                                    & (this->__PVT__spi_rg_spi_cfg_dr5 
                                                       << 8U)))
                                                   : 0U))));
}

void VmkSoc_mkspi::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+          VmkSoc_mkspi::_ctor_var_reset\n"); );
    // Body
    CLK = VL_RAND_RESET_I(1);
    RST_N = VL_RAND_RESET_I(1);
    io_mosi = VL_RAND_RESET_I(1);
    io_sclk = VL_RAND_RESET_I(1);
    io_nss = VL_RAND_RESET_I(1);
    io_miso_dat = VL_RAND_RESET_I(1);
    slave_m_awvalid_awvalid = VL_RAND_RESET_I(1);
    slave_m_awvalid_awaddr = VL_RAND_RESET_I(32);
    slave_m_awvalid_awsize = VL_RAND_RESET_I(2);
    slave_m_awvalid_awprot = VL_RAND_RESET_I(3);
    slave_awready = VL_RAND_RESET_I(1);
    slave_m_wvalid_wvalid = VL_RAND_RESET_I(1);
    slave_m_wvalid_wdata = VL_RAND_RESET_I(32);
    slave_m_wvalid_wstrb = VL_RAND_RESET_I(4);
    slave_wready = VL_RAND_RESET_I(1);
    slave_bvalid = VL_RAND_RESET_I(1);
    slave_bresp = VL_RAND_RESET_I(2);
    slave_m_bready_bready = VL_RAND_RESET_I(1);
    slave_m_arvalid_arvalid = VL_RAND_RESET_I(1);
    slave_m_arvalid_araddr = VL_RAND_RESET_I(32);
    slave_m_arvalid_arsize = VL_RAND_RESET_I(2);
    slave_m_arvalid_arprot = VL_RAND_RESET_I(3);
    slave_arready = VL_RAND_RESET_I(1);
    slave_rvalid = VL_RAND_RESET_I(1);
    slave_rresp = VL_RAND_RESET_I(2);
    slave_rdata = VL_RAND_RESET_I(32);
    slave_m_rready_rready = VL_RAND_RESET_I(1);
    __PVT__spi_wr_rd_data_wget = VL_RAND_RESET_I(32);
    __PVT___write_RL_spi_rl_data_transmit_EN_spi_rg_nss_wget = VL_RAND_RESET_I(1);
    __PVT__spi_wr_clk_en_wget = VL_RAND_RESET_I(1);
    __PVT__spi_wr_transfer_en_whas = VL_RAND_RESET_I(1);
    __PVT__spi_rg_bit_count = VL_RAND_RESET_I(8);
    __PVT__spi_rg_bit_count_D_IN = VL_RAND_RESET_I(8);
    __PVT__spi_rg_clk = VL_RAND_RESET_I(1);
    __PVT__spi_rg_clk_D_IN = VL_RAND_RESET_I(1);
    __PVT__spi_rg_clk_counter = VL_RAND_RESET_I(3);
    __PVT__spi_rg_clk_counter_D_IN = VL_RAND_RESET_I(3);
    __PVT__spi_rg_data_counter = VL_RAND_RESET_I(8);
    __PVT__spi_rg_data_counter_D_IN = VL_RAND_RESET_I(8);
    __PVT__spi_rg_data_counter_EN = VL_RAND_RESET_I(1);
    __PVT__spi_rg_data_rx = VL_RAND_RESET_I(8);
    __PVT__spi_rg_data_tx = VL_RAND_RESET_I(8);
    __PVT__spi_rg_data_tx_D_IN = VL_RAND_RESET_I(8);
    __PVT__spi_rg_nss = VL_RAND_RESET_I(1);
    __PVT__spi_rg_receive_state = VL_RAND_RESET_I(2);
    __PVT__spi_rg_receive_state_EN = VL_RAND_RESET_I(1);
    __PVT__spi_rg_spi_cfg_cr1 = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_cr1_D_IN = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_cr2 = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_crcpr = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_dr1 = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_dr1_D_IN = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_dr2 = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_dr3 = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_dr4 = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_dr5 = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_dr5_D_IN = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_rxcrcr = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_sr = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_sr_D_IN = VL_RAND_RESET_I(32);
    __PVT__spi_rg_spi_cfg_txcrcr = VL_RAND_RESET_I(32);
    __PVT__spi_rg_transfer_done = VL_RAND_RESET_I(1);
    __PVT__spi_rg_transmit_state = VL_RAND_RESET_I(2);
    __PVT__spi_rg_tx_rx_start = VL_RAND_RESET_I(1);
    __PVT__spi_tx_data_en = VL_RAND_RESET_I(1);
    __PVT__spi_tx_data_en_EN = VL_RAND_RESET_I(1);
    __PVT__spi_wr_spi_out_io1 = VL_RAND_RESET_I(1);
    __PVT__ff_rd_req_dEMPTY_N = VL_RAND_RESET_I(1);
    __PVT__ff_wr_req_dEMPTY_N = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_rd_addr_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_rd_data_DEQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_addr_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_data_ENQ = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_resp_DEQ = VL_RAND_RESET_I(1);
    __PVT__spi_rx_fifo_D_OUT = VL_RAND_RESET_I(8);
    __PVT__spi_rx_fifo_ENQ = VL_RAND_RESET_I(1);
    __PVT__spi_tx_fifo_D_OUT = VL_RAND_RESET_I(8);
    __PVT__spi_tx_fifo_CLR = VL_RAND_RESET_I(1);
    __PVT__spi_tx_fifo_DEQ = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_rl_read_request_from_core = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_rl_read_response_from_controller = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_rl_read_response_to_core = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_rl_write_request_from_core = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_spi_rl_abort_tx_rx = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_spi_rl_receive_fifo_to_read_datareg = VL_RAND_RESET_I(1);
    __PVT__CAN_FIRE_RL_spi_rl_receive_idle_stop = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_abort_bitcount_finish = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_chip_select_control = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_data_receive = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_data_transmit = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_receive_done = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_receive_idle = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_receive_start_receive = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_transmit_idle = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_transmit_start = VL_RAND_RESET_I(1);
    __PVT__WILL_FIRE_RL_spi_rl_write_to_cfg = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FVAL_4 = VL_RAND_RESET_I(32);
    __PVT__MUX_spi_rg_data_counter_write_1___05FVAL_6 = VL_RAND_RESET_I(8);
    __PVT__MUX_spi_rg_data_rx_write_1___05FVAL_1 = VL_RAND_RESET_I(8);
    __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_4 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_6 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_7 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_bit_count_write_1___05FSEL_8 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_11 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_5 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_data_counter_write_1___05FSEL_9 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_nss_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_nss_write_1___05FSEL_3 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_spi_cfg_dr1_write_1___05FSEL_2 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_rg_spi_cfg_sr_write_1___05FSEL_6 = VL_RAND_RESET_I(1);
    __PVT__MUX_spi_tx_data_en_write_1___05FSEL_1 = VL_RAND_RESET_I(1);
    __PVT__v___05Fh11853 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh3428 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh5769 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh6289 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh6816 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh7130 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh7329 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh7628 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh8248 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh8744 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh8863 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh9087 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh9152 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh10919 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh9738 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh9992 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh7823 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh4119 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh5228 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh5330 = VL_RAND_RESET_I(32);
    VL_RAND_RESET_W(160, __PVT___theResult___05F___05Fh10026);
    __PVT__x___05Fh2909 = VL_RAND_RESET_I(32);
    __PVT__x___05Fh2934 = VL_RAND_RESET_I(32);
    __PVT__x___05Fh2959 = VL_RAND_RESET_I(32);
    __PVT__x___05Fh2984 = VL_RAND_RESET_I(32);
    __PVT__x___05Fh3086 = VL_RAND_RESET_I(32);
    __PVT__x___05Fh3121 = VL_RAND_RESET_I(32);
    __PVT__x___05Fh3146 = VL_RAND_RESET_I(32);
    __PVT__x___05Fh3171 = VL_RAND_RESET_I(32);
    __PVT__v___05Fh2759 = VL_RAND_RESET_I(8);
    __PVT__v___05Fh8399 = VL_RAND_RESET_I(8);
    __PVT__NOT_spi_rg_data_counter_0_ULT_6_98_21_OR_spi_w_ETC___05F_d223 = VL_RAND_RESET_I(1);
    __PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d315 = VL_RAND_RESET_I(1);
    __PVT__NOT_spi_rg_spi_cfg_cr2_00_BIT_15_43_44_AND_NOT_ETC___05F_d319 = VL_RAND_RESET_I(1);
    __PVT__NOT_spi_tx_fifo_i_notEmpty___05F48_62_AND_NOT_spi___05FETC___05F_d233 = VL_RAND_RESET_I(1);
    __PVT__NOT_spi_wr_clk_en_wget___05F50_51_AND_spi_wr_trans_ETC___05F_d156 = VL_RAND_RESET_I(1);
    __PVT_____05Fduses58 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d299 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_bit_count_92_EQ_spi_rg_spi_cfg_cr1_BITS_ETC___05F_d306 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_data_counter_0_EQ_7_04_AND_NOT_spi_wr_c_ETC___05F_d209 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d211 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_data_counter_0_ULT_6_98_AND_NOT_spi_wr___05FETC___05F_d214 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_data_counter_0_ULT_6_98_OR_spi_rg_data___05FETC___05F_d217 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d273 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_data_counter_0_ULT_8_69_AND_spi_rx_fifo_ETC___05F_d280 = VL_RAND_RESET_I(1);
    __PVT__spi_rg_spi_cfg_cr1_BITS_5_TO_3_28_EQ_spi_rg_cl_ETC___05F_d130 = VL_RAND_RESET_I(1);
    __PVT__spi_wr_clk_en_wget___05F50_OR_spi_rg_spi_cfg_cr1_B_ETC___05F_d182 = VL_RAND_RESET_I(1);
    __PVT__v___05Fh5419 = VL_RAND_RESET_I(1);
    __PVT__ff_rd_req__DOT__syncFIFO1Data = VL_RAND_RESET_Q(35);
    __PVT__ff_rd_req__DOT__sEnqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_rd_req__DOT__sDeqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_rd_req__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__ff_rd_req__DOT__dEnqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_rd_req__DOT__dDeqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_rd_req__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__ff_sync_rd_resp__DOT__syncFIFO1Data = VL_RAND_RESET_I(32);
    __PVT__ff_sync_rd_resp__DOT__sEnqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_sync_rd_resp__DOT__sDeqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_sync_rd_resp__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__ff_sync_rd_resp__DOT__dEnqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_sync_rd_resp__DOT__dDeqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_sync_rd_resp__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(67, __PVT__ff_wr_req__DOT__syncFIFO1Data);
    __PVT__ff_wr_req__DOT__sEnqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_wr_req__DOT__sDeqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_wr_req__DOT__sSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__ff_wr_req__DOT__dEnqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_wr_req__DOT__dDeqToggle = VL_RAND_RESET_I(1);
    __PVT__ff_wr_req__DOT__dSyncReg1 = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_rd_addr__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_rd_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_rd_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_spi_f_rd_addr__DOT__data1_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_spi_f_rd_data__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_rd_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_rd_data__DOT__data0_reg = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_spi_f_rd_data__DOT__data1_reg = VL_RAND_RESET_Q(34);
    __PVT__s_xactor_spi_f_wr_addr__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_spi_f_wr_addr__DOT__data1_reg = VL_RAND_RESET_Q(37);
    __PVT__s_xactor_spi_f_wr_data__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_data__DOT__data0_reg = VL_RAND_RESET_Q(36);
    __PVT__s_xactor_spi_f_wr_data__DOT__data1_reg = VL_RAND_RESET_Q(36);
    __PVT__s_xactor_spi_f_wr_resp__DOT__full_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_resp__DOT__empty_reg = VL_RAND_RESET_I(1);
    __PVT__s_xactor_spi_f_wr_resp__DOT__data0_reg = VL_RAND_RESET_I(2);
    __PVT__s_xactor_spi_f_wr_resp__DOT__data1_reg = VL_RAND_RESET_I(2);
    __PVT__spi_rx_fifo__DOT__not_ring_full = VL_RAND_RESET_I(1);
    __PVT__spi_rx_fifo__DOT__ring_empty = VL_RAND_RESET_I(1);
    __PVT__spi_rx_fifo__DOT__head = VL_RAND_RESET_I(5);
    __PVT__spi_rx_fifo__DOT__next_head = VL_RAND_RESET_I(5);
    __PVT__spi_rx_fifo__DOT__tail = VL_RAND_RESET_I(5);
    __PVT__spi_rx_fifo__DOT__next_tail = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<19; ++__Vi0) {
        __PVT__spi_rx_fifo__DOT__arr[__Vi0] = VL_RAND_RESET_I(8);
    }
    __PVT__spi_rx_fifo__DOT__hasodata = VL_RAND_RESET_I(1);
    spi_rx_fifo__DOT____Vlvbound2 = VL_RAND_RESET_I(8);
    __PVT__spi_tx_fifo__DOT__not_ring_full = VL_RAND_RESET_I(1);
    __PVT__spi_tx_fifo__DOT__ring_empty = VL_RAND_RESET_I(1);
    __PVT__spi_tx_fifo__DOT__head = VL_RAND_RESET_I(5);
    __PVT__spi_tx_fifo__DOT__next_head = VL_RAND_RESET_I(5);
    __PVT__spi_tx_fifo__DOT__tail = VL_RAND_RESET_I(5);
    __PVT__spi_tx_fifo__DOT__next_tail = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<19; ++__Vi0) {
        __PVT__spi_tx_fifo__DOT__arr[__Vi0] = VL_RAND_RESET_I(8);
    }
    __PVT__spi_tx_fifo__DOT__hasodata = VL_RAND_RESET_I(1);
    spi_tx_fifo__DOT____Vlvbound2 = VL_RAND_RESET_I(8);
    __Vdly__s_xactor_spi_f_rd_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_spi_f_rd_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __Vdly__s_xactor_spi_f_rd_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_spi_f_wr_addr__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_spi_f_wr_addr__DOT__data0_reg = VL_RAND_RESET_Q(37);
    __Vdly__s_xactor_spi_f_wr_data__DOT__empty_reg = VL_RAND_RESET_I(1);
    __Vdly__s_xactor_spi_f_wr_data__DOT__data0_reg = VL_RAND_RESET_Q(36);
    __Vdly__s_xactor_spi_f_wr_resp__DOT__full_reg = VL_RAND_RESET_I(1);
    __Vdly__spi_rx_fifo__DOT__head = VL_RAND_RESET_I(5);
    __Vdly__spi_rx_fifo__DOT__tail = VL_RAND_RESET_I(5);
    __Vdly__spi_rx_fifo__DOT__ring_empty = VL_RAND_RESET_I(1);
    __Vdly__spi_rx_fifo__DOT__not_ring_full = VL_RAND_RESET_I(1);
    __Vdly__spi_rx_fifo__DOT__hasodata = VL_RAND_RESET_I(1);
    __Vdlyvdim0__spi_rx_fifo__DOT__arr__v0 = 0;
    __Vdlyvval__spi_rx_fifo__DOT__arr__v0 = VL_RAND_RESET_I(8);
    __Vdlyvset__spi_rx_fifo__DOT__arr__v0 = 0;
    __Vdly__spi_tx_fifo__DOT__head = VL_RAND_RESET_I(5);
    __Vdly__spi_tx_fifo__DOT__tail = VL_RAND_RESET_I(5);
    __Vdly__spi_tx_fifo__DOT__ring_empty = VL_RAND_RESET_I(1);
    __Vdly__spi_tx_fifo__DOT__not_ring_full = VL_RAND_RESET_I(1);
    __Vdly__spi_tx_fifo__DOT__hasodata = VL_RAND_RESET_I(1);
    __Vdlyvdim0__spi_tx_fifo__DOT__arr__v0 = 0;
    __Vdlyvval__spi_tx_fifo__DOT__arr__v0 = VL_RAND_RESET_I(8);
    __Vdlyvset__spi_tx_fifo__DOT__arr__v0 = 0;
    __Vdly__ff_rd_req__DOT__sEnqToggle = VL_RAND_RESET_I(1);
    __Vdly__ff_sync_rd_resp__DOT__sEnqToggle = VL_RAND_RESET_I(1);
    __Vdly__ff_rd_req__DOT__dDeqToggle = VL_RAND_RESET_I(1);
    __Vdly__ff_sync_rd_resp__DOT__dDeqToggle = VL_RAND_RESET_I(1);
    __Vdly__ff_wr_req__DOT__dDeqToggle = VL_RAND_RESET_I(1);
}
