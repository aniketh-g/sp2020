// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VmkSoc.h for the primary calling header

#include "VmkSoc.h"
#include "VmkSoc__Syms.h"

VL_INLINE_OPT void VmkSoc::_sequent__TOP__41(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__41\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__rtc_rtc_mc_CLK_IN = (1U & (
                                                   (0U 
                                                    == vlTOPp->mkSoc__DOT__rtc_rtc_cntr_div)
                                                    ? 
                                                   (~ (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_osc))
                                                    : (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_osc)));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__42(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__42\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*95:0*/ __Vtemp2720[3];
    WData/*95:0*/ __Vtemp2723[3];
    // Body
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg 
        = vlTOPp->__Vdly__mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg;
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__empty_reg 
        = vlTOPp->__Vdly__mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__empty_reg;
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__empty_reg 
        = vlTOPp->__Vdly__mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__empty_reg;
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp_D_IN 
        = (((0x41510U == (IData)((vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg 
                                  >> 5U))) & (((0U 
                                                == 
                                                (3U 
                                                 & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg))) 
                                               | (1U 
                                                  == 
                                                  (3U 
                                                   & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg)))) 
                                              | (2U 
                                                 == 
                                                 (3U 
                                                  & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg)))))
            ? 0U : 2U);
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp_ENQ) 
             & (~ (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp_DEQ)))) {
            vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__full_reg 
                = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__empty_reg)));
        } else {
            if (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp_DEQ) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp_ENQ)))) {
                vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
            }
        }
    } else {
        vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__full_reg = 1U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_connect_debug_response_to_syncfifo) 
             | (IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_read_synced_request_to_dm))) {
            vlTOPp->mkSoc__DOT__debug_module__DOT__dmi_response 
                = ((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_connect_debug_response_to_syncfifo)
                    ? 0ULL : (0x400000000ULL | ((1U 
                                                 == 
                                                 (3U 
                                                  & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data)))
                                                 ? 
                                                (((QData)((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh18219)) 
                                                  << 2U) 
                                                 | (QData)((IData)(
                                                                   ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_40_TO_34_74_EQ_16_75_O_ETC___05F_d669)
                                                                     ? 0U
                                                                     : 2U))))
                                                 : (QData)((IData)(
                                                                   ((2U 
                                                                     == 
                                                                     (3U 
                                                                      & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data)))
                                                                     ? (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__v___05Fh12456)
                                                                     : 0U))))));
        }
    } else {
        vlTOPp->mkSoc__DOT__debug_module__DOT__dmi_response = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__debug_module__DOT__ackHaveReset_1_whas) {
            vlTOPp->mkSoc__DOT__debug_module__DOT__dmActive 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                 >> 2U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__debug_module__DOT__dmActive = 0U;
    }
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__empty_reg 
        = vlTOPp->__Vdly__mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__empty_reg;
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr_DEQ 
        = (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__empty_reg) 
            & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__empty_reg)) 
           & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__full_reg));
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data_DEQ 
        = (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__empty_reg) 
            & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__empty_reg)) 
           & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__full_reg));
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp_ENQ 
        = (((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__empty_reg) 
            & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__empty_reg)) 
           & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__full_reg));
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_resp_ENQ 
        = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__empty_reg) 
           & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_resp__DOT__full_reg));
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp_DEQ 
        = ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__fabric_xactors_to_slaves_5_f_wr_resp__DOT__full_reg) 
           & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_resp__DOT__empty_reg));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status 
        = (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__dmActive) 
            << 4U) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_core_halted) 
                       << 3U) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_step) 
                                  << 2U) | (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_dcsr_stepie) 
                                             << 1U) 
                                            | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp3__DOT__rg_csr_denable)))));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50 
        = ((((0x60000U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[5U] 
                           >> 8U) & (- (IData)((1U 
                                                & (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status) 
                                                    >> 4U) 
                                                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status))))))) 
             | (((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__x___05Fh329 
                  & (- (IData)((1U & ((3U != (3U & 
                                              (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[5U] 
                                               >> 0x1bU))) 
                                      | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[1U] 
                                         >> 0x1bU)))))) 
                 & (0x7f000U | (0xfffU & (~ ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[2U] 
                                              << 0x14U) 
                                             | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[1U] 
                                                >> 0xcU)))))) 
                & (- (IData)((1U & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status) 
                                       >> 3U))))))) 
            | (((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__x___05Fh327 
                 & (- (IData)(((0U == (3U & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[5U] 
                                             >> 0x1bU))) 
                               | ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[1U] 
                                   >> 0x19U) & (1U 
                                                == 
                                                (3U 
                                                 & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[5U] 
                                                    >> 0x1bU)))))))) 
                & (0x7f000U | (0xfffU & (~ vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[1U])))) 
               & (- (IData)((1U & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status) 
                                      >> 3U))))))) 
           | (((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__x___05Fh327 
                & (- (IData)(((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[1U] 
                               >> 0x18U) & (0U == (3U 
                                                   & (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[5U] 
                                                      >> 0x1bU))))))) 
               & (0x7f000U | (0xfffU & (~ vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1_mv_csrs_to_decode[1U])))) 
              & (- (IData)((1U & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status) 
                                     >> 3U)))))));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__chk_interrupt___05F_d42 
        = (0x40U | (((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_step_done) 
                       & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status) 
                             >> 3U))) ? 0x14U : ((0x20000U 
                                                  & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                  ? 0x13U
                                                  : 
                                                 ((0x40000U 
                                                   & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                   ? 0x16U
                                                   : 
                                                  ((0x800U 
                                                    & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                    ? 0xbU
                                                    : 
                                                   ((8U 
                                                     & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                     ? 3U
                                                     : 
                                                    ((0x80U 
                                                      & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                      ? 7U
                                                      : 
                                                     ((0x200U 
                                                       & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                       ? 9U
                                                       : 
                                                      ((2U 
                                                        & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                        ? 1U
                                                        : 
                                                       ((0x20U 
                                                         & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                         ? 5U
                                                         : 
                                                        ((0x100U 
                                                          & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                          ? 8U
                                                          : 
                                                         ((1U 
                                                           & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                           ? 0U
                                                           : 
                                                          ((0x10U 
                                                            & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50)
                                                            ? 4U
                                                            : 0x1fU)))))))))))) 
                     << 1U) | ((0U != vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__instance_chk_interrupt_1__DOT__pending_interrupts___05Fh50) 
                               | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_step_done) 
                                  & (~ ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2_debug_status_status) 
                                        >> 3U))))));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049 
        = (0x7fU & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_rerun)
                     ? ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_fencei_rerun)
                         ? 0x18U : ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_sfence_rerun)
                                     ? 0x19U : 0x17U))
                     : ((1U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__chk_interrupt___05F_d42))
                         ? (0x3fU & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__chk_interrupt___05F_d42) 
                                     >> 1U)) : ((0x100000U 
                                                 & vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[0U])
                                                 ? 
                                                (0x3fU 
                                                 & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                     << 0x12U) 
                                                    | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[0U] 
                                                       >> 0xeU)))
                                                 : 
                                                ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                                  << 0x1eU) 
                                                 | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[0U] 
                                                    >> 2U))))));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54 
        = ((1U & (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_rerun) 
                   | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__chk_interrupt___05F_d42)) 
                  | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[0U] 
                     >> 0x14U))) ? 6U : (0xfU & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[2U] 
                                                  << 0x14U) 
                                                 | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                                    >> 0xcU))));
    if ((6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54))) {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144 = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs2addr___05Fh3145 = 0U;
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs1addr___05Fh3144 = 0U;
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144 
            = (3U & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[2U] 
                      << 0x10U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                   >> 0x10U)));
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs2addr___05Fh3145 
            = (0x1fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[2U] 
                         << 8U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                   >> 0x18U)));
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs1addr___05Fh3144 
            = (0x1fU & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[2U] 
                         << 3U) | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                   >> 0x1dU)));
    }
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d67 
        = (((7U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54)) 
            & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3_flush_from_exe_fst))) 
           & (~ (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_flush_fst)));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__MUX_rg_rerun_write_1___05FSEL_2 
        = (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__CAN_FIRE_RL_decode_and_opfetch) 
            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__eEpoch_read___05F4_CONCAT_wEpoch_read___05F5_6_EQ_IF_r_ETC___05F_d31)) 
           & (7U != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54)));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_wfi_EN 
        = ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__CAN_FIRE_RL_decode_and_opfetch) 
             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__eEpoch_read___05F4_CONCAT_wEpoch_read___05F5_6_EQ_IF_r_ETC___05F_d31)) 
            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d67)) 
           | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_wfi) 
              & (((0ULL != (((((QData)((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__r___05Fh4064)) 
                               << 0x12U) | ((QData)((IData)(
                                                            (((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__r___05Fh4088) 
                                                              << 9U) 
                                                             | ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_meip) 
                                                                  & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_meie)) 
                                                                 << 3U) 
                                                                | ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_seip___05Fread___05Fh4108) 
                                                                     & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_seie)) 
                                                                    << 1U) 
                                                                   | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ueip___05Fread___05Fh4112) 
                                                                      & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ueie))))))) 
                                            << 8U)) 
                             | ((QData)((IData)(((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mtip) 
                                                   & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_mtie)) 
                                                  << 3U) 
                                                 | ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_stip) 
                                                      & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_stie)) 
                                                     << 1U) 
                                                    | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_utip) 
                                                       & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_utie)))))) 
                                << 4U)) | (QData)((IData)(
                                                          ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_msip) 
                                                             & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_msie)) 
                                                            << 3U) 
                                                           | ((((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ssip) 
                                                                & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_ssie)) 
                                                               << 1U) 
                                                              | ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_usip) 
                                                                 & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5__DOT__csr__DOT__csrfile__DOT__mk_grp1__DOT__rg_usie)))))))) 
                  | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage5_flush_fst)) 
                 | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage3_flush_from_exe_fst))));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_connect_ena_data_2 
        = ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_meta_FULL_N) 
           & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__MUX_rg_rerun_write_1___05FSEL_2));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__CAN_FIRE_RL_connect_ena_data_3 
        = ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe2_mtval_FULL_N) 
           & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__MUX_rg_rerun_write_1___05FSEL_2));
    vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op3_port1___05Fread 
        = ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__MUX_rg_rerun_write_1___05FSEL_2)
            ? vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__op4___05Fh2821
            : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op3);
    __Vtemp2720[1U] = ((3U & ((IData)(((2U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                        ? ((1U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                            ? 2ULL : 4ULL)
                                        : ((1U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                            ? vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__op4___05Fh2821
                                            : (((0U 
                                                 == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs2addr___05Fh3145)) 
                                                & (0U 
                                                   == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144)))
                                                ? 0ULL
                                                : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                               [(0x1fU 
                                                 & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                     << 0x15U) 
                                                    | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                       >> 0xbU)))])))) 
                              >> 0x1eU)) | (0xfffffffcU 
                                            & ((IData)(
                                                       (((2U 
                                                          & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                                          ? 
                                                         ((1U 
                                                           & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                                           ? 2ULL
                                                           : 4ULL)
                                                          : 
                                                         ((1U 
                                                           & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                                           ? vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__op4___05Fh2821
                                                           : 
                                                          (((0U 
                                                             == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs2addr___05Fh3145)) 
                                                            & (0U 
                                                               == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144)))
                                                            ? 0ULL
                                                            : 
                                                           vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                                           [
                                                           (0x1fU 
                                                            & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                                << 0x15U) 
                                                               | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                                  >> 0xbU)))]))) 
                                                        >> 0x20U)) 
                                               << 2U)));
    if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__MUX_rg_rerun_write_1___05FSEL_2) {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[0U] 
            = ((0xfffffffcU & ((IData)(((2U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                         ? ((1U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                             ? 2ULL
                                             : 4ULL)
                                         : ((1U & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                             ? vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__op4___05Fh2821
                                             : (((0U 
                                                  == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs2addr___05Fh3145)) 
                                                 & (0U 
                                                    == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144)))
                                                 ? 0ULL
                                                 : 
                                                vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                                [(0x1fU 
                                                  & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                      << 0x15U) 
                                                     | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                        >> 0xbU)))])))) 
                               << 2U)) | (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144));
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[1U] 
            = __Vtemp2720[1U];
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
            = ((0xfffffffcU & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs2addr___05Fh3145) 
                               << 2U)) | (3U & ((IData)(
                                                        (((2U 
                                                           & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                                           ? 
                                                          ((1U 
                                                            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                                            ? 2ULL
                                                            : 4ULL)
                                                           : 
                                                          ((1U 
                                                            & (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144))
                                                            ? vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__op4___05Fh2821
                                                            : 
                                                           (((0U 
                                                              == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs2addr___05Fh3145)) 
                                                             & (0U 
                                                                == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_ETC___05F_d144)))
                                                             ? 0ULL
                                                             : 
                                                            vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                                            [
                                                            (0x1fU 
                                                             & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                                 << 0x15U) 
                                                                | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                                   >> 0xbU)))]))) 
                                                         >> 0x20U)) 
                                                >> 0x1eU)));
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[0U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[0U];
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[1U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[1U];
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2_port1___05Fread[2U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op2[2U];
    }
    __Vtemp2723[1U] = ((1U & ((IData)(((((6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54))
                                          ? ((1U != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)) 
                                             & (0xcU 
                                                != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)))
                                          : (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                                >> 0x12U))) 
                                        & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs1addr___05Fh3144)))
                                        ? 0ULL : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                       [(0x1fU & ((
                                                   vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                   << 0x1aU) 
                                                  | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                     >> 6U)))])) 
                              >> 0x1fU)) | (0xfffffffeU 
                                            & ((IData)(
                                                       (((((6U 
                                                            == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54))
                                                            ? 
                                                           ((1U 
                                                             != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)) 
                                                            & (0xcU 
                                                               != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)))
                                                            : 
                                                           (~ 
                                                            (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                                             >> 0x12U))) 
                                                          & (0U 
                                                             == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs1addr___05Fh3144)))
                                                          ? 0ULL
                                                          : 
                                                         vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                                         [
                                                         (0x1fU 
                                                          & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                              << 0x1aU) 
                                                             | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                                >> 6U)))]) 
                                                        >> 0x20U)) 
                                               << 1U)));
    if (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__MUX_rg_rerun_write_1___05FSEL_2) {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1_port1___05Fread[0U] 
            = ((0xfffffffeU & ((IData)(((((6U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54))
                                           ? ((1U != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)) 
                                              & (0xcU 
                                                 != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)))
                                           : (~ (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                                 >> 0x12U))) 
                                         & (0U == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs1addr___05Fh3144)))
                                         ? 0ULL : vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                        [(0x1fU & (
                                                   (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                    << 0x1aU) 
                                                   | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                      >> 6U)))])) 
                               << 1U)) | (1U & ((6U 
                                                 == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54))
                                                 ? 
                                                ((1U 
                                                  == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)) 
                                                 | (0xcU 
                                                    == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)))
                                                 : 
                                                (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                                 >> 0x12U))));
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1_port1___05Fread[1U] 
            = __Vtemp2723[1U];
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1_port1___05Fread[2U] 
            = ((0xfffffffeU & ((IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs1addr___05Fh3144) 
                               << 1U)) | (1U & ((IData)(
                                                        (((((6U 
                                                             == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__IF_rg_rerun_2_OR_chk_interrupt_2_BIT_0_3_OR_IF_ETC___05F_d54))
                                                             ? 
                                                            ((1U 
                                                              != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)) 
                                                             & (0xcU 
                                                                != (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__func_cause___05Fh2049)))
                                                             : 
                                                            (~ 
                                                             (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__decoder_func_32___05F_d52[1U] 
                                                              >> 0x12U))) 
                                                           & (0U 
                                                              == (IData)(vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__x1_avValue_op_addr_rs1addr___05Fh3144)))
                                                           ? 0ULL
                                                           : 
                                                          vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__registerfile__DOT__integer_rf__DOT__arr
                                                          [
                                                          (0x1fU 
                                                           & ((vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[2U] 
                                                               << 0x1aU) 
                                                              | (vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__pipe1__DOT__data0_reg[1U] 
                                                                 >> 6U)))]) 
                                                         >> 0x20U)) 
                                                >> 0x1fU)));
    } else {
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1_port1___05Fread[0U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1[0U];
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1_port1___05Fread[1U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1[1U];
        vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1_port1___05Fread[2U] 
            = vlTOPp->mkSoc__DOT__ccore__DOT__riscv__DOT__stage2__DOT__rg_op1[2U];
    }
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__43(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__43\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__dSyncReg2));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__dSyncReg1));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__44(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__44\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__rtc_rtc_en_lo__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_en_lo__DOT__sync__DOT__sToggleReg;
    vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_btos_lo__DOT__sync__DOT__sToggleReg;
    vlTOPp->mkSoc__DOT__sync_response_from_dm__DOT__sEnqToggle 
        = vlTOPp->__Vdly__mkSoc__DOT__sync_response_from_dm__DOT__sEnqToggle;
    vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config_D_IN 
        = ((0U == (3U & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg)))
            ? ((0xffffff00U & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config) 
               | (0xffU & (IData)((vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__data0_reg 
                                   >> 4U)))) : ((1U 
                                                 == 
                                                 (3U 
                                                  & (IData)(vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_addr__DOT__data0_reg)))
                                                 ? 
                                                ((0xffff0000U 
                                                  & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config) 
                                                 | (0xffffU 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 4U))))
                                                 : (IData)(
                                                           (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 4U))));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__sToggleReg;
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_sync_ctrl_reg_to_lo_clk) {
            vlTOPp->mkSoc__DOT__rtc_rtc_en_lo__DOT__sDataSyncIn 
                = (1U & vlTOPp->mkSoc__DOT__rtc_rtc_ctrl_reg);
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_en_lo__DOT__sDataSyncIn = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_sync_ctrl_reg_to_lo_clk) {
            vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo__DOT__sDataSyncIn 
                = (1U & (vlTOPp->mkSoc__DOT__rtc_rtc_ctrl_reg 
                         >> 4U));
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo__DOT__sDataSyncIn = 0U;
    }
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__dSyncReg2));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__dSyncReg2));
    vlTOPp->mkSoc__DOT__CAN_FIRE_RL_connect_debug_response_to_syncfifo 
        = (((IData)(vlTOPp->mkSoc__DOT__sync_response_from_dm__DOT__sEnqToggle) 
            == (IData)(vlTOPp->mkSoc__DOT__sync_response_from_dm__DOT__sDeqToggle)) 
           & (IData)((vlTOPp->mkSoc__DOT__debug_module__DOT__dmi_response 
                      >> 0x22U)));
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__dSyncReg2) 
             != (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__dLastState))) {
            vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write_dD_OUT 
                = vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sDataSyncIn;
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write_dD_OUT = 0U;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__dSyncReg2) 
             != (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__dLastState))) {
            vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write_dD_OUT 
                = vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sDataSyncIn;
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write_dD_OUT = 0U;
    }
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg_sRDY 
        = ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__sSyncReg2) 
           == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_ctrl_reg__DOT__sync__DOT__sToggleReg));
    vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_sync_ctrl_reg_to_lo_clk 
        = (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo__DOT__sync__DOT__sSyncReg2) 
            == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo__DOT__sync__DOT__sToggleReg)) 
           & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_en_lo__DOT__sync__DOT__sSyncReg2) 
              == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_en_lo__DOT__sync__DOT__sToggleReg)));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__dSyncReg1));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__dSyncReg1));
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__dSyncReg2));
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__dSyncReg2));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__sToggleReg));
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__sToggleReg));
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__dSyncReg1));
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__dSyncReg1));
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__sToggleReg));
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__45(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__45\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if ((((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_write_time_date_data) 
              & (IData)((vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_dD_OUT 
                         >> 0x20U))) | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_gate))) {
            vlTOPp->mkSoc__DOT__rtc_rtc_time_reg = vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_D_IN;
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_time_reg = 0xa0U;
    }
    if (vlTOPp->RST_N) {
        if ((((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_write_time_date_data) 
              & (IData)((vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_dD_OUT 
                         >> 0x20U))) | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_gate))) {
            vlTOPp->mkSoc__DOT__rtc_rtc_date_reg = vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_D_IN;
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_date_reg = 0x1040f220U;
    }
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1733 
        = (((1U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (1U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1795 
        = (((3U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (1U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1800 
        = (((4U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (0U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1804 
        = (((5U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (1U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1808 
        = (((6U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (0U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1812 
        = (((7U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (1U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1816 
        = (((8U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (1U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1820 
        = (((9U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x11U)) | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (0U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1824 
        = (((0x10U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                 >> 0x11U)) | (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (1U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1828 
        = (((0x11U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                 >> 0x11U)) | (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (0U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1832 
        = (((0x12U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                 >> 0x11U)) | (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
            & (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                            >> 0x1aU)))) & (1U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))));
    vlTOPp->mkSoc__DOT__x___05Fh82753 = ((0xf0U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                   >> 9U)) 
                                         | (0xfU & 
                                            (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x11U)));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_16_TO_13_722_CONCAT___05FETC___05F_d1788 
        = (((0U == (IData)(vlTOPp->mkSoc__DOT__x___05Fh82753)) 
            & ((((vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                  >> 5U) & (~ (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                               >> 0xcU))) & (2U == 
                                             (3U & 
                                              (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                               >> 9U)))) 
               | (((~ (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                       >> 5U)) & (0U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                               >> 9U)))) 
                  & ((1U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                   >> 0xbU))) | (~ 
                                                 (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0xbU)))))) 
           | (((~ (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                   >> 0xdU)) & (0U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x11U)))) 
              & (0U != (IData)(vlTOPp->mkSoc__DOT__x___05Fh82753))));
    vlTOPp->mkSoc__DOT__NOT_rtc_rtc_date_reg_719_BITS_16_TO_13_722_CON_ETC___05F_d1767 
        = (1U & (((0U != (IData)(vlTOPp->mkSoc__DOT__x___05Fh82753)) 
                  | ((((~ (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                           >> 5U)) | (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                      >> 0xcU)) | (2U 
                                                   != 
                                                   (3U 
                                                    & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                       >> 9U)))) 
                     & (((vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                          >> 5U) | (0U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                 >> 9U)))) 
                        | ((1U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                         >> 0xbU))) 
                           & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0xbU))))) & (((vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                >> 0xdU) 
                                               | (0U 
                                                  != 
                                                  (3U 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x11U)))) 
                                              | (0U 
                                                 == (IData)(vlTOPp->mkSoc__DOT__x___05Fh82753)))));
    vlTOPp->mkSoc__DOT__NOT_rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_ETC___05F_d1912 
        = ((((((1U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                 >> 0x11U)) | (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
               | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                               >> 0x1aU)))) | (1U != 
                                               (0xfU 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                   >> 0x1cU)))) 
             & (((((2U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                     >> 0x11U)) | (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x16U)))) 
                   | (2U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                   >> 0x1aU)))) | (8U 
                                                   != 
                                                   (0xfU 
                                                    & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                       >> 0x1cU)))) 
                 | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_16_TO_13_722_CONCAT___05FETC___05F_d1788)) 
                | ((vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                    >> 0xdU) & (2U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x11U)))))) 
            & ((((2U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                   >> 0x11U)) | (0xfU 
                                                 & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                    >> 0x16U)))) 
                 | (2U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                 >> 0x1aU)))) | (9U 
                                                 != 
                                                 (0xfU 
                                                  & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                     >> 0x1cU)))) 
               | ((IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_date_reg_719_BITS_16_TO_13_722_CON_ETC___05F_d1767) 
                  & ((~ (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                         >> 0xdU)) | (2U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                   >> 0x11U))))))) 
           & (((3U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                 >> 0x11U)) | (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
               | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                               >> 0x1aU)))) | (1U != 
                                               (0xfU 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                   >> 0x1cU)))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1773 
        = (((((2U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                >> 0x11U)) | (0xfU 
                                              & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                 >> 0x16U)))) 
              & (2U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x1aU)))) & (8U == 
                                              (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x1cU)))) 
            & (IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_date_reg_719_BITS_16_TO_13_722_CON_ETC___05F_d1767)) 
           & ((~ (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                  >> 0xdU)) | (2U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                            >> 0x11U)))));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__46(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__46\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N_trst) {
        if (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_connect_tap_request_to_syncfifo) 
             & ((IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sEnqToggle) 
                == (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sDeqToggle)))) {
            vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sEnqToggle 
                = (1U & (~ (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sEnqToggle)));
            vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                = vlTOPp->mkSoc__DOT__jtag_tap__DOT__request_to_DM_D_OUT;
        }
    } else {
        vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sEnqToggle = 0U;
        vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data = 0ULL;
    }
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_1_TO_0_72_EQ_0b10_50_A_ETC___05F_d469 
        = ((((2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
             & ((4U > (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                        >> 0x22U)))) 
                | (0xfU < (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U)))))) 
            & (0x20U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                          >> 0x22U))))) 
           & (0x2fU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U)))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_40_TO_34_74_EQ_16_75_O_ETC___05F_d669 
        = ((((((((((((((((((((((((((((0x10U == (0x7fU 
                                                & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U)))) 
                                     | (0x11U == (0x7fU 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x22U))))) 
                                    | (0x12U == (0x7fU 
                                                 & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                                   | (0x13U == (0x7fU 
                                                & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                                  | (0x14U == (0x7fU 
                                               & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                                 | (0x15U == (0x7fU 
                                              & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                                | (0x16U == (0x7fU 
                                             & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                               | (0x17U == (0x7fU & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x22U))))) 
                              | (0x18U == (0x7fU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                             | (0x19U == (0x7fU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                            | (0x1aU == (0x7fU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                           | (0x1bU == (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                          | (0x1cU == (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         | (0x1dU == (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        | (0x30U == (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       | (0x34U == (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      | (0x35U == (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     | (0x36U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    | (0x38U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   | (0x39U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  | (0x3aU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 | (0x3cU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U))))) 
                | (0x3dU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U))))) 
               | (0x3eU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))) 
              | (0x3fU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))) 
             | (0x40U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                           >> 0x22U))))) 
            | ((4U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                        >> 0x22U)))) 
               & (0xfU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U)))))) 
           | ((0x20U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                          >> 0x22U)))) 
              & (0x2fU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__v___05Fh12456 
        = ((((((((((((((((((((0x10U == (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U)))) 
                             | (0x11U == (0x7fU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                            | (0x12U == (0x7fU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                           | (0x14U == (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                          | (0x15U == (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         | (0x16U == (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        | (0x17U == (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       | (0x18U == (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      | (0x30U == (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     | (0x36U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    | (0x38U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   | (0x39U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  | (0x3aU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 | (0x3cU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U))))) 
                | (0x3dU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U))))) 
               | (0x3eU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))) 
              | (0x3fU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))) 
             | ((4U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U)))) 
                & (0xfU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U)))))) 
            | ((0x20U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                           >> 0x22U)))) 
               & (0x2fU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))))
            ? 0U : 2U);
    vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sDeqToggle 
        = (1U & ((~ (IData)(vlTOPp->RST_N_trst)) | (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sSyncReg1)));
    vlTOPp->mkSoc__DOT__CAN_FIRE_RL_connect_tap_request_to_syncfifo 
        = (((IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sEnqToggle) 
            == (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sDeqToggle)) 
           & (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__request_to_DM__DOT__empty_reg));
    vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__sSyncReg1 
        = ((IData)(vlTOPp->RST_N_trst) & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__dDeqToggle));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__47(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__47\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_write_request) 
             & (0x10U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                            >> 5U)))))) {
            vlTOPp->mkSoc__DOT__rtc_rtc_ctrl_reg = (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 4U));
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_ctrl_reg = 0x1e0U;
    }
    vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg;
    vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_resp_D_IN 
        = ((((((0U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                         >> 5U)))) 
               | (4U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
              | (8U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                          >> 5U))))) 
             | (0xcU == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U))))) 
            | (0x10U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                           >> 5U)))))
            ? 0U : 2U);
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__48(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__48\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__sToggleReg;
    vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__sToggleReg;
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__sToggleReg;
    vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__sToggleReg;
    vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ___05FETC___05F_d1887 
        = (((5U != (7U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                          >> 0x15U))) | (9U != (0xfU 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                   >> 0x18U)))) 
           | ((9U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                              >> 0x1cU))) & (~ (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo_dD_OUT))));
    vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680 
        = (((5U == (7U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                          >> 0x15U))) & (9U == (0xfU 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                   >> 0x18U)))) 
           & ((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                              >> 0x1cU))) | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo_dD_OUT)));
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_write_time_date_data) {
            vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sDataSyncIn 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_dD_OUT 
                                 >> 0x20U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sDataSyncIn = 0U;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_write_time_date_data) {
            vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sDataSyncIn 
                = (1U & (IData)((vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_dD_OUT 
                                 >> 0x20U)));
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sDataSyncIn = 0U;
    }
    vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ___05FETC___05F_d1890 
        = ((((9U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                             >> 0xaU))) | (5U != (7U 
                                                  & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                     >> 0xeU)))) 
            | (9U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                              >> 0x11U)))) | (IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ___05FETC___05F_d1887));
    vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ___05FETC___05F_d1892 
        = ((((3U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                             >> 0xaU))) | (5U != (7U 
                                                  & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                     >> 0xeU)))) 
            | (9U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                              >> 0x11U)))) | (IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ___05FETC___05F_d1887));
    vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1699 
        = (((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                              >> 0xaU))) | ((2U == 
                                             (3U & 
                                              (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                               >> 8U))) 
                                            & (3U == 
                                               (0xfU 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                   >> 0xaU))))) 
             & (5U == (7U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                             >> 0xeU)))) & (9U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                      >> 0x11U)))) 
           & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680));
    vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704 
        = (((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                              >> 0xaU))) | (3U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                      >> 0xaU)))) 
             & (5U == (7U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                             >> 0xeU)))) & (9U == (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                      >> 0x11U)))) 
           & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680));
    vlTOPp->mkSoc__DOT__NOT_rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_ETC___05F_d1950 
        = ((((((((((((IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_ETC___05F_d1912) 
                     & (((4U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                           >> 0x11U)) 
                                 | (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                            >> 0x16U)))) 
                         | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                         >> 0x1aU)))) 
                        | (0U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                          >> 0x1cU))))) 
                    & (((5U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                          >> 0x11U)) 
                                | (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                           >> 0x16U)))) 
                        | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                        >> 0x1aU)))) 
                       | (1U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                         >> 0x1cU))))) 
                   & (((6U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                         >> 0x11U)) 
                               | (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                          >> 0x16U)))) 
                       | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                       >> 0x1aU)))) 
                      | (0U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                        >> 0x1cU))))) 
                  & (((7U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                        >> 0x11U)) 
                              | (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                         >> 0x16U)))) 
                      | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                      >> 0x1aU)))) 
                     | (1U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                       >> 0x1cU))))) 
                 & (((8U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                       >> 0x11U)) | 
                             (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                      >> 0x16U)))) 
                     | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                     >> 0x1aU)))) | 
                    (1U != (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                    >> 0x1cU))))) & 
                (((9U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                    >> 0x11U)) | (0xfU 
                                                  & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                     >> 0x16U)))) 
                  | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                  >> 0x1aU)))) | (0U 
                                                  != 
                                                  (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))))) 
               & (((0x10U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                        >> 0x11U)) 
                              | (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                         >> 0x16U)))) 
                   | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                   >> 0x1aU)))) | (1U 
                                                   != 
                                                   (0xfU 
                                                    & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                       >> 0x1cU))))) 
              & (((0x11U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                       >> 0x11U)) | 
                             (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                      >> 0x16U)))) 
                  | (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                  >> 0x1aU)))) | (0U 
                                                  != 
                                                  (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x1cU))))) 
             & (((0x12U != ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                      >> 0x11U)) | 
                            (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                     >> 0x16U)))) | 
                 (3U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                               >> 0x1aU)))) | (1U != 
                                               (0xfU 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                   >> 0x1cU))))) 
            | (2U != (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                            >> 8U)))) | ((IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ___05FETC___05F_d1890) 
                                         & (IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ___05FETC___05F_d1892)));
    vlTOPp->mkSoc__DOT__MUX_rtc_rtc_time_reg_write_1___05FVAL_2 
        = (((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                              >> 0x1cU))) | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo_dD_OUT))
              ? 0U : (0xfU & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_en_lo_dD_OUT)
                               ? ((IData)(1U) + (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                 >> 0x1cU))
                               : (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                  >> 0x1cU)))) << 0x1cU) 
           | (((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                 >> 0x18U))) & ((9U 
                                                 == 
                                                 (0xfU 
                                                  & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                     >> 0x1cU))) 
                                                | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo_dD_OUT)))
                 ? 0U : (0xfU & (((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                  >> 0x1cU))) 
                                  | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo_dD_OUT))
                                  ? ((IData)(1U) + 
                                     (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                      >> 0x18U)) : 
                                 (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                  >> 0x18U)))) << 0x18U) 
              | ((((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680)
                    ? 0U : (7U & (((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                   >> 0x18U))) 
                                   & ((9U == (0xfU 
                                              & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                 >> 0x1cU))) 
                                      | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_btos_lo_dD_OUT)))
                                   ? ((IData)(1U) + 
                                      (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                       >> 0x15U)) : 
                                  (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                   >> 0x15U)))) << 0x15U) 
                 | (((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                       >> 0x11U))) 
                       & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680))
                       ? 0U : (0xfU & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680)
                                        ? ((IData)(1U) 
                                           + (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                              >> 0x11U))
                                        : (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                           >> 0x11U)))) 
                     << 0x11U) | ((((((5U == (7U & 
                                              (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                               >> 0xeU))) 
                                      & (9U == (0xfU 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                   >> 0x11U)))) 
                                     & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680))
                                     ? 0U : (7U & (
                                                   ((9U 
                                                     == 
                                                     (0xfU 
                                                      & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                         >> 0x11U))) 
                                                    & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680))
                                                    ? 
                                                   ((IData)(1U) 
                                                    + 
                                                    (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                     >> 0xeU))
                                                    : 
                                                   (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                    >> 0xeU)))) 
                                   << 0xeU) | ((((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1699)
                                                  ? 0U
                                                  : 
                                                 (0xfU 
                                                  & ((((5U 
                                                        == 
                                                        (7U 
                                                         & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                            >> 0xeU))) 
                                                       & (9U 
                                                          == 
                                                          (0xfU 
                                                           & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                              >> 0x11U)))) 
                                                      & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_23_TO_21_664_EQ_5_67_ETC___05F_d1680))
                                                      ? 
                                                     ((IData)(1U) 
                                                      + 
                                                      (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                       >> 0xaU))
                                                      : 
                                                     (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                      >> 0xaU)))) 
                                                << 0xaU) 
                                               | (((((2U 
                                                      == 
                                                      (3U 
                                                       & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                          >> 8U))) 
                                                     & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704))
                                                     ? 0U
                                                     : 
                                                    (3U 
                                                     & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1699)
                                                         ? 
                                                        ((IData)(1U) 
                                                         + 
                                                         (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                          >> 8U))
                                                         : 
                                                        (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                         >> 8U)))) 
                                                   << 8U) 
                                                  | (((((7U 
                                                         == 
                                                         (7U 
                                                          & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                             >> 5U))) 
                                                        & (2U 
                                                           == 
                                                           (3U 
                                                            & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                               >> 8U)))) 
                                                       & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704))
                                                       ? 1U
                                                       : 
                                                      (7U 
                                                       & (((2U 
                                                            == 
                                                            (3U 
                                                             & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                                >> 8U))) 
                                                           & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704))
                                                           ? 
                                                          ((IData)(1U) 
                                                           + 
                                                           (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                            >> 5U))
                                                           : 
                                                          (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                           >> 5U)))) 
                                                     << 5U))))))));
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855 
        = ((((((2U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                 >> 0x11U)) | (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                  >> 0x1bU)) | (3U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                             >> 0x1aU)))) 
             & (((((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                     >> 0x1cU))) | 
                     (((((((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1733) 
                           | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1773)) 
                          | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1795)) 
                         | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1800)) 
                        | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1804)) 
                       | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1808)) 
                      | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1812))) 
                    | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1816)) 
                   | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1820)) 
                  | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1824)) 
                 | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1828)) 
                | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1832))) 
            & (2U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                            >> 8U)))) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704));
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__dSyncReg2) 
             != (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__dLastState))) {
            vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_dD_OUT 
                = vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sDataSyncIn;
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_dD_OUT = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__dSyncReg2) 
             != (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__dLastState))) {
            vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_dD_OUT 
                = vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sDataSyncIn;
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_dD_OUT = 0ULL;
    }
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961 
        = ((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                             >> 0x11U))) & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                            >> 0x15U)) 
            & (2U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                              >> 0x16U)))) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855));
    vlTOPp->mkSoc__DOT__MUX_rtc_rtc_date_reg_write_1___05FVAL_2 
        = ((((((((((((((((((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1733) 
                           | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1773)) 
                          | ((((2U == ((0x10U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                 >> 0x11U)) 
                                       | (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
                               & (2U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                               >> 0x1aU)))) 
                              & (9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                >> 0x1cU)))) 
                             & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_16_TO_13_722_CONCAT___05FETC___05F_d1788) 
                                | ((vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                    >> 0xdU) & (2U 
                                                == 
                                                (3U 
                                                 & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                    >> 0x11U))))))) 
                         | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1795)) 
                        | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1800)) 
                       | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1804)) 
                      | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1808)) 
                     | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1812)) 
                    | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1816)) 
                   | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1820)) 
                  | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1824)) 
                 | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1828)) 
                | (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1832)) 
               & (2U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                               >> 8U)))) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704))
              ? 1U : ((((9U == (0xfU & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                        >> 0x1cU))) 
                        & (2U == (3U & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                        >> 8U)))) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704))
                       ? 0U : (0xfU & (((2U == (3U 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                   >> 8U))) 
                                        & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704))
                                        ? ((IData)(1U) 
                                           + (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                              >> 0x1cU))
                                        : (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                           >> 0x1cU))))) 
            << 0x1cU) | ((((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855)
                            ? 0U : (3U & ((((9U == 
                                             (0xfU 
                                              & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                 >> 0x1cU))) 
                                            & (2U == 
                                               (3U 
                                                & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                   >> 8U)))) 
                                           & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ_9_69_ETC___05F_d1704))
                                           ? ((IData)(1U) 
                                              + (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                 >> 0x1aU))
                                           : (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                              >> 0x1aU)))) 
                          << 0x1aU) | ((((((vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                            >> 0x15U) 
                                           & (2U == 
                                              (0xfU 
                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x16U)))) 
                                          & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855))
                                          ? 1U : ((
                                                   ((~ 
                                                     (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0x15U)) 
                                                    & (9U 
                                                       == 
                                                       (0xfU 
                                                        & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                           >> 0x16U)))) 
                                                   & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855))
                                                   ? 0U
                                                   : 
                                                  (0xfU 
                                                   & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855)
                                                       ? 
                                                      ((IData)(1U) 
                                                       + 
                                                       (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                        >> 0x16U))
                                                       : 
                                                      (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                       >> 0x16U))))) 
                                        << 0x16U) | 
                                       ((0x200000U 
                                         & ((((~ (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x15U)) 
                                              | (((2U 
                                                   != 
                                                   (0xfU 
                                                    & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                       >> 0x16U))) 
                                                  | (((2U 
                                                       != 
                                                       ((0x10U 
                                                         & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                            >> 0x11U)) 
                                                        | (0xfU 
                                                           & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                              >> 0x16U)))) 
                                                      | (~ 
                                                         (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                          >> 0x1bU))) 
                                                     & (3U 
                                                        != 
                                                        (3U 
                                                         & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                            >> 0x1aU))))) 
                                                 | ((((9U 
                                                       != 
                                                       (0xfU 
                                                        & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                           >> 0x1cU))) 
                                                      | (2U 
                                                         != 
                                                         (3U 
                                                          & (vlTOPp->mkSoc__DOT__rtc_rtc_time_reg 
                                                             >> 8U)))) 
                                                     | ((IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ___05FETC___05F_d1890) 
                                                        & (IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_time_reg_659_BITS_13_TO_10_661_EQ___05FETC___05F_d1892))) 
                                                    & (IData)(vlTOPp->mkSoc__DOT__NOT_rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_ETC___05F_d1950)))) 
                                             & ((((~ 
                                                   (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                    >> 0x15U)) 
                                                  & (9U 
                                                     == 
                                                     (0xfU 
                                                      & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                         >> 0x16U)))) 
                                                 & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855))
                                                 ? 
                                                ((IData)(1U) 
                                                 + 
                                                 (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x15U))
                                                 : 
                                                (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                 >> 0x15U))) 
                                            << 0x15U)) 
                                        | ((((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961)
                                              ? 0U : 
                                             (0xfU 
                                              & ((((vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                    >> 0x15U) 
                                                   & (2U 
                                                      == 
                                                      (0xfU 
                                                       & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                          >> 0x16U)))) 
                                                  & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BIT_21_724_CONCAT_rtc_rtc_ETC___05F_d1855))
                                                  ? 
                                                 ((IData)(1U) 
                                                  + 
                                                  (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                   >> 0x11U))
                                                  : 
                                                 (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                  >> 0x11U)))) 
                                            << 0x11U) 
                                           | (((((9U 
                                                  == 
                                                  (0xfU 
                                                   & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0xdU))) 
                                                 & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961))
                                                 ? 0U
                                                 : 
                                                (0xfU 
                                                 & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961)
                                                     ? 
                                                    ((IData)(1U) 
                                                     + 
                                                     (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                      >> 0xdU))
                                                     : 
                                                    (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                     >> 0xdU)))) 
                                               << 0xdU) 
                                              | ((((((9U 
                                                      == 
                                                      (0xfU 
                                                       & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                          >> 9U))) 
                                                     & (9U 
                                                        == 
                                                        (0xfU 
                                                         & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                            >> 0xdU)))) 
                                                    & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961))
                                                    ? 0U
                                                    : 
                                                   (0xfU 
                                                    & (((9U 
                                                         == 
                                                         (0xfU 
                                                          & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                             >> 0xdU))) 
                                                        & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961))
                                                        ? 
                                                       ((IData)(1U) 
                                                        + 
                                                        (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                         >> 9U))
                                                        : 
                                                       (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                        >> 9U)))) 
                                                  << 9U) 
                                                 | ((((((9U 
                                                         == 
                                                         (0xfU 
                                                          & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                             >> 5U))) 
                                                        & (9U 
                                                           == 
                                                           (0xfU 
                                                            & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                               >> 9U)))) 
                                                       & (9U 
                                                          == 
                                                          (0xfU 
                                                           & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                              >> 0xdU)))) 
                                                      & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961))
                                                      ? 0U
                                                      : 
                                                     (0xfU 
                                                      & ((((9U 
                                                            == 
                                                            (0xfU 
                                                             & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                                >> 9U))) 
                                                           & (9U 
                                                              == 
                                                              (0xfU 
                                                               & (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                                  >> 0xdU)))) 
                                                          & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_719_BITS_20_TO_17_723_EQ_9_95_ETC___05F_d1961))
                                                          ? 
                                                         ((IData)(1U) 
                                                          + 
                                                          (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                           >> 5U))
                                                          : 
                                                         (vlTOPp->mkSoc__DOT__rtc_rtc_date_reg 
                                                          >> 5U)))) 
                                                    << 5U))))))));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__dSyncReg2));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__dLastState 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__dSyncReg2));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__dSyncReg1));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__dSyncReg2 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__dSyncReg1));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__sToggleReg));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__dSyncReg1 
        = ((IData)(vlTOPp->RST_N) & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__sToggleReg));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__49(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__49\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if ((1U & (~ (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_clk)))) {
        vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_gate 
            = vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__new_gate;
    }
    vlTOPp->mkSoc__DOT__rtc_rtc_mc_CLK_OUT = ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_clk) 
                                              & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_gate));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__50(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__50\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_data_D_IN 
        = (((QData)((IData)(((((((0U == (0x1ffU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                           >> 5U)))) 
                                 | (4U == (0x1ffU & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                             >> 5U))))) 
                                | (8U == (0x1ffU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                            >> 5U))))) 
                               | (0xcU == (0x1ffU & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                             >> 5U))))) 
                              | (0x10U == (0x1ffU & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                             >> 5U)))))
                              ? 0U : 2U))) << 0x20U) 
           | (QData)((IData)(((0U == (0x1ffU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                        >> 5U))))
                               ? vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read_dD_OUT
                               : ((4U == (0x1ffU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                            >> 5U))))
                                   ? vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read_dD_OUT
                                   : ((8U == (0x1ffU 
                                              & (IData)(
                                                        (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                         >> 5U))))
                                       ? vlTOPp->mkSoc__DOT__rtc_rtc_talrm_reg
                                       : ((0xcU == 
                                           (0x1ffU 
                                            & (IData)(
                                                      (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                       >> 5U))))
                                           ? vlTOPp->mkSoc__DOT__rtc_rtc_darlm_reg
                                           : ((0x10U 
                                               == (0x1ffU 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_rd_addr__DOT__data0_reg 
                                                              >> 5U))))
                                               ? vlTOPp->mkSoc__DOT__rtc_rtc_ctrl_reg
                                               : 0U))))))));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__sToggleReg;
    vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__sToggleReg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__sToggleReg;
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_sEN) {
            vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sDataSyncIn 
                = ((IData)(vlTOPp->mkSoc__DOT__MUX_rtc_rtc_get_time_write_data_write_1___05FSEL_1)
                    ? (((QData)((IData)((((IData)(vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data_first___05F012_BITS_35_TO_3_ETC___05F_d2038) 
                                          & (3U != 
                                             (3U & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0xcU))))) 
                                         & (0U != (7U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 9U))))))) 
                        << 0x20U) | (QData)((IData)(
                                                    (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                     >> 4U))))
                    : 0ULL);
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sDataSyncIn = 0ULL;
    }
    if (vlTOPp->RST_N) {
        if (vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_sEN) {
            vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sDataSyncIn 
                = ((IData)(vlTOPp->mkSoc__DOT__MUX_rtc_rtc_get_date_write_data_write_1___05FSEL_1)
                    ? (((QData)((IData)(vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data_first___05F012_BITS_35_TO_3_ETC___05F_d2152)) 
                        << 0x20U) | (QData)((IData)(
                                                    (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                     >> 4U))))
                    : 0ULL);
        }
    } else {
        vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sDataSyncIn = 0ULL;
    }
    vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_sRDY 
        = ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__sSyncReg2) 
           == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data__DOT__sync__DOT__sToggleReg));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_sRDY 
        = ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__sSyncReg2) 
           == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data__DOT__sync__DOT__sToggleReg));
    vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_write_request 
        = (((((IData)(vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__empty_reg) 
              & (IData)(vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__empty_reg)) 
             & (IData)(vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_resp__DOT__full_reg)) 
            & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_sRDY)) 
           & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_sRDY));
    vlTOPp->mkSoc__DOT__MUX_rtc_rtc_get_date_write_data_write_1___05FSEL_1 
        = ((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_write_request) 
           & (4U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U)))));
    vlTOPp->mkSoc__DOT__MUX_rtc_rtc_get_time_write_data_write_1___05FSEL_1 
        = ((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_write_request) 
           & (0U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                       >> 5U)))));
    vlTOPp->mkSoc__DOT__WILL_FIRE_RL_rtc_rtc_disable_after_writing 
        = (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_sRDY) 
            & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_sRDY)) 
           & (~ (IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_write_request)));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_sEN 
        = (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_write_request) 
            & (4U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
           | ((IData)(vlTOPp->mkSoc__DOT__WILL_FIRE_RL_rtc_rtc_disable_after_writing) 
              & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write_dD_OUT)));
    vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_sEN 
        = (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_write_request) 
            & (0U == (0x1ffU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_addr__DOT__data0_reg 
                                        >> 5U))))) 
           | ((IData)(vlTOPp->mkSoc__DOT__WILL_FIRE_RL_rtc_rtc_disable_after_writing) 
              & (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write_dD_OUT)));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__51(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__51\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iocell_io_io10_cell_out = (1U & ((0x80U 
                                              & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                              ? ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 6U)) 
                                                 & ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_pwm_output_enable) 
                                                    & (IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_pwm_output)))
                                              : ((0x40U 
                                                  & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                                  ? (IData)(vlSymsp->TOP__mkSoc__DOT__uart_cluster__DOT__uart2.__PVT__user_ifc_uart_rXmitDataOut)
                                                  : 
                                                 ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster_pinmuxtop_peripheral_side_gpioa_out_put) 
                                                  >> 3U))));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__52(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__52\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iocell_io_io12_cell_out = (1U & ((0x200U 
                                              & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                              ? ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 8U)) 
                                                 & ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_pwm_output_enable) 
                                                    & (IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_pwm_output)))
                                              : ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 8U)) 
                                                 & ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster_pinmuxtop_peripheral_side_gpioa_out_put) 
                                                    >> 4U))));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__53(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__53\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iocell_io_io13_cell_out = (1U & ((0x800U 
                                              & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                              ? ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 0xaU)) 
                                                 & ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_pwm_output_enable) 
                                                    & (IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_pwm_output)))
                                              : ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 0xaU)) 
                                                 & ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster_pinmuxtop_peripheral_side_gpioa_out_put) 
                                                    >> 5U))));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__54(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__54\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iocell_io_io16_cell_out = (1U & ((0x2000U 
                                              & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                              ? ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 0xcU)) 
                                                 & ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_pwm_output_enable) 
                                                    & (IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_pwm_output)))
                                              : ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 0xcU)) 
                                                 & ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster_pinmuxtop_peripheral_side_gpioa_out_put) 
                                                    >> 6U))));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__55(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__55\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iocell_io_io17_cell_out = (1U & ((0x8000U 
                                              & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                              ? ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 0xeU)) 
                                                 & ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_pwm_output_enable) 
                                                    & (IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_pwm_output)))
                                              : ((0x4000U 
                                                  & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                                  ? (IData)(vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1.__PVT__spi_rg_nss)
                                                  : 
                                                 ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster_pinmuxtop_peripheral_side_gpioa_out_put) 
                                                  >> 7U))));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__56(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__56\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iocell_io_io18_cell_out = (1U & ((0x20000U 
                                              & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                              ? ((~ 
                                                  (vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config 
                                                   >> 0x10U)) 
                                                 & ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_pwm_output_enable) 
                                                    & (IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_pwm_output)))
                                              : ((0x10000U 
                                                  & vlTOPp->mkSoc__DOT__mixed_cluster__DOT__pinmuxtop__DOT__user_ifc_pinmux_muxsel_config)
                                                  ? (IData)(vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1.__PVT__spi_wr_spi_out_io1)
                                                  : 
                                                 ((IData)(vlTOPp->mkSoc__DOT__mixed_cluster_pinmuxtop_peripheral_side_gpioa_out_put) 
                                                  >> 8U))));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__57(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__57\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (vlTOPp->mkSoc__DOT__jtag_tap__DOT__MUX_capture_repsonse_from_dm_write_1___05FSEL_4) {
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__request_to_DM_D_OUT 
            = (0xffffffffffULL & (((QData)((IData)(
                                                   vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U])) 
                                   << 0x26U) | (((QData)((IData)(
                                                                 vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U])) 
                                                 << 6U) 
                                                | ((QData)((IData)(
                                                                   vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[2U])) 
                                                   >> 0x1aU))));
    }
    if (vlTOPp->RST_N_trst) {
        if ((((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_capture) 
              | (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_shift_mdr)) 
             | (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_update))) {
            vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[0U] 
                = vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[0U];
            vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[1U] 
                = vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[1U];
            vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[2U] 
                = vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[2U];
            vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U] 
                = vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[3U];
            vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U] 
                = vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[4U];
        }
    } else {
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[0U] = 0U;
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[1U] = 0U;
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[2U] = 0U;
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U] = 0U;
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U] = 0U;
    }
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_BIT_138_1_AND_rg_pseudo_ir_2_EQ_0x11_7_ETC___05F_d43 
        = ((((vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U] 
              >> 0xaU) & (0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))) 
            & (~ (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__request_to_DM__DOT__empty_reg))) 
           & (~ (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__capture_repsonse_from_dm)));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__58(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__58\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_1_TO_0_72_EQ_0b10_50_A_ETC___05F_d336 
        = (((((((((((((((((((((2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                              & (0x10U != (0x7fU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                             & (0x11U != (0x7fU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                            & (0x12U != (0x7fU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                           & (0x14U != (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                          & (0x15U != (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         & (0x16U != (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        & (0x17U != (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       & (0x18U != (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      & (0x30U != (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     & (0x36U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    & (0x38U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   & (0x39U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  & (0x3aU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 & (0x3cU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U))))) 
                & (0x3dU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U))))) 
               & (0x3eU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))) 
              & (0x3fU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))) 
             & (4U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                        >> 0x22U))))) 
            & (0xfU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U))))) 
           & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_busy));
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_1_TO_0_72_EQ_0b10_50_A_ETC___05F_d398 
        = ((((2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
             & (0x38U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                           >> 0x22U))))) 
            & (7U == (7U & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                    >> 0xeU))))) & 
           (0U != (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbError)));
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_1_TO_0_72_EQ_0b10_50_A_ETC___05F_d422 
        = (((((((((((((((((((((2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                              & (0x10U != (0x7fU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                             & (0x11U != (0x7fU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                            & (0x12U != (0x7fU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                           & (0x14U != (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                          & (0x15U != (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         & (0x16U != (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        & (0x17U != (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       & (0x18U != (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      & (0x30U != (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     & (0x36U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    & (0x38U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   & (0x39U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  & (0x3aU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 & (0x3cU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U))))) 
                & (0x3dU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U))))) 
               & (0x3eU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))) 
              & (0x3fU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))) 
             & (4U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                        >> 0x22U))))) 
            & (0xfU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U))))) 
           & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_busy)));
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_1_TO_0_72_EQ_0b1_73_AN_ETC___05F_d321 
        = ((((((((((((((((((((((((((((((1U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                                       & (0x10U != 
                                          (0x7fU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                                      & (0x11U != (0x7fU 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U))))) 
                                     & (0x12U != (0x7fU 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x22U))))) 
                                    & (0x13U != (0x7fU 
                                                 & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                                   & (0x14U != (0x7fU 
                                                & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                                  & (0x15U != (0x7fU 
                                               & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                                 & (0x16U != (0x7fU 
                                              & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                                & (0x17U != (0x7fU 
                                             & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                               & (0x18U != (0x7fU & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x22U))))) 
                              & (0x19U != (0x7fU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                             & (0x1aU != (0x7fU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                            & (0x1bU != (0x7fU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                           & (0x1cU != (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                          & (0x1dU != (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         & (0x30U != (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        & (0x34U != (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       & (0x35U != (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      & (0x36U != (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     & (0x38U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    & (0x39U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   & (0x3aU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  & (0x3cU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 & (0x3dU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U))))) 
                & (0x3eU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U))))) 
               & (0x3fU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))) 
              & (0x40U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))) 
             & (4U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                        >> 0x22U))))) 
            & (0xfU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U))))) 
           & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_busy));
    vlTOPp->mkSoc__DOT__debug_module__DOT__NOT_dtm_putCommand_put_BITS_40_TO_34_74_ULT_4___05FETC___05F_d344 
        = ((((4U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                      >> 0x22U)))) 
             & (0xfU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                          >> 0x22U))))) 
            & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_busy))) 
           & ((0xbU >= (0xfU & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U)) 
                                - (IData)(4U)))) & 
              ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__autoExecData) 
               >> (0xfU & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                    >> 0x22U)) - (IData)(4U))))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__v___05Fh11711 
        = ((0x40U & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                              >> 0x22U)) - (IData)(4U)))
            ? 0U : ((0x20U & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                       >> 0x22U)) - (IData)(4U)))
                     ? 0U : ((0x10U & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U)) 
                                       - (IData)(4U)))
                              ? 0U : ((8U & ((IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U)) 
                                             - (IData)(4U)))
                                       ? ((4U & ((IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U)) 
                                                 - (IData)(4U)))
                                           ? 0U : (
                                                   (2U 
                                                    & ((IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)) 
                                                       - (IData)(4U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_11
                                                     : vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_10)
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_9
                                                     : vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_8)))
                                       : ((4U & ((IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U)) 
                                                 - (IData)(4U)))
                                           ? ((2U & 
                                               ((IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U)) 
                                                - (IData)(4U)))
                                               ? ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_7
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_6)
                                               : ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_5
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_4))
                                           : ((2U & 
                                               ((IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U)) 
                                                - (IData)(4U)))
                                               ? ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_3
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_2)
                                               : ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_1
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__abst_data_0)))))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__dmi_response_data___05F_1___05Fh18333 
        = ((0x40U & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                              >> 0x22U)) - (IData)(0x20U)))
            ? 0U : ((0x20U & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                       >> 0x22U)) - (IData)(0x20U)))
                     ? 0U : ((0x10U & ((IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U)) 
                                       - (IData)(0x20U)))
                              ? 0U : ((8U & ((IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U)) 
                                             - (IData)(0x20U)))
                                       ? ((4U & ((IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U)) 
                                                 - (IData)(0x20U)))
                                           ? ((2U & 
                                               ((IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U)) 
                                                - (IData)(0x20U)))
                                               ? ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_15
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_14)
                                               : ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_13
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_12))
                                           : ((2U & 
                                               ((IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U)) 
                                                - (IData)(0x20U)))
                                               ? ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_11
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_10)
                                               : ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_9
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_8)))
                                       : ((4U & ((IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U)) 
                                                 - (IData)(0x20U)))
                                           ? ((2U & 
                                               ((IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U)) 
                                                - (IData)(0x20U)))
                                               ? ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_7
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_6)
                                               : ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_5
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_4))
                                           : ((2U & 
                                               ((IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U)) 
                                                - (IData)(0x20U)))
                                               ? ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_3
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_2)
                                               : ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x22U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_1
                                                   : vlTOPp->mkSoc__DOT__debug_module__DOT__progbuf_0)))))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_1_TO_0_72_EQ_0b10_50_A_ETC___05F_d364 
        = ((((((((2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                 & (0x10U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U))))) 
                & (0x11U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U))))) 
               & (0x12U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))) 
              & (0x14U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))) 
             & (0x15U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                           >> 0x22U))))) 
            & (0x16U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                          >> 0x22U))))) 
           & ((0x17U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                          >> 0x22U)))) 
              | (((((((((((0x18U != (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U)))) 
                          & (0x30U != (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         & (0x36U != (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        & (0x38U != (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       & (0x39U != (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      & (0x3aU != (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     & (0x3cU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    & (0x3dU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   & (0x3eU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  & (0x3fU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__NOT_dtm_putCommand_put_BITS_40_TO_34_74_ULT_4___05FETC___05F_d344))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__dtm_putCommand_put_BITS_1_TO_0_72_EQ_0b1_73_AN_ETC___05F_d349 
        = ((((((((((((((((((((((((((((1U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                                     & (0x10U != (0x7fU 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x22U))))) 
                                    & (0x11U != (0x7fU 
                                                 & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                                   & (0x12U != (0x7fU 
                                                & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                                  & (0x13U != (0x7fU 
                                               & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                                 & (0x14U != (0x7fU 
                                              & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                                & (0x15U != (0x7fU 
                                             & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                               & (0x16U != (0x7fU & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x22U))))) 
                              & (0x17U != (0x7fU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x22U))))) 
                             & (0x18U != (0x7fU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                           >> 0x22U))))) 
                            & (0x19U != (0x7fU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                           & (0x1aU != (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                          & (0x1bU != (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         & (0x1cU != (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        & (0x1dU != (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       & (0x30U != (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      & (0x34U != (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     & (0x35U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    & (0x36U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   & (0x38U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  & (0x39U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 & (0x3aU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U))))) 
                & (0x3cU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U))))) 
               & (0x3dU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                             >> 0x22U))))) 
              & (0x3eU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U))))) 
             & (0x3fU != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                           >> 0x22U))))) 
            & (0x40U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                          >> 0x22U))))) 
           & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__NOT_dtm_putCommand_put_BITS_40_TO_34_74_ULT_4___05FETC___05F_d344));
    vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351 
        = (((4U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                     >> 0x22U)))) & 
            (0xfU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                       >> 0x22U)))))
            ? ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_busy)
                ? 0U : vlTOPp->mkSoc__DOT__debug_module__DOT__v___05Fh11711)
            : (((0x20U <= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                            >> 0x22U)))) 
                & (0x2fU >= (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                              >> 0x22U)))))
                ? vlTOPp->mkSoc__DOT__debug_module__DOT__dmi_response_data___05F_1___05Fh18333
                : 0U));
    vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh18219 
        = ((1U & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                          >> 0x28U))) ? ((1U & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x27U)))
                                          ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                          : ((1U & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x26U)))
                                              ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                              : ((1U 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x25U)))
                                                  ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x24U)))
                                                   ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                     : (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__vrg_halted_0)))))))
            : ((1U & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                              >> 0x27U))) ? ((1U & (IData)(
                                                           (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                            >> 0x26U)))
                                              ? ((1U 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x25U)))
                                                  ? 
                                                 ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x24U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? 0U
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__sbData1
                                                     : vlTOPp->mkSoc__DOT__debug_module__DOT__sbData0))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                     : vlTOPp->mkSoc__DOT__debug_module__DOT__sbAddress1)
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__sbAddress0
                                                     : 
                                                    (0x2000040fU 
                                                     | ((((((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusyError) 
                                                            << 0x16U) 
                                                           | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy) 
                                                              << 0x15U)) 
                                                          | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbReadOnAddr) 
                                                              << 0x14U) 
                                                             | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbAccess) 
                                                                << 0x11U))) 
                                                         | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbAutoIncrement) 
                                                             << 0x10U) 
                                                            | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbReadOnData) 
                                                               << 0xfU))) 
                                                        | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbError) 
                                                           << 0xcU))))))
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x24U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                     : 0U)
                                                    : 0U)
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                     : vlTOPp->mkSoc__DOT__debug_module__DOT__auth_data))))
                                              : vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351)
                : ((1U & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                  >> 0x26U))) ? ((1U 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                             >> 0x25U)))
                                                  ? 
                                                 ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x24U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351
                                                    : 0U)
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? 0U
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? 0U
                                                     : (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__autoExecData))))
                                                  : 
                                                 ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                              >> 0x24U)))
                                                   ? 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? 
                                                    (((((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_cmdType) 
                                                        << 0x18U) 
                                                       | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_aarSize) 
                                                          << 0x14U)) 
                                                      | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_aarPostIncrement) 
                                                          << 0x13U) 
                                                         | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_transfer) 
                                                            << 0x11U))) 
                                                     | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_write) 
                                                         << 0x10U) 
                                                        | (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_regno)))
                                                     : 
                                                    (0xcU 
                                                     | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_busy) 
                                                         << 0xcU) 
                                                        | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_cmderr) 
                                                           << 8U))))
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__maskData)
                                                     : 0U))
                                                   : 
                                                  ((1U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                               >> 0x23U)))
                                                    ? 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? 0U
                                                     : 0xc7c0U)
                                                    : 
                                                   ((1U 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                                >> 0x22U)))
                                                     ? 
                                                    (0x32U 
                                                     | (((((((((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__allHaveReset) 
                                                               << 0x13U) 
                                                              | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__anyHaveReset) 
                                                                  << 0x12U) 
                                                                 | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__allResumeAck) 
                                                                    << 0x11U))) 
                                                             | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__anyResumeAck) 
                                                                 << 0x10U) 
                                                                | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__allNonExistent) 
                                                                   << 0xfU))) 
                                                            | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__anyNonExistent) 
                                                                << 0xeU) 
                                                               | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__allUnAvail) 
                                                                  << 0xdU))) 
                                                           | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__anyUnAvail) 
                                                               << 0xcU) 
                                                              | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__allRunning) 
                                                                 << 0xbU))) 
                                                          | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__anyRunning) 
                                                              << 0xaU) 
                                                             | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__allHalted) 
                                                                << 9U))) 
                                                         | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__anyHalted) 
                                                             << 8U) 
                                                            | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__authenticated) 
                                                               << 7U))) 
                                                        | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__authbusy) 
                                                           << 6U)))
                                                     : 
                                                    ((((((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__haltReq) 
                                                         << 0x1fU) 
                                                        | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__resumeReq) 
                                                           << 0x1eU)) 
                                                       | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__hartReset) 
                                                           << 0x1dU) 
                                                          | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__ackHaveReset) 
                                                             << 0x1cU))) 
                                                      | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__setResetHaltRequest) 
                                                          << 3U) 
                                                         | ((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__clrResetHaltReq) 
                                                            << 2U))) 
                                                     | (((IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__nDMReset) 
                                                         << 1U) 
                                                        | (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__dmActive)))))))
                    : vlTOPp->mkSoc__DOT__debug_module__DOT__y_avValue_fst___05Fh12351)));
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__59(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__59\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__dDeqToggle 
        = vlTOPp->__Vdly__mkSoc__DOT__sync_request_to_dm__DOT__dDeqToggle;
    vlTOPp->mkSoc__DOT__CAN_FIRE_RL_read_synced_request_to_dm 
        = (((IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__dEnqToggle) 
            != (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__dDeqToggle)) 
           & ((~ (IData)((vlTOPp->mkSoc__DOT__debug_module__DOT__dmi_response 
                          >> 0x22U))) & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_command_good))));
    vlTOPp->mkSoc__DOT__WILL_FIRE_RL_response = ((vlTOPp->mkSoc__DOT__ccore__DOT__rg_abst_response[2U] 
                                                  & ((2U 
                                                      == (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_command_good)) 
                                                     & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_busy))) 
                                                 & (~ (IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_read_synced_request_to_dm)));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__60(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__60\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_sync_date_time_regs_to_current_clock 
        = (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_gate) 
            & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__sSyncReg2) 
               == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_time_reg_read__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__sSyncReg2) 
              == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_sync_date_reg_read__DOT__sync__DOT__sToggleReg)));
    vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_write_time_date_data 
        = (((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_mc__DOT__current_gate) 
            & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__sSyncReg2) 
               == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_time_write__DOT__sync__DOT__sToggleReg))) 
           & ((IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__sSyncReg2) 
              == (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_validate_date_write__DOT__sync__DOT__sToggleReg)));
    vlTOPp->mkSoc__DOT__rtc_rtc_time_reg_D_IN = (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_write_time_date_data) 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_dD_OUT 
                                                             >> 0x20U)))
                                                  ? 
                                                 ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_dD_OUT 
                                                              >> 0x20U)))
                                                   ? (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_time_write_data_dD_OUT)
                                                   : vlTOPp->mkSoc__DOT__rtc_rtc_time_reg)
                                                  : vlTOPp->mkSoc__DOT__MUX_rtc_rtc_time_reg_write_1___05FVAL_2);
    vlTOPp->mkSoc__DOT__rtc_rtc_date_reg_D_IN = (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_rtc_rtc_write_time_date_data) 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_dD_OUT 
                                                             >> 0x20U)))
                                                  ? 
                                                 ((1U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_dD_OUT 
                                                              >> 0x20U)))
                                                   ? (IData)(vlTOPp->mkSoc__DOT__rtc_rtc_get_date_write_data_dD_OUT)
                                                   : vlTOPp->mkSoc__DOT__rtc_rtc_date_reg)
                                                  : vlTOPp->mkSoc__DOT__MUX_rtc_rtc_date_reg_write_1___05FVAL_2);
}

VL_INLINE_OPT void VmkSoc::_sequent__TOP__61(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_sequent__TOP__61\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
        = vlTOPp->__Vdly__mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg;
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_shift_mdr 
        = ((IData)(vlTOPp->mkSoc__DOT__sel) & (IData)(vlTOPp->mkSoc__DOT__shift));
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_capture 
        = ((((IData)(vlTOPp->mkSoc__DOT__sel) & (IData)(vlTOPp->mkSoc__DOT__capture)) 
            & (~ (IData)(vlTOPp->mkSoc__DOT__update))) 
           & (~ (IData)(vlTOPp->mkSoc__DOT__shift)));
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_update 
        = ((((IData)(vlTOPp->mkSoc__DOT__sel) & (IData)(vlTOPp->mkSoc__DOT__update)) 
            & (~ (IData)(vlTOPp->mkSoc__DOT__capture))) 
           & (~ (IData)(vlTOPp->mkSoc__DOT__shift)));
    vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data_first___05F012_BITS_35_TO_3_ETC___05F_d2038 
        = (((((9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                      >> 0x20U)))) 
              & (9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                        >> 0x1cU))))) 
             & (5U >= (7U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                     >> 0x19U))))) 
            & (5U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                      >> 0x15U))))) 
           & (((9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                       >> 0xeU)))) 
               & (2U != (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                       >> 0xcU))))) 
              | ((3U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                         >> 0xeU)))) 
                 & (2U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                         >> 0xcU)))))));
    vlTOPp->mkSoc__DOT__x___05Fh86183 = ((0xf0U & ((IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x11U)) 
                                                   << 4U)) 
                                         | (0xfU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x15U))));
    if (((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                     >> 0x19U))) & (2U == (0xfU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 0x1aU)))))) {
        vlTOPp->mkSoc__DOT__IF_NOT_rtc_s_xactor_f_wr_data_first___05F012_BIT_2_ETC___05F_d2197 
            = (1U & (((((0U != (IData)(vlTOPp->mkSoc__DOT__x___05Fh86183)) 
                        | ((((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                         >> 9U))) | (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 0x10U))) 
                            | (2U != (3U & (IData)(
                                                   (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                    >> 0xdU))))) 
                           & (((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                        >> 9U)) | (0U 
                                                   != 
                                                   (3U 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 0xdU))))) 
                              | ((1U != (3U & (IData)(
                                                      (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 0xfU)))) 
                                 & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                            >> 0xfU)))))) 
                       & (((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 0x11U)) | (0U 
                                                  != 
                                                  (3U 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 0x15U))))) 
                          | (0U == (IData)(vlTOPp->mkSoc__DOT__x___05Fh86183)))) 
                      & ((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                     >> 0x11U))) | 
                         (2U != (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                               >> 0x15U)))))) 
                     & (9U == (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                               >> 0x20U))))));
        vlTOPp->mkSoc__DOT__IF_NOT_rtc_s_xactor_f_wr_data_first___05F012_BIT_2_ETC___05F_d2145 
            = (((((0U == (IData)(vlTOPp->mkSoc__DOT__x___05Fh86183)) 
                  & ((((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                >> 9U)) & (~ (IData)(
                                                     (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                      >> 0x10U)))) 
                      & (2U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                              >> 0xdU))))) 
                     | (((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                     >> 9U))) & (0U 
                                                 == 
                                                 (3U 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 0xdU))))) 
                        & ((1U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                 >> 0xfU)))) 
                           | (~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                         >> 0xfU))))))) 
                 | (((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                 >> 0x11U))) & (0U 
                                                == 
                                                (3U 
                                                 & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x15U))))) 
                    & (0U != (IData)(vlTOPp->mkSoc__DOT__x___05Fh86183)))) 
                | ((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                            >> 0x11U)) & (2U == (3U 
                                                 & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x15U)))))) 
               | (9U != (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                         >> 0x20U)))));
    } else {
        vlTOPp->mkSoc__DOT__IF_NOT_rtc_s_xactor_f_wr_data_first___05F012_BIT_2_ETC___05F_d2197 
            = (1U & ((((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                >> 0x19U)) | (0U == 
                                              (0xfU 
                                               & (IData)(
                                                         (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 0x1aU))))) 
                      | (9U < (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                               >> 0x1aU))))) 
                     & ((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 0x19U))) | (
                                                   (1U 
                                                    != 
                                                    (0xfU 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                                >> 0x1aU)))) 
                                                   & (2U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(
                                                                 (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                                  >> 0x1aU))))))));
        vlTOPp->mkSoc__DOT__IF_NOT_rtc_s_xactor_f_wr_data_first___05F012_BIT_2_ETC___05F_d2145 
            = ((((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                             >> 0x19U))) & (0U != (0xfU 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 0x1aU))))) 
                & (9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                          >> 0x1aU))))) 
               | ((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                           >> 0x19U)) & ((1U == (0xfU 
                                                 & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x1aU)))) 
                                         | (2U == (0xfU 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 0x1aU)))))));
    }
    if ((2U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                              >> 0x1eU))))) {
        vlTOPp->mkSoc__DOT__CASE_rtc_s_xactor_f_wr_dataD_OUT_BITS_31_TO_3_ETC___05Fq4 
            = (1U & (IData)(vlTOPp->mkSoc__DOT__IF_NOT_rtc_s_xactor_f_wr_data_first___05F012_BIT_2_ETC___05F_d2197));
        vlTOPp->mkSoc__DOT__CASE_rtc_s_xactor_f_wr_dataD_OUT_BITS_31_TO_3_ETC___05Fq3 
            = vlTOPp->mkSoc__DOT__IF_NOT_rtc_s_xactor_f_wr_data_first___05F012_BIT_2_ETC___05F_d2145;
    } else {
        vlTOPp->mkSoc__DOT__CASE_rtc_s_xactor_f_wr_dataD_OUT_BITS_31_TO_3_ETC___05Fq4 
            = (1U & ((3U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                           >> 0x1eU))))
                      ? ((((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                       >> 0x19U))) 
                           & (((((1U == (0xfU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                         >> 0x1aU)))) 
                                 | (3U == (0xfU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 0x1aU))))) 
                                | (5U == (0xfU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 0x1aU))))) 
                               | (7U == (0xfU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                         >> 0x1aU))))) 
                              | (8U == (0xfU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                        >> 0x1aU)))))) 
                          | ((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                      >> 0x19U)) & 
                             ((0U == (0xfU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                      >> 0x1aU)))) 
                              | (2U == (0xfU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                        >> 0x1aU)))))))
                          ? (1U < (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                   >> 0x20U))))
                          : ((((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                        >> 0x19U)) 
                               | (((4U != (0xfU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 0x1aU)))) 
                                   & (6U != (0xfU & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 0x1aU))))) 
                                  & (9U != (0xfU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x1aU)))))) 
                              & ((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                             >> 0x19U))) 
                                 | (1U != (0xfU & (IData)(
                                                          (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 0x1aU)))))) 
                             | (0U != (0xfU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                       >> 0x20U))))))
                      : (((1U != (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                >> 0x1eU)))) 
                          & (0U != (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                  >> 0x1eU))))) 
                         | ((((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                       >> 0x19U)) | 
                              (0U == (0xfU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                      >> 0x1aU))))) 
                             | (9U < (0xfU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                      >> 0x1aU))))) 
                            & ((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                           >> 0x19U))) 
                               | ((1U != (0xfU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 0x1aU)))) 
                                  & (2U != (0xfU & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x1aU))))))))));
        vlTOPp->mkSoc__DOT__CASE_rtc_s_xactor_f_wr_dataD_OUT_BITS_31_TO_3_ETC___05Fq3 
            = ((3U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                     >> 0x1eU)))) ? 
               ((((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                              >> 0x19U))) & (((((1U 
                                                 == 
                                                 (0xfU 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 0x1aU)))) 
                                                | (3U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 0x1aU))))) 
                                               | (5U 
                                                  == 
                                                  (0xfU 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 0x1aU))))) 
                                              | (7U 
                                                 == 
                                                 (0xfU 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 0x1aU))))) 
                                             | (8U 
                                                == 
                                                (0xfU 
                                                 & (IData)(
                                                           (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                            >> 0x1aU)))))) 
                 | ((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                             >> 0x19U)) & ((0U == (0xfU 
                                                   & (IData)(
                                                             (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                              >> 0x1aU)))) 
                                           | (2U == 
                                              (0xfU 
                                               & (IData)(
                                                         (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                          >> 0x1aU)))))))
                 ? (1U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                           >> 0x20U))))
                 : ((((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                  >> 0x19U))) & (((4U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 0x1aU)))) 
                                                  | (6U 
                                                     == 
                                                     (0xfU 
                                                      & (IData)(
                                                                (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                                 >> 0x1aU))))) 
                                                 | (9U 
                                                    == 
                                                    (0xfU 
                                                     & (IData)(
                                                               (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                                >> 0x1aU)))))) 
                     | ((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                 >> 0x19U)) & (1U == 
                                               (0xfU 
                                                & (IData)(
                                                          (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                           >> 0x1aU)))))) 
                    & (0U == (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                              >> 0x20U))))))
                : (((1U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                          >> 0x1eU)))) 
                    | (0U == (3U & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                            >> 0x1eU))))) 
                   & ((((~ (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                    >> 0x19U))) & (0U 
                                                   != 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 0x1aU))))) 
                       & (9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                 >> 0x1aU))))) 
                      | ((IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                  >> 0x19U)) & ((1U 
                                                 == 
                                                 (0xfU 
                                                  & (IData)(
                                                            (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                             >> 0x1aU)))) 
                                                | (2U 
                                                   == 
                                                   (0xfU 
                                                    & (IData)(
                                                              (vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                                               >> 0x1aU)))))))));
    }
    vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data_first___05F012_BITS_35_TO_3_ETC___05F_d2152 
        = ((((9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                     >> 0x20U)))) & 
             (9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                     >> 0x15U))))) 
            & (9U >= (0xfU & (IData)((vlTOPp->mkSoc__DOT__rtc_s_xactor_f_wr_data__DOT__data0_reg 
                                      >> 0x11U))))) 
           & (IData)(vlTOPp->mkSoc__DOT__CASE_rtc_s_xactor_f_wr_dataD_OUT_BITS_31_TO_3_ETC___05Fq3));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__62(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__62\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__debug_module__DOT__MUX_abst_ar_regno_write_1___05FSEL_2 
        = (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_read_synced_request_to_dm) 
            & (2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data)))) 
           & (0x17U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U)))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__ackHaveReset_1_whas 
        = (((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_read_synced_request_to_dm) 
            & (2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data)))) 
           & (0x10U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                         >> 0x22U)))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusyError_EN 
        = ((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_read_synced_request_to_dm) 
           & (((1U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
               & (((0x3cU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                               >> 0x22U)))) 
                   | (((0x3dU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U)))) 
                       | (0x3eU == (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      | (0x3fU == (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U)))))) 
                  & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
              | ((2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                 & ((((((((0x36U == (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U)))) 
                          & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy)) 
                         | ((0x38U == (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U)))) 
                            & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                       >> 0x18U)))) 
                        | ((0x39U == (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U)))) 
                           & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
                       | ((0x3aU == (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U)))) 
                          & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
                      | ((0x3cU == (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U)))) 
                         & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
                     | ((0x3dU == (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U)))) 
                        & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
                    | (((0x3eU == (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U)))) 
                        | (0x3fU == (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__MUX_startSBAccess_write_1___05FSEL_2 
        = ((IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_read_synced_request_to_dm) 
           & ((((((1U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                  & (0x3cU == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
                & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusyError))) 
               & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbReadOnData)) 
              | (((((((((((((2U == (3U & (IData)(vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data))) 
                            & (0x10U != (0x7fU & (IData)(
                                                         (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                          >> 0x22U))))) 
                           & (0x11U != (0x7fU & (IData)(
                                                        (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                         >> 0x22U))))) 
                          & (0x12U != (0x7fU & (IData)(
                                                       (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                        >> 0x22U))))) 
                         & (0x14U != (0x7fU & (IData)(
                                                      (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                       >> 0x22U))))) 
                        & (0x15U != (0x7fU & (IData)(
                                                     (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                      >> 0x22U))))) 
                       & (0x16U != (0x7fU & (IData)(
                                                    (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                     >> 0x22U))))) 
                      & (0x17U != (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U))))) 
                     & (0x18U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U))))) 
                    & (0x30U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                  >> 0x22U))))) 
                   & (0x36U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                 >> 0x22U))))) 
                  & (0x38U != (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                >> 0x22U))))) 
                 & (((((0x39U == (0x7fU & (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                   >> 0x22U)))) 
                       & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
                      & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusyError))) 
                     & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbReadOnAddr)) 
                    | (((0x3cU == (0x7fU & (IData)(
                                                   (vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                                    >> 0x22U)))) 
                        & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusy))) 
                       & (~ (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__sbBusyError)))))));
    vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_regno_D_IN 
        = (0xffffU & (((IData)(vlTOPp->mkSoc__DOT__WILL_FIRE_RL_response) 
                       & (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_aarPostIncrement))
                       ? ((IData)(1U) + (IData)(vlTOPp->mkSoc__DOT__debug_module__DOT__abst_ar_regno))
                       : (IData)((vlTOPp->mkSoc__DOT__sync_request_to_dm__DOT__syncFIFO1Data 
                                  >> 2U))));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__63(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__63\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*127:0*/ __Vtemp2727[4];
    WData/*159:0*/ __Vtemp2737[5];
    WData/*159:0*/ __Vtemp2739[5];
    WData/*159:0*/ __Vtemp2746[5];
    // Body
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__MUX_dmistat_write_1___05FSEL_1 
        = (((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_update) 
            & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U] 
               >> 0xaU)) & (0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir)));
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__wr_dmi_hardreset_whas 
        = (((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_update) 
            & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U] 
               >> 0xaU)) & (0x10U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir)));
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__MUX_capture_repsonse_from_dm_write_1___05FSEL_4 
        = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_update) 
           & (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_BIT_138_1_AND_rg_pseudo_ir_2_EQ_0x11_7_ETC___05F_d43));
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_dmireset_generated 
        = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__wr_dmi_hardreset_whas) 
           & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U] 
              >> 0x13U));
    VL_EXTEND_WI(119,1, __Vtemp2727, ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__wr_dmi_hardreset_whas) 
                                      & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U] 
                                         >> 0x14U)));
    __Vtemp2737[1U] = ((0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                        ? ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__response_from_DM__DOT__empty_reg)
                            ? (0xffffffe0U | (IData)(
                                                     ((((QData)((IData)(
                                                                        (vlTOPp->mkSoc__DOT__jtag_tap__DOT__response_from_DM_D_OUT 
                                                                         >> 2U))) 
                                                        << 5U) 
                                                       | (QData)((IData)(
                                                                         ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__x___05Fh2310) 
                                                                          << 3U)))) 
                                                      >> 0x20U)))
                            : (0xfffff000U | (IData)(
                                                     (((0xfffffffffe0ULL 
                                                        & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_packet 
                                                           << 3U)) 
                                                       | (QData)((IData)(
                                                                         ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__capture_repsonse_from_dm)
                                                                           ? 0x18U
                                                                           : 0U)))) 
                                                      >> 0x20U))))
                        : (0xfffff000U | (IData)(((0xfffffffffe0ULL 
                                                   & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_packet 
                                                      << 3U)) 
                                                  >> 0x20U))));
    __Vtemp2739[0U] = ((0x10U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                        ? (0x38308U | ((0xfff00000U 
                                        & (__Vtemp2727[0U] 
                                           << 0x14U)) 
                                       | ((0xfff80000U 
                                           & (((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__wr_dmi_hardreset_whas) 
                                               << 0x13U) 
                                              & vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U])) 
                                          | ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__dmistat) 
                                             << 0xdU))))
                        : ((0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                            ? ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__response_from_DM__DOT__empty_reg)
                                ? (IData)((((QData)((IData)(
                                                            (vlTOPp->mkSoc__DOT__jtag_tap__DOT__response_from_DM_D_OUT 
                                                             >> 2U))) 
                                            << 5U) 
                                           | (QData)((IData)(
                                                             ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__x___05Fh2310) 
                                                              << 3U)))))
                                : (IData)(((0xfffffffffe0ULL 
                                            & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_packet 
                                               << 3U)) 
                                           | (QData)((IData)(
                                                             ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__capture_repsonse_from_dm)
                                                               ? 0x18U
                                                               : 0U))))))
                            : (IData)((0xfffffffffe0ULL 
                                       & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_packet 
                                          << 3U)))));
    if (vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_capture) {
        __Vtemp2746[2U] = ((0x10U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                            ? ((0xfffffU & (__Vtemp2727[1U] 
                                            >> 0xcU)) 
                               | (0xfff00000U & (__Vtemp2727[2U] 
                                                 << 0x14U)))
                            : ((0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                                ? 0xffffffffU : 0xffffffffU));
        __Vtemp2746[3U] = ((0x10U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                            ? ((0xfffffU & (__Vtemp2727[2U] 
                                            >> 0xcU)) 
                               | (0xfff00000U & (__Vtemp2727[3U] 
                                                 << 0x14U)))
                            : ((0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                                ? 0xffffffffU : 0xffffffffU));
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[0U] 
            = __Vtemp2739[0U];
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[1U] 
            = ((0x10U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                ? ((0xfffffU & (__Vtemp2727[0U] >> 0xcU)) 
                   | (0xfff00000U & (__Vtemp2727[1U] 
                                     << 0x14U))) : 
               __Vtemp2737[1U]);
    } else {
        __Vtemp2746[2U] = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_shift_mdr)
                            ? ((vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U] 
                                << 0x1fU) | (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[2U] 
                                             >> 1U))
                            : 0U);
        __Vtemp2746[3U] = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_shift_mdr)
                            ? ((vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U] 
                                << 0x1fU) | (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U] 
                                             >> 1U))
                            : 0U);
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[0U] 
            = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_shift_mdr)
                ? ((vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[1U] 
                    << 0x1fU) | (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[0U] 
                                 >> 1U)) : 0U);
        vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[1U] 
            = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_shift_mdr)
                ? ((vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[2U] 
                    << 0x1fU) | (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[1U] 
                                 >> 1U)) : 0U);
    }
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[2U] 
        = __Vtemp2746[2U];
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[3U] 
        = __Vtemp2746[3U];
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr_D_IN[4U] 
        = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_capture)
            ? ((0x10U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                ? (0xfffffU & (__Vtemp2727[3U] >> 0xcU))
                : ((0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))
                    ? 0xffU : 0xffU)) : ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_shift_mdr)
                                          ? ((0xfffffc00U 
                                              & ((IData)(vlTOPp->mkSoc__DOT__tdi) 
                                                 << 0xaU)) 
                                             | (0x3ffU 
                                                & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[4U] 
                                                   >> 1U)))
                                          : 0U));
}

VL_INLINE_OPT void VmkSoc::_multiclk__TOP__64(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_multiclk__TOP__64\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__WILL_FIRE_RL_dmihardreset_generated 
        = ((((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__wr_dmi_hardreset_whas) 
             & (vlTOPp->mkSoc__DOT__jtag_tap__DOT__srg_mdr[3U] 
                >> 0x14U)) & (~ (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_capture))) 
           & (~ (IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_connect_tap_request_to_syncfifo)));
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__request_to_DM_DEQ 
        = ((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__WILL_FIRE_RL_dmihardreset_generated) 
           | (IData)(vlTOPp->mkSoc__DOT__CAN_FIRE_RL_connect_tap_request_to_syncfifo));
    vlTOPp->mkSoc__DOT__jtag_tap__DOT__response_from_DM_DEQ 
        = ((((IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__CAN_FIRE_RL_tunneled_capture) 
             & (0x11U == (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__rg_pseudo_ir))) 
            & (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__response_from_DM__DOT__empty_reg)) 
           | (IData)(vlTOPp->mkSoc__DOT__jtag_tap__DOT__WILL_FIRE_RL_dmihardreset_generated));
}

void VmkSoc::_eval(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_eval\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT)) 
         & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT))) {
        vlTOPp->_sequent__TOP__3(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK)))) {
        vlTOPp->_sequent__TOP__6(vlSymsp);
        vlTOPp->_sequent__TOP__7(vlSymsp);
        vlTOPp->_sequent__TOP__8(vlSymsp);
        vlTOPp->_sequent__TOP__9(vlSymsp);
        vlTOPp->_sequent__TOP__10(vlSymsp);
        vlTOPp->_sequent__TOP__11(vlSymsp);
        vlTOPp->_sequent__TOP__12(vlSymsp);
        vlTOPp->_sequent__TOP__13(vlSymsp);
        vlTOPp->_sequent__TOP__14(vlSymsp);
        vlTOPp->_sequent__TOP__15(vlSymsp);
        vlTOPp->_sequent__TOP__16(vlSymsp);
        vlTOPp->_sequent__TOP__17(vlSymsp);
        vlTOPp->_sequent__TOP__18(vlSymsp);
        vlTOPp->_sequent__TOP__19(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi0._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__3(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__4(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__13(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__14(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__15(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__16(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__17(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__18(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__uart_cluster__DOT__uart0._sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__4(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__uart_cluster__DOT__uart1._sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart1__5(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__uart_cluster__DOT__uart2._sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart2__6(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0._sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__3(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1._sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1__4(vlSymsp);
        vlTOPp->_sequent__TOP__20(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK_rtc_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_rtc_clk)))) {
        vlTOPp->_sequent__TOP__21(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk)))) {
        vlTOPp->_sequent__TOP__22(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK_rtc_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_rtc_clk))) 
         | ((~ (IData)(vlTOPp->RST_N_rtc_rst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_rtc_rst)))) {
        vlTOPp->_sequent__TOP__23(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->RST_N_trst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_trst)))) {
        vlTOPp->_sequent__TOP__24(vlSymsp);
    }
    if (((~ (IData)(vlTOPp->CLK_tck_clk)) & (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk))) {
        vlTOPp->_sequent__TOP__26(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK_rtc_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_rtc_clk))) 
         | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)))) {
        vlTOPp->_sequent__TOP__27(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__19(vlSymsp);
        vlTOPp->_sequent__TOP__28(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi0._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__5(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__5(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT))))) {
        vlTOPp->_sequent__TOP__29(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk))) 
         | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)))) {
        vlTOPp->_sequent__TOP__30(vlSymsp);
    }
    if (((~ (IData)(vlTOPp->CLK)) & (IData)(vlTOPp->__Vclklast__TOP__CLK))) {
        vlSymsp->TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0._sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__5(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__mixed_cluster__DOT__i2c1._sequent__TOP__mkSoc__DOT__mixed_cluster__DOT__i2c0__5(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi0._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__7(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__7(vlSymsp);
        vlTOPp->_sequent__TOP__31(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__uart_cluster__DOT__uart0._sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__7(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__uart_cluster__DOT__uart1._sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__7(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__uart_cluster__DOT__uart2._sequent__TOP__mkSoc__DOT__uart_cluster__DOT__uart0__7(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_control_reset__DOT__rst)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_control_reset__DOT__rst)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_control_reset__DOT__rst)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_control_reset__DOT__rst)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_control_reset__DOT__rst)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_control_reset__DOT__rst)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_control_reset__DOT__rst)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_control_reset__DOT__rst)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_control_reset__DOT__rst)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_control_reset__DOT__rst)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_control_reset__DOT__rst)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_control_reset__DOT__rst)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__25(vlSymsp);
    }
    vlTOPp->_combo__TOP__40(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi0._combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__11(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__37(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1._combo__TOP__mkSoc__DOT__spi_cluster__DOT__spi1__12(vlSymsp);
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__43(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK_rtc_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_rtc_clk)))) {
        vlTOPp->_sequent__TOP__41(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__49(vlSymsp);
        vlTOPp->_sequent__TOP__42(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi0._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__13(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__13(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK_rtc_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_rtc_clk))) 
         | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)))) {
        vlTOPp->_sequent__TOP__43(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)))) {
        vlTOPp->_sequent__TOP__44(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi0._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__15(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__15(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__55(vlSymsp);
    }
    if (((IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT) 
         & (~ (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT)))) {
        vlTOPp->_sequent__TOP__45(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(vlSymsp);
    }
    if (((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
         & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_clock_divider_clock_selector_CLK_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(vlSymsp);
    }
    if (((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
         & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_clock_divider_clock_selector_CLK_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(vlSymsp);
    }
    if (((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
         & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_clock_divider_clock_selector_CLK_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(vlSymsp);
    }
    if (((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
         & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_clock_divider_clock_selector_CLK_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(vlSymsp);
    }
    if (((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
         & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_clock_divider_clock_selector_CLK_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__61(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__62(vlSymsp);
    }
    if (((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
         & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_clock_divider_clock_selector_CLK_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__63(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk))) 
         | ((~ (IData)(vlTOPp->RST_N_trst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_trst)))) {
        vlTOPp->_sequent__TOP__46(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK)))) {
        vlTOPp->_sequent__TOP__47(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi0._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__17(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__spi_cluster__DOT__spi1._sequent__TOP__mkSoc__DOT__spi_cluster__DOT__spi0__17(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__79(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT))))) {
        vlTOPp->_sequent__TOP__48(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK_rtc_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_rtc_clk))) 
         | ((~ (IData)(vlTOPp->RST_N_rtc_rst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_rtc_rst)))) {
        vlTOPp->_sequent__TOP__49(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__86(vlSymsp);
        vlTOPp->_sequent__TOP__50(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N))) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlTOPp->_multiclk__TOP__51(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N))) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlTOPp->_multiclk__TOP__52(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N))) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlTOPp->_multiclk__TOP__53(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N))) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlTOPp->_multiclk__TOP__54(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N))) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlTOPp->_multiclk__TOP__55(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__85(vlSymsp);
    }
    if ((((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__92(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N))) 
         | ((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_clock_divider_clock_selector_CLK_OUT))))) {
        vlTOPp->_multiclk__TOP__56(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk)))) {
        vlTOPp->_sequent__TOP__57(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk)))) 
         | ((~ (IData)(vlTOPp->RST_N_trst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_trst)))) {
        vlTOPp->_multiclk__TOP__58(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->RST_N_trst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_trst)))) {
        vlTOPp->_sequent__TOP__59(vlSymsp);
    }
    if ((((((IData)(vlTOPp->CLK_rtc_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_rtc_clk))) 
           | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N))) 
          | ((~ (IData)(vlTOPp->RST_N_rtc_rst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_rtc_rst))) 
         | ((IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT) 
            & (~ (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT))))) {
        vlTOPp->_multiclk__TOP__60(vlSymsp);
    }
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(vlSymsp);
    vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._combo__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__103(vlSymsp);
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__110(vlSymsp);
        vlTOPp->_sequent__TOP__61(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((~ (IData)(vlTOPp->RST_N)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__116(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(vlSymsp);
    }
    if ((((IData)(vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_clock_divider_clock_selector_CLK_OUT) 
          & (~ (IData)(vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_clock_divider_clock_selector_CLK_OUT))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__109(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk)))) 
         | ((~ (IData)(vlTOPp->RST_N_trst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_trst)))) {
        vlTOPp->_multiclk__TOP__62(vlSymsp);
    }
    if ((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
         | ((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk))))) {
        vlTOPp->_multiclk__TOP__63(vlSymsp);
    }
    if (((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK)))) {
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(vlSymsp);
        vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5._sequent__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__127(vlSymsp);
    }
    if (((((IData)(vlTOPp->CLK) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK))) 
          | ((IData)(vlTOPp->CLK_tck_clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__CLK_tck_clk)))) 
         | ((~ (IData)(vlTOPp->RST_N_trst)) & (IData)(vlTOPp->__Vclklast__TOP__RST_N_trst)))) {
        vlTOPp->_multiclk__TOP__64(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT;
    vlTOPp->__Vclklast__TOP__CLK = vlTOPp->CLK;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vclklast__TOP__CLK_rtc_clk = vlTOPp->CLK_rtc_clk;
    vlTOPp->__Vclklast__TOP__CLK_tck_clk = vlTOPp->CLK_tck_clk;
    vlTOPp->__Vclklast__TOP__RST_N_rtc_rst = vlTOPp->RST_N_rtc_rst;
    vlTOPp->__Vclklast__TOP__RST_N_trst = vlTOPp->RST_N_trst;
    vlTOPp->__Vclklast__TOP__RST_N = vlTOPp->RST_N;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_control_reset__DOT__rst 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_control_reset__DOT__rst 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_control_reset__DOT__rst 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_control_reset__DOT__rst 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_control_reset__DOT__rst 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_control_reset__DOT__rst 
        = vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_clock_divider_clock_selector_CLK_OUT;
    vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_clock_divider_clock_selector_CLK_OUT;
    vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_clock_divider_clock_selector_CLK_OUT;
    vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_clock_divider_clock_selector_CLK_OUT;
    vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_clock_divider_clock_selector_CLK_OUT;
    vlTOPp->__Vclklast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_clock_divider_clock_selector_CLK_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_clock_divider_clock_selector_CLK_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT 
        = vlTOPp->mkSoc__DOT__rtc_rtc_mc_CLK_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0____PVT__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1____PVT__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2____PVT__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3____PVT__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4____PVT__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__VinpClk__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5____PVT__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_control_reset__DOT__rst;
}

VL_INLINE_OPT QData VmkSoc::_change_request(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_change_request\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    return (vlTOPp->_change_request_1(vlSymsp));
}

VL_INLINE_OPT QData VmkSoc::_change_request_1(VmkSoc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_change_request_1\n"); );
    VmkSoc* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    __req |= ((vlTOPp->mkSoc__DOT__rtc_rtc_mc_CLK_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__pwm_overall_reset_RST_OUT)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__pwm_control_reset__DOT__rst)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__pwm_overall_reset_RST_OUT)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__pwm_control_reset__DOT__rst)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__pwm_overall_reset_RST_OUT)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__pwm_control_reset__DOT__rst)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__pwm_overall_reset_RST_OUT)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__pwm_control_reset__DOT__rst)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__pwm_overall_reset_RST_OUT)
        || (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__pwm_control_reset__DOT__rst)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__pwm_overall_reset_RST_OUT)
         | (vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__pwm_control_reset__DOT__rst));
    VL_DEBUG_IF( if(__req && ((vlTOPp->mkSoc__DOT__rtc_rtc_mc_CLK_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/mkSoc.v:2867: mkSoc.rtc_rtc_mc_CLK_OUT\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__pwm_overall_reset_RST_OUT))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/mkpwm.v:264: pwm_overall_reset_RST_OUT\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__pwm_control_reset__DOT__rst))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/MakeResetA.v:38: pwm_control_reset.rst\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__pwm_overall_reset_RST_OUT))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/mkpwm.v:264: pwm_overall_reset_RST_OUT\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__pwm_control_reset__DOT__rst))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/MakeResetA.v:38: pwm_control_reset.rst\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__pwm_overall_reset_RST_OUT))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/mkpwm.v:264: pwm_overall_reset_RST_OUT\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__pwm_control_reset__DOT__rst))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/MakeResetA.v:38: pwm_control_reset.rst\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__pwm_overall_reset_RST_OUT))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/mkpwm.v:264: pwm_overall_reset_RST_OUT\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__pwm_control_reset__DOT__rst))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/MakeResetA.v:38: pwm_control_reset.rst\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__pwm_overall_reset_RST_OUT))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/mkpwm.v:264: pwm_overall_reset_RST_OUT\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__pwm_control_reset__DOT__rst))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/MakeResetA.v:38: pwm_control_reset.rst\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_overall_reset_RST_OUT ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__pwm_overall_reset_RST_OUT))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/mkpwm.v:264: pwm_overall_reset_RST_OUT\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_control_reset__DOT__rst ^ vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__pwm_control_reset__DOT__rst))) VL_DBG_MSGF("        CHANGE: build/hw/verilog/MakeResetA.v:38: pwm_control_reset.rst\n"); );
    // Final
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__rtc_rtc_mc_CLK_OUT 
        = vlTOPp->mkSoc__DOT__rtc_rtc_mc_CLK_OUT;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm0.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm1.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm2.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm3.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm4.__PVT__pwm_control_reset__DOT__rst;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__pwm_overall_reset_RST_OUT 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_overall_reset_RST_OUT;
    vlTOPp->__Vchglast__TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5__pwm_control_reset__DOT__rst 
        = vlSymsp->TOP__mkSoc__DOT__pwm_cluster__DOT__pwm5.__PVT__pwm_control_reset__DOT__rst;
    return __req;
}

#ifdef VL_DEBUG
void VmkSoc::_eval_debug_assertions() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    VmkSoc::_eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((CLK_tck_clk & 0xfeU))) {
        Verilated::overWidthError("CLK_tck_clk");}
    if (VL_UNLIKELY((RST_N_trst & 0xfeU))) {
        Verilated::overWidthError("RST_N_trst");}
    if (VL_UNLIKELY((CLK_rtc_clk & 0xfeU))) {
        Verilated::overWidthError("CLK_rtc_clk");}
    if (VL_UNLIKELY((RST_N_rtc_rst & 0xfeU))) {
        Verilated::overWidthError("RST_N_rtc_rst");}
    if (VL_UNLIKELY((CLK & 0xfeU))) {
        Verilated::overWidthError("CLK");}
    if (VL_UNLIKELY((RST_N & 0xfeU))) {
        Verilated::overWidthError("RST_N");}
    if (VL_UNLIKELY((spi0_io_miso_dat & 0xfeU))) {
        Verilated::overWidthError("spi0_io_miso_dat");}
    if (VL_UNLIKELY((uart0_io_SIN & 0xfeU))) {
        Verilated::overWidthError("uart0_io_SIN");}
    if (VL_UNLIKELY((i2c0_out_scl_in_in & 0xfeU))) {
        Verilated::overWidthError("i2c0_out_scl_in_in");}
    if (VL_UNLIKELY((i2c0_out_sda_in_in & 0xfeU))) {
        Verilated::overWidthError("i2c0_out_sda_in_in");}
    if (VL_UNLIKELY((i2c1_out_scl_in_in & 0xfeU))) {
        Verilated::overWidthError("i2c1_out_scl_in_in");}
    if (VL_UNLIKELY((i2c1_out_sda_in_in & 0xfeU))) {
        Verilated::overWidthError("i2c1_out_sda_in_in");}
    if (VL_UNLIKELY((xadc_master_m_awready_awready 
                     & 0xfeU))) {
        Verilated::overWidthError("xadc_master_m_awready_awready");}
    if (VL_UNLIKELY((xadc_master_m_wready_wready & 0xfeU))) {
        Verilated::overWidthError("xadc_master_m_wready_wready");}
    if (VL_UNLIKELY((xadc_master_m_bvalid_bvalid & 0xfeU))) {
        Verilated::overWidthError("xadc_master_m_bvalid_bvalid");}
    if (VL_UNLIKELY((xadc_master_m_bvalid_bresp & 0xfcU))) {
        Verilated::overWidthError("xadc_master_m_bvalid_bresp");}
    if (VL_UNLIKELY((xadc_master_m_arready_arready 
                     & 0xfeU))) {
        Verilated::overWidthError("xadc_master_m_arready_arready");}
    if (VL_UNLIKELY((xadc_master_m_rvalid_rvalid & 0xfeU))) {
        Verilated::overWidthError("xadc_master_m_rvalid_rvalid");}
    if (VL_UNLIKELY((xadc_master_m_rvalid_rresp & 0xfcU))) {
        Verilated::overWidthError("xadc_master_m_rvalid_rresp");}
    if (VL_UNLIKELY((eth_master_m_awready_awready & 0xfeU))) {
        Verilated::overWidthError("eth_master_m_awready_awready");}
    if (VL_UNLIKELY((eth_master_m_wready_wready & 0xfeU))) {
        Verilated::overWidthError("eth_master_m_wready_wready");}
    if (VL_UNLIKELY((eth_master_m_bvalid_bvalid & 0xfeU))) {
        Verilated::overWidthError("eth_master_m_bvalid_bvalid");}
    if (VL_UNLIKELY((eth_master_m_bvalid_bresp & 0xfcU))) {
        Verilated::overWidthError("eth_master_m_bvalid_bresp");}
    if (VL_UNLIKELY((eth_master_m_arready_arready & 0xfeU))) {
        Verilated::overWidthError("eth_master_m_arready_arready");}
    if (VL_UNLIKELY((eth_master_m_rvalid_rvalid & 0xfeU))) {
        Verilated::overWidthError("eth_master_m_rvalid_rvalid");}
    if (VL_UNLIKELY((eth_master_m_rvalid_rresp & 0xfcU))) {
        Verilated::overWidthError("eth_master_m_rvalid_rresp");}
    if (VL_UNLIKELY((mem_master_AWREADY & 0xfeU))) {
        Verilated::overWidthError("mem_master_AWREADY");}
    if (VL_UNLIKELY((mem_master_WREADY & 0xfeU))) {
        Verilated::overWidthError("mem_master_WREADY");}
    if (VL_UNLIKELY((mem_master_BVALID & 0xfeU))) {
        Verilated::overWidthError("mem_master_BVALID");}
    if (VL_UNLIKELY((mem_master_BRESP & 0xfcU))) {
        Verilated::overWidthError("mem_master_BRESP");}
    if (VL_UNLIKELY((mem_master_BID & 0xf0U))) {
        Verilated::overWidthError("mem_master_BID");}
    if (VL_UNLIKELY((mem_master_ARREADY & 0xfeU))) {
        Verilated::overWidthError("mem_master_ARREADY");}
    if (VL_UNLIKELY((mem_master_RVALID & 0xfeU))) {
        Verilated::overWidthError("mem_master_RVALID");}
    if (VL_UNLIKELY((mem_master_RRESP & 0xfcU))) {
        Verilated::overWidthError("mem_master_RRESP");}
    if (VL_UNLIKELY((mem_master_RLAST & 0xfeU))) {
        Verilated::overWidthError("mem_master_RLAST");}
    if (VL_UNLIKELY((mem_master_RID & 0xf0U))) {
        Verilated::overWidthError("mem_master_RID");}
    if (VL_UNLIKELY((iocell_io_io7_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io7_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io8_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io8_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io9_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io9_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io10_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io10_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io12_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io12_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io13_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io13_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io16_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io16_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io17_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io17_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io18_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io18_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io19_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io19_cell_in_in");}
    if (VL_UNLIKELY((iocell_io_io20_cell_in_in & 0xfeU))) {
        Verilated::overWidthError("iocell_io_io20_cell_in_in");}
    if (VL_UNLIKELY((gpio_4_in & 0xfeU))) {
        Verilated::overWidthError("gpio_4_in");}
    if (VL_UNLIKELY((gpio_7_in & 0xfeU))) {
        Verilated::overWidthError("gpio_7_in");}
    if (VL_UNLIKELY((gpio_8_in & 0xfeU))) {
        Verilated::overWidthError("gpio_8_in");}
    if (VL_UNLIKELY((gpio_14_in & 0xfeU))) {
        Verilated::overWidthError("gpio_14_in");}
    if (VL_UNLIKELY((gpio_15_in & 0xfeU))) {
        Verilated::overWidthError("gpio_15_in");}
    if (VL_UNLIKELY((gpio_16_in & 0xfeU))) {
        Verilated::overWidthError("gpio_16_in");}
    if (VL_UNLIKELY((gpio_17_in & 0xfeU))) {
        Verilated::overWidthError("gpio_17_in");}
    if (VL_UNLIKELY((gpio_18_in & 0xfeU))) {
        Verilated::overWidthError("gpio_18_in");}
    if (VL_UNLIKELY((gpio_19_in & 0xfeU))) {
        Verilated::overWidthError("gpio_19_in");}
    if (VL_UNLIKELY((gpio_20_in & 0xfeU))) {
        Verilated::overWidthError("gpio_20_in");}
    if (VL_UNLIKELY((gpio_21_in & 0xfeU))) {
        Verilated::overWidthError("gpio_21_in");}
    if (VL_UNLIKELY((gpio_22_in & 0xfeU))) {
        Verilated::overWidthError("gpio_22_in");}
    if (VL_UNLIKELY((gpio_23_in & 0xfeU))) {
        Verilated::overWidthError("gpio_23_in");}
    if (VL_UNLIKELY((gpio_24_in & 0xfeU))) {
        Verilated::overWidthError("gpio_24_in");}
    if (VL_UNLIKELY((gpio_25_in & 0xfeU))) {
        Verilated::overWidthError("gpio_25_in");}
    if (VL_UNLIKELY((gpio_26_in & 0xfeU))) {
        Verilated::overWidthError("gpio_26_in");}
    if (VL_UNLIKELY((gpio_27_in & 0xfeU))) {
        Verilated::overWidthError("gpio_27_in");}
    if (VL_UNLIKELY((gpio_28_in & 0xfeU))) {
        Verilated::overWidthError("gpio_28_in");}
    if (VL_UNLIKELY((gpio_29_in & 0xfeU))) {
        Verilated::overWidthError("gpio_29_in");}
    if (VL_UNLIKELY((gpio_30_in & 0xfeU))) {
        Verilated::overWidthError("gpio_30_in");}
    if (VL_UNLIKELY((gpio_31_in & 0xfeU))) {
        Verilated::overWidthError("gpio_31_in");}
    if (VL_UNLIKELY((wire_tms_tms_in & 0xfeU))) {
        Verilated::overWidthError("wire_tms_tms_in");}
    if (VL_UNLIKELY((wire_tdi_tdi_in & 0xfeU))) {
        Verilated::overWidthError("wire_tdi_tdi_in");}
    if (VL_UNLIKELY((wire_capture_capture_in & 0xfeU))) {
        Verilated::overWidthError("wire_capture_capture_in");}
    if (VL_UNLIKELY((wire_run_test_run_test_in & 0xfeU))) {
        Verilated::overWidthError("wire_run_test_run_test_in");}
    if (VL_UNLIKELY((wire_sel_sel_in & 0xfeU))) {
        Verilated::overWidthError("wire_sel_sel_in");}
    if (VL_UNLIKELY((wire_shift_shift_in & 0xfeU))) {
        Verilated::overWidthError("wire_shift_shift_in");}
    if (VL_UNLIKELY((wire_update_update_in & 0xfeU))) {
        Verilated::overWidthError("wire_update_update_in");}
    if (VL_UNLIKELY((ext_interrupts_i & 0xf8U))) {
        Verilated::overWidthError("ext_interrupts_i");}
}
#endif  // VL_DEBUG
